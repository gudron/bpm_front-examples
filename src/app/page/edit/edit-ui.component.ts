import { Component, Input, OnInit } from '@angular/core';
import { ElmaActiveModal } from '../../../shared/common';
import { PageType } from '../../app.constants';
import { PageJson } from '../../models/page.json';
import { PageModel } from '../../models/page.model';
import { PageStore } from '../../stores/page.store';

@Component({
    selector: 'app-page-edit-ui',
    templateUrl: 'edit-ui.component.html',
})

export class PageEditUiComponent implements OnInit {

    @Input() page: PageModel;
    pageJson: PageJson;
    PageType = PageType;

    constructor (
        private _pageStore: PageStore,
        private _activeModal: ElmaActiveModal,
    ) {
    }

    ngOnInit () {
        this.pageJson = this.page.toJSON();
    }


    save () {
        this._pageStore.update(this.pageJson);
        this.close();
    }

    close () {
        if (this._activeModal) {
            this._activeModal.close();
        }
    }

}
