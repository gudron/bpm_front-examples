import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ElmaActiveModal } from '../../../shared/common';
import { ElmaPopupComponent } from '../../../shared/common/elma-popup';
import { Namespaces } from '../../app.constants';
import { PageModel } from '../../models/page.model';
import { PageStore } from '../../stores/page.store';
import { PageModule } from '../page.module';

@Component({
    selector: 'app-page-edit',
    templateUrl: 'edit.component.html',
})

export class PageEditComponent implements OnInit {

    @Input() page: PageModel;

    @Input() namespace: string;
    @Input() code: string;

    canRender = false;

    constructor (
        private _pageStore: PageStore,
    ) {
    }

    async ngOnInit () {
        if (!this.page) {
            if (!this.namespace) {
                this.namespace = Namespaces.Global;
            }
            const pages = this._pageStore.filterList({ namespace: this.namespace, code: this.code });
            if (pages.length === 1) {
                this.page = pages[0];
            }
        }
        if (!this.page) {
            throw new Error('Page not found');
        }
        this.canRender = true;
    }
}
