import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { PageJson } from '../../../models/page.json';

@Component({
    selector: 'app-page-edit-separator',
    templateUrl: 'separator.component.html',
})

export class PageEditSeparatorComponent implements OnInit, AfterViewInit {

    @Input() pageJson: PageJson;

    @ViewChild('name') nameInput: ElementRef;

    constructor () {
    }

    ngOnInit () {
    }

    ngAfterViewInit () {
        this.nameInput.nativeElement.focus();
    }
}
