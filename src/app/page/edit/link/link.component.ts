import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { PageDataJson, PageJson } from '../../../models/page.json';

@Component({
    selector: 'app-page-edit-link',
    templateUrl: 'link.component.html',
})

export class PageEditLinkComponent implements OnInit, AfterViewInit {

    @Input() pageJson: PageJson;

    @ViewChild('name') nameInput: ElementRef;

    data: PageDataJson;

    constructor () {
    }

    ngOnInit () {
        this.data = <PageDataJson>this.pageJson.data;
    }

    ngAfterViewInit () {
        this.nameInput.nativeElement.focus();
    }
}
