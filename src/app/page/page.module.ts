import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ElmaCommonModule } from '../../shared/common';
import { AppCommonModule } from '../common/common.module';
import { PageEditUiComponent } from './edit/edit-ui.component';
import { PageEditComponent } from './edit/edit.component';
import { PageEditLinkComponent } from './edit/link/link.component';
import { PageEditSeparatorComponent } from './edit/separator/separator.component';

@NgModule({
    providers: [],
    imports: [
        CommonModule,
        FormsModule,
        ElmaCommonModule,
        AppCommonModule,
    ],
    declarations: [
        PageEditComponent,
        PageEditUiComponent,
        PageEditSeparatorComponent,
        PageEditLinkComponent,
    ],
    entryComponents: [
        PageEditComponent,
    ],
    exports: [
        PageEditComponent,
    ],
})
export class PageModule {
}
