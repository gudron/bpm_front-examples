import { EventEmitter, Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { action, observable } from 'mobx-angular';
import { ElmaTranslateService } from '../../shared/common';
import { ElmaNotificationService } from '../../shared/common/toast/notification.service';
import { MAX_SEARCH_SIZE, Namespaces, PermissionError, ROOT_CATEGORY, SystemCollections } from '../app.constants';
import { DirectoryJson, FileJson, LinkJson, PostlinkJson } from '../models/disk.json';
import { DirectoryModel, FileModel, LinkModel, PostlinkModel } from '../models/disk.model';
import { PermissionsModel } from '../models/permission.model';
import { MoveEvent, SelectedItem } from '../modules/disk/disk-listing/disk-listing.interface';
import { Action, ItemOuterEvent, TreeItem } from '../modules/disk/disk-sidebar/tree-item';
import { baseDirectoryId } from '../modules/disk/disk.enum';
import { CacheItem, CollectionCache, CollectionCacheService } from '../services/collection-cache.service';
import { ServerError } from '../services/error.service';
import { ProfileService } from '../services/profile.service';
import { PermissionsJson } from './../models/permission.json';

import { APIService } from './../services/api.service';
import { UserStore } from './user.store';

const apiUrlPrefix = '/api/disk';
const dirsQueryUrl = '/api/query/system/disk/categories/directory';
const linksQueryUrl = '/api/query/system/disk_symlinks/items';
const postlinkUrl = apiUrlPrefix + '/files/postlink';

@Injectable()
export class DiskStore {

    @observable private _directoryMap = new Map<string, DirectoryModel>(); // мапа директорий, для построения дерева директорий
    public currentDirID: string;
    @observable
    public currentFiles: FileModel[] = [];
    @observable
    public currentDirs: DirectoryModel[] = [];
    @observable
    public currentLinks: LinkModel[] = [];
    @observable
    public selectedItemList: SelectedItem[] = [];
    public sidebarEmitter: EventEmitter<ItemOuterEvent>;

    private cache: CollectionCache;

    constructor (
        private http: APIService,
        private notificationService: ElmaNotificationService,
        private profileService: ProfileService,
        private ts: ElmaTranslateService,
        private collectionCacheService: CollectionCacheService,
        private userStore: UserStore,
    ) {

        this.cache = new CollectionCache(Namespaces.System, SystemCollections.Disk, collectionCacheService);

        this.profileService.onLogout.subscribe(() => {
            this.cleanup();
        });
        this._directoryMap.set(ROOT_CATEGORY, new DirectoryModel(null, {
            __id: ROOT_CATEGORY,
            name: 'root',
            parent: '',
        }));
        this.sidebarEmitter = new EventEmitter<ItemOuterEvent>();
    }

    @action
    async createLink (json: LinkJson | LinkJson): Promise<LinkModel> {
        const res = await this.http.put(apiUrlPrefix + '/links', json);
        if (res.status !== 201) {
            throw new ServerError(res);
        }
        json = <LinkJson>res.json();
        if (json.favorite === false) {
            this.notificationService.success(this.ts.get('app.stores.disk@link-created'));
        } else {
            this.notificationService.success(this.ts.get('app.disk.listing.directory.context-menu@add-to-favorite-notification'));
        }
        this.cache.set(<CacheItem>json);
        return new LinkModel(this, json);
    }

    @action
    async generateWOPIToken (fileID: string): Promise<{ value: string, ttl: string }> {
        const res = await this.http.get(apiUrlPrefix + `/files/${fileID}/token`);
        if (res.status === 403) {
            throw new PermissionError();
        }
        if (res.status !== 200) {
            throw new ServerError(res);
        }
        const json = res.json();
        return { value: json['value'], ttl: json['ttl'] };
    }

    @action
    async deleteLink (linkId: string): Promise<void> {

        const queryUrl = apiUrlPrefix + `/links/` + linkId;
        const res = await this.http.delete(queryUrl);
        if (res.status !== 204) {
            throw new ServerError(res);
        }
        this.cache.delete(linkId);
    }

    @action
    public async loadLinksInDir (dir: string, favorite = false): Promise<LinkModel[]> {

        if (!dir) {
            dir = ROOT_CATEGORY;
        }

        const queryUrl = `${linksQueryUrl}?size=${MAX_SEARCH_SIZE}&active=true&q=directory:${dir} AND favorite:${favorite}`;
        const res = await this.http.get(queryUrl);
        if (res.status !== 200) {
            throw new ServerError(res);
        }
        const linkArray: LinkModel[] = [];
        const jsons = <LinkJson[]>this.cache.update<CacheItem>(<CacheItem[]>res.json()['result']);
        for (const json of jsons) {
            const link = new LinkModel(this, json);

            linkArray.push(link);
        }

        return linkArray;
    }

    @action
    public cleanCache () {
        this._directoryMap = new Map<string, DirectoryModel>();
        this._directoryMap.set(ROOT_CATEGORY, new DirectoryModel(null, {
            __id: ROOT_CATEGORY,
            name: 'root',
            parent: '',
        }));
    }


    @action
    public async loadFilesInDir (dir: string): Promise<FileModel[]> {
        if (!dir) {
            dir = ROOT_CATEGORY;
        }
        const res = await this.http.get(`${apiUrlPrefix}/directories/${dir}/files?size=${MAX_SEARCH_SIZE}`);
        if (res.status !== 200) {
            if (res.status === 403) {
                throw new PermissionError();
            }
            throw new ServerError(res);
        }

        const files: FileModel[] = [];
        const jsons = <FileJson[]>this.cache.update<CacheItem>(<CacheItem[]>res.json()['result']);
        for (const json of jsons) {

            const file = new FileModel(this, json);

            files.push(file);
        }

        return files;
    }


    @action
    public async loadDirectoriesInDir (parentDirId: string, skipCache: boolean = false): Promise<DirectoryModel[]> {
        if (!parentDirId) {
            parentDirId = ROOT_CATEGORY;
        }

        let parentDir = this._directoryMap.get(parentDirId);

        if (parentDir != null) {
            if (parentDir.childrenIsSet && !skipCache) {
                return parentDir.children;
            }
        } else {
            parentDir = await this.loadDirectory(parentDirId);
        }

        const res = await this.http.get(`${dirsQueryUrl}?size=${MAX_SEARCH_SIZE}&q=parent:${parentDirId}`);

        if (res.status !== 200) {

            if (res.status === 403) {
                throw new PermissionError();
            }
            throw new ServerError(res);
        }

        const dirs: DirectoryModel[] = [];
        const jsons = <DirectoryJson[]>this.cache.update<CacheItem>(<CacheItem[]>res.json()['result']);
        for (const json of jsons) {
            const dir = new DirectoryModel(this, json);

            if (!this._directoryMap.has(dir.id)) {
                this._directoryMap.set(dir.id, dir);
            }

            dirs.push(dir);
        }

        parentDir.setChildren(dirs);
        parentDir.markChildrenSet();

        return dirs;
    }


    @action
    public getRootDir (): DirectoryModel | undefined {
        return this._directoryMap.get(ROOT_CATEGORY);
    }

    @action
    async createFile (json: FileJson | FileJson, generateName?: boolean): Promise<FileModel> {
        const generateNameParam = generateName ? 'generateName=true' : 'generateName=false';
        const res = await this.http.put(apiUrlPrefix + `/files?${generateNameParam}`, json);
        if (res.status === 403) {
            throw new PermissionError();
        }
        if (res.status !== 201) {
            throw new ServerError(res);
        }
        json = <FileJson>res.json();
        this.cache.set(<CacheItem>json);
        return new FileModel(this, json);
    }

    @action
    async getFile (id: string): Promise<FileModel> {

        let json: FileJson|undefined = undefined;
        const res = await this.http.get(apiUrlPrefix + `/files/${id}`);
        if (res.status !== 200) {
            if (res.status === 403) {
                throw new PermissionError();
            } else if (res.status === 404) {
                json = this.cache.get(id);
            }
            if (!json) {
                throw new ServerError(res);
            }
        }
        if (!json) {
            json = <FileJson>this.cache.updateItem<CacheItem>(<CacheItem>res.json()['result']);
        }

        return new FileModel(this, <FileJson>json);
    }

    @action
    async getFilesByIds (fileIds: string[]): Promise<FileModel[]> {
        return await this.loadFiles(fileIds);
    }

    async loadFiles (fileIds: string[]): Promise<FileModel[]> {
        if (!fileIds || fileIds && fileIds.length === 0) {
            return [];
        }
        const res = await this.http.post(apiUrlPrefix + `/files/getfiles`, { fileIds: fileIds });
        if (res.status !== 200) {
            if (res.status === 403) {
                throw new PermissionError();
            }
            throw new ServerError(res);
        }
        const filesJson = <FileJson[]>this.cache.update<CacheItem>(<CacheItem[]>res.json()['result']);
        const models = filesJson.map(fileJson => new FileModel(this, fileJson));

        return models;
    }

    @action
    async renameFile (fileId: string, name: string): Promise<Response> {

        const fileUpdate = {
            name: name,
        };
        const response = await this.http.put(apiUrlPrefix + `/files/${fileId}/update`, fileUpdate);
        if (response.status === 200) {
            this.cache.set(<CacheItem>response.json());
        } else {
            throw new ServerError(response);
        }

        return response;
    }

    @action
    async moveFile (fileId: string, json: { dirId: string }): Promise<Response> {
        const response = await this.http.put(apiUrlPrefix + `/files/${fileId}/move`, json);
        if (response.status === 200) {
            this.cache.set(<CacheItem>response.json());
        } else {
            throw new ServerError(response);
        }

        return response;
    }

    @action
    async fileGetlink (file: FileModel, contentType: string = ''): Promise<string> {

        contentType = encodeURIComponent(contentType);
        const res = await this.http.get(apiUrlPrefix + `/files/${file.hash}/getlink?filename=${file.name}&contentType=${contentType}`);
        if (res.status !== 200) {
            throw new ServerError(res);
        }

        return res.json()['link'];
    }

    @action
    async downloadFile (file: FileModel) {
        const res = await this.http.get(apiUrlPrefix + `/files/${file.hash}/download?filename=${file.name}`);
        window.open(res.url);
    }

    @action
    async createDirectory (json: DirectoryJson | DirectoryJson): Promise<DirectoryModel> {
        const res = await this.http.put(apiUrlPrefix + '/directories', json);
        if (res.status === 403) {
            throw new PermissionError();
        }
        if (res.status !== 201) {
            throw new ServerError(res);
        }
        json = <DirectoryJson>res.json();
        this.cache.set(<CacheItem>json);

        return new DirectoryModel(this, json);
    }

    @action
    async renameDirectory (dirId: string, name: string): Promise<Response> {

        const dirUpdate = {
            name: name,
        };
        const response = await this.http.put(apiUrlPrefix + `/directories/${dirId}/update`, dirUpdate);
        if (response.status === 200) {
            this.cache.set(<CacheItem>response.json());
        } else {
            throw new ServerError(response);
        }

        return response;
    }

    @action
    async getFreeName (dirId: string, json: { name: string }, type: string): Promise<Response> {
        const response = await this.http.post(apiUrlPrefix + `/directories/${dirId}/${type}/freename`, json);
        if (response.status !== 200) {
            throw new ServerError(response);
        }
        return response
    }

    @action
    async moveDirectory (dirId: string, json: any): Promise<Response> {

        const response = await this.http.put(apiUrlPrefix + `/directories/${dirId}/move`, json);
        if (response.status === 200) {
            this.cache.set(<CacheItem>response.json());
        } else {
            throw new ServerError(response);
        }

        return response;
    }

    @action
    async loadDirectory (dirID: string): Promise<DirectoryModel> {
        let dir = this._directoryMap.get(dirID);
        if (dir != null) {
            return dir;
        }

        let json: DirectoryJson|undefined = undefined;

        const res = await this.http.get(`${dirsQueryUrl}/${dirID}`);
        if (res.status !== 200) {
            if (res.status === 403) {
                throw new PermissionError();
            } else if (res.status === 404) {
                json = this.cache.get(dirID);
            }
            throw new ServerError(res);
        }

        if (!json) {
            json = <DirectoryJson>this.cache.updateItem<CacheItem>(<CacheItem>res.json()['result']);
        }
        dir = new DirectoryModel(this, <DirectoryJson>json);
        if (!this._directoryMap.has(dirID)) {
            this._directoryMap.set(dirID, dir);
        }

        return dir;
    }


    // Получить список вышестоящих директорий
    @action
    public async getDirectoryParentsList (dirID: string): Promise<DirectoryModel[]> {
        let dir = this._directoryMap.get(dirID);
        if (dir == null) {
            dir = await this.loadDirectory(dirID);
            this._directoryMap.set(dirID, dir!);
        }

        const parents: DirectoryModel[] = [dir];
        const missedParents: string[] = [];

        // Ищем в кэше директорию
        for (const parentStr of dir.parentsList) {
            if (this._directoryMap.has(parentStr)) {
                continue;
            }
            // Если директории в кэше на нашлось, отмечаем это, запросим позже
            missedParents.push(parentStr);
        }

        // Запрашиваем директории, которых у нас нет
        if (missedParents.length > 0) {
            const res = await this.http.get(dirsQueryUrl + `?size=${MAX_SEARCH_SIZE}&q=__id:(` + missedParents.join(' OR ') + ')');
            if (res.status !== 200) {
                throw new ServerError(res);
            }

            for (const json of <DirectoryJson[]>res.json()['result']) {
                const d = new DirectoryModel(this, json);
                // здесь порядок радномен, поэтому сначала добавим в кэш, а потом достанем оттуда
                if (!this._directoryMap.has(d.id)) {
                    this._directoryMap.set(d.id, d);
                }
            }
        }

        for (const d of dir.parentsList) {
            if (d !== ROOT_CATEGORY) {
                const dd = this._directoryMap.get(d);
                if (dd == null) {
                    continue;
                }
                parents.push(dd);
            }
        }

        return parents.reverse();
    }

    @action
    async getPostlink (size: number, session: boolean = false, key?: string): Promise<PostlinkModel> {
        let url = postlinkUrl + `?size=${size}&session=${session}`;
        if (key) {
            url = url + `&key=${key}`;
        }
        const res = await this.http.get(url);
        if (res.status !== 200) {
            throw new ServerError(res);
        }
        const json = <PostlinkJson>res.json();

        return new PostlinkModel(this, json);
    }

    @action
    public async getFilePermissions (fileID: string): Promise<PermissionsModel> {
        const requrl = `/api/query/system/disk/items/${fileID}/permissions?size=${MAX_SEARCH_SIZE}`;

        const res = await this.http.get(requrl);
        if (res.status !== 200) {
            throw new ServerError(res);
        }

        const permissions = new PermissionsModel(<PermissionsJson>res.json());

        return permissions;
    }

    @action
    public async saveFilePermissions (fileID: string, ps: PermissionsJson | PermissionsJson) {

        const res = await this.http.put(apiUrlPrefix + `/files/${fileID}/permissions`, ps);
        if (res.status !== 200) {
            if (res.status === 403) {
                throw new PermissionError();
            }
            throw new ServerError(res);
        }
        const json = <PermissionsJson>res.json();

        return new PermissionsModel(json);
    }


    @action
    public async getDirPermissions (dirID: string): Promise<PermissionsModel> {
        const requrl = `/api/query/system/disk/categories/directory/${dirID}/permissions?size=${MAX_SEARCH_SIZE}`;

        const res = await this.http.get(requrl);
        if (res.status !== 200) {
            throw new ServerError(res);
        }

        const permissions = new PermissionsModel(<PermissionsJson>res.json());

        return permissions;

    }

    @action
    public async saveDirPermissions (dirID: string, ps: PermissionsJson | PermissionsJson) {
        ps.values = ps.values.filter(item => item.inherited === false);
        const res = await this.http.put(apiUrlPrefix + `/directories/${dirID}/permissions`, ps);
        if (res.status !== 200) {
            if (res.status === 403) {
                throw new PermissionError();
            }
            throw new ServerError(res);
        }
        const json = <PermissionsJson>res.json();

        return new PermissionsModel(json);
    }


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  /
     /    Нижележащая хрень нужна только для сайдбара                                                        /
     /    Ибо там приходится делать кучу рекурсивных запросов, а так можно все решить в два запроса          /
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


    // Возвращает просто массив строк (uuid) для всех передеанных директорий
    // Нужен для дискового сайдбара
    @action
    public async sidebarDirsParentList (dirIDs: string[]): Promise<string[]> {
        const resultParents: string[] = [];

        const missedParents: string[] = [];
        for (const dirID of dirIDs) {
            // Ищем в кэше
            const dir = this._directoryMap.get(dirID);
            if (dir != null) {
                resultParents.push(...dir.parentsList);
                continue;
            }
            missedParents.push(dirID);
            // Не нашлось, помечаем
        }

        if (missedParents.length > 0) {
            const res = await this.http.get(dirsQueryUrl + `?size=${MAX_SEARCH_SIZE}&q=__id:(` + missedParents.join(' OR ') + ')');
            if (res.status !== 200) {
                throw new ServerError(res);
            }

            for (const json of <DirectoryJson[]>res.json()['result']) {
                const dir = new DirectoryModel(this, json);

                resultParents.push(...dir.parentsList);

                if (!this._directoryMap.has(dir.id)) {
                    this._directoryMap.set(dir.id, dir);
                }
            }
        }

        return this.uniqArr(resultParents);
    }

    // Возвращает все открытые директории, а также их потомков
    @action
    public async sidebarAllOpenedDirs (dirIDs: string[]): Promise<DirectoryModel[]> {

        const resultDirs: DirectoryModel[] = [];

        const missedDirs: string[] = [];
        // Сначала в кэше поищем
        for (const dirID of dirIDs) {
            if (dirID === ROOT_CATEGORY) {
                continue;
            }
            const dir = this._directoryMap.get(dirID);
            if (dir != null) {
                resultDirs.push(dir);
            } else {
                missedDirs.push(dirID);
            }
        }

        // А теперь получим всех потомков, для открытых директорий
        if (dirIDs.length > 0) {
            const childDirs = await this.loadAllChildrenInDirs(dirIDs);

            // Я кароч неуверен, правильно ли мотать этот массив два раза подряд, но я уже заебался
            // пущай так пока работает
            for (const cd of childDirs) {
                if (!resultDirs.find(elem => elem.id === cd.id)) {
                    resultDirs.push(cd);
                }
            }

            for (const cd of childDirs) {
                const parentDir = resultDirs.find(elem => elem.id === cd.parent);
                if (parentDir != null) {
                    parentDir.addChild(cd);
                }
            }

            const depthChildIDs: string[] = childDirs.map(el => el.id);

            if (depthChildIDs.length > 0) {
                const depthChild = await this.loadAllChildrenInDirs(depthChildIDs);

                for (const cd of depthChild) {
                    const parentDir = resultDirs.find(elem => elem.id === cd.parent);
                    if (parentDir != null) {
                        parentDir.addChild(cd);
                    }
                }
            }
        }

        return resultDirs;
    }

    @action
    public async loadAllChildrenInDirs (dirIDs: string[]): Promise<DirectoryModel[]> {
        const resultDirs: DirectoryModel[] = [];

        // FIXME: если длинна массива оч большая, может быть жопа, надо для безопасности попилить его на куски по 40 uuid примерно
        const res = await this.http.get(dirsQueryUrl + `?size=${MAX_SEARCH_SIZE}&q=parent:(` + dirIDs.join(' OR ') + ')');
        if (res.status !== 200) {
            throw new ServerError(res);
        }
        for (const json of <DirectoryJson[]>res.json()['result']) {
            const dir = new DirectoryModel(this, json);

            resultDirs.push(dir);

            if (!this._directoryMap.has(dir.id)) {
                this._directoryMap.set(dir.id, dir);
            }

        }

        return resultDirs;
    }


    private uniqArr (arr: string[]): string[] {
        const tmp = [];
        for (let i = 0; i < arr.length; i++) {
            if (tmp.indexOf(arr[i]) === -1) {
                tmp.push(arr[i]);
            }
        }
        return tmp;
    }

    public async tcBaseDirectoryName (directoryId: string): Promise<string> {

        switch (directoryId) {
        case baseDirectoryId.home:
            return this.ts.get('app.stores.disk@home');
        case baseDirectoryId.shared:
            return this.ts.get('app.stores.disk@shared');
        default:
            return '';
        }
    }

    public handleMovedItems (moveEvent: MoveEvent): void {

        const itemList: SelectedItem[] = moveEvent.selectedItem;

        for (const item of itemList) {

            if (item instanceof FileModel) {
                const index = this.currentFiles.findIndex(file => file.id === item.id);
                if (index > -1) {
                    this.currentFiles.splice(index, 1);
                }
            } else if (item instanceof DirectoryModel) {
                const index = this.currentDirs.findIndex(dir => dir.id === item.id);
                if (index > -1) {
                    this.currentDirs.splice(index, 1);
                }

                this.moveDirectoryEventSidebar(item.id, moveEvent.targetId);
            }

            const selIndex = this.selectedItemList.findIndex(file => file.id === item.id);
            if (selIndex > -1) {
                this.selectedItemList.splice(selIndex, 1);
            }
        }
    }

    public async outputResultMoveDir (moveEvent: MoveEvent): Promise<void> {

        const itemListLength = moveEvent.selectedItem.length + '';
        const dispatchDirectory = await this.loadDirectory(moveEvent.targetId);

        const user = this.profileService.userId ? await this.userStore.getByID(this.profileService.userId) : null;
        const infoMessage = this.ts.get('app.stores.disk@move-item-message',
            {
                directoryName:
                    (user && user.id === dispatchDirectory.id)
                        ? this.ts.get('app.disk.listing.directory@my-files') : dispatchDirectory.name,
                itemSum: itemListLength
            });

        this.notificationService.success(infoMessage);
    }

    public sidebarOpenDirectory (dirID: string): void {
        if (!dirID) {
            dirID = ''; // так прощем, чем ROOT_CATEGORY использовать
        }
        this.sidebarEmitter.emit({
            itemid: dirID,
            action: Action.Activate,
        });
        this.currentDirID = dirID;
    }

    public addDirectory (dir: DirectoryModel): void {
        this.sidebarEmitter.emit({
            item: new TreeItem(dir),
            action: Action.Add,
        });
    }

    public renameDirectoryEventSidebar (name: string, dirId: string): void {

        this.sidebarEmitter.emit({
            name: name,
            directoryId: dirId,
            action: Action.Rename,
        });
    }

    public moveDirectoryEventSidebar (dirId: string, parentId: string): void {
        this.sidebarEmitter.emit({
            action: Action.Move,
            itemid: dirId,
            parentId: parentId,
        });
    }

    @action
    private cleanup () {
        this._directoryMap.clear();
        this.currentDirID = '';
        this.currentFiles = [];
        this.currentDirs = [];
        this.currentLinks = [];
        this.selectedItemList = [];
    }

    @action
    public async sendFileBody (postLink: PostlinkModel, file: File): Promise<any> {

        return new Promise( (resolve, reject) => {

            const formData = new FormData();
            formData.append('file', file);
            for (const key in postLink.formdata) {
                if (postLink.formdata[key]) {
                    formData.append(key, postLink.formdata[key]);
                }
            }

            const request = new XMLHttpRequest();
            request.open('POST', postLink.link);
            request.send(formData);
            request.onload = () => { resolve() };
            request.onloadend = () => { reject(request.response) };
        });
    }
}
