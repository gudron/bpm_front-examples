import { AfterViewInit, Component, ComponentRef } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router } from '@angular/router';
import { computed } from 'mobx-angular';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { EmptyComponent } from './modules/managers/empty/empty.component';
import { ProfileService } from './services/profile.service';
import { SidebarService } from './services/sidebar.service';
import { MentionClickService } from './shared/quill/mention/mention-click.service';
import { WidgetRegistrarService } from './widgets/registrar.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})
export class AppComponent implements AfterViewInit {

    isShowPopup = false;
    isSidebarOpen = false;
    fullscreen$ = new BehaviorSubject<boolean>(true);

    @computed
    get isUserLoggedIn(): boolean {
        // AppComponent инициализируется для дефолтного роута. В этот момент пользователь может быть не авторизован.
        // В этом случае нет необходимости ни показывать левое меню, ни загружать страницы - и, соответственно,
        // нет необходимости инициализировать pageStore.
        return this.profileService.loggedIn;
    }

    constructor(
        private profileService: ProfileService,
        private activatedRoute: ActivatedRoute,
        private sidebarService: SidebarService,
        private mentionClickService: MentionClickService, // Надо где-то подключить этот сервис, иначе он не инстанцируется
        private router: Router,
        widgetRegistrar: WidgetRegistrarService,
    ) {
        this.sidebarService.openToggle.subscribe((isOpen: boolean) => {
            this.isSidebarOpen = isOpen;
        });

        this.router.events
            .filter(event => event instanceof NavigationEnd)
            .subscribe((event: NavigationEnd) => {
                let shot: ActivatedRouteSnapshot | null = this.activatedRoute.snapshot;
                while (shot) {
                    if (shot.data.hasOwnProperty('fullScreen') && shot.data.fullScreen === true) {
                        this.fullscreen$.next(true);
                        return;
                    }
                    shot = shot.firstChild;
                }
                this.fullscreen$.next(false);
            });

        // Регистрируем виджеты уровня приложения
        widgetRegistrar.init();
    }

    ngAfterViewInit() {
        const fs = new (<any>window).FastClick((<any>document).body);
    }

    popupToggle (component: ComponentRef<any>) {
        this.isShowPopup = !(component instanceof EmptyComponent);
    }
}
