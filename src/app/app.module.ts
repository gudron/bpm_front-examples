import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Http, HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { TranslatePoHttpLoader } from '@biesbjerg/ngx-translate-po-http-loader';
import { TranslateLoader, TranslateModule as NgxTranslateModule } from '@ngx-translate/core';
import { SimpleNotificationsModule } from 'angular2-notifications';

import { MobxAngularModule } from 'mobx-angular';
import * as moment from 'moment';
import { ToastrModule } from 'ngx-toastr';
import { ElmaUIService } from '../shared/common';
import { ElmaCommonModule } from '../shared/common/common.module';
import { CRUMBS_CONFIG, CrumbsService } from '../shared/common/crumbs';
import { ElmaTranslateResolver } from '../shared/common/translate/translate.resolver.service';
import { WidgetModule } from '../widget';

import { AppComponent } from './app.component';
import { ApplicationBuilder } from './application/models/application.builder';
import { ApplicationStore } from './application/services/application.store';
import { ItemChangeHandler } from './application/services/item-change.handler';
import { BPMDiagramModule } from './bpm/modules/diagram/diagram.module';
import { BPMRunModule } from './bpm/modules/run/run.module';
import { BPTemplateStore } from './bpm/services/bptemplate.store';
import { ContextBuilder } from './bpm/services/context.builder';
import { BPIo } from './bpm/services/io';
import { AppCommonModule } from './common/common.module';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { InviteModule } from './invite/invite.module';
import { ChannelsStore } from './messages/channel/services/channel.store';
import { ChatStore } from './messages/chat/services/chat.store';
import { ElmaPushNotificationsModule } from './messages/push-notifications/push-notifications.module';
import { UserModule } from './modules/admin/user/user.module';
import { WorktimerStore } from './modules/admin/worktimer/store/worktimer.store';

import { DiskModule } from './modules/disk/disk.module';
import { FillerFactory } from './modules/dynamic-form/value-filler';
import { AuthModule } from './modules/entry-points/auth/auth.module';
import { LoadingModule } from './modules/entry-points/loading/loading.module';
import { EmptyComponent } from './modules/managers/empty/empty.component';
import { EmptyModule } from './modules/managers/empty/empty.module';
import { NavManagerModule } from './modules/managers/nav-manager/nav-manager.module';
import { PageManagerComponent } from './modules/managers/page-manager/page-manager.component';
import { PageManagerModule } from './modules/managers/page-manager/page-manager.module';
import { ProxyModule } from './modules/managers/proxy/proxy.module';
import { MonitorHistoryStore } from './modules/monitor/history/store/monitorHistory.store';
import { MonitorModule } from './modules/monitor/monitor.module';
import { NavigationMainModule } from './modules/navigation/navigation-main/navigation-main.module';
// todo выпилить НАЧАЛО
import { NavigationNamespaceModule } from './modules/navigation/navigation-namespace/navigation-namespace.module';
import { PageModule } from './page/page.module';
import { APIService } from './services/api.service';
import { CollectionCacheService } from './services/collection-cache.service';
import { ElasticQueryStringService } from './services/elastic-query-string.service';
import { ErrorInterceptor } from './services/error.interceptor';
import { GlobalErrorHandler } from './services/error.service';

import { LanguageService } from './services/language.service';
import { PageResolver } from './services/page.resolver';
import { ProfileService } from './services/profile.service';
import { SearchService } from './services/search.service';
import { SidebarService } from './services/sidebar.service';
import { SocketService } from './services/socket.service';
import { TitleService } from './services/title.service';
import { TokenInterceptor } from './services/token.interceptor';
import { IsAdminGuard, IsAuthUserGuard, IsGuestUserGuard } from './services/user-guards';
import { VahterService } from './services/vahter.service';
import { WidgetRegistrarService } from './widgets/registrar.service';
import { MSOfficeHelper } from './shared/file/type-file/msoffice/msoffice.helper';
import { HeaderService } from './shared/headers/header.service';
import { HtmlEditorModule } from './shared/quill/editor/editor.module';
import { AppSharedModule } from './shared/shared.module';
import { DiskStore } from './stores/disk.store';
import { GroupStore } from './stores/group.store';
import { OrgstructStore } from './stores/orgstruct.store';
import { OrgunitStore } from './stores/orgunit.store';
import { PageStore } from './stores/page.store';
import { RegistryStore } from './stores/registry.store';

import { UserStore } from './stores/user.store';
import { WidgetStore } from './stores/widget.store';

export function HttpLoaderFactory (http: Http) {
    return new TranslatePoHttpLoader(http, 'assets/locale', '.po');
}

/**
 * Локализация и глобализация moment компонента
 */
moment.locale(LanguageService.getAlias());

const crumbsResources = require('./crumbs.resources.json');

@NgModule({
    declarations: [
        AppComponent,
        UserProfileComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,

        MobxAngularModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        ElmaCommonModule,
        AppCommonModule,
        AppSharedModule,
        HtmlEditorModule.forRoot(),
        WidgetModule.forRoot(),

        NavManagerModule,
        PageManagerModule,

        EmptyModule,
        ProxyModule,

        NavigationMainModule,
        NavigationNamespaceModule,

        LoadingModule,
        AuthModule,
        InviteModule,
        UserModule,
        DiskModule, // здесь идет роутинг к files
        BPMRunModule, // роутинг (p:run/)
        BPMDiagramModule, // роутинг (p:diagram/)

        MonitorModule,
        SimpleNotificationsModule,
        ElmaPushNotificationsModule,

        PageModule,

        RouterModule.forRoot([
            {
                path: '',
                component: PageManagerComponent,
                canActivate: [IsAuthUserGuard],
                resolve: {
                    translate: ElmaTranslateResolver,
                },
            },
            {
                path: 'admin',
                loadChildren: 'app/modules/admin/admin.module#AdminModule',
                canActivate: [IsAdminGuard],
                resolve: {
                    translate: ElmaTranslateResolver,
                },
            },
            {
                path: 'profile',
                component: UserProfileComponent,
            },
            {
                path: `:namespace`,
                component: PageManagerComponent,
                canActivate: [IsAuthUserGuard],
                resolve: {
                    translate: ElmaTranslateResolver,
                },
            },
            {
                path: `:namespace/:page`,
                component: PageManagerComponent,
                canActivate: [IsAuthUserGuard],
                resolve: {
                    translate: ElmaTranslateResolver,
                },
            },
            {
                path: `:namespace/:page/:subpage`,
                component: PageManagerComponent,
                canActivate: [IsAuthUserGuard],
                resolve: {
                    translate: ElmaTranslateResolver,
                },
            },
            {
                path: '',
                component: EmptyComponent,
                outlet: 'p',
                // canActivate: [IsAuthUserGuard],
            },
            {
                path: '',
                component: EmptyComponent,
                outlet: 'f',
            },
        ], {
            preloadingStrategy: PreloadAllModules,
            paramsInheritanceStrategy: 'always',
        }),
        HttpClientModule,
        NgxTranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [Http],
            },
        }),
    ],
    providers: [
        {
            provide: ErrorHandler,
            useClass: GlobalErrorHandler,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptor,
            multi: true,
        },
        {
            provide: CRUMBS_CONFIG,
            useValue: {
                component: 'elma365',
                widget: 'crumbs',
                version: 'SET 0.0.1 VERSION HERE',
                defaults: crumbsResources,
            },
        }, CrumbsService,

        TitleService,
        SidebarService,
        VahterService,

        APIService,

        ProfileService,
        SocketService,

        IsAuthUserGuard,
        IsGuestUserGuard,
        IsAdminGuard,

        CollectionCacheService,

        UserStore,
        MonitorHistoryStore,
        GroupStore,
        WidgetStore,
        OrgunitStore,
        OrgstructStore,
        PageStore,
        ApplicationStore,
        ApplicationBuilder,
        ChannelsStore,
        ChatStore,
        WorktimerStore,
        SearchService,
        ElasticQueryStringService,
        RegistryStore,

        BPTemplateStore,
        BPIo,
        FillerFactory,
        ItemChangeHandler,

        DiskStore,
        MSOfficeHelper,
        ElmaUIService,
        HeaderService,
        PageResolver,
        WidgetRegistrarService,

        ContextBuilder,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
