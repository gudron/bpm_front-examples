import {
    Component, OnInit, ChangeDetectionStrategy, ViewContainerRef,
    SystemJsNgModuleLoader, NgModuleFactoryLoader, Injector, Compiler, NgModule, ViewChild, NgModuleRef, Input,
    ModuleWithComponentFactories, ComponentFactory, OnDestroy,
} from '@angular/core';
import * as ngCore from '@angular/core';
import { CommonModule } from '@angular/common';
import { JitCompilerFactory } from '@angular/platform-browser-dynamic';
import { ExternalWidgetService } from './external-widget.service';
import { SidebarService } from '../../services/sidebar.service';
import { Subscription } from 'rxjs/Subscription';
declare var SystemJS: any;

export function CustomNgModule(annotation: any) {
    return function (target: Function) {
        const metaData = new NgModule(annotation);
        NgModule(metaData)(target)
    }
}

export function createJitCompiler () {
    return new JitCompilerFactory().createCompiler([{useJit: true}]);
}


@Component({
    selector: 'app-external-widget',
    template: `<pre *ngIf="error" style="color: red; display: block; width: 100%;" [innerHTML]="error"></pre>
                <ng-content select=".cog-button"></ng-content>
                <div class="widget-container" #widgetContainer></div>`,
    styles: [],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NgModuleFactoryLoader,
            useClass: SystemJsNgModuleLoader
        },
        ExternalWidgetService,
        { provide: Compiler, useFactory:  createJitCompiler},
    ]
})
export class ExternalWidgetComponent implements OnInit, OnDestroy {

    @ViewChild('widgetContainer', {read: ViewContainerRef}) widgetContainer: ViewContainerRef;

    @Input('isOnFlyCompilation') isOnFlyCompilation = false;
    @Input('Component') Component: Function;
    @Input('styles') styles: string;
    @Input('template') template: string;

    @Input('customWidgetId') customWidgetId: string;

    error?: string;

    private sidebarSubscription: Subscription;

    constructor (
        private _injector: Injector,
        private _compiler: Compiler,
        private _m: NgModuleRef<any>,
        private service: ExternalWidgetService,
        private sidebarService: SidebarService,
    ) {
        SystemJS.config({
            meta: {
                '*': {
                    authorization: `Bearer ${localStorage.getItem('token')}`
                }
            }
        })

        this.sidebarSubscription = this.sidebarService.componentChanged.subscribe(() => {
            setTimeout(() => {
                if (!this.isOnFlyCompilation && this.customWidgetId) {
                    this.loadCustomWidget.call(this);
                }
            }, 0);
        });
    }

    ngOnInit () {
        if (!this.isOnFlyCompilation && this.customWidgetId) {
            this.loadCustomWidget.call(this);
        }
    }

    ngOnDestroy () {
        this.sidebarSubscription.unsubscribe();
    }

    public reCompile() {
        this.compile.call(this, this.Component, this.styles, this.template);
    }

    async compile(componentClass: Function, styles: string, template: string) {

        this.error = undefined;

        let component: any;

        try {
            component = <any>Component({
                // TODO: брать код виджета
                selector: 'app-external-widget-selector',
                template,
                styles: [styles],
            })(componentClass);
        } catch (e) {
            console.error('COMPILE ERROR', e);
            return;
        }

        try {

            this.renderComponent.call(this, component);

        } catch (e) {
            console.error('Render component error', e);
            this.error = e;
            return;
        }
    }

    async renderComponent(component: any) {

        this.widgetContainer.clear();

        let componentFactory: ComponentFactory<any> | undefined;

        if (!this.service.cache.has(component)) {
            const metadata = {
                imports: [
                    CommonModule,
                ],
                declarations: [
                    component,
                ]
            };

            @NgModule(metadata)
            @CustomNgModule(metadata)
            class TemporaryModule {}

            let moduleWithComponents: ModuleWithComponentFactories<any>;
            moduleWithComponents = await this._compiler.compileModuleAndAllComponentsAsync(TemporaryModule);

            componentFactory = moduleWithComponents.componentFactories.find(f => f.componentType === component);

            if (!componentFactory) {
                throw new Error('Factory not found!');
            }

            this.service.cache.set(component, componentFactory);
        } else {
            componentFactory = this.service.cache.get(component);
        }

        if (componentFactory) {
            const componentRef = componentFactory.create(this._injector, [], null, this._m);
            this.widgetContainer.insert(componentRef.hostView);
        }
    }

    async loadCustomWidget() {
        try {
            SystemJS.set('@angular/core', SystemJS.newModule(ngCore));
            const src = `${window.location.origin}/api/widgets/${this.customWidgetId}/js?t=${new Date().getTime()}`;
            await SystemJS.import(src);
            const script: any = SystemJS.registry.get(src);
            const componentName = Object.keys(script.default).find((s: string) => { return s !== '__esModule' });
            if (!componentName) {
                throw new Error('Script is empty');
            }
            const CustomComponent = script[componentName];
            this.renderComponent.call(this, CustomComponent);
        } catch (e) {
            console.error('Error loading custom widget', e);
        }
    }
}
