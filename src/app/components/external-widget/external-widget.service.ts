import { ComponentFactory, ComponentRef, Injectable } from '@angular/core';

@Injectable()
export class ExternalWidgetService {

    cache = new Map<Function, ComponentFactory<any>>();

    constructor () {
    }

}
