import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ExternalWidgetComponent } from './external-widget.component';
import { ExternalWidgetService } from './external-widget.service';
import { ElmaCommonModule } from '../../../shared/common/common.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ElmaCommonModule,
    ],
    declarations: [
        ExternalWidgetComponent,
    ],
    exports: [
        ExternalWidgetComponent,
    ],
    providers: [
        ExternalWidgetService,
    ]
})
export class ExternalWidgetModule {
}
