import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-hider',
    templateUrl: './hider.component.html',
    styleUrls: ['./hider.component.scss']
})
export class HiderComponent implements OnInit {

    @Input() isOpen = false;
    @Input() openWord = 'show';
    @Input() hideWord = 'hide';

    ngOnInit() { }

}
