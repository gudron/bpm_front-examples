import { Component, OnInit } from '@angular/core';
import {ProfileService} from '../../services/profile.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

    alertPopupIsActive = false;

    constructor(private profileService: ProfileService,
                private router: Router) { }

    ngOnInit() {
    }

    openAlertPopup() {
        this.alertPopupIsActive = true;
    }

    closeAlertPopup() {
        this.alertPopupIsActive = false;
    }

    async logout() {
        await this.profileService.logout();
        this.router.navigateByUrl('/login');
    }
}
