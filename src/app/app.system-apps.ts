import { ElmaTranslateService } from '../shared/common';
import { GetAdminAppsJson } from './modules/admin/admin.apps';
import { ApplicationJson } from './application/schema/application.json';
import { GetMonitorAppsJson } from './modules/monitor/monitor.apps';
import { GetTaskAppsJson } from './modules/system-widgets/tasks/task.apps';

export function GetSystemAppJsons(translationService: ElmaTranslateService): ApplicationJson[] {
    return [].concat.apply([], [
        GetAdminAppsJson(translationService),
        GetTaskAppsJson(translationService),
        GetMonitorAppsJson(translationService)
    ])
}
