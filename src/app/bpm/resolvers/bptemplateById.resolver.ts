import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Template } from '../schema';
import { BPTemplateStore } from '../services/bptemplate.store';


@Injectable()
export class BPTemplateByIdResolver implements Resolve<Template> {

    constructor (
        private location: Location,
        private templateStore: BPTemplateStore,
    ) {
    }

    async resolve (
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Promise<Template> {
        try {

            const templateID = route.paramMap.get('id');
            if (templateID == null) {
                throw new Error('id not found');
            }

            return await this.templateStore.getTemplateByID(templateID);

        } catch (err) {
            this.location.back();

            throw err;
        }
    }
}
