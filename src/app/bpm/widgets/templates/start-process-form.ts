import { Button, ElmaButtonsComponent } from '../../../../shared/common/buttons';
import { CommonWidgetFieldTypes } from '../../../../shared/common/widgets/field-types';
import { WidgetTemplateBuilder } from '../../../../widget';
import { WidgetTemplateModel } from '../../../../widget/models/widget-template.model';
import { WidgetField } from '../../../../widget/schema/widget-field';
import { FieldType } from '../../../app.constants';
import { DynamicFormComponent, Field } from '../../../modules/dynamic-form/dynamic-form.component';
import { ItemForm } from '../../../widgets/descriptors/item-form';
import { ItemFormPopup } from '../../../widgets/descriptors/item-form-popup';
import { AppWidgetFieldTypes } from '../../../widgets/field-types';
import { ProcessFormTypeDescriptor } from './form-type-descriptor';

/**
 * Форма запуска процесса
 */
export namespace StartProcessForm {
    // Дескриптор данной формы
    export const Descriptor = <ProcessFormTypeDescriptor> {
        code: 'start-process-form',
        create: getTemplate,
    };

    // Поля, доступные для данной формы
    export namespace Fields {
        export const context = <WidgetField> {
            code: 'context',
            type: AppWidgetFieldTypes.Object,
        };
        export const fields = <WidgetField> {
            code: 'fields',
            type: AppWidgetFieldTypes.ObjectField,
            array: true,
        };
        export const actions = <WidgetField> {
            code: 'actions',
            type: CommonWidgetFieldTypes.Action,
            array: true,
        };
        export const instruction = <WidgetField> {
            code: 'instruction',
            type: FieldType.String,
        };
    }

    // Интерфейс для работы со значениями полей
    export interface Data {
        context: any;
        fields: Field[];
        actions: Button[];
        instruction?: string;
    }

    // Получить список полей, доступных для данной формы
    export function getFields (): WidgetField[] {
        return [
            Fields.context,
            Fields.fields,
            Fields.actions,
            Fields.instruction,
        ];
    }

    // Сформировать шаблон формы
    export function getTemplate (builder: WidgetTemplateBuilder): WidgetTemplateModel {
        return builder.create(ItemFormPopup.code)
            .zone(ItemForm.Zones.content.code, zb => zb
                .add(DynamicFormComponent, df => df
                    .mapValue('item', [ Fields.context.code ])
                    .mapValue('fields', [ Fields.fields.code ])
                )
            )
            .zone(ItemForm.Zones.footer.code, zb => zb
                .add(ElmaButtonsComponent, df => df
                    .mapValue('buttons', [ Fields.actions.code ])
                )
            )
            .template;
    }
}
