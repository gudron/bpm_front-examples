import { WidgetTemplateBuilder } from '../../../../widget/index';
import { WidgetTemplateModel } from '../../../../widget/models/widget-template.model';

/**
 * Интерфейс описания типа формы процесса (например "Форма для стартового события", "Форма для задачи" и т.д.)
 */
export interface ProcessFormTypeDescriptor {
    // Код типа формы процесса
    code: string;
    // Функция создания новой формы
    create: (builder: WidgetTemplateBuilder) => WidgetTemplateModel;
}
