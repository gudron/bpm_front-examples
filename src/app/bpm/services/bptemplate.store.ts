import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { ElmaTranslateService } from '../../../shared/common';
import { NamedWidgetTemplateJson } from '../../../widget';

import { Namespaces, ROOT_CATEGORY } from '../../app.constants';
import { FieldJson } from '../../models/field.json';
import { PageModel } from '../../models/page.model';
import { APIService, APIUrlBuilder } from '../../services/api.service';
import { PageStore } from '../../stores/page.store';
import { BadRequestError } from '../exceptions/bad-request-error';
import { CategoryTree, ImmutableCategoryTreeWithTemplates } from '../models';

import { Category, Instance, Template, ValidationError } from '../schema';
import { ExecutionError } from '../schema/execute-error.interface';
import { BPIo } from './io';

const categoryUrl = '/api/bpm/categories';
const itemUrl = '/api/bpm/templates';
const publicUrl = '/api/bpm/templates/public';

export enum BPTemplateStoreError {
    DublicateName,
    DublicateCode,
    CategoryNotFound,
    CategoryNotEmpty,
    TemplateNotFound,
    TemplateIsPublic,
    InvalidID,
    AlreadyLocked,
    LockNotFound,
}

export class ValidationException {
    constructor (public errors: ValidationError[]) {}
}

@Injectable()
export class BPTemplateStore {
    private categoryUrlBuilder = new APIUrlBuilder(categoryUrl);
    private templateUrlBuilder = new APIUrlBuilder(itemUrl);
    private publicUrlBuilder = new APIUrlBuilder(publicUrl);

    private templateCache = new Map<string, [Template, any]>();

    constructor (
        private api: APIService,
        private http: HttpClient,
        private ets: ElmaTranslateService,
        private IO: BPIo,
        private pageStore: PageStore,
    ) {
    }

    async getImmutableTree (draft = false, namespace: string = ''): Promise<ImmutableCategoryTreeWithTemplates> {
        const root = await this.getRoot(namespace);
        const [categories, opened, templates] = await Promise.all([
            this.getCategories(root.namespace), this.getOpenedCategories(), this.getTemplates(root.namespace, draft),
        ]);

        const tree = new ImmutableCategoryTreeWithTemplates(categories, opened, root, templates, draft);

        tree.onOpenChange.subscribe(this.saveOpenedCategories.bind(this));

        return tree;
    }

    async getTree (namespace: string = '', draft = true): Promise<CategoryTree> {
        const root = await this.getRoot(namespace);
        const [categories, opened, templates] = await Promise.all([
            this.getCategories(root.namespace), this.getOpenedCategories(), this.getTemplates(root.namespace, draft),
        ]);

        const tree = new CategoryTree(this, this.ets, categories, opened, root, templates);

        tree.onOpenChange.subscribe(this.saveOpenedCategories.bind(this));

        return tree;
    }

    async getTemplateByID (id: string): Promise<Template> {
        const pair = this.templateCache.get(id);
        if (pair !== undefined) {
            const [tmp, to] = pair;

            clearTimeout(to);
            this.templateCache.delete(id);

            return tmp;
        }
        const res = await this.api.get(this.templateUrlBuilder.build({
            id: id,
        }));
        switch (res.status) {
        case 200:

            return res.json();

        case 400:

            throw BPTemplateStoreError.InvalidID;

        case 404:

            throw BPTemplateStoreError.TemplateNotFound;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async getPublicTemplateByID (id: string, version?: number): Promise<Template> {
        const res = await this.api.get(this.publicUrlBuilder.build({
            id: id,
            suffix: version == null ? undefined : version.toString(),
        }));
        switch (res.status) {
        case 200:

            return res.json();

        case 400:

            throw BPTemplateStoreError.InvalidID;

        case 404:

            throw BPTemplateStoreError.TemplateNotFound;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async createCategory (json: Category): Promise<Category> {
        const res = await this.api.post(this.categoryUrlBuilder.build({}), json);
        switch (res.status) {
        case 201:

            return res.json();

        case 404:

            throw BPTemplateStoreError.CategoryNotFound;

        case 409:

            throw BPTemplateStoreError.DublicateName;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async updateCategory (json: Category): Promise<Category> {
        const res = await this.api.put(this.categoryUrlBuilder.build({
            id: json.__id,
        }), json);
        switch (res.status) {
        case 202:

            return res.json();

        case 404:

            throw BPTemplateStoreError.CategoryNotFound;

        case 409:

            throw BPTemplateStoreError.DublicateName;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async removeCategory (id: string): Promise<void> {
        const res = await this.api.delete(this.categoryUrlBuilder.build({
            id: id,
        }));
        switch (res.status) {
        case 202:

            return res.json();

        case 404:

            throw BPTemplateStoreError.CategoryNotFound;

        case 423:

            throw BPTemplateStoreError.CategoryNotEmpty;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async createTemplate (json: Template, publish?: boolean): Promise<Template> {
        const res = await this.api.post(this.templateUrlBuilder.build(publish ? { query: [{ key: 'publish', val: true }] } : {}), json);
        switch (res.status) {
        case 201:
            const tmp = <Template>res.json();

            this.templateCache.set(tmp.__id!, [tmp, setTimeout(() => {
                this.templateCache.delete(tmp.__id!);
            }, 1000)]);

            return tmp;

        case 404:

            throw BPTemplateStoreError.CategoryNotFound;

        case 409:
            if (res.text().includes('namespace+code')) {

                throw BPTemplateStoreError.DublicateCode;
            }

            throw BPTemplateStoreError.DublicateName;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async renameTemplate (id: string, name: string): Promise<Template> {
        const res = await this.api.put(this.templateUrlBuilder.build({
            id: id,
            suffix: 'rename',
        }), { name: name });
        switch (res.status) {
        case 202:

            return res.json();

        case 404:

            throw BPTemplateStoreError.TemplateNotFound;

        case 409:

            throw BPTemplateStoreError.DublicateName;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async moveTemplate (id: string, cid: string): Promise<Template> {
        const res = await this.api.put(this.templateUrlBuilder.build({
            id: id,
            suffix: 'move',
        }), { category: cid });
        switch (res.status) {
        case 202:

            return res.json();

        case 404:
            if (res.text().trim() === 'category') {
                throw BPTemplateStoreError.CategoryNotFound;
            } else {
                throw BPTemplateStoreError.TemplateNotFound;
            }

        case 409:

            throw BPTemplateStoreError.DublicateName;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async updateTemplate (
        id: string,
        process: any,
        context: FieldJson[],
        scripts: string,
        forms: NamedWidgetTemplateJson[],
        apiRun: boolean,
        manualRun: boolean,
        logged: boolean,
        hash: string,
    ): Promise<Template> {
        const res = await this.api.put(this.templateUrlBuilder.build({
            id: id,
        }), {
            process: process,
            context: context,
            scripts: scripts,
            forms: forms,
            apiRun: apiRun,
            manualRun: manualRun,
            logged: logged,
        }, { 'Lock-Hash': hash });
        switch (res.status) {
        case 202:

            return res.json();

        case 404:

            throw BPTemplateStoreError.TemplateNotFound;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async validateTemplate (json: Template): Promise<ValidationError[]> {

        return this.http
            .put<{ errors: ValidationError[] } | null>(this.templateUrlBuilder.build({
                suffix: 'validate',
            }), json)
            .map(res => res ? res.errors : [])
            .toPromise();
    }

    async publishTemplate (id: string, hash: string): Promise<Template> {
        const res = await this.api.put(this.templateUrlBuilder.build({
            id: id,
            suffix: 'publish',
        }), null, { 'Lock-Hash': hash });
        switch (res.status) {
        case 202:

            return res.json();

        case 404:

            throw BPTemplateStoreError.TemplateNotFound;

        case 412:

            throw new ValidationException(res.json()['errors']);
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async runTemplate (id: string, data: { [key: string]: any }): Promise<Instance> {
        const res = await this.api.put(this.publicUrlBuilder.build({
            id: id,
            suffix: 'run',
        }), data);
        if (res.status !== 202) {
            if (res.status === 400) {
                const err = <ExecutionError[]>res.json();
                this.IO.errors.push(err);
                throw new BadRequestError('validate error');
            }
            throw new Error(res.text());
        }
        const instance = <Instance>res.json();
        this.IO.output.push(instance);
        return instance;
    }

    async removeTemplate (id: string): Promise<void> {
        const res = await this.api.delete(this.templateUrlBuilder.build({
            id: id,
        }));
        switch (res.status) {
        case 202:

            return res.json();

        case 404:

            throw BPTemplateStoreError.TemplateNotFound;

        case 423:

            throw BPTemplateStoreError.TemplateIsPublic;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async lock (id: string): Promise<string> {
        const res = await this.api.post(this.templateUrlBuilder.build({
            id: id,
            suffix: 'lock',
        }), null);
        switch (res.status) {
        case 201:

            return res.text().trim();

        case 423:

            throw BPTemplateStoreError.AlreadyLocked;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async uplock (id: string, hash: string): Promise<void> {
        const res = await this.api.put(this.templateUrlBuilder.build({
            id: id,
            suffix: `lock`,
        }), null, { 'Lock-Hash': hash });
        switch (res.status) {
        case 200:

            return;

        case 404:
        case 423:

            throw BPTemplateStoreError.LockNotFound;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async unlock (id: string, hash: string): Promise<void> {
        const res = await this.api.delete(this.templateUrlBuilder.build({
            id: id,
            suffix: `lock`,
        }), { 'Lock-Hash': hash });
        switch (res.status) {
        case 200:

            return;

        case 404:
        case 423:

            throw BPTemplateStoreError.LockNotFound;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async getRoot (namespace: string): Promise<Category> {
        if (namespace === '' || namespace === Namespaces.Global) {
            namespace = Namespaces.Global;

            return {
                __id: ROOT_CATEGORY,
                name: this.ets.get('bpm.tree@global-processes'),
                parent: '',
                namespace: Namespaces.Global,
            };
        }

        const appConfig = namespace.split('.');
        let app: PageModel;
        if (appConfig.length > 1) {
            app = await this.pageStore.getByNamespaceAndCode(appConfig[0], appConfig[1]);
        } else {
            app = await this.pageStore.getByNamespaceAndCode(Namespaces.Global, appConfig[0]);
        }

        return {
            __id: ROOT_CATEGORY,
            name: app.name,
            parent: '',
            namespace: namespace,
        };
    }

    async getCategories (namespace: string = ''): Promise<Category[]> {
        const query = [
            { key: 'namespace', val: namespace },
        ];
        const res = await this.api.get(this.categoryUrlBuilder.build({ query: query }));
        switch (res.status) {
        case 200:

            return res.json();

        case 404:

            return [];
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async getCategoriesByParentIds (ids: string[]|string, namespace: string = ''): Promise<Response|any> {
        let categoryIds: string[] = [];
        const query = [
            { key: 'namespace', val: namespace },
        ];

        categoryIds = categoryIds.concat(ids);

        categoryIds.forEach(function (value, index) {
            query.push({ key: 'ids', val: value });
        });

        const res = await this.api.get(this.categoryUrlBuilder.build({suffix: 'byparent', query: query }));
        switch (res.status) {
        case 200:

            return res;

        case 404:

            return [];
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    private async getOpenedCategories (): Promise<string[]> {
        const raw = localStorage.getItem('bpm-opened-categories');
        if (raw == null) {
            return [];
        }

        return JSON.parse(raw);
    }

    private saveOpenedCategories (list: string[]) {
        localStorage.setItem('bpm-opened-categories', JSON.stringify(list));
    }

    async getTemplates (namespace: string, drafts: boolean = false): Promise<Template[]> {
        let urlBuilder: APIUrlBuilder;
        if (drafts) {
            urlBuilder = this.templateUrlBuilder;
        } else {
            urlBuilder = this.publicUrlBuilder;
        }
        const query = [{
            key: 'namespace',
            val: namespace,
        }];
        const res = await this.api.get(urlBuilder.build({ query: query }));
        switch (res.status) {
        case 200:

            return res.json();
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }
}
