import { EventEmitter, Injectable } from '@angular/core';
import { ItemChangeHandler } from '../../application/services/item-change.handler';
import { Instance } from '../schema';
import { ExecutionError } from '../schema/execute-error.interface';

/**
 * Стэк ввода/вывода для БП
 * Если нужно предзаполнить форму перед вызовом стартовой формы, то нужно поместить данные в поток ввода (input)
 * После выполнения действия результат действия складывается в стэк вывода и происходит событие onPush у output
 * Можно подписаться на это событие и получать результаты всех действий
 */


/**
 * Стэк с событием на помещение в него элемента
 */
export class BPIoStack<T> {
    private stack: T[] = [];
    readonly onPush = new EventEmitter<T>();

    pop (): T | undefined {
        return this.stack.pop();
    }

    push (val: T) {
        this.stack.push(val);
        this.onPush.emit(val);
    }
}


/**
 * Сервис для предоставления механизма ввода/вывода
 */
@Injectable()
export class BPIo {
    readonly input: BPIoStack<any>;
    readonly output: BPIoStack<Instance>;
    readonly errors: BPIoStack<ExecutionError[]>;

    constructor (
        private _handler: ItemChangeHandler,
    ) {
        this.input = new BPIoStack<any>();
        this.output = new BPIoStack<Instance>();
        this.errors = new BPIoStack<ExecutionError[]>();
        this.output.onPush.subscribe((data: any) => this.parseBPResult(data));
    }

    private parseBPResult (data: Instance) {
        if (data.__objects) {
            const objects = data.__objects;
            for (const id of Object.keys(objects)) {
                const object = objects[id];
                this._handler.handle(object);
            }
        }
    }
}

