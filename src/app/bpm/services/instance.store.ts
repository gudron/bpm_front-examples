import { Injectable } from '@angular/core';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/takeWhile';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { APIService } from '../../services/api.service';

import { Instance, Task } from '../schema';
import { TaskStore } from './task.store';

const url = '/api/query';

@Injectable()
export class InstanceStore {
    constructor (
        private api: APIService,
        readonly taskStore: TaskStore,
    ) {
    }

    async getInstance (namespace: string, code: string, id: string): Promise<Instance> {
        const res = await this.api.get(`${ url }/${ namespace }/_process_${ code }/items/${ id }`);

        if (res.ok) {

            return res.json()['result'];
        }

        throw new Error(res.text());
    }

    waitNewTasks (namespace: string, code: string, instance: Instance): Promise<Instance> {
        const done = new Subject();
        let lastState = instance;

        return new Promise(resolve => {
            Observable.interval(1000)
                .takeWhile(retry => retry < 3)
                .takeUntil(done)
                .forEach(async (retry) => {
                    lastState = await this.getInstance(namespace, code, instance.__id);
                    if (lastState.__tasks == null) {

                        return;
                    }
                    const newTasks: Task[] = [];

                    for (const id of Object.keys(lastState.__tasks)) {
                        if (instance.__tasks == null || !instance.__tasks.hasOwnProperty(id)) {
                            newTasks.push(lastState.__tasks![id]);
                        }
                    }

                    if (newTasks.length > 0) {
                        done.complete();
                        resolve(lastState);
                    }
                })
                .then(() => resolve(lastState));
        });
    }
}
