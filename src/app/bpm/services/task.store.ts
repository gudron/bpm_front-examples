import { EventEmitter, Injectable } from '@angular/core';
import { Collection } from '../../application/schema/collection.interface';
import { DefaultCollectionStore } from '../../application/services/collection.store.default';
import { APIService, APIUrlBuilder } from '../../services/api.service';
import { TaskModel } from '../models/task.model';
import { Instance, Task } from '../schema';

const apiUrlPrefix = '/api/bpm/tasks';

@Injectable()
export class TaskStore extends DefaultCollectionStore {
    private url = new APIUrlBuilder(apiUrlPrefix);
    private reassignUrl = new APIUrlBuilder(apiUrlPrefix);
    private _taskCache = new Map<string, [Task, any]>();

    constructor (
        _apiService: APIService,
    ) {
        super(_apiService,
            <Collection>{
                namespace: 'system',
                code: 'tasks',
            },
            apiUrlPrefix,
        );
    }

    cacheTask (task: Task) {
        const to = setTimeout(() => {
            this._taskCache.delete(task.__id);
        }, 1000);

        this._taskCache.set(task.__id, [task, to]);
    }

    async getTaskByID (id: string): Promise<TaskModel> {
        const pair = this._taskCache.get(id);
        if (pair !== undefined) {
            const [task, to] = pair;

            clearTimeout(to);
            this._taskCache.delete(id);

            return new TaskModel(this, task);

        }
        const res = await this.apiService.get(
            this.url.build({
                id: id,
            }),
        );
        if (res.status !== 200) {
            throw new Error(res.text());
        }

        return new TaskModel(this, res.json());
    }

    async submitTask (id: string, instanceId: string, exit: string, data: { [key: string]: any }): Promise<Instance> {
        const res = await this.apiService.put(
            this.url.build({ id: id }),
            {
                instanceId: instanceId,
                data: Object.assign({}, data, {__exit: exit}),
            },
        );

        return res.json();
    }

    async reassignTask (processInstanceId: string, taskId: string, userId: string, timeForWork: number, comment: string )
    : Promise<Instance> {
        const res = await this.apiService.post(
            this.reassignUrl.build({ id: processInstanceId, suffix: 'reassign' }),
            {
                taskId: taskId,
                userId: userId,
                timeForWork: timeForWork,
                comment: comment,
            },
        );

        if (res.status !== 200) {
            throw new Error(res.text());
        }

        return res.json();
    }
}
