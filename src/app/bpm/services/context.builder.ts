import { Injectable } from '@angular/core';
import { FieldType, Namespaces } from '../../app.constants';
import { ApplicationStore } from '../../application/services/application.store';
import { FieldJson } from '../../models/field.json';
import { Page } from '../../shared/pages/page.component';

@Injectable()
export class ContextBuilder {

    constructor (private _appStore: ApplicationStore) {

    }

    async byBPNamespace (namespace: string): Promise<FieldJson[]> {
        const parts = namespace.split('.');
        if (parts.length === 2) {
            return await this.byNamespaceCode(parts[0], parts[1]);
        }
        return [];
    }

    private async byNamespaceCode (namespace: string, code: string): Promise<FieldJson[]> {
        const app = await this._appStore.getModelByNamespaceAndCode(namespace, code);
        return [
            {
                type: FieldType.SystemCollection,
                code: app.code,
                single: true,
                data: {
                    namespace: app.namespace,
                    code: app.code,
                },
                view: {
                    name: app.name,
                },
            },
        ];
    }

    getBPNamespaceByPage (page: Page | undefined) {
        if (page === undefined) {
            return Namespaces.Global;
        } else if (page.namespace === Namespaces.Global) {
            return page.code;
        } else {
            return page.namespace + '.' + page.code;
        }
    }
}
