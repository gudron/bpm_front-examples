export class BadRequestError extends Error {
    constructor (message: any) {
        super(message);
        Object.setPrototypeOf(this, BadRequestError.prototype);
    }
}
