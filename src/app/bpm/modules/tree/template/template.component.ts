import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild, } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryTree, TemplateModel } from '../../../models';
import { BPTemplateStoreError } from '../../../services/bptemplate.store';
import { BPType } from '../../../schema';

@Component({
    selector: 'app-bptemplate-admin-page-tree-template',
    templateUrl: './template.component.html',
    styles: [`
        :host {
            display: block;
            background: #fff;
        }
        .template:not(.deleted):hover {
            background: #f9f9f9;
        }

        .template.deleted {
            opacity: 0.6;
        }

        .prefix {
            box-sizing: content-box;
            width: 18px;
        }

        .icon {
            height: 18px;
        }

        .name {
            display: flex;
            flex-grow: 1;
        }

        .version {
            width: 100px;
            text-align: center;
        }

        .updated {
            width: 250px;
            text-align: center;
        }

        .actions {
            width: 50px;
            cursor: pointer;
        }

        .actions .dots {
            visibility: hidden;
        }

        .template:hover .actions .dots,
        .template.actions-showed .actions .dots {
            visibility: visible;
        }

        .dropdown-toggle:after {
            display: none;
        }

        .dropdown-item {
            cursor: pointer;
        }
    `],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TemplateComponent {
    readonly BPType = BPType;

    @Input() tree: CategoryTree;
    @Input() linkBuilder: (template: TemplateModel) => any[];
    @Input() template: TemplateModel;
    @Input() drafts: boolean;
    @Input() depth: number;

    @Output() onMoveTemplate = new EventEmitter<TemplateModel>();
    @Output() onRemoveTemplate = new EventEmitter<TemplateModel>();
    @Output() onSelectTemplate = new EventEmitter<TemplateModel>();

    editMode = false;
    name: string;
    dublicateName = false;

    @ViewChild('nameInput') private nameInput: ElementRef;

    constructor (
        public router: Router,
        private cdr: ChangeDetectorRef,
    ) {
    }

    get linkBuilderType(): string {
        return typeof this.linkBuilder;
    }

    edit () {
        this.name = this.template.name;
        this.dublicateName = false;
        this.editMode = true;
        setTimeout(() => (<HTMLInputElement>this.nameInput.nativeElement).select(), 0);
    }

    cancel () {
        this.editMode = false;
    }

    async rename () {
        if (this.name.length === 0) {
            return;
        }
        try {
            await this.tree.renameTemplate(this.template, this.name);
            this.editMode = false;
            this.dublicateName = false;
        } catch (err) {
            (<HTMLInputElement>this.nameInput.nativeElement).focus();
            switch (err) {
            case BPTemplateStoreError.DublicateName:
                this.dublicateName = true;
                break;

            default:
                throw err;
            }
        } finally {
            this.cdr.markForCheck();
        }
    }
}
