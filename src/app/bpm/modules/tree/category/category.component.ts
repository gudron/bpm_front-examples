import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild, } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { ElmaTranslateService } from '../../../../../shared/common';
import { ElmaPopoverComponent } from '../../../../../shared/common/elma-popover';
import { CategoryModel, CategoryTree, ImmutableCategoryTree, TemplateModel } from '../../../models';
import { BPTemplateStoreError } from '../../../services/bptemplate.store';

@Component({
    selector: 'app-bptemplate-admin-page-tree-category',
    templateUrl: './category.component.html',
    styles: [`
        .category:hover,
        .category.actions-showed {
            background: #f9f9f9;
        }

        .icon {
            color: #2261AB;
        }

        .name {
            display: flex;
            flex-grow: 1;
        }

        .actions {
            width: 50px;
            cursor: pointer;
        }

        .actions .dots {
            visibility: hidden;
        }

        .category:hover .actions .dots,
        .category.actions-showed .actions .dots {
            visibility: visible;
        }

        .opened-indicator {
            box-sizing: content-box;
            width: 18px;
            transition: transform 200ms;
            cursor: pointer;
        }

        .opened-indicator.opened {
            transform: rotate(90deg);
        }

        .dropdown-toggle:after {
            display: none;
        }

        .dropdown-item {
            cursor: pointer;
        }
    `],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryComponent {
    @Input() tree: ImmutableCategoryTree;
    @Input() showRoot = true;
    @Input() linkBuilder: (template: TemplateModel) => any[];
    @Input() category: CategoryModel;
    @Input() drafts: boolean;
    @Input() depth = 0;

    @Output() onCreateCategory = new EventEmitter<string>();
    @Output() onCreateTemplate = new EventEmitter<string>();
    @Output() onMoveCategory = new EventEmitter<CategoryModel>();
    @Output() onCategoryClick = new EventEmitter<CategoryModel>();
    @Output() onMoveTemplate = new EventEmitter<TemplateModel>();
    @Output() onRemoveCategory = new EventEmitter<CategoryModel>();
    @Output() onRemoveTemplate = new EventEmitter<TemplateModel>();
    @Output() onSelectTemplate = new EventEmitter<TemplateModel>();
    @ViewChild('menu') menu: ElmaPopoverComponent;
    editMode = false;
    name: string;
    dublicateName = false;
    @ViewChild('nameInput') private nameInput: ElementRef;

    constructor (
        private transaltionService: ElmaTranslateService,
        private cdr: ChangeDetectorRef,
    ) {
    }

    edit () {
        this.name = this.category.name;
        this.dublicateName = false;
        this.editMode = true;
        setTimeout(() => (<HTMLInputElement>this.nameInput.nativeElement).select(), 0);
    }

    cancel () {
        this.editMode = false;
    }

    async rename () {
        try {
            await (<CategoryTree>this.tree).renameCategory(this.category, this.name);
            this.dublicateName = false;
            this.editMode = false;
        } catch (err) {
            (<HTMLInputElement>this.nameInput.nativeElement).focus();
            switch (err) {
            case BPTemplateStoreError.DublicateName:
                this.dublicateName = true;
                break;

            default:

                throw err;
            }
        } finally {
            this.cdr.markForCheck();
        }
    }

    // TODO: перенести в admin-page
    async removeCategory () {
        try {
            await (<CategoryTree>this.tree).removeCategory(this.category);
        } catch (err) {
            switch (err) {
            case BPTemplateStoreError.CategoryNotEmpty:
                const phrase = await this.transaltionService
                    .get('bpm.tree@category-not-empty-error');
                err = new Error(phrase);
            }

            throw err;
        }
    }
}
