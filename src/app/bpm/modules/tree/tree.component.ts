import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CategoryModel, ImmutableCategoryTree, TemplateModel } from '../../models';

@Component({
    selector: 'app-bpm-tree',
    templateUrl: './tree.component.html',
    styleUrls: ['tree.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BPMTreeComponent implements OnInit {
    @Input() tree: ImmutableCategoryTree | ImmutableCategoryTree[];
    @Input() showRoot = true;
    @Input() drafts = false;
    @Input() linkBuilder: (template: TemplateModel) => any[];
    @Input() depth = 0;

    @Output() onCreateCategory = new EventEmitter<string>();
    @Output() onCreateTemplate = new EventEmitter<string>();
    @Output() onMoveCategory = new EventEmitter<CategoryModel>();
    @Output() onCategoryClick = new EventEmitter<CategoryModel>();
    @Output() onMoveTemplate = new EventEmitter<TemplateModel>();
    @Output() onRemoveCategory = new EventEmitter<CategoryModel>();
    @Output() onRemoveTemplate = new EventEmitter<TemplateModel>();
    @Output() onSelectTemplate = new EventEmitter<TemplateModel>();

    ngOnInit () {
        if (!Array.isArray(this.tree)) {
            this.tree = [this.tree];
        }
        this.tree[0].root.open();
    }
}
