import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MobxAngularModule } from 'mobx-angular';
import { ElmaCommonModule } from '../../../../shared/common';
import { CategoryComponent } from './category/category.component';
import { TemplateComponent } from './template/template.component';
import { BPMTreeComponent } from './tree.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,

        MobxAngularModule,

        ElmaCommonModule,
    ],
    declarations: [
        BPMTreeComponent,
        CategoryComponent,
        TemplateComponent,
    ],
    providers: [],
    exports: [
        BPMTreeComponent,
    ],
})
export class BPMTreeModule {
}
