import { Process } from '../../../diagram/bpmn/schema';
import { deepCopy } from '../../../shared/utils/deep-copy';

export function improveState (state: Process): Process {

    return state;
}

export function cleanupState (state: Process): Process {
    const res: Process = deepCopy(state);

    for (const key of Object.keys(res.lanes)) {
        delete res.lanes[key].kind;
    }
    for (const key of Object.keys(res.items)) {
        delete res.items[key].kind;
    }

    return res;
}
