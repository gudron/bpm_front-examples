import { computed } from 'mobx-angular';
import { fromStream } from 'mobx-utils';
import { AddOperation, Operation } from 'rfc6902/diff';
import { Observable } from 'rxjs/Observable';
import { Item as BaseItem, ItemType, Lane, LaneDirection, Process } from '../../../diagram/bpmn/schema';
import { DiagramTouchEvent } from '../../../diagram/schema/event.interface';
import { ButtonState } from '../../../diagram/shared/header-actions/header-actions.component';
import { ElmaTranslateService } from '../../../shared/common';
import { JSONPatchHistoryContainer } from '../../../shared/utils/json-patch-history.container';
import { uuid4 } from '../../../shared/utils/uuid';
import { FieldType, NULL_UUID } from '../../app.constants';
import { getTemplate } from '../../application/modules/config/fields/fields.component';
import { FieldJson } from '../../models/field.json';
import { Item, LaneSettings, LaneType, StartEventSettings, Transition } from '../schema';
import { cleanupState, improveState } from './bpmn-process-edit.rules';
import { BpmnProcessModel } from './bpmn-process.model';

interface HistoryContainer<T> {
    readonly state: Observable<T>;
    snapshot: T;
    readonly isUndoAvailable: Observable<boolean>;
    readonly isRedoAvailable: Observable<boolean>;

    getPatchedState (patch: Operation[]): T;
    getObjectByPath (path: string): any;
    push (state: T): void;
    undo (): void;
    redo (): void;
}

export class BpmnProcessEditModel extends BpmnProcessModel {
    static default (ets: ElmaTranslateService): [Process, FieldJson[]] {
        const laneId = uuid4();

        return [{
            paper: {
                format: 'A4',
                orientation: 'landscape',
            },
            lanes: {
                [laneId]: <Lane>{
                    id: laneId,
                    x: 8,
                    y: 8,
                    width: 224,
                    height: 600,
                    color: '#d6f0cc',
                    name: ets.get('bpm.editor.diagram@initiator'),
                    multiInstance: false,
                    direction: LaneDirection.Vertical,
                    settings: <LaneSettings>{
                        type: LaneType.Dynamic,
                        variable: 'initiator',
                    },
                },
            },
            items: {
                [NULL_UUID]: <BaseItem>{
                    id: NULL_UUID,
                    type: ItemType.Start,
                    x: 96,
                    y: 80,
                    width: 32,
                    height: 32,
                    color: '#ffffff',
                    lane: laneId,
                    name: ets.get('bpm.editor.diagram@start'),
                    settings: <StartEventSettings>{
                        instruction: '',
                        startButtonTitle: ets.get('bpm.editor.diagram@start-button-title'),
                        formFields: [],
                        titlePrompt: true,
                        titleTemplate: '',
                        exits: [],
                    },
                },
            },
            transitions: {},
        }, [
            <FieldJson>{
                code: 'initiator',
                type: FieldType.SystemUser,
                array: true,
                single: true,
                view: {
                    name: ets.get('bpm.editor.diagram@initiator'),
                },
                isNewField: true,
            },
        ]];
    }

    private history: HistoryContainer<Process>;

    constructor (
        protected ets: ElmaTranslateService,
        state: Process,
    ) {
        super();
        state = improveState(state);

        this.history = new JSONPatchHistoryContainer<Process>(state);

        this._undoState = fromStream<ButtonState>(
            this.history.isUndoAvailable
                .map(available => available ? ButtonState.Default : ButtonState.Disabled),
        );
        this._redoState = fromStream<ButtonState>(
            this.history.isRedoAvailable
                .map(available => available ? ButtonState.Default : ButtonState.Disabled),
        );
    }

    private _undoState: { current: ButtonState };

    @computed
    get undoState (): ButtonState {
        return this._undoState.current;
    }

    private _redoState: { current: ButtonState };

    @computed
    get redoState (): ButtonState {
        return this._redoState.current;
    }

    get state (): Observable<Process> {
        return this.history.state;
    }

    get snapshot (): Process {
        return this.history.snapshot;
    }

    get json (): Process {
        return this.snapshot;
    }

    get cleanedSnapshot (): Process {

        return cleanupState(this.snapshot);
    }

    private get newLaneName (): string {

        return BpmnProcessEditModel.getSequensedName(
            this.ets.get('bpm.editor.settings@lane-default-name'),
            Object.keys(this.snapshot.lanes).map(key => this.snapshot.lanes[key].name!),
        );
    }

    private newItemName (defaultName: string): string {

        return BpmnProcessEditModel.getSequensedName(
            defaultName,
            Object.keys(this.snapshot.items).map(key => this.snapshot.items[key].name!),
        );
    }

    private static getSequensedName (prefix: string, names: string[]): string {
        let n = 1;
        while (true) {
            const name = `${ prefix } ${ n }`;
            if (names.indexOf(name) === -1) {

                return name;
            }
            n++;
        }
    }

    applyPatch (patch: Operation[], withHistory: boolean = true): DiagramTouchEvent | undefined {
        const newItem = patch.find(op => (<any>op).isNew);

        this.addDefaultSettingsToNewTransition(patch);

        const withExits = this.extendPatchWithExits(patch);

        let state = this.history.getPatchedState(withExits);
        state = improveState(state);
        if (withHistory) {
            this.history.push(state);
        } else {
            this.history.snapshot = state;
        }

        if (newItem !== undefined) {
            const item = (<AddOperation>newItem).value;

            if (item.type === undefined) {
                item.name = this.newLaneName;
            } else if (item.name !== undefined) {
                item.name = this.newItemName(item.name);
            }

            // TODO: если надо открыть настройки сразу
            // return [newItem.path, this.history.getObjectByPath(newItem.path)];
        }

        return undefined;
    }

    undo () {
        this.history.undo();
    }

    redo () {
        this.history.redo();
    }

    getItemByPath (path: string): Item | undefined {

        return this.history.getObjectByPath(path);
    }

    /**
     * При создании перехода надо добавить ему дефолтные настройки
     */
    private addDefaultSettingsToNewTransition (patch: Operation[]) {
        const searchRE = new RegExp(/^\/transitions\/([0-9a-hA-H\-]+)?$/);

        for (const op of patch) {
            if (op.op === 'add') {
                const arr = searchRE.exec(op.path);
                if (arr !== null) {
                    (<Transition>op.value).settings = {
                        descritpion: '',
                        exit: {
                            buttonClass: 'default',
                            skipCheck: false,
                            confirm: 'none',
                            confirmText: '',
                            formFields: [],
                            popoverSize: 'default',
                            interrupt: false,
                        },
                        conditions: [],
                    };
                }
            }
        }
    }

    /**
     * При создании, перекидывании или удалении переходов надо изменить массивы exits смежных элементов
     */
    private extendPatchWithExits (patch: Operation[]): Operation[] {
        const res: Operation[] = [];
        // Нас интересуют операции целиком над объектом
        const searchRE = new RegExp(/^\/transitions\/([0-9a-hA-H\-]+)?$/);

        for (const op of patch) {
            const arr = searchRE.exec(op.path);
            if (arr !== null) {
                const id = arr[1];

                switch (op.op) {
                case 'add':
                    res.push(...this.addExit(op.value));
                    break;

                case 'remove':
                    res.push(...this.removeExit(id));
                    break;

                case 'replace':
                    res.push(...this.updateExits(op.value));
                    break;
                }
            }
        }

        return res.concat(patch);
    }

    private updateExits (transition: Transition): Operation[] {
        const previous: Transition = <any>this.snapshot.transitions[transition.id];

        if (previous.source === transition.source) {

            return [];
        }

        return (<Operation[]>[]).concat(
            this.removeExit(transition.id),
            this.addExit(transition),
        );
    }

    private addExit (transition: Transition): Operation[] {
        const source: Item = <any>this.snapshot.items[transition.source];
        const index = source.settings.exits.indexOf(transition.id);

        if (index !== -1) {

            return [];
        }

        return [{
            op: 'add',
            path: `/items/${ transition.source }/settings/exits/${ source.settings.exits.length }`,
            value: transition.id,
        }];
    }

    private removeExit (transitionId: string): Operation[] {
        const transition: Transition = <any>this.snapshot.transitions[transitionId];
        const source: Item = <any>this.snapshot.items[transition.source];
        const index = source.settings.exits.indexOf(transitionId);

        if (index === -1) {

            return [];
        }

        return [{
            op: 'remove',
            path: `/items/${ transition.source }/settings/exits/${ index }`,
        }];
    }
}
