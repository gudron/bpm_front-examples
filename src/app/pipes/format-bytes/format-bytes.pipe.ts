import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'formatBytes'
})
export class FormatBytesPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB'];

        if (value === 0) {
            return '0 Byte';
        }

        const val: number = Math.floor(Math.log(value) / Math.log(1024));
        return `${Math.round(value / Math.pow(1024, val))} ${sizes[val]}`;
    }

}
