import { environment } from '../environments/environment';

const backend = (environment.backend ? environment.backend : location.host);

export class AppSettings {

    public static uploadFilesUrl = `//${location.host}/upload`;
    public static downloadFiles = `https://${backend}`;

    public static tcUrl = '//tc.dev.elma365.com/api';

    public static websockets = {
        url: `wss://${backend}/ws/`,
        methods: {
            serv: {
                auth: 'serv_auth.Auth',
                status: 'serv_wsproxy.Status',
                graphql: 'serv_graphql.Query',
                signout: 'serv_wsproxy.Logout',
                pushSv: 'notification',
                postLink: 'serv_file.Postlink',
                getLink: 'serv_file.Getlink',
                getGroups: 'serv_wsproxy.GetGroups',
                // aggr_collection
                createCollection: 'aggr_collection.Create',
                updateCollection: 'aggr_collection.Update',
                createCategory: 'aggr_collection.CreateCategory',
                renameCategory: 'aggr_collection.RenameCategory',
                moveCategory: 'aggr_collection.MoveCategory',
                deleteCategory: 'aggr_collection.DeleteCategory',
                createItem: 'aggr_collection.CreateItem',
                updateItem: 'aggr_collection.UpdateItem',
                deleteItem: 'aggr_collection.DeleteItem',
                // end aggr_collection
                getCollection: 'query_collection.Query',
                deleteFile: 'serv_file.DeleteFile',
            },
            user: {
                registry: 'aggr_user.Create',
            },
            permission: {
                get: 'query_collection.GetPermissions',
                update: 'aggr_collection.UpdatePermissions',
            },
            message: {
                toRead: 'aggr_message.MarkRead',
                createChain: 'aggr_message.CreateChain',
                addComment: 'aggr_message.AddComment'
            },
            bpinstance: {
                closeTask: 'aggr_bpinstance.CloseTask',
            },
            bptemplate: {
                create: 'aggr_bptemplate.Create',
                resetDraft: 'aggr_bptemplate.ResetDraft',
                updateDraft: 'aggr_bptemplate.UpdateDraft',
                run: 'aggr_bptemplate.Run',
                publishDraft: 'aggr_bptemplate.PublishDraft',
            },
            page: {
                create: 'aggr_page.Create',
                delete: 'aggr_page.Delete',
                update: 'aggr_page.Update',
                updatePermissions: 'aggr_page.UpdatePermissions',
            }
        }
    };

    public static debounceTime = {
        searchuser: 300,
    };

    public static pushNotifications = {
        closeDelay: 20000,
    };
}
