import {
    ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, forwardRef, Input, OnInit, Output,
    ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ElmaTranslateService } from '../../../shared/common';
import * as TimeZoneData from './data/timezones.data.json';
import * as moment from 'moment';
import { TimeZoneDataInterface } from './schema/timeZoneData.interface';
import { TimeZoneSelectOption } from './schema/timeZoneSelectOption.interface';

const noop = () => {
};

@Component({
    selector: 'elma-timezone-select',
    templateUrl: './elma-timezone-select.component.html',
    styleUrls: ['./elma-timezone-select.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ElmaTimezoneSelectComponent), multi: true },
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ElmaTimezoneSelectComponent implements ControlValueAccessor {
    private innerValue = '';
    public translateLabels = true;
    public options: TimeZoneSelectOption[];

    constructor (
        protected translate: ElmaTranslateService,
    ) {
        this.fillTimeZoneOptions();
    }

    private fillTimeZoneOptions() {
        const timeZoneSelectorOptions: TimeZoneSelectOption[] = [];
        const timezones: TimeZoneDataInterface[] = (<any>TimeZoneData);

        timezones.forEach(function (timeZone: any, index: number) {

            timeZoneSelectorOptions.push({
                value: timeZone.name,
                label: this.prepareUtcLabel(timeZone)
            })

        }, this);

        this.options = timeZoneSelectorOptions;
    }

    private prepareUtcLabel(timezone: TimeZoneDataInterface): string {
        const hOffsetSight = timezone.offset <= 0 ? '+' : '-' ;
        const hours = moment.duration(timezone.offset * -1, 'minutes');
        const hString = hOffsetSight +
            moment.utc(hours.asMilliseconds()).format('HH:mm');

        return timezone.name + ' ( UTC ' + hString + ' )';
    }

    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    get value(): any {
        return this.innerValue;
    };

    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
    }

    onBlur() {
        this.onTouchedCallback();
    }

    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }
}
