export interface TimeZoneSelectOption {
    label?: string;
    icon?: string;
    value: any;
}
