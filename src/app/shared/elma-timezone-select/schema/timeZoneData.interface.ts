export interface TimeZoneDataInterface {
    name: string;
    abbr: string;
    offset: number;
}
