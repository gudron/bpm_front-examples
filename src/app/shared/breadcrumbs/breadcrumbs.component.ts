import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-breadcrumbs',
    templateUrl: 'breadcrumbs.component.html',
    styleUrls: ['breadcrumbs.component.scss'],
})

export class BreadcrumbsComponent {
    @Input() homeLabel: string;
    @Input() homeRouterLink: any;
    @Output() home = new EventEmitter<any>();

    @Input() label: string;
    @Input() labelRouterLink: any;
    @Output() labelClick = new EventEmitter<any>();

    @Input() label2: string;
    @Input() hasSettings: boolean;
    @Output() back = new EventEmitter<any>();
    @Output() settings = new EventEmitter<any>();
}
