import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { UserHeaderComponent } from 'app/shared/headers/user-header/user-header.component';
import { MobxAngularModule } from 'mobx-angular';
import { ImageCropperModule } from 'ngx-img-cropper';
import { NgUploaderModule } from 'ngx-uploader';
import { ElmaCommonModule } from '../../shared/common';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { PageContentComponent } from './content/content.component';
import { ContextFieldsEditorModule } from './contextFieldsEditor/contextFieldsEditor.module';
import { ElmaFileUploaderComponent } from './elma-file-uploader/elma-file-uploader.component';
import { ElmaImageCropperComponent } from './elma-image-uploader/elma-image-cropper/elma-image-cropper.component';
import { ElmaImageUploaderComponent } from './elma-image-uploader/elma-image-uploader.component';
import { ElmaImageViewComponent } from './elma-image-uploader/elma-image-view/elma-image-view.component';
import { ElmaTimezoneSelectComponent } from './elma-timezone-select/elma-timezone-select.component';
import { ElmaTreeComponent } from './elma-tree/elma-tree.component';
import { FieldEditorModule } from './fieldEditor/fieldEditor.module';
import {
    DeclarationComponents as FileModuleDeclarationComponents,
    ExportComponents as FileModuleExportComponents,
} from './file/file.module';
import { FormEditorComponent } from './formEditor/formEditor.component';
import { FormEditorModule } from './formEditor/formEditor.module';
import { GroupNameComponent } from './group-name/group-name.component';
import { MobileMenuComponent } from './headers/_mobile-menu/mobile-menu.component';
import { HeaderMobileButtonsComponent } from './headers/buttons/mobile-buttons.component';
import { MainHeaderComponent } from './headers/main-header.component';
import { PageHeaderComponent } from './headers/page-header.component';
import { OrgstructNameComponent } from './orgstruct-name/orgstruct-name.component';
import { GroupSelectComponent } from './orgunit-select/group-select/group-select.component';
import { OrgstructSelectComponent } from './orgunit-select/orgstruct-select/orgstruct-select.component';
import { OrgunitSelectComponent } from './orgunit-select/orgunit-select.component';
import { OrgunitUserSelectComponent } from './orgunit-select/user-select/user-select.component';
import { ConfigPageComponent } from './pages/config-page.component';
import { PageComponent } from './pages/page.component';
import { HtmlEditorComponent } from './quill/editor/editor.component';
import { HtmlEditorModule } from './quill/editor/editor.module';
import { ReadMoreComponent } from './read-more/read-more.component';
import { TemplateFillerFieldApplicationComponent } from './template-filler/fields/application/field-application.component';
import { TemplateFillerFieldComponent } from './template-filler/fields/field.component';
import { TemplateFillerFieldSimpleComponent } from './template-filler/fields/simple/field-simple.component';
import { TemplateFillerComponent } from './template-filler/template-filler.component';
import { UserNameComponent } from './user/user-name/user-name.component';
import { UserPhotoComponent } from './user/user-photo/user-photo.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserComponent } from './user/user.component';
import { NotificationsComponent } from './user-profile/settings-tabs/notifications/notifications.component';
import { UserModule } from './user/user.module';

import { TreeModule } from 'primeng/primeng';

@NgModule({
    imports: [
        CommonModule,
        ElmaCommonModule,
        NgxDnDModule,
        FormsModule,
        RouterModule,
        FieldEditorModule,
        FormEditorModule,
        ContextFieldsEditorModule,
        NgUploaderModule,
        MobxAngularModule,
        TreeModule,
        ImageCropperModule,
        UserModule,
        HtmlEditorModule,
    ],
    declarations: [
        PageComponent,
        ConfigPageComponent,
        BreadcrumbsComponent,
        ElmaFileUploaderComponent,
        ElmaImageUploaderComponent,
        ElmaTimezoneSelectComponent,
        ElmaTreeComponent,
        OrgunitSelectComponent,
        OrgunitUserSelectComponent,
        GroupSelectComponent,
        OrgstructSelectComponent,
        GroupNameComponent,
        OrgstructNameComponent,
        TemplateFillerComponent,
        TemplateFillerFieldComponent,
        TemplateFillerFieldSimpleComponent,
        TemplateFillerFieldApplicationComponent,
        FileModuleDeclarationComponents,
        ElmaImageCropperComponent,
        ElmaImageViewComponent,
        MainHeaderComponent,
        PageHeaderComponent,
        UserHeaderComponent,
        MobileMenuComponent,
        HeaderMobileButtonsComponent,
        UserProfileComponent,
        PageContentComponent,
        NotificationsComponent,
        ReadMoreComponent,
    ],
    exports: [
        FormEditorComponent,
        ConfigPageComponent,
        PageComponent,
        BreadcrumbsComponent,
        ElmaFileUploaderComponent,
        ElmaImageUploaderComponent,
        OrgunitSelectComponent,
        GroupNameComponent,
        OrgstructNameComponent,
        TemplateFillerComponent,
        FileModuleExportComponents,

        ElmaTimezoneSelectComponent,
        ElmaTreeComponent,
        MainHeaderComponent,
        PageHeaderComponent,
        UserHeaderComponent,
        HeaderMobileButtonsComponent,
        UserComponent,
        UserPhotoComponent,
        UserNameComponent,
        PageContentComponent,
        HtmlEditorComponent,
        ReadMoreComponent,
    ],
    entryComponents: [
        ElmaImageCropperComponent,
        UserProfileComponent,
    ],
    providers: [],
})
export class AppSharedModule {
}
