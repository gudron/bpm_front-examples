export interface ElmaTreeCategoryInterface {
    id: string;
    __id: string;
    parent: string;
    name: string;
    sort: number;
}
