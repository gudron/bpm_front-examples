import { TreeNode} from 'primeng/primeng';

export interface ElmaTreeNode extends TreeNode {
    label: string;
    parent?: ElmaTreeNode;
    children: ElmaTreeNode[];
}
