import { ElmaTreeNode } from './elma-tree-node.interface';

export interface ElmaTreeNodeRenderedEventInterface {
    items: ElmaTreeNode[];
    activeNode: ElmaTreeNode;
}
