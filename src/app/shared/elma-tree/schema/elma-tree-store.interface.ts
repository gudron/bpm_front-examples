import { Response } from '@angular/http';
import { ElmaTreeCategoryInterface } from './elma-tree-category.interface';

export interface ElmaTreeStoreInterface {
    getByParentIds (id: string[]|string): Promise<Response> | any;
}
