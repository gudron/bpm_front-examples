import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Response } from '@angular/http';
import { type } from 'os';
import { ElmaTranslateService } from '../../../shared/common';
import { NULL_UUID } from '../../app.constants';
import { ElmaTreeCategoryInterface } from './schema/elma-tree-category.interface';
import { ElmaTreeNodeRenderedEventInterface } from './schema/elma-tree-node-rendered-event.interface';
import { ElmaTreeNode } from './schema/elma-tree-node.interface';
import { ElmaTreeStoreInterface } from './schema/elma-tree-store.interface';

@Component({
    selector: 'elma-tree',
    templateUrl: './elma-tree.component.html',
    styleUrls: ['./elma-tree.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ElmaTreeComponent implements OnInit {
    resolve: Promise<ElmaTreeNode[]>;

    public _items: ElmaTreeNode[];
    public currentActiveNode: ElmaTreeNode;

    public nullUUID = NULL_UUID;

    @Input() store: ElmaTreeStoreInterface;
    @Input() localStorageKey?: string;
    @Input() saveToLocalStorage? = false;
    @Input() activeId? = NULL_UUID;

    @Output() onSelectCategory = new EventEmitter<ElmaTreeNode>();
    @Output() onExpandCategory = new EventEmitter<ElmaTreeNode>();
    @Output() onTreeRendered = new EventEmitter<ElmaTreeNodeRenderedEventInterface>();

    constructor (
        protected translate: ElmaTranslateService,
        private cdr: ChangeDetectorRef,
    ) {
    }

    get items(): ElmaTreeNode[] {
        return this._items
    }

    ngOnInit() {
        let allIds: string[] = [NULL_UUID];

        if (this.localStorageKey != null) {
            allIds = allIds.concat(this.getExpanded());
        }

        this.loadByParentIds(allIds)
            .then(res => {
                this._items = <ElmaTreeNode[]>this.prepareTree( res.json() );
                this.cdr.markForCheck();

                this.onTreeRendered.emit(<ElmaTreeNodeRenderedEventInterface> {
                    items: this._items,
                    activeNode: this.currentActiveNode
                });
            });
    }

    protected loadByParentIds(ids: string[]): Promise<Response> {
        const result = this.store.getByParentIds(ids);

        return result;
    }

    private prepareTree(raw: ElmaTreeCategoryInterface[]): ElmaTreeNode[] {
        const nodes: { [key: string]: ElmaTreeNode} = {};

        // prepare map
        raw.forEach((value, index, array) => {
            const id = value['__id'];
            nodes[id] = this.prepareNodeToInsert(value)
        }, this);

        // Build crrrrs links
        this.buildTreeNodeMapWithCrossLinks(nodes);
        // soft childrens
        this.sortChildrens(nodes);

        return this.buildTree(nodes);
    }

    protected sortCallback(a: ElmaTreeNode, b: ElmaTreeNode) {
        if (a.label > b.label) {
            return -1;
        }

        if (a.label < b.label) {
            return 1;
        }

        return 0;
    }

    private sortChildrens(treeNodesMap: { [key: string]: ElmaTreeNode}) {
        Object.keys(treeNodesMap).map((nodeId, index) => {
            const currentNode = treeNodesMap[nodeId];
            const childrens = currentNode.children;

            if (childrens != null && childrens.length > 1) {
                childrens.sort(this.sortCallback);
            }

        }, this);
    }

    private buildTreeNodeMapWithCrossLinks(treeNodesMap: { [key: string]: ElmaTreeNode}) {
        Object.keys(treeNodesMap).map((nodeId, index) => {
            const currentNode = treeNodesMap[nodeId];
            const parentId = currentNode.data.parentId;

            if (parentId != null && parentId !== NULL_UUID) {

                const parrentNode = treeNodesMap[parentId];

                currentNode.parent = parrentNode;

                if (parrentNode.children != null ) {
                    parrentNode.expanded = true;
                    parrentNode.children.push(currentNode);
                }

                delete treeNodesMap[nodeId];
            }
        }, this);
    }

    private buildTree(treeNodesMap: { [key: string]: ElmaTreeNode}) {
        const tmpTree: ElmaTreeNode[] = [];

        Object.keys(treeNodesMap).map((nodeId, index) => {
            const currentNode = treeNodesMap[nodeId];
            if (currentNode.data.id === this.activeId) {
                this.currentActiveNode = currentNode;
            }

            if (currentNode.data.parentId === NULL_UUID) {
                tmpTree.push(currentNode);
            }
        }, this);

        tmpTree.sort(this.sortCallback);

        if (this.currentActiveNode == null) {
            this.currentActiveNode = tmpTree[0];
        }

        return tmpTree;
    }

    loadNode(event: ElmaTreeNode) {
        if (event) {

            if (!event.expanded) {
                event.expanded = true;

                if (event.children && event.children.length <= 0) {
                    this.loadByParentIds([event.data.id]).then(res => {
                        const items = <ElmaTreeCategoryInterface[]>res.json();

                        if (items.length > 0) {
                            items.forEach(function (value: ElmaTreeCategoryInterface) {
                                this.addChildrenToNode(event, value);
                            }, this);

                            if (this.saveToLocalStorage) {
                                this.addToExpanded(event);
                            }

                            this.onExpandCategory.emit(event);

                            this.swapActiveNode(event);

                            this.cdr.markForCheck();
                        }

                    });
                }

                if (event.children && event.children.length > 0) {
                    if (this.saveToLocalStorage) {
                        this.swapActiveNode(event);

                        this.addToExpanded(event);
                    }
                }

            } else {
                this.colapseNode(event);

                if (this.saveToLocalStorage) {
                    this.removeFromExpanded(event);
                }

                this.swapActiveNode(event);

                this.cdr.markForCheck();
            }
        }
    }

    selectNode(event: ElmaTreeNode) {
        this.loadNode(event);

        this.onSelectCategory.emit(event);

        this.cdr.markForCheck();
    }

    colapseNode(collapsedNode: ElmaTreeNode) {
        collapsedNode.expanded = false;

        if (collapsedNode.children.length > 0) {
            collapsedNode.children.forEach((value, index, array) => {
                this.colapseNode(value)
            }, this);
        }
    }

    swapActiveNode(activeNode: ElmaTreeNode) {
        this.currentActiveNode.data.isActive = false;

        activeNode.data.isActive = true;
        this.currentActiveNode = activeNode;
    }

    private prepareNodeToInsert(value: ElmaTreeCategoryInterface): ElmaTreeNode {
        return <ElmaTreeNode>{
            label: value.name,
            data: {
                id: value['__id'],
                isActive: this.activeId === value['__id'],
                parentId: value['parent']
            },
            expandedIcon: 'folder_type_opened',
            collapsedIcon: 'folder',
            children: []
        };
    }

    private addChildrenToNode(node: ElmaTreeNode, value: ElmaTreeCategoryInterface) {
        if (node.children) {
            node.children.push(this.prepareNodeToInsert(value));

            this.cdr.markForCheck();
        }
    }

    private getExpanded(): string[] {
        const result = JSON.parse( <string> localStorage.getItem(<string>this.localStorageKey));
        return result == null ? [] : result;
    }

    private addToExpanded(node: ElmaTreeNode) {
        const saved = this.getExpanded();
        saved.push(node.data.id);
        localStorage.setItem(<string>this.localStorageKey, JSON.stringify(saved));
    }

    private removeFromExpanded(node: ElmaTreeNode) {
        const saved = this.getExpanded();

        const index = saved.indexOf(node.data.id);
        if (index !== -1) {
            saved.splice(index, 1);
        }

        localStorage.setItem(<string>this.localStorageKey, JSON.stringify(saved));
    }

    findNodeById(currentNode: ElmaTreeNode, id: string): ElmaTreeNode | null {
        if (currentNode.data.id === id) {
            return currentNode;
        }

        for (const [index, node] of currentNode.children.entries()) {
            return this.findNodeById(node, id);
        }

        return null;
    }

    setActiveNodeById(id: string): ElmaTreeNode | null {
        this.activeId = id;
        let activeNode: ElmaTreeNode | null = null;

        for (const [index, node] of this._items.entries()) {
            const findedNode = this.findNodeById(node, id);
            if (findedNode != null) {
                activeNode = findedNode;
                break;
            }
        }

        if (activeNode) {
            this.swapActiveNode(activeNode);
            this.cdr.markForCheck();
        }

        return activeNode;
    }
}
