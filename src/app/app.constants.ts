import { isNullOrUndefined } from 'util';

export const NULL_UUID = '00000000-0000-0000-0000-000000000000';

export const ROOT_CATEGORY = NULL_UUID;

/**
 * Системные роли
 */
export enum Role {
    /** Автор */
    Creator = '532c74d7-e76b-5ede-a70c-42e1108bc84c',
}
export namespace Role {
    /**
     * Получение кода роли по значению
     */
    export function getCode(value: string): string | undefined {
        return Object.keys(Role).find(key => Role[<any>key] === value);
    }
}

export enum NavTypes {
    NAV_TYPE_MAIN = 'main',
    NAV_TYPE_MAIN_SUBMENU = 'submenu',
    NAV_TYPE_DISK = 'files',
    NAV_TYPE_MONITOR = 'monitor',
    NAV_TYPE_NAMESPACE = 'namespace',
    NAV_TYPE_ADMIN = 'admin',
    NAV_TYPE_FEED = 'feed',
    NAV_TYPE_PROJECTS = 'projects',
    NAV_TYPE_HTML = 'html',
}

export enum MenuWidgetType {
    Feed = 'feed',
    Projects = 'projects',
    Disk = 'disk',
    Namespace = 'namespace',
    Html = 'html',
}

export enum WidgetType {
    Empty = '-',
    Application = 'application',
    Appview = 'appview',
    Disk = 'disk',
    Feed = 'feed',
    Html = 'html',
    Tasks = 'tasks',
    Admin = 'admin',
    Custom = 'custom',
}

export enum AdminWidgetType {
    WidgetsList = 'widgets-list',
}

export enum Namespaces {
    Global = 'global',
    System = 'system',
    Virtual = 'virtual',
}

export enum SystemCollections {
    Groups = 'groups',
    Tasks = 'tasks',
    Users = 'users',
    Disk = 'disk',
}

export enum ApplicationType {
    Standard = 'STANDARD',
    Document = 'DOCUMENT',
}

export enum PageType {
    Namespace = 'NAMESPACE',
    Page = 'PAGE',
    Link = 'LINK',
    Separator = 'SEPARATOR',
    Application = 'APPLICATION',
}
/**
 * MaxSearchSize определяет максимальное количество результатов (from + size) поиска по умолчанию
 * (см. https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-from-size.html)
 */
export const MAX_SEARCH_SIZE = 10000;

/**
 * Количество элементов на странице по умолчанию
 */
export const DEFAULT_PAGE_SIZE = 10;

/**
 * Количество предложений контрола автозаполнения
 */
export const DEFAULT_AUTOCOMPLETE_PREVIEW_SIZE = 10;

export enum Currency {
    RUB = 'RUB',
    USD = 'USD',
    EUR = 'EUR',
    BYR = 'BYR',
    CNY = 'CNY',
    DKK = 'DKK',
    GBP = 'GBP',
    GEL = 'GEL',
    ILS = 'ILS',
    JPY = 'JPY',
    KZT = 'KZT',
    NOK = 'NOK',
    TRY = 'TRY',
    UAH = 'UAH',
}

export interface CurrencyProperties {
    [key: string]: {
        symbol: string;
        decimalSeparator: string;
        thousandSeparator: string;
        // true - перед числом, false - после числа
        startPositionCurrency: boolean;
        decimalCapacity: number;
    }
}

export const CURRENCY_PROPERTIES: CurrencyProperties =  {
    [Currency.RUB]: { symbol: '₽', decimalSeparator: ',', thousandSeparator: ' ', startPositionCurrency: false, decimalCapacity: 2 },
    [Currency.USD]: { symbol: '$', decimalSeparator: '.', thousandSeparator: ',', startPositionCurrency: true, decimalCapacity: 2 },
    [Currency.EUR]: { symbol: '€', decimalSeparator: '.', thousandSeparator: ',', startPositionCurrency: true, decimalCapacity: 2 },
    // todo для валют ниже проверить разделители https://git.elewise.com/elma365-frontend/main-app/issues/425
    [Currency.GBP]: { symbol: '£', decimalSeparator: '.', thousandSeparator: ',', startPositionCurrency: true, decimalCapacity: 2 },
    [Currency.JPY]: { symbol: '¥', decimalSeparator: '.', thousandSeparator: ',', startPositionCurrency: false, decimalCapacity: 2 },
    [Currency.CNY]: { symbol: '元', decimalSeparator: '.', thousandSeparator: ',', startPositionCurrency: true, decimalCapacity: 2 },
    [Currency.KZT]: { symbol: '₸', decimalSeparator: '.', thousandSeparator: ',', startPositionCurrency: false, decimalCapacity: 2 },
    [Currency.GEL]: { symbol: '₾', decimalSeparator: '.', thousandSeparator: ',', startPositionCurrency: true, decimalCapacity: 2 },
    [Currency.ILS]: { symbol: '₪', decimalSeparator: '.', thousandSeparator: ',', startPositionCurrency: true, decimalCapacity: 2 },
    [Currency.TRY]: { symbol: '₺', decimalSeparator: '.', thousandSeparator: ',', startPositionCurrency: true, decimalCapacity: 2 },
    [Currency.UAH]: { symbol: '₴', decimalSeparator: '.', thousandSeparator: ',', startPositionCurrency: true, decimalCapacity: 2 },
}

/**
 * Типы полей
 *
 * FIXME: Перенести в field.json
 */
export enum FieldType {
    String = 'STRING',
    Float = 'FLOAT',
    // INTEGER = 'INTEGER', не используется на фронте
    Boolean = 'BOOLEAN',
    Datetime = 'DATETIME',
    Duration = 'DURATION',
    Category = 'CATEGORY',
    Tag = 'TAG',
    Money = 'MONEY',
    File = 'FILE',
    Phone = 'PHONE',
    Email = 'EMAIL',
    Image = 'IMAGE',
    STATUS = 'STATUS',
    // VERSION = 'VERSION',
    // JSON = 'JSON',
    SystemUser = 'SYS_USER',
    // SYS_OSNODE = 'SYS_OSNODE',
    SystemCollection = 'SYS_COLLECTION',
    // REF_ITEM = 'REF_ITEM',
    Template = 'TEMPLATE',
    FullName = 'FULL_NAME',
    Link = 'LINK',
}

export namespace FieldType {
    /**
     * Преобразование к типу FieldType из строки
     */
    export function fromString(value: string): FieldType | undefined {
        const valueKey = Object.keys(FieldType).find(key => FieldType[<any>key] === value);
        if (isNullOrUndefined(valueKey)) {
            return undefined;
        }

        return <FieldType>FieldType[<any>valueKey!];
    }
}


export class PermissionError extends Error {
    constructor () {
        super('permission denied');

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, PermissionError.prototype);
    }
}

/**
 * ИД папки с аватарками пользователей
 */
export const USERS_DIR_ID = '93bc623f-bbf1-5a6a-8db1-9fbb7c8fab36';
