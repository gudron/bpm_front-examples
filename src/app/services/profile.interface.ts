export interface UserSettings {
    locale: string;
    timezone: string;
    name: string; // company name
    notifications: {
        policy: NotificationPolicy;
    }
    channels: {
        muted: string[];
    }
    chats: {
        muted: string[];
    }
}

export enum NotificationPolicy {
    All = 'all',
    Mentions = 'mentions',
    Nothing = 'nothing'
}
