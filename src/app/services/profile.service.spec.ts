import { Http, RequestOptions, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { APIService } from './api.service';
import { ProfileService } from './profile.service';

describe('ProfileService', () => {
    let apiService: APIService;
    let mockBackend: MockBackend;

    beforeEach(() => {
        mockBackend = new MockBackend();
        const http = new Http(mockBackend, new RequestOptions());
        apiService = new APIService(http);
    });

    it('should make request on construct', (done: DoneFn) => {
        spyOn(apiService, 'get').and.returnValue(Promise.resolve(
            new Response(new ResponseOptions({
                status: 200,
                body: {
                    userId: 'ac41be54-404b-4ed8-8941-0d1993df6280',
                    privileges: ['administration'],
                },
            })),
        ));
        const profileService = new ProfileService(apiService);
        profileService.resolve.then(() => {
            expect(apiService.get).toHaveBeenCalled();
            done();
        });
    });

    it('should detect login status', (done: DoneFn) => {
        spyOn(apiService, 'get').and.returnValue(Promise.resolve(
            new Response(new ResponseOptions({
                status: 200,
                body: {
                    userId: 'ac41be54-404b-4ed8-8941-0d1993df6280',
                    privileges: ['administration'],
                },
            })),
        ));
        const profileService = new ProfileService(apiService);
        profileService.resolve.then(() => {
            expect(profileService.loggedIn).toBeTruthy();
            expect(profileService.userId).toBe('ac41be54-404b-4ed8-8941-0d1993df6280');
            done();
        });
    });

    it('should detect unauthorized status', (done: DoneFn) => {
        spyOn(apiService, 'get').and.returnValue(Promise.resolve(
            new Response(new ResponseOptions({
                status: 401,
            })),
        ));
        const profileService = new ProfileService(apiService);
        profileService.resolve.then(() => {
            expect(profileService.loggedIn).toBeFalsy();
            expect(profileService.userId).toBeNull();
            done();
        });
    });

    it('should send login request', (done: DoneFn) => {
        spyOn(apiService, 'get').and.returnValue(Promise.resolve(
            new Response(new ResponseOptions({
                status: 401,
            })),
        ));
        spyOn(apiService, 'post').and.returnValue(Promise.resolve(
            new Response(new ResponseOptions({
                status: 200,
                body: {
                    userId: 'ac41be54-404b-4ed8-8941-0d1993df6280',
                    privileges: ['administration'],
                },
            })),
        ));
        const profileService = new ProfileService(apiService);
        profileService
            .login({ email: 'some@email.me', password: 'some password' })
            .then(() => {
                expect(apiService.post).toHaveBeenCalled();
                expect(profileService.loggedIn).toBeTruthy();
                expect(profileService.userId).toBe('ac41be54-404b-4ed8-8941-0d1993df6280');
                done();
            });
    });

    it('should pass logout to api service', (done: DoneFn) => {
        spyOn(apiService, 'get').and.returnValue(Promise.resolve(
            new Response(new ResponseOptions({
                status: 200,
                body: {
                    userId: 'ac41be54-404b-4ed8-8941-0d1993df6280',
                    privileges: ['administration'],
                },
            })),
        ));
        const profileService = new ProfileService(apiService);
        spyOn(apiService, 'unauthorize');
        profileService.logout();
        expect(apiService.unauthorize).toHaveBeenCalled();
        expect(profileService.loggedIn).toBeFalsy();
        done();
    });

    it('should parse privileges', (done: DoneFn) => {
        spyOn(apiService, 'get').and.returnValue(Promise.resolve(
            new Response(new ResponseOptions({
                status: 200,
                body: {
                    userId: 'ac41be54-404b-4ed8-8941-0d1993df6280',
                    privileges: ['administration'],
                },
            })),
        ));
        const profileService = new ProfileService(apiService);
        profileService.resolve.then(() => {
            expect(profileService.hasAdministrationPrivilege).toBeTruthy();
            done();
        });
    });

    it('should parse privileges', (done: DoneFn) => {
        spyOn(apiService, 'get').and.returnValue(Promise.resolve(
            new Response(new ResponseOptions({
                status: 200,
                body: {
                    userId: 'ac41be54-404b-4ed8-8941-0d1993df6280',
                    privileges: [],
                },
            })),
        ));
        const profileService = new ProfileService(apiService);
        profileService.resolve.then(() => {
            expect(profileService.hasAdministrationPrivilege).toBeFalsy();
            done();
        });
    });
});
