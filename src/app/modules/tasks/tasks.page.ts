import { ElmaTranslateService } from '../../../shared/common';
import { uuid4 } from '../../../shared/utils/uuid';
import { Namespaces, NavTypes, PageType, WidgetType } from '../../app.constants';
import { PageDataJson, PageJson, PageMenuJson, PageWidgetJson } from '../../models/page.json';

export const TasksSystemPageCode = 'tasks';

export function GetTasksSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.tasks@name'),
        namespace: Namespaces.Global,
        code: TasksSystemPageCode,
        type: PageType.Namespace,
        icon: 'system_task',
        sort: 0,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            columns: 1,
            menu: <PageMenuJson>{
                type: NavTypes.NAV_TYPE_NAMESPACE,
            },
            // todo распаковать логику
            redirectTo: 'income',
            widgets: [
                <PageWidgetJson>{
                    type: WidgetType.Tasks,
                    data: {},
                },
            ],
        },
    };
}

export function GetTasksIncomeSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.tasks.income@name'),
        namespace: TasksSystemPageCode,
        code: 'income',
        type: PageType.Page,
        icon: 'task_in',
        sort: 0,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            columns: 1,
            menu: <PageMenuJson>{
                type: NavTypes.NAV_TYPE_NAMESPACE,
            },
            // todo распаковать логику
            // redirectTo: 'income',
            widgets: [
                <PageWidgetJson>{
                    type: WidgetType.Tasks,
                    data: { filter: 'income' },
                },
            ],
        },
    };
}

export function GetTasksParticipateSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.tasks.participate@name'),
        namespace: TasksSystemPageCode,
        code: 'participate',
        type: PageType.Page,
        icon: 'task_inc',
        sort: 0,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            columns: 1,
            menu: <PageMenuJson>{
                type: NavTypes.NAV_TYPE_NAMESPACE,
            },
            // todo распаковать логику
            // redirectTo: 'income',
            widgets: [
                <PageWidgetJson>{
                    type: WidgetType.Tasks,
                    data: { filter: 'participate' },
                },
            ],
        },
    };
}

export function GetTasksOutcomeSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.tasks.outcome@name'),
        namespace: TasksSystemPageCode,
        code: 'outcome',
        type: PageType.Page,
        icon: 'task_out',
        sort: 0,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            columns: 1,
            menu: <PageMenuJson>{
                type: NavTypes.NAV_TYPE_NAMESPACE,
            },
            // todo распаковать логику
            // redirectTo: 'income',
            widgets: [
                <PageWidgetJson>{
                    type: WidgetType.Tasks,
                    data: { filter: 'outcome' },
                },
            ],
        },
    };
}

export function GetTasksPagesJson (translationService: ElmaTranslateService): PageJson[] {
    return [
        GetTasksSystemPageJson(translationService),
        GetTasksIncomeSystemPageJson(translationService),
        GetTasksParticipateSystemPageJson(translationService),
        GetTasksOutcomeSystemPageJson(translationService),
    ];
}
