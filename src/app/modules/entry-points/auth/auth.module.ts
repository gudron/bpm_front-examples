import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ElmaCommonModule } from '../../../../shared/common';
import { ElmaTranslateResolver } from '../../../../shared/common/translate/translate.resolver.service';
import { IsGuestUserGuard } from '../../../services/user-guards';
import { AuthComponent } from './auth.component';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ElmaCommonModule,

        RouterModule.forRoot([
            {
                path: 'login',
                component: AuthComponent,
                canActivate: [IsGuestUserGuard],
                data: {
                    fullScreen: true,
                },
                resolve: {
                    translate: ElmaTranslateResolver,
                },
            }
        ]),
    ],
    declarations: [AuthComponent],
    exports: [AuthComponent],
    entryComponents: [AuthComponent],
})
export class AuthModule {
}
