import { AfterViewInit, OnInit, Component, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { APIUrlBuilder } from '../../../services/api.service';
import { Credentials, ProfileService } from '../../../services/profile.service';
import { VahterService } from '../../../services/vahter.service';
import { ForbiddenError } from '../../../shared/errors/forbidden-error';
import { UnauthorizedError } from '../../../shared/errors/unauthorized-error';
import { ValidationError } from '../../../shared/errors/validation-error';
import { RouteQueryParams } from 'angular-route-xxl';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, AfterViewInit {

    @ViewChild('email') emailControl: ElementRef;
    @ViewChild('password') passwordControl: ElementRef;

    @RouteQueryParams('email', { observable: false }) predefinedEmail: string;

    public creds = <Credentials>{
        email: 'admin@mail.com',
        password: 'test',
    };

    company = location.hostname;
    isSubmitPending: boolean;
    isNotAuthorized = false;
    isBlocked = false;
    vahterHost: string;

    encode = APIUrlBuilder.encode;

    private isPredefinedEmail: boolean;

    constructor(
        private profileService: ProfileService,
        private vahterService: VahterService,
        private activatedRoute: ActivatedRoute,
        private route: ActivatedRoute,
        protected router: Router,
    ) {
        this.vahterHost = this.vahterService.host;
    }

    ngOnInit (): void {
        this.isPredefinedEmail = !!this.predefinedEmail && this.predefinedEmail.length > 0;
        if (this.isPredefinedEmail) {
            this.creds = <Credentials>{
                email: this.predefinedEmail,
                password: '',
            }
        }
    }

    ngAfterViewInit (): void {
        if (this.isPredefinedEmail) {
            this.passwordControl.nativeElement.focus();
        } else {
            this.setDefaultFocus();
        }
    }

    async onSubmit() {
        this.isSubmitPending = true;
        this.isNotAuthorized = false;
        this.isBlocked = false;

        try {
            await this.profileService.login(this.creds);
            if (this.activatedRoute.snapshot.queryParams && this.activatedRoute.snapshot.queryParams.returnUrl) {
                this.router.navigateByUrl(decodeURIComponent(this.activatedRoute.snapshot.queryParams.returnUrl));
            } else {
                this.router.navigate(['/']);
            }
        } catch (err) {
            this.isSubmitPending = false;

            if (err instanceof ValidationError || err instanceof UnauthorizedError || err instanceof ForbiddenError) {
                const isBlocked = err.message.includes(UnauthorizedError.BlockedMessage);
                this.isNotAuthorized = !isBlocked;
                this.isBlocked = isBlocked;
                this.setDefaultFocus();
                return
            }

            throw err;
        }
    }

    private setDefaultFocus() {
        setTimeout(() => {
            this.emailControl.nativeElement.focus();
        });
    }
}
