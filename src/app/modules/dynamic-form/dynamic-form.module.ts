import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { MobxAngularModule } from 'mobx-angular';
import { ElmaCommonModule } from 'app/../shared/common';
import { ItemSelectModule } from '../../application/modules/item-select/item-select.module';
import { AppSharedModule } from '../../shared/shared.module';
import { UserSelectModule } from '../../shared/user-select/user-select.module';
import { DocumentFormComponent } from './document-form/document.component';
import { DocumentFormUploaderComponent } from './document-form/uploader/uploader.component';
import { DynamicFormComponent } from './dynamic-form.component';
import { EditFormComponent } from './edit-form/edit-form.component';
import { EditFormUserGuideComponent } from './edit-form/userGuide/userGuide.component';
import { RowComponent } from './row/row.component';
import { SimpleFormComponent } from './simple-form/simple-form.component';
import { BooleanComponent } from './types/boolean/boolean.component';
import { DatetimeComponent } from './types/datetime/datetime.component';
import { EmailComponent } from './types/email/email.component';
import { FullNameComponent } from './types/full-name/full-name.component';
import { ImageComponent } from './types/image/image.component';
import { LinkComponent } from './types/link/link.component';
import { NumberComponent } from './types/number/number.component';
import { PhoneComponent } from './types/phone/phone.component';
import { StringComponent } from './types/string/string.component';
import { PipesModule } from '../../pipes/pipes.module';
import { AppviewListModule } from '../../application/modules/appview/list/list.module';
import { CategoryComponent } from './types/category/category.component';
import { NgUploaderModule } from 'ngx-uploader';
import { MoneyComponent } from './types/money/money.component';
import { FileComponent } from './types/file/file.component';
import { SystemCollectionComponent } from './types/system-collection/system-collection.component';
import { DynamicUserSelectComponent } from './types/user-select/user-select.component';

@NgModule({
    imports: [
        CommonModule,
        ElmaCommonModule,
        FormsModule,
        ReactiveFormsModule,
        MobxAngularModule,

        TextMaskModule,

        PipesModule,
        ItemSelectModule,
        UserSelectModule,

        AppviewListModule,
        NgUploaderModule,
        AppSharedModule,
    ],
    declarations: [
        DynamicFormComponent,
        RowComponent,

        StringComponent,
        NumberComponent,
        BooleanComponent,
        EmailComponent,
        PhoneComponent,
        DatetimeComponent,
        MoneyComponent,
        LinkComponent,

        SystemCollectionComponent,
        DynamicUserSelectComponent,
        CategoryComponent,
        FileComponent,
        FullNameComponent,
        ImageComponent,
        EditFormComponent,
        EditFormUserGuideComponent,
        DocumentFormComponent,
        DocumentFormUploaderComponent,
        SimpleFormComponent,
    ],
    exports: [
        DynamicFormComponent,
        StringComponent,
        NumberComponent,
        BooleanComponent,
        DatetimeComponent,
        PhoneComponent,
        SystemCollectionComponent,
        DynamicUserSelectComponent,
        CategoryComponent,
        FileComponent,
        FullNameComponent,
        ImageComponent,
        LinkComponent,
        EditFormComponent,
        DocumentFormComponent,
        SimpleFormComponent,
    ],
    entryComponents: [
        DynamicFormComponent,
    ],
})
export class DynamicFormModule {
}
