import { Component, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormWidget } from '../../../models/form-widget';
import { EditItemInterface } from '../../../application/modules/appview/edit/edit.component';
import { FormField } from '../../../application/schema/form-field';
import { DynamicFormComponent, FormError } from '../dynamic-form.component';

@Component({
    selector: 'app-edit-form',
    templateUrl: 'edit-form.component.html',
})

export class EditFormComponent implements OnInit, EditItemInterface {

    @Input() title: string;
    @Input() okButtonTitle: string;
    @Input() okButtonDisabled = false;

    @Input() formFields: FormField[];
    @Input() namespace: string;

    @Input() instruction: string;
    @Input() isInstructionEditable: boolean;

    @Output() onSubmit = new EventEmitter();
    @Output() onClose = new EventEmitter();

    @Output() onUpdateInstruction = new EventEmitter<string>();

    @ViewChild('form') form: DynamicFormComponent;

    get value (): { [key: string]: any } {
        return this.form.value;
    }

    get showSidebar (): boolean {
        return !!this.instruction || this.isInstructionEditable;
    }

    constructor () {
    }

    ngOnInit () {
    }

    save () {
        this.onSubmit.emit();
    }

    close () {
        this.onClose.emit();
    }

    async init (item?: { [key: string]: any }) {
        if (item) {
            this.form.init(item);
        }
    }


    updateInstruction (instruction: string) {
        this.onUpdateInstruction.emit(instruction);
    }

    @HostListener('document:keydown.escape', ['$event'])
    handleKeyboardEvent () {
        this.close();
    }

    setErrors (errors: FormError[]) {
        this.form.setErrors(errors);
    }

    get widgets (): FormWidget[] {
        return [];
    }
}
