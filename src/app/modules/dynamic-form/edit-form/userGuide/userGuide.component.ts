import { Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output } from '@angular/core';

declare var SystemJS: any;
declare var jQuery: any;

@Component({
    selector: 'app-edit-form-userguide',
    templateUrl: './userGuide.component.html',
})
export class EditFormUserGuideComponent implements OnDestroy {
    @Input() userGuide: string;
    @Input() isEditable = true;
    @Input() isEditMode = false;

    @Output() onClose = new EventEmitter<void>();
    @Output() onUpdate = new EventEmitter<string>();

    tinymce: any;

    constructor (
        private elementRef: ElementRef,
    ) {
    }

    setEditMode (mode: boolean) {
        this.isEditMode = !!mode;
        if (this.isEditMode) {
            this.initTinyMce();
        }
    }

    async initTinyMce () {
        if (this.tinymce) {
            this.tinymce.destroy();
        }
        const src = `${window.location.origin}/assets/tinymce/tinymce.min.js`;
        await SystemJS.import(src);
        const script: any = SystemJS.registry.get(src);
        const tinymce: any = script.default;
        tinymce.init({
            selector: '#editUserGuide',
            branding: false,
            menubar: false,
            toolbar: false,
            statusbar: false,
            external_plugins: {
                'autolink': '/assets/tinymce/plugins/autolink/plugin.min.js',
            },
            theme_url: '/assets/tinymce/themes/modern/theme.min.js',
            skin_url: '/assets/tinymce/skins/lightgray',
            init_instance_callback: (editor: any) => {
                this.tinymce = editor;
                tinymce.execCommand('mceFocus', false, 'editUserGuide');
                const el = this.elementRef.nativeElement;
                const $iframe = jQuery(el.querySelector('iframe'));
                $iframe.addClass('focus');
                const editorBody = editor.getDoc().body;
                editorBody.focus(() => $iframe.addClass('focus'));
                editorBody.blur(() => $iframe.removeClass('focus'));
            },
        });
    }

    ngOnDestroy () {
        if (this.tinymce) {
            this.tinymce.destroy();
        }
    }

    update (): void {
        this.userGuide = this.tinymce.getContent();
        this.onUpdate.emit(this.userGuide);
    }

    close (): void {
        this.onClose.emit();
        this.ngOnDestroy();
    }
}
