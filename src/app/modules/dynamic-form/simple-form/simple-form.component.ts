import { Component, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormField } from '../../../application/schema/form-field';
import { DynamicFormComponent } from '../dynamic-form.component';

@Component({
    selector: 'app-simple-form',
    templateUrl: 'simple-form.component.html',
})

export class SimpleFormComponent implements OnInit {

    @Input() okButtonTitle: string;
    @Input() okButtonDisabled = false;

    @Input() formFields: FormField[];
    @Input() namespace: string;

    @Output() onSubmit = new EventEmitter();
    @Output() onClose = new EventEmitter();

    @ViewChild('form') form: DynamicFormComponent;

    get value (): { [key: string]: any } {
        return this.form.value;
    }

    constructor () {
    }

    ngOnInit () {
    }

    save () {
        this.onSubmit.emit();
    }

    close () {
        this.onClose.emit();
    }

    init (item?: { [key: string]: any }) {
        if (item) {
            this.form.init(item);
        }
    }

    @HostListener('document:keydown.escape', ['$event'])
    handleKeyboardEvent () {
        this.close();
    }
}
