import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { humanizeBytes, UploadFile, UploadInput, UploadOutput } from 'ngx-uploader';
import { FileJson } from '../../../../models/disk.json';
import { FileModel, PostlinkModel } from '../../../../models/disk.model';
import { DiskStore } from '../../../../stores/disk.store';

@Component({
    selector: 'app-document-form-uploader',
    templateUrl: 'uploader.component.html',
    styleUrls: ['uploader.component.scss'],
})
export class DocumentFormUploaderComponent implements OnInit {
    @Input() dirId: string;
    @Input() file: FileModel;
    @Output() fileUploaded = new EventEmitter<FileModel>();
    files: UploadFile[];
    uploadInput: EventEmitter<UploadInput>;
    humanizeBytes: Function;
    dragOver: boolean;
    existFiles: FileModel[];
    isUploading = false;
    uploadedFile: UploadFile;

    constructor (
        private cdr: ChangeDetectorRef,
        private _diskStore: DiskStore,
    ) {
        this.files = []; // local uploading files array
        this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
        this.humanizeBytes = humanizeBytes;

    }

    async ngOnInit () {
        this.existFiles = await this._diskStore.loadFilesInDir(this.dirId);
    }

    onUploadOutput (output: UploadOutput): void {
        if (output.type === 'allAddedToQueue') {
            this.isUploading = true;
            this.saveFiles();
        } else if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') { // add file to array when added
            output.file.name = this.deduplicateName(output.file.name);
            this.uploadedFile = output.file;
            this.files.push(output.file);
        } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
            // update current data in files array for uploading file
            const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
            this.files[index] = output.file;
        } else if (output.type === 'removed') {
            // remove file from array when removed
            this.files = this.files.filter((file: UploadFile) => file !== output.file);
        } else if (output.type === 'dragOver') {
            this.dragOver = true;
        } else if (output.type === 'dragOut') {
            this.dragOver = false;
        } else if (output.type === 'drop') {
            this.dragOver = false;
        } else if (output.type === 'done') {
            this.createFile(output);
            this.files = [];
        }
    }

    deduplicateName (fileName: string): string {
        let num = 1;
        let resultName = fileName;
        while (this.existFiles.find(file => file.name === resultName)) {
            const nameParts = fileName.split('.');
            if (nameParts.length === 1) {
                resultName = fileName + '(' + num.toString() + ')';

            } else {
                nameParts[nameParts.length - 2] = nameParts[nameParts.length - 2] + '(' + num.toString() + ')';
                resultName = nameParts.join('.');
            }
            num++;
        }
        return resultName;
    }

    private async saveFiles () {
        for (const ufile of this.files) {
            try {
                const postlink: any = await this._diskStore.getPostlink(<number>ufile.size);
                this.uploadFile(ufile, postlink);
            } catch (e) {
                console.error('file save error :: ', e);
            }
        }

    }

    private async uploadFile (file: UploadFile, postlink: PostlinkModel) {
        const mergedFileObject = Object.assign({}, file, postlink.formdata);

        // лайфхак, нам дальше нужен хэш, а положить его некуда, потому сунем в id файла, там всеравно нужно уникальное значение
        file.id = postlink.hash;
        const uploadInput: UploadInput = {
            type: 'uploadFile',
            url: postlink.link,
            id: file.id,
            method: 'POST',
            fileIndex: file.fileIndex,
            file: file,
            data: mergedFileObject,
            concurrency: 0,
        };
        this.uploadInput.emit(uploadInput);
    }

    private async createFile (output: UploadOutput) {
        if (!output.file) {
            throw new Error('File not found');
        }
        const nameAr = output.file.name.split('.');
        nameAr[0] = output.file.id;
        const name = nameAr.join('.');
        const filejson: FileJson = {
            name: name,
            originalName: output.file.name,
            size: output.file.size,
            hash: output.file.id,
            directory: this.dirId,
        };
        try {
            this.file = await this._diskStore.createFile(filejson);
            this.cdr.markForCheck();
        } catch (e) {
            console.error('create file in collection error: ', e);
        }
        this.existFiles.push(this.file);
        this.fileUploaded.emit(this.file);
        this.isUploading = false;
    }
}
