import { Component, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FileModel } from '../../../models/disk.model';
import { DiskStore } from '../../../stores/disk.store';
import { DynamicFormComponent } from '../../dynamic-form/dynamic-form.component';
import { FormField } from '../../../application/schema/form-field';

@Component({
    selector: 'app-document-form',
    templateUrl: 'document.component.html',
    styleUrls: ['document.component.scss'],
})
export class DocumentFormComponent implements OnInit {
    @Input() title: string;
    @Input() okButtonTitle: string;

    @Input() formFields: FormField[];
    @Input() namespace: string;

    @Input() instruction: string;
    @Input() isInstructionEditable: boolean;

    @Input() pathId: string;
    @Input() fileFieldCode: string;

    @Output() onSubmit = new EventEmitter();
    @Output() onClose = new EventEmitter();

    @Output() onUpdateInstruction = new EventEmitter<string>();

    @Output() onFileUploaded = new EventEmitter<FileModel>();
    @Output() onDeleteFile = new EventEmitter();

    @ViewChild('form') form: DynamicFormComponent;

    file: FileModel | null;

    get value (): { [key: string]: any } {
        return this.form.value;
    }

    constructor (
        private _diskStore: DiskStore,
    ) {
    }

    ngOnInit () {
    }

    fileUploaded (file: FileModel) {
        this.file = file;
        this.onFileUploaded.emit(file);
    }

    save () {
        this.onSubmit.emit();
    }

    close () {
        this.onClose.emit();
    }


    async init (item?: { [key: string]: any }) {
        if (item && item[this.fileFieldCode]) {
            this.file = await this._diskStore.getFile(item[this.fileFieldCode]);
        }
        this.form.init(item);
    }

    updateInstruction (instruction: string) {
        this.onUpdateInstruction.emit(instruction);
    }

    @HostListener('document:keydown.escape', ['$event'])
    handleKeyboardEvent () {
        this.close();
    }

    deleteFile () {
        this.file = null;
        this.onDeleteFile.emit();
    }
}
