import { ValidationErrors } from '@angular/forms/src/directives/validators';
import { AbstractControl } from '@angular/forms/src/model';
import { CustomError } from '../../../shared/common/forms/error/custom-error';

export class CustomValidator {

    errors: CustomError[] = [];

    validate (c: AbstractControl): ValidationErrors | null {
        // Ошибки надо отобразить 1 раз, поэтому удаляем все ошибки с другими занчениями.
        this.errors = this.errors.filter(e => e.value === c.value);
        if (this.errors.length > 0) {
            const validationErrors = <ValidationErrors>{};
            for (let i = 0; i < this.errors.length; i++) {
                validationErrors[`custom_error_${i}`] = this.errors[i];
            }
            return validationErrors;
        }
        return null;
    }

    setError (message: string, value: any) {
        const err = new CustomError(message, value);
        this.errors.push(err);
    }
}

