/**
 * Интерфейс определяет метод для установки фокуса
 */
export interface Focuser {
    setFocus(): void;
}
