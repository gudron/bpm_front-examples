import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ElmaNotificationService } from '../../../shared/common';
import { ComponentWidget } from '../../../widget/metadata';
import { FormFieldModel } from '../../application/models/form-field.model';
import { AppWidgetFieldTypes } from '../../widgets/field-types';
import { CustomValidator } from './custom-validator';
import { UndefinedFieldError } from './exceptions/undefined-field-error';
import { RowComponent } from './row/row.component';

export interface Field {
    type: string;
    code: string;
    required: boolean;
    readonly: boolean;
    hidden: boolean;
    single: boolean;
    defaultValue: any;
    name: string;
    tooltip: string;
    data: any; // view.data
    fieldData?: any | undefined; // data
}

export interface FormError {
    code: string;
    message: string;
}

/**
 * @Warning Этот класс для статического анализа. Нигде не использовать. Проверка того, что FormFieldModel реализует интерфейс Field
 */
class DynFormField extends FormFieldModel implements Field {
}

@Component({
    selector: 'app-dynamic-form',
    templateUrl: './dynamic-form.component.html',
    styleUrls: ['./dynamic-form.component.scss'],
})
@ComponentWidget({
    code: 'dynamic-form',
    name: 'Item form',
    fields: [
        { code: 'item', type: AppWidgetFieldTypes.Object },
        { code: 'fields', type: AppWidgetFieldTypes.ObjectField, array: true },
    ],
})
export class DynamicFormComponent implements OnInit, AfterViewInit {
    @Input() item?: { [key: string]: any };
    @Input() fields: Field[];
    @Input() namespace: string;

    @Output() onSubmit = new EventEmitter<void>();

    @ViewChildren(RowComponent) rows: QueryList<RowComponent>;

    form: FormGroup;

    constructor (
        private cdr: ChangeDetectorRef,
        private notificator: ElmaNotificationService,
    ) {
    }

    get valid (): boolean {

        return this.form.valid;
    }

    get value (): { [key: string]: any } {

        return this.form.value;
    }

    private get defaultValues (): { [key: string]: any } {
        const item: { [key: string]: any } = {};
        if (this.fields) {
            for (const field of this.fields) {
                item[field.code] = field.defaultValue;
            }
        }

        return item;
    }

    ngOnInit () {
        this.initForm();
        if (this.item != null) {
            this.init(this.item);
        }
    }

    init (item?: { [key: string]: any }) {
        this.updateFormValuesValue(item);

        this.form.markAsPristine();

        // Установка фокуса на первый контрол формы
        if (this.rows && this.rows.length > 0) {
            this.rows.first.setFocus();
        }
    }

    ngAfterViewInit () {
        // Установка фокуса на первый контрол формы
        if (this.rows.length > 0) {
            this.rows.first.setFocus();
        }
    }

    markAllAsDirty () {
        for (const key of Object.keys(this.form.controls)) {
            this.form.controls[key].markAsDirty();
        }
    }

    submit () {
        if (!this.form.valid) {
            this.markAllAsDirty();

            // Установка фокуса на первый элемент с валидатором
            const invalidRow = this.rows.find(row => !row.formControl.valid);
            if (invalidRow) {
                invalidRow.setFocus();
            }

            return;
        }

        this.onSubmit.emit();
    }

    private initForm () {
        const controls: { [key: string]: FormControl } = {};
        if (this.fields) {
            for (const field of this.fields) {
                controls[field.code] = new FormControl(
                    field.defaultValue,
                    this.getFieldGeneralValidators(field));
            }
        }
        this.form = new FormGroup(controls);
    }

    private updateFormValuesValue (item?: { [key: string]: any }) {
        if (item == null) {
            item = this.defaultValues;
        }
        this.form.patchValue(item);
        this.cdr.markForCheck();
    }

    private customValidators: { [key: string]: CustomValidator } = {};

    private getFieldGeneralValidators (field: Field): ValidatorFn[] {
        const validators: ValidatorFn[] = [];
        this.customValidators[field.code] = new CustomValidator();
        validators.push(this.getCustomValidatorFn(field.code));
        if (field.required) {
            validators.push(Validators.required);
        }

        return validators;
    }

    getCustomValidatorFn (code: string): ValidatorFn {
        if (!this.customValidators[code]) {
            this.customValidators[code] = new CustomValidator();
        }
        return this.customValidators[code].validate.bind(this.customValidators[code]);
    }

    setErrors (errors: FormError[]) {
        for (const error of errors) {
            const control = this.form.controls[error.code];
            if (control) {
                const value = control.value;
                this.customValidators[error.code].setError(error.message, value);
                control.setValue(value);
            } else {
                this.notificator.error(new UndefinedFieldError(error.message));
            }

        }
    }
}
