import { Injectable } from '@angular/core';
import { FieldType } from '../../app.constants';
import { FieldJson, SystemCollectionDataJson } from '../../models/field.json';
import { ApplicationStore } from '../../application/services/application.store';
import { CollectionStoreFactory } from '../../application/services/collection.store.factory';
import { ItemModel } from '../../application/models/item.model';
import { Field } from './dynamic-form.component';

/**
 * Заполнение вложенных полей формы на основе связей.
 * Пример:
 *
 * В контексте есть поле
 * collection: SystemCollection
 *
 * В форме есть вложенное поле
 * collection.field: any
 *
 * В контексте сохранен только UUID элемента коллекции, по нему нужно значение для поля.
 * Это сервис разбирает поля формы и по полям контекста и значениям в item формирует значения для полей формы
 *
 */

export class FormValueFiller {
    private values: { [key: string]: any };

    constructor (
        private appStore: ApplicationStore,
        private collectionStoreFactory: CollectionStoreFactory,
        private context: FieldJson[],
        private formFields: Field[],
        private item: { [key: string]: any },
    ) {

    }

    async getValues (): Promise<{ [key: string]: any }> {
        this.values = {};
        if (!this.item) {
            return this.values;
        }
        for (const field of this.formFields) {
            const value = await this.getValue(field);
            if (value !== undefined) {
                this.values[field.code] = value;
            }
        }
        return this.values;
    }

    async getValue (field: Field): Promise<any> {
        const path = field.code.split('.');
        const rest: string[] = [];
        let value: any = undefined;
        while (true) {
            if (this.item.hasOwnProperty(path.join('.'))) {
                value = this.extractValue(this.item[path.join('.')], path, rest);

                break;
            }
            const nextLevel = path.pop();
            if (!nextLevel) {

                break;
            }
            rest.unshift(nextLevel);
        }
        return value;
    }

    async extractValue (value: any, path: string[], rest: string[]): Promise<any> {
        const pathField = await this.untanglePath(path, this.context);
        return await this.untangleRest(rest, path, pathField, value);
    }

    async untanglePath (path: string[], fields: FieldJson[]): Promise<FieldJson> {
        path = path.slice();
        const code = path.shift();
        const field = fields.find(f => f.code === code);
        if (!field) {

            throw new Error('untagle path: field not found');
        }
        if (path.length === 0) {

            return field;
        } else {
            if (field.type !== FieldType.SystemCollection) {

                throw new Error('untagle path: field isn`t a system collection');
            }
            field.data = <SystemCollectionDataJson>field.data;
            const app = await this.appStore.getModelByNamespaceAndCode(field.data.namespace, field.data.code);

            return await this.untanglePath(path, app.fields);
        }

    }

    async untangleRest (rest: string[], path: string[], field: FieldJson, value: any): Promise<any> {
        rest = rest.slice();
        path = path.slice();
        if (rest.length === 0) {

            return value;
        }
        if (field.type !== FieldType.SystemCollection) {

            throw new Error('untagle rest: field isn`t a system collection');
        }
        field.data = <SystemCollectionDataJson>field.data;
        const app = await this.appStore.getModelByNamespaceAndCode(field.data.namespace, field.data.code);
        const code = rest.shift() || '';
        const nextField = app.fields.find(f => f.code === code);
        if (!nextField) {
            return undefined;
        }
        const itemJson = await this.collectionStoreFactory.get(app.collection).getItem(value);

        if (!itemJson) {

            throw new Error('untagle rest: item not found');
        }
        this.values[path.join('.') + '.__id'] = value;
        const item = new ItemModel(app.collection, itemJson);
        path.push(code);
        return await this.untangleRest(rest, path, nextField, item.getByKey(code));
    }

}

@Injectable()
export class FillerFactory {
    constructor (
        private appStore: ApplicationStore,
        private collectionStoreFactory: CollectionStoreFactory,
    ) {
    }

    getFiller (context: FieldJson[], formFields: Field[], item: { [key: string]: any }): FormValueFiller {
        return new FormValueFiller(this.appStore, this.collectionStoreFactory, context, formFields, item);
    }
}
