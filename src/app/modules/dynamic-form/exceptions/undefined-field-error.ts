export class UndefinedFieldError extends Error {
    constructor (message: string) {
        super(message);
        Object.setPrototypeOf(this, UndefinedFieldError.prototype);
    }
}
