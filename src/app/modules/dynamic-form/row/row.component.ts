import { Component, Input, forwardRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { FieldType, Namespaces } from 'app/app.constants';
import { Field } from '../dynamic-form.component';

import { Focuser } from '../focuser.interfaces';
import { UsersCollectionCode } from '../../../modules/admin/user/user.application';

@Component({
    selector: 'app-dynamic-form-row',
    templateUrl: './row.component.html',
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => RowComponent), multi: true },
    ]
})
export class RowComponent implements ControlValueAccessor {

    FieldType = FieldType;

    @Input() namespace: string;
    @Input() field: Field | Field;
    @Input() formControl: FormControl;
    @Input() formGroup: FormGroup;
    @Input() isEditMode: boolean;

    @ViewChild('control') control: Focuser;

    public UsersCollectionCode = UsersCollectionCode;
    public namespaceSystem = Namespaces.System;

    setFocus() {
        if (this.control) {
            this.control.setFocus();
        }
    }

    /*
     * ControlValueAccessor implementation
     */

    writeValue() { }
    registerOnChange() { }
    registerOnTouched() { }
}
