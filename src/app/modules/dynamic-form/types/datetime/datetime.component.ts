import * as moment from 'moment';
import { ChangeDetectorRef, Component, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DatetimeTypes } from 'app/models/field.json';
import { Field } from '../../dynamic-form.component';
import { Focuser } from '../../focuser.interfaces';
import { ElmaDatepickerComponent } from '../../../../../shared/common';

@Component({
    selector: 'app-dynamic-form-types-datetime',
    templateUrl: './datetime.component.html',
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DatetimeComponent), multi: true },
    ],
})
export class DatetimeComponent implements OnInit, ControlValueAccessor, Focuser {

    DatetimeTypes = DatetimeTypes;

    @Input() field: Field | Field;
    @Input() currentFormControl: AbstractControl;
    @Input() isEditMode: boolean;
    @Input() placeholder: string;

    @ViewChild('input') input: ElmaDatepickerComponent;

    private _value: Date | null;
    get value() {
        return this._value;
    }
    @Input()
    set value(val) {
        this.writeValue(val);
    }

    type: DatetimeTypes;

    constructor(private cdr: ChangeDetectorRef) {
    }

    onModelChange: any = () => { };
    onModelTouched: any = () => { };

    ngOnInit() {
        this.type = <DatetimeTypes>this.field.data.additionalType;
    }

    /*
     * FocusChanger implementation
     */

    setFocus() {
        this.input.setFocus();
        this.cdr.markForCheck();
    }

    /*
     * ControlValueAccessor implementation
     */

    writeValue(val: Date | null) {
        if (!val) {
            // Установка текущей даты и времени, если задана эта настройка
            this._value = this.field.data.setCurrentDatetime && !this.isEditMode ? new Date() : null;
        } else if (val) {
            if (this.type === DatetimeTypes.Time) {
                // Вычтем сдвиг текущей временной зоны из даты, для корректного отображения времени
                const originTimeMoment =  moment(val, moment.ISO_8601);
                const currentTimezoneOffset = (new Date()).getTimezoneOffset();
                const normalizedTime = originTimeMoment.add(currentTimezoneOffset, 'minutes');
                this._value = normalizedTime.toDate();
            } else {
                this._value = new Date(val);
            }
        }

        if (this.input) {
            this.input.writeValue(this._value);
        }
    }

    updateModel(val: Date | null) {
        // Не меняем значение, если оно уже пустое
        if (!this._value && !val) {
            return
        }

        this._value = val;

        if (val == null) {
            if (this.onModelChange) {
                this.onModelChange(null);
            }
            return;
        }

        if (this.type !== DatetimeTypes.Time) {
            if (this.onModelChange) {
                this.onModelChange(val.toISOString());
            }
            return;
        }

        /**
         * Для time типа будем хранить время в UTC дате 1970 года
         */
        const normalizedTime = new Date(Date.UTC(1970, 0, 1, val.getHours(), val.getMinutes()));
        if (this.onModelChange) {
            this.onModelChange(normalizedTime.toISOString());
        }
    }

    registerOnChange(fn: Function) {
        this.onModelChange = fn;
    }

    registerOnTouched(fn: Function) {
        this.onModelTouched = fn;
    }
}
