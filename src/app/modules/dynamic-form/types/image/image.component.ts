import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { AbstractControl, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ImageCroppingOptions, UploadedFileList } from '../../../../shared/elma-file-uploader/elma-file-uploader.interface';
import { DiskStore } from '../../../../stores/disk.store';
import { Field } from '../../dynamic-form.component';

@Component({
    selector: 'app-dynamic-form-types-image',
    templateUrl: './image.component.html',
    styles: [],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ImageComponent), multi: true },
    ],
})

export class ImageComponent implements OnInit, ControlValueAccessor {

    @Input() namespace: string;
    @Input() field: Field;
    @Input() currentFormControl: AbstractControl;
    public imageCroppingOption: ImageCroppingOptions;

    private _uploadedFileList: UploadedFileList[] = [];
    public get uploadedFileList (): UploadedFileList[] {
        return this._uploadedFileList;
    }
    public set uploadedFileList (uploadedFileList: UploadedFileList[]) {
        this._uploadedFileList = uploadedFileList;

        const uploadedFileIDList: string[] = [];
        for (const item of uploadedFileList) {
            if (typeof item.id === 'string') {
                uploadedFileIDList.push(item.id);
            }
        }

        this.value = uploadedFileIDList;
    }

    private _value: string[] = [];
    get value(): string[] {
        return this._value;
    }
    set value(val: string[]) {

        this._value = val;
        if (this.propagateChange != null) {
            this.propagateChange(this._value)
        }
    }

    propagateChange: any = () => {
    };

    constructor (private diskStore: DiskStore) {
    }

    ngOnInit () {
        this.imageCroppingOption = {
            selectFragment: this.field.data.selectFragment,
            fragmentType: this.field.data.fragmentType,
        };
    }

    async writeValue (value: string[]) {
        this.value = value ? value : [];
        await this.setUploadedFileList();
    }

    registerOnChange (fn: Function) {
        this.propagateChange = fn;
    }

    registerOnTouched () {
    }

    public async setUploadedFileList (): Promise<void> {
        this._uploadedFileList = <UploadedFileList[]>await this.diskStore.getFilesByIds(this.value);
    }
}
