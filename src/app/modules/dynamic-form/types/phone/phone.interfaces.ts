export interface PhoneJson {
    type: string
    tel: string | null
    // Пока не используются
    // context: string
    // ext: string
}
