import { Component, Input, OnChanges, forwardRef, ViewChild, ViewChildren, ElementRef, QueryList, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, AbstractControl } from '@angular/forms';
import { ElmaTranslateService } from '../../../../../shared/common';

import { Field } from '../../dynamic-form.component';
import { Focuser } from '../../focuser.interfaces';
import { PhoneJson } from './phone.interfaces';

const resources = require('./phone.resources.json');

@Component({
    selector: 'app-dynamic-form-types-phone',
    templateUrl: './phone.component.html',
    styleUrls: ['./phone.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => PhoneComponent), multi: true },
    ]
})
export class PhoneComponent implements OnInit, ControlValueAccessor, OnChanges, Focuser {

    private PHONE_TYPE_CODES = ['home', 'work', 'mobile', 'main', 'home-fax', 'work-fax', 'pager'];
    private MASK = ['+', /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/];
    private readonly DefaultPlaceHolder = '+0 000 000 0000';

    @Input() field: Field | Field;
    @Input() currentFormControl: AbstractControl;
    @Input() hideType: boolean; // Скрывать метку типа в случае единичного варианта
    @Input() placeholder: string;
    @Input() disableMask: string;

    @ViewChild('add') addItemLink: ElementRef;
    @ViewChild('single') singleInput: ElementRef;

    @ViewChildren('input') inputs: QueryList<ElementRef>;

    types: { value: string, label: string }[] = [];
    maskOptions: any = { mask: this.MASK, showMask: false, guide: false };

    private _value: PhoneJson[] | null;
    get value() {
        return this._value;
    }
    @Input()
    set value(val) {
        this._value = val;
        this.propagateChange(val);
    }

    propagateChange: any = () => { };

    constructor(ts: ElmaTranslateService) {
        this.types = this.PHONE_TYPE_CODES.map(value => {
            return { value, label: ts.get('app.dynamicform.types.phone@type-' + value), }
        })
    }

    ngOnInit (): void {
        if (!this.placeholder) {
            this.placeholder = this.DefaultPlaceHolder;
        }

        if (this.disableMask) {
            this.maskOptions.mask = false;
        }
    }

    addItem(e: Event) {
        e.stopPropagation();

        if (!this.value) {
            this.value = [];
        }

        this.value.push({ type: this.PHONE_TYPE_CODES[0], tel: '' });
        this.propagateChange(this.value);

        // Установка фокуса в добавленный контрол
        setTimeout(() => { this.inputs.last.nativeElement.focus() });
    }

    removeItem(elementIndex: number) {
        if (this.value) {
            const items = this.value.filter((item, index) => index !== elementIndex);
            this.value = items.length ? items : null;
        }
    }

    singleGet(): PhoneJson {
        if (this.value != null) {
            if (this.value.length > 0) {
                return this.value[0]
            }
        }

        return <PhoneJson>{};
    }

    singleSet(value: string) {
        const unmaskedValue = this.unmask(value);
        if (unmaskedValue) {
            this.value = [{
                type: this.field.data.type,
                tel: unmaskedValue,
            }];
        } else {
            this.value = null;
        }
    }

    unmask(value: string): string | null {
        if (!value || !value.length) {
            return null;
        }

        return  value.replace(new RegExp(/\D/g), '');
    }

    /*
     * OnChanges implementation
     */

    ngOnChanges() {
        this.propagateChange(this.value);
    }

    /*
     * FocusChanger implementation
     */

    setFocus() {
        const control = this.addItemLink || this.singleInput;
        setTimeout(() => { control.nativeElement.focus() });
    }

    /*
     * ControlValueAccessor implementation
     */

    writeValue(value: PhoneJson[] | null) {
        if (value) {
            this.value = value;
        }
    }

    registerOnChange(fn: Function) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }
}
