import { Component, OnInit, Input, forwardRef, HostBinding } from '@angular/core';
import { AbstractControl, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { observable } from 'mobx-angular';
import { ICategory } from '../../../../../shared/common/elma-dnd-listbox';
import { Option } from '../../../../../shared/common/elma-select';
import { Field } from '../../dynamic-form.component';

@Component({
    selector: 'app-dynamic-form-types-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => CategoryComponent), multi: true },
    ]
})
export class CategoryComponent implements OnInit, ControlValueAccessor {

    @Input() namespace: string;
    @Input() field: Field;
    @Input() currentFormControl: AbstractControl;
    @Input() isEditMode: boolean;

    @HostBinding('class.itemscope') public scope = true;

    private _value: string[];

    private set value (val: string[]) {
        this._value = val;

        const categoryList: ICategory[] = [];
        this._value.forEach(categoryId => {
            const itemList = this.field.data.categoryItems.filter((item: ICategory) => item.id === categoryId);
            if (itemList.length) {
                const item = itemList[0];
                categoryList.push(<ICategory>{
                    id: item.id,
                    name: item.name,
                });
            }
        });

        this.propagateChange(categoryList);
    }

    get singleValue (): string {
        return this._value && this._value.length ? this._value[0] : '';
    }
    set singleValue (val: string) {
        this.value = val ? [val] : [];
    }

    optionList: Option[];
    propagateChange: any = () => { };

    constructor () {
    }

    ngOnInit () {
        this.initOptionList();
    }

    initOptionList (): void {
        this.optionList = this.field.data.categoryItems.map((item: any) => {
            return {
                label: item.name,
                value: item.id,
                checked: item.checked,
            };
        });
    }

    updateValues () {
        this.value = this.optionList.filter(o => (<any>o).checked).map(o => o.value);
    }

    writeValue(value: ICategory[]) {
        if (value) {
            this._value = value.map(item => item.id);
        } else {
            const v = [];
            if (!this.isEditMode && this.field.data.categoryItems) {
                // Присваиваем значение по умолчанию для режима создания
                for (const item of this.field.data.categoryItems) {
                    if (item.checked) {
                        v.push(item.id);
                        if (this.field && this.field.single) {
                            break;
                        }
                    }
                }
            }
            this._value = v;
        }
        if (this.optionList) {
            for (const option of this.optionList) {
                (<any>option).checked = this._value.filter(v => v === option.value).length;
            }
        }
    }

    registerOnChange(fn: Function) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }

}
