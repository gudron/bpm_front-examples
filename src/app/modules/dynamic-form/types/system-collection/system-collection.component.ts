import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    forwardRef,
    Input,
    OnInit,
    ViewChild,
} from '@angular/core';
import { AbstractControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Focuser } from 'app/modules/dynamic-form/focuser.interfaces';
import { ControlWrapperComponent } from '../../../../../shared/common/control-wrapper/control-wrapper.component';
import { ApplicationModel } from '../../../../application/models/application.model';
import { ItemSelectComponent } from '../../../../application/modules/item-select/item-select.component';
import { SystemCollectionDataJson } from '../../../../models/field.json';
import { Field } from '../../dynamic-form.component';

@Component({
    selector: 'app-dynamic-form-types-system-collection',
    templateUrl: './system-collection.component.html',
    styleUrls: ['./system-collection.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SystemCollectionComponent), multi: true },
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SystemCollectionComponent extends ControlWrapperComponent<string[] | null> implements OnInit, AfterViewInit, Focuser {

    @Input() namespace: string;
    @Input() field: Field;
    @Input() currentFormControl: AbstractControl;

    @ViewChild('input') input: ItemSelectComponent;
    @ViewChild('searchInput') searchInput: ElementRef;

    public application: ApplicationModel;
    public data = <SystemCollectionDataJson>{};

    constructor () {
        super();
    }

    ngOnInit () {
        this.data = <SystemCollectionDataJson>this.field.fieldData;
    }

    ngAfterViewInit () {
        this.initWrapper(this.input);
    }

    setFocus () {
        this.input.setFocus();
    }

}
