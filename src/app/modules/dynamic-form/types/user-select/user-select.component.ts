import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component, forwardRef,
    Input,
    OnInit, ViewChild,
} from '@angular/core';
import { AbstractControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlWrapperComponent } from '../../../../../shared/common/control-wrapper/control-wrapper.component';
import { ItemSelectComponent } from '../../../../application/modules/item-select/item-select.component';
import { Field } from '../../dynamic-form.component';
import { Focuser } from '../../focuser.interfaces';

@Component({
    selector: 'app-dynamic-form-types-user-select',
    templateUrl: './user-select.component.html',
    styleUrls: ['./user-select.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DynamicUserSelectComponent), multi: true },
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DynamicUserSelectComponent extends ControlWrapperComponent<string[] | null> implements OnInit, AfterViewInit, Focuser {

    @Input() namespace: string;
    @Input() field: Field;
    @Input() currentFormControl: AbstractControl;

    @ViewChild('input') input: ItemSelectComponent;

    constructor () {
        super();
    }

    ngOnInit () {
    }

    ngAfterViewInit () {
        this.initWrapper(this.input);
    }

    setFocus () {
        this.input.setFocus();
    }

}
