import { Component, Input, OnChanges, forwardRef, ViewChild, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { Field } from '../../dynamic-form.component';
import { Focuser } from '../../focuser.interfaces';

@Component({
    selector: 'app-dynamic-form-types-boolean',
    templateUrl: './boolean.component.html',
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => BooleanComponent), multi: true },
    ]
})
export class BooleanComponent implements ControlValueAccessor, OnChanges, Focuser {

    @Input() field: Field | Field;
    @Input() focus: boolean;

    @ViewChild('radio') radio: ElementRef;
    @ViewChild('checkbox') checkbox: ElementRef;

    private _value: boolean;
    get value() {
        return this._value;
    }

    propagateChange: any = () => { };

    setValue(value: boolean) {
        this._value = value;
        this.propagateChange(value);
    }

    /*
     * Focuser implementation
     */

    setFocus() {
        const control = this.radio || this.checkbox;
        if (control) {
            setTimeout(() => { control.nativeElement.focus() });
        }
    }

    /*
     * OnChanges implementation
     */

    ngOnChanges(inputs: any) {
        this.propagateChange(this.value);
    }

    /*
     * ControlValueAccessor implementation
     */

    writeValue(value: boolean) {
        this._value = value;
    }

    registerOnChange(fn: Function) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }
}
