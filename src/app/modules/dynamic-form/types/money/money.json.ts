import { Currency } from '../../../../app.constants';

export interface MoneyJson {
    cents: number;
    currency: Currency;
}
