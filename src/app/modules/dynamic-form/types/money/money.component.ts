import { Component, OnInit, Input, ViewChild, forwardRef, AfterViewInit } from '@angular/core';
import { AbstractControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ElmaNumberComponent } from '../../../../../shared/common';
import { Field } from '../../dynamic-form.component';
import { MoneyJson } from './money.json';
import { MoneyModel } from './money.model';

@Component({
    selector: 'app-dynamic-form-types-money',
    templateUrl: './money.component.html',
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MoneyComponent), multi: true },
    ],
})
export class MoneyComponent implements OnInit, AfterViewInit {

    @Input() field: Field;
    @Input() currentFormControl: AbstractControl;
    @Input() focus: boolean;
    @ViewChild('input') input: ElmaNumberComponent;

    public decimalSeparator = ',';
    public thousandSeparator = ' ';
    public startPositionCurrency = false;
    public currencySymbol = '';
    public _viewValue = 0;
    public get viewValue(): number {
        return this._viewValue;
    }
    public set viewValue(val: number) {

        val = parseFloat(this._value.cutOffexcess(val + ''));
        this._viewValue = Number.isNaN(val) ? 0 : val;

        const moneyJson: MoneyJson = {
            cents: this._viewValue,
            currency: this.value.currency,
        };
        this.value = moneyJson;
    }

    public _value: MoneyModel;
    get value (): MoneyJson {
        return this._value.toJSON();
    }
    set value (val: MoneyJson) {
        val.cents = Number.isNaN(val.cents) ? Number.MIN_VALUE : val.cents;
        this._value = new MoneyModel(val.cents, val.currency);
        this.propagateChange(this._value.toJSON());
    }

    constructor () {
    }

    ngOnInit () {
    }

    ngAfterViewInit () {
        this.setTypography();
        this.setNewFunctionForNgprime();
    }

    propagateChange: any = () => {
    };

    setFocus () {
        setTimeout(() => {
            this.input.setFocus();
        });
    }

    writeValue (value: MoneyJson) {
        if (value) {
            this.value = (new MoneyModel()).centsToAccountingCurrency(value);
            this.viewValue = value.cents;
        }
    }

    registerOnChange (fn: Function) {
        this.propagateChange = fn;
    }

    registerOnTouched () {
    }

    private setNewFunctionForNgprime (): void {

        this.input.control.onInputKeydown = (event: KeyboardEvent) => {

            const testValue = /\d/.test(event.key) ? this.viewValue + event.key : this.viewValue + '';
            const pattern = `^\\d+((\\.\\d{0,${this._value.decimalCapacity}})|(\\.)|())$`;
            const centsReg = new RegExp(pattern);

            if (!centsReg.test(testValue)) {
                event.preventDefault();
            }
        };
    }

    public setTypography (): void {
        this.decimalSeparator = this._value.decimalSeparator;
        this.thousandSeparator = this._value.thousandSeparator;
        this.startPositionCurrency = this._value.startPositionCurrency;
        this.currencySymbol = this._value.currencySymbol;
    }

}
