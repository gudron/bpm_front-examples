import { Currency, CURRENCY_PROPERTIES } from '../../../../app.constants';
import { MoneyJson } from './money.json';

export class MoneyModel {

    public cents = '';
    // разрядность копеешной части
    public decimalCapacity = 2;

    public currency: Currency = Currency.RUB;
    public decimalSeparator = '.';
    public thousandSeparator = ',';
    public startPositionCurrency = true;
    public currencySymbol = '';

    constructor (cents = 0, currency = Currency.RUB) {
        this.cents = cents + '';
        this.currency = currency;
        this.setTypography();
    }

    public setTypography (): void {

        if (CURRENCY_PROPERTIES[this.currency]) {

            this.decimalSeparator = CURRENCY_PROPERTIES[this.currency].decimalSeparator;
            this.thousandSeparator = CURRENCY_PROPERTIES[this.currency].thousandSeparator;
            this.startPositionCurrency = CURRENCY_PROPERTIES[this.currency].startPositionCurrency;
            this.decimalCapacity = CURRENCY_PROPERTIES[this.currency].decimalCapacity;
            this.currencySymbol = CURRENCY_PROPERTIES[this.currency].symbol;
        }
    }

    public toJSON (): MoneyJson {
        const cents = Number(this.cents) * Math.pow(10, this.decimalCapacity);
        return {
            cents: cents,
            currency: this.currency,
        };
    }

    public centsToView (): string {
        let centsView = Number(this.cents) / Math.pow(10, this.decimalCapacity) + '';
        centsView = centsView.replace('.', this.decimalSeparator);
        centsView = centsView.replace(/\B(?=(\d{3})+(?!\d))/g, this.thousandSeparator);
        return centsView;
    }

    // обрезает лишние разряды дробной части в зависимости от валюты
    public cutOffexcess (moneyValue: string): string {

        const decimalSeparator = '.';
        const intAndFloatMoney = moneyValue.split(decimalSeparator);
        let moneyResult = intAndFloatMoney[0];

        if (intAndFloatMoney[1]) {

            let fractionalRemainder = intAndFloatMoney[1];
            fractionalRemainder = fractionalRemainder.replace(/\D/, '');
            let realFractionRemainder = '';
            for (let i = 0; i < this.decimalCapacity; i++) {
                realFractionRemainder += fractionalRemainder[i];
            }

            moneyResult += decimalSeparator + realFractionRemainder;
        }

        return moneyResult;
    }

    public centsToAccountingCurrency (moneyJson: MoneyJson): MoneyJson {
        moneyJson.cents = moneyJson.cents / Math.pow(10, this.decimalCapacity);
        return moneyJson;
    }
}
