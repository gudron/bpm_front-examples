import { Component, Input, OnChanges, forwardRef, ViewChild, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, AbstractControl } from '@angular/forms';
import { Field } from '../../dynamic-form.component';
import { Focuser } from '../../focuser.interfaces';

@Component({
    selector: 'app-dynamic-form-types-link',
    templateUrl: './link.component.html',
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => LinkComponent), multi: true },
    ]
})
export class LinkComponent implements ControlValueAccessor, OnChanges, Focuser {

    @Input() field: Field | Field;
    @Input() currentFormControl: AbstractControl;
    @Input() focus: boolean;

    @ViewChild('input') input: ElementRef;

    private _value: string;
    get value() {
        return this._value;
    }
    @Input()
    set value(val) {
        this._value = val.trim();
        this.propagateChange(this._value);
    }

    propagateChange: any = () => { };

    /*
     * OnChanges implementation
     */

    ngOnChanges() {
        this.propagateChange(this.value);
    }

    /*
     * Focuser implementation
     */

    setFocus() {
        if (this.input) {
            setTimeout(() => { this.input.nativeElement.focus() });
        }
    }

    /*
     * ControlValueAccessor implementation
     */

    writeValue(value: string) {
        if (value) {
            this.value = value;
        }
    }

    registerOnChange(fn: Function) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }
}
