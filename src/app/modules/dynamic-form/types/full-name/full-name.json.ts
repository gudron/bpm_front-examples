export interface FullNameJson {
    firstname: string;
    lastname: string;
    middlename: string;
}
