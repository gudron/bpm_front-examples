import { Component, ElementRef, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { AbstractControl, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { FullNameService } from '../../../../services/full-name.service';
import { Field } from '../../dynamic-form.component';
import { Focuser } from '../../focuser.interfaces';
import { FullNameJson } from './full-name.json';

@Component({
    selector: 'app-dynamic-form-types-full-name',
    templateUrl: './full-name.component.html',
    styleUrls: ['./full-name.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => FullNameComponent), multi: true },
    ],
})
export class FullNameComponent implements ControlValueAccessor, Focuser {

    @Input() namespace: string;
    @Input() field: Field;
    @Input() currentFormControl: AbstractControl;
    @Input() readonly: boolean;

    @Output() keyup = new EventEmitter<Event>();

    @ViewChild('lastname') lastnameControl: ElementRef;

    private _value: FullNameJson | null = null;
    get value (): FullNameJson | null {
        return this._value;
    }

    set value (val: FullNameJson | null) {
        this._value = !FullNameService.isEmpty(val) ? val : null;
        if (this.propagateChange != null) {
            this.propagateChange(this._value);
        }
    }

    changeNamePart (val: string, part: string) {
        let currentVal = this._value;
        if (!currentVal) {
            currentVal = <FullNameJson>{
                lastname: '',
                middlename: '',
                firstname: '',
            };
        }

        (<any>currentVal)[part] = val.trim();
        this.value = currentVal;
    }

    /*
     * Focuser implementation
     */
    setFocus () {
        this.lastnameControl.nativeElement.focus();
    }

    writeValue (value: FullNameJson | null) {
        if (value) {
            this.value = <FullNameJson>value;
        }
    }

    propagateChange: any = () => {
    };

    registerOnChange (fn: Function) {
        this.propagateChange = fn;
    }

    registerOnTouched () {
    }
}
