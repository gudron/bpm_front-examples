import { Component, Input, OnChanges, forwardRef, ViewChild, ElementRef } from '@angular/core';
import { AbstractControl, ControlValueAccessor, Validator, NG_VALUE_ACCESSOR, NG_VALIDATORS } from '@angular/forms';
import { ElmaNumberComponent } from '../../../../../shared/common';
import { Field } from '../../dynamic-form.component';
import { isNullOrUndefined } from 'util';
import { Focuser } from '../../focuser.interfaces';
import { NumberType } from './number.enum';

@Component({
    selector: 'app-dynamic-form-types-number',
    templateUrl: './number.component.html',
    styleUrls: ['./number.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => NumberComponent), multi: true },
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => NumberComponent), multi: true },
    ],
})
export class NumberComponent implements ControlValueAccessor, OnChanges, Validator, Focuser {

    @Input() field: Field | Field;
    @Input() currentFormControl: AbstractControl;
    @Input() focus: boolean;

    @ViewChild('input') input: ElmaNumberComponent;
    public numberType = NumberType;

    private _value: number;
    get value () {
        return this._value;
    }
    @Input()
    set value (val) {
        // _value всегда содержит рельный выхлоп контрола
        this._value = val;
        // В случае кривого числа, устанавливаем в модель Number.MIN_VALUE для отработки других валидаторов
        this.propagateChange(Number.isNaN(val) ? Number.MIN_VALUE : val);
    }

    propagateChange: any = () => {
    };

    setFocus () {
        setTimeout(() => {
            this.input.setFocus();
        });
    }

    ngOnChanges (inputs: any) {
        this.propagateChange(this.value);
    }

    writeValue (value: number) {
        if (!isNullOrUndefined(value)) {
            // Устанавливаем значение если оно не null или undefined.
            // Иначе при открытии формы контрол будет иметь состояние dirty.
            this.value = value;
        }
    }

    registerOnChange (fn: Function) {
        this.propagateChange = fn;
    }

    registerOnTouched () {
    }

    validate () {
        // Используется _value, т.к. оно всегда содержит рельный выхлоп контрола. value, в случае кривого числа,
        // содержит Number.MIN_VALUE для означивания модели и отработки других валидаторов.
        if (this.input) {
            return numberValidator(this.field.data.additionalType, this._value);
        } else {
            return null;
        }
    }
}

export function numberValidator (type: string, value: number) {
    // Не валим валидатор, если контрол возвращает null, а нативный контрол говорит что не может привести значение
    // к number. undefined тут возникает при установке дефолтного значения при инициализации контрола.
    // обязательное проверяем на null и на NaN, иначе при начальной инциализации компонента будут валиться ошибки
    if ((value == null) || Number.isNaN(value)) {
        return null;
    }

    if (type === NumberType.integer) {
        if (!Number.isInteger(value)) {
            return {
                integer: true,
            };
        }
    }

    if (type === NumberType.float) {
        if (isNaN(value) || !isFinite(value)) {
            return {
                float: true,
            };
        }
    }

    return null;
}
