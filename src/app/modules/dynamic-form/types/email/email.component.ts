import { Component, Input, OnChanges, forwardRef, ViewChild, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, AbstractControl } from '@angular/forms';
import { ElmaTranslateService } from '../../../../../shared/common';

import { Field } from '../../dynamic-form.component';
import { Focuser } from '../../focuser.interfaces';

const resources = require('./email.resources.json');

export interface ListItem {
    type: string
    email: string
}

@Component({
    selector: 'app-dynamic-form-types-email',
    templateUrl: './email.component.html',
    styleUrls: ['./email.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => EmailComponent), multi: true },
    ]
})
export class EmailComponent implements ControlValueAccessor, OnChanges, Focuser {

    private EMAIL_TYPE_CODES = ['home', 'work', 'mobile', 'main'];

    @Input() field: Field | Field;
    @Input() currentFormControl: AbstractControl;

    @ViewChild('add') addItemLink: ElementRef;
    @ViewChild('single') singleInput: ElementRef;

    @ViewChildren('input') inputs: QueryList<ElementRef>;

    types: { value: string, label: string }[] = [];

    private _value: ListItem[] | null;
    get value() {
        return this._value;
    }
    @Input()
    set value(val) {
        this._value = val;
        this.propagateChange(val);
    }

    propagateChange: any = () => { };

    constructor(ts: ElmaTranslateService) {
        this.types = this.EMAIL_TYPE_CODES.map(value => {
            return { value, label: ts.get('app.dynamicform.types.email@email-' + value), }
        })
    }

    addItem(e: Event) {
        e.stopPropagation();

        if (!this.value) {
            this.value = [];
        }

        this.value.push({ type: this.EMAIL_TYPE_CODES[0], email: '' });
        this.propagateChange(this.value);

        // Установка фокуса в добавленный контрол
        setTimeout(() => { this.inputs.last.nativeElement.focus() });
    }

    removeItem(elementIndex: number) {
        if (this.value) {
            const items = this.value.filter((item, index) => index !== elementIndex);
            this.value = items.length ? items : null;
        }
    }

    singleGet(): ListItem {
        if (this.value != null) {
            if (this.value.length > 0) {
                return this.value[0]
            }
        }

        return <ListItem>{};
    }

    singleSet(value: string) {
        if (value && value.length) {
            if (!this.value) {
                this.value = [{
                    type: this.field.data.type,
                    email: value,
                }];
            } else {
                this.value[0].email = value;
            }
        } else {
            this.value = null;
        }
    }

    /*
     * OnChanges implementation
     */

    ngOnChanges() {
        this.propagateChange(this.value);
    }

    /*
     * FocusChanger implementation
     */

    setFocus() {
        const control = this.addItemLink || this.singleInput;
        setTimeout(() => { control.nativeElement.focus() });
    }

    /*
     * ControlValueAccessor implementation
     */

    writeValue(value: ListItem[] | null) {
        if (value) {
            this.value = value;
        }
    }

    registerOnChange(fn: Function) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }
}
