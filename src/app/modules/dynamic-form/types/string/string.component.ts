import { Component, Input, OnChanges, forwardRef, ViewChild, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, AbstractControl } from '@angular/forms';
import { Field } from '../../dynamic-form.component';
import { Focuser } from '../../focuser.interfaces';

@Component({
    selector: 'app-dynamic-form-types-string',
    templateUrl: './string.component.html',
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => StringComponent), multi: true },
    ]
})
export class StringComponent implements ControlValueAccessor, OnChanges, Focuser {

    @Input() field: Field | Field;
    @Input() currentFormControl: AbstractControl;
    @Input() focus: boolean;

    @ViewChild('input') input: ElementRef;
    @ViewChild('textarea') textarea: ElementRef;

    private _value: string;
    get value() {
        return this._value;
    }
    @Input()
    set value(val) {
        this._value = val;
        this.propagateChange(val);
    }

    propagateChange: any = () => { };

    /*
     * OnChanges implementation
     */

    ngOnChanges() {
        this.propagateChange(this.value);
    }

    /*
     * Focuser implementation
     */

    setFocus() {
        const control = this.input || this.textarea;
        if (control) {
            setTimeout(() => { control.nativeElement.focus() });
        }
    }

    /*
     * ControlValueAccessor implementation
     */

    writeValue(value: string) {
        if (value) {
            this.value = value;
        }
    }

    registerOnChange(fn: Function) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }
}
