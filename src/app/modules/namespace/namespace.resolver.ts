import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Namespaces } from '../../app.constants';
import { PageModel } from '../../models/page.model';
import { PageStore } from '../../stores/page.store';

@Injectable()
export class NamespaceResolver implements Resolve<PageModel> {

    constructor (
        private store: PageStore,
    ) {
    }

    resolve (
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Promise<PageModel> {
        return this.store.getByNamespaceAndCode(Namespaces.Global, route.params['code']);
    }
}
