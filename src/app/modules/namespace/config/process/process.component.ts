import { Component, Input } from '@angular/core';
import { Namespaces } from '../../../../app.constants';
import { ContextBuilder } from '../../../../bpm/services/context.builder';
import { PageModel } from '../../../../models/page.model';

@Component({
    selector: 'app-namespace-process',
    templateUrl: 'process.component.html',
})
export class NamespaceProcessComponent {

    @Input() page: PageModel;

    constructor (private _ctxBuilder: ContextBuilder) {
    }

    get namespace (): string {
        return this._ctxBuilder.getBPNamespaceByPage(this.page);
    }

    get canRender (): boolean {
        return !!this.page;
    }
}
