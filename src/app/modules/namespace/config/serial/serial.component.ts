import { Component, Input, OnInit } from '@angular/core';
import { PageModel } from '../../../../models/page.model';

@Component({
    selector: 'app-namespace-serial',
    templateUrl: 'serial.component.html',
})

export class NamespaceSerialComponent implements OnInit {
    @Input() page: PageModel;

    get canRender (): boolean {
        return !!this.page;
    }

    constructor () {
    }

    ngOnInit () {
    }
}
