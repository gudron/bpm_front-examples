import 'rxjs/add/operator/toPromise';
import { Subscription } from 'rxjs/Subscription';
import { SystemCollections } from '../../../../app.constants';
import { Collection } from '../../../../application/schema/collection.interface';
import { ListResult } from '../../../../application/services/collection.store';
import { DefaultCollectionStore } from '../../../../application/services/collection.store.default';
import { ItemJson } from '../../../../models/item.json';
import { APIService } from '../../../../services/api.service';
import { GroupStore } from '../../../../stores/group.store';

export class NamespaceGroupStore extends DefaultCollectionStore {

    private groupStore: GroupStore;

    constructor (
        _apiService: APIService,
        groupStore: GroupStore,
        namespace: string,
    ) {
        super(_apiService,
            <Collection>{
                namespace: namespace,
                code: SystemCollections.Groups,
            },
            '/api/groups',
        );

        this.groupStore = groupStore;
    }

    async getItems (page?: number, size = 10, query?: string, withPermissions?: boolean): Promise<ListResult> {
        return this.groupStore.getItemsByNamespace(this.collection.namespace, page, size, query, false).toPromise();
    }

    subscribeToItemChanges (id: string | null, callback: (json: ItemJson) => void): Subscription {
        return this.groupStore.subscribeToItemChanges(id, callback);
    }
}
