import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ElmaTranslateService } from '../../../../../shared/common';
import { PageModel } from '../../../../models/page.model';
import { APIService } from '../../../../services/api.service';
import { GroupStore } from '../../../../stores/group.store';
import { GetOrgunitsSystemApplicationJson } from '../../../admin/group/group.application';
import { ApplicationStore } from '../../../../application/services/application.store';
import { Collection } from '../../../../application/schema/collection.interface';
import { CollectionStoreFactory } from '../../../../application/services/collection.store.factory';
import { AppviewConfig } from '../../../../application/modules/appview/appview.interface';
import { Layout } from '../../../../application/modules/appview/list/list.enums';
import { NamespaceGroupStore } from './group.store';

@Component({
    selector: 'app-namespace-groups',
    templateUrl: 'groups.component.html',
})
export class NamespaceGroupsComponent implements OnChanges {

    Layout = Layout;

    @Input() page: PageModel;

    config: AppviewConfig;

    get canRender (): boolean {
        return !!this.page && !!this.config;
    }

    constructor (
        private apiService: APIService,
        private translationService: ElmaTranslateService,
        private groupStore: GroupStore,
        private applicationStore: ApplicationStore,
        private collectionStoreFactory: CollectionStoreFactory,
    ) {
    }

    ngOnChanges (changes: SimpleChanges): void {
        const pageChange = changes['page'];
        if (!pageChange || !pageChange.currentValue) {
            return;
        }

        this.config = <AppviewConfig>{
            namespaceCode: this.page.code,
            pageCode: '_groups',
        };

        const collection = <Collection>{
            namespace: this.page.code,
            code: this.config.pageCode,
        };

        /*
         * При первом отображении контрола, динамически создаем справочник и приложения для отображения групп раздела
         * на основе NamespaceGroupStore и кладем в кэш.
         */

        if (!this.collectionStoreFactory.has(collection)) {
            const store = new NamespaceGroupStore(this.apiService, this.groupStore, collection.namespace);
            this.collectionStoreFactory.push(collection, store);
        }

        if (!this.applicationStore.getFromCache(this.config.namespaceCode, this.config.pageCode)) {
            const json = GetOrgunitsSystemApplicationJson(this.translationService, true);
            json.namespace = this.config.namespaceCode;
            json.code = this.config.pageCode;
            this.applicationStore.addToCache(json);
        }
    }
}
