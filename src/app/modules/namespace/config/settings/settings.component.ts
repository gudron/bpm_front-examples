import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { ElmaActiveModal } from '../../../../../shared/common';
import { PageJson } from '../../../../models/page.json';
import { PageModel } from '../../../../models/page.model';
import { PageStore } from '../../../../stores/page.store';

@Component({
    selector: 'app-namespace-settings',
    templateUrl: 'settings.component.html',
})

export class NamespaceSettingsComponent implements OnInit, AfterViewInit {

    @Input() page: PageModel;

    @ViewChild('name') name: ElementRef;

    editPage: PageJson;
    baseUrl = `${window.location.origin}/`;

    constructor (
        private _cdr: ChangeDetectorRef,
        private _pageStore: PageStore,
        private _activeModal: ElmaActiveModal,
    ) {
    }

    ngOnInit () {
        this.editPage = this.page.toJSON();
    }

    ngAfterViewInit () {
        this.name.nativeElement.focus();
    }

    save () {
        this._pageStore.update(this.editPage);
        this.close();
        setTimeout(() => this._cdr.markForCheck());
    }

    close () {
        this._activeModal.close();
    }
}
