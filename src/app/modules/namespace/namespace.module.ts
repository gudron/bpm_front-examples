import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MobxAngularModule } from 'mobx-angular';
import { ElmaCommonModule } from '../../../shared/common';
import { BPMListModule } from '../../bpm/modules/list/list.module';
import { AppCommonModule } from '../../common/common.module';
import { AppSharedModule } from '../../shared/shared.module';
import { GroupModule } from '../admin/group/group.module';
import { AppviewSharedModule } from '../../application/modules/appview/shared/shared.module';
import { WidgetPageCreateModule } from '../pages/widget-page-create/widget-page-create.module';
import { NamespaceGroupsComponent } from './config/groups/groups.component';
import { NamespaceProcessComponent } from './config/process/process.component';
import { NamespaceSerialComponent } from './config/serial/serial.component';
import { NamespaceSettingsComponent } from './config/settings/settings.component';
import { NamespaceDetailsComponent } from './details/details.component';
import { NamespaceResolver } from './namespace.resolver';

@NgModule({
    imports: [
        CommonModule,
        ElmaCommonModule,
        RouterModule,
        MobxAngularModule,
        WidgetPageCreateModule,
        AppSharedModule,
        BPMListModule,
        AppviewSharedModule,
        GroupModule,
        FormsModule,
        AppCommonModule,
    ],
    declarations: [
        NamespaceDetailsComponent,
        NamespaceProcessComponent,
        NamespaceSerialComponent,
        NamespaceGroupsComponent,
        NamespaceSettingsComponent,
    ],
    exports: [
        NamespaceDetailsComponent,
        NamespaceProcessComponent,
        NamespaceSerialComponent,
        NamespaceGroupsComponent,
        NamespaceSettingsComponent,
    ],
    providers: [
        NamespaceResolver,
    ],
    entryComponents: [
        NamespaceSettingsComponent,
    ]
})
export class NamespaceModule {
}
