import { Component, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { computed } from 'mobx-angular';

import { PageModel } from './../../../models/page.model';
import { PageStore } from './../../../stores/page.store';

import { WidgetPageCreateComponent } from '../../pages/widget-page-create/widget-page-create.component';

@Component({
    selector: 'app-namespace-details',
    templateUrl: './details.component.html',
    styleUrls: ['details.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceDetailsComponent {

    BASE_URL = `${window.location.origin}/${window.location.pathname.split('/')[1]}`;

    @Input() namespace: PageModel;

    @ViewChild('createPageWidget') createPageWidget: WidgetPageCreateComponent;

    @computed
    get applications(): PageModel[] {
        return this.pageStore.list
            .filter(p => p.namespace === this.namespace.code)
    }

    constructor(
        private router: Router,
        private pageStore: PageStore,
    ) {
    }
}
