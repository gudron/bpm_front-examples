import { Component, Input } from '@angular/core';
import { PageModel } from '../../../models/page.model';
import { PageWidgetJson } from '../../../models/page.json';
import { PageType, WidgetType } from '../../../app.constants';
import { Router } from '@angular/router';

@Component({
    selector: 'app-page-wrapper',
    templateUrl: './page-wrapper.component.html',
    styleUrls: ['./page-wrapper.component.scss'],
})
export class PageWrapperComponent {

    @Input() page: PageModel;
    @Input() widget: PageWidgetJson;
    @Input() widgetNum: number;

    WidgetType = WidgetType;
    PageType = PageType;
    loading = false;

    constructor(
        public router: Router,
    ) {
    }

    changeWidget(pageWidgetJson: PageWidgetJson) {
        this.loading = true;
        this.widget = <PageWidgetJson>{type: WidgetType.Empty, data: {}};
        setTimeout(() => {
            this.widget = pageWidgetJson;
            this.loading = false;
        }, 500);
    }
}
