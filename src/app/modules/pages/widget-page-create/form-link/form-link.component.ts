import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as getSlug from 'speakingurl';
import { setTimeout } from 'timers';
import { ElmaPopupComponent } from '../../../../../shared/common/elma-popup';

import { PageType } from '../../../../app.constants';
import { IconpickerComponent } from '../../../../common/iconpicker/iconpicker.component';
import { Focuser } from '../focuser.interface';
import { INewPage } from '../widget-page-create.interface';
import { PageDataJson, PageJson } from './../../../../models/page.json';
import { PageStore } from './../../../../stores/page.store';

@Component({
    selector: 'app-form-link',
    templateUrl: './form-link.component.html',
    styleUrls: ['../widget-page-create.component.scss'],
})
export class FormLinkComponent implements OnInit, Focuser {

    @Input() page: INewPage;
    @Input() modal: ElmaPopupComponent;
    @Input() namespace: string;

    @ViewChild('iconPicker') iconPicker: IconpickerComponent;
    @ViewChild('name') name: ElementRef;

    constructor (
        public pageStore: PageStore,
        private router: Router,
    ) {
    }

    ngOnInit () {
    }

    setFocus () {
        setTimeout(() => {
            this.name.nativeElement.focus();
        }, 0);
    }

    async create () {
        try {
            const json = <PageJson>{
                namespace: this.namespace,
                type: PageType.Link,
                name: this.page.name,
                code: 'link_' + getSlug(this.page.name, { separator: '_' }),
                icon: this.page.icon || this.iconPicker.defaultIcon,
                data: <PageDataJson>{
                    redirectTo: this.page.url,
                    openInNewTab: this.page.openInNewTab,
                },
            };

            if (this.page.__id) {
                json.__id = this.page.__id;
                await this.pageStore.update(json);
            } else {
                await this.pageStore.create(json);
            }
            this.modal.closePopup();
        } catch (err) {
            console.error('Error creating link', err);
        }
    }
}
