import { AfterViewInit, Component, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ApplicationType, MenuWidgetType, NavTypes, PageType } from 'app/app.constants';
import { PageModel } from 'app/models/page.model';
import { ElmaPopupComponent } from '../../../../shared/common/elma-popup';
import { Layout } from '../../../application/modules/appview/list/list.enums';
import { Focuser } from './focuser.interface';
import { INewPage } from './widget-page-create.interface';

export enum Tabs {
    Namespace = 'namespace',
    Application = 'application',
    Page = 'page',
    Link = 'link',
    Separator = 'separator',
}
@Component({
    selector: 'app-widget-page-create',
    templateUrl: './widget-page-create.component.html',
    styleUrls: ['./widget-page-create.component.scss'],
})
export class WidgetPageCreateComponent implements OnInit, AfterViewInit {

    @Input() showNamespace = false;
    @Input() showApplication = false;
    @Input() defaultTab: Tabs;
    @Input() baseUrl: string;
    @Input() namespaceCode: string;

    @ViewChild('modal') modal: ElmaPopupComponent;
    @ViewChildren('form') forms: QueryList<Focuser>;

    public Tabs = Tabs;
    public createdItemList: Tabs[] = [];
    tab: Tabs;
    newPage: INewPage;
    public readonly labelPrefix = 'app.pages.widget-page-create@';

    constructor() {
    }

    ngOnInit() {
        this.setCreatedItemList();
    }

    ngAfterViewInit (): void {
        this.forms.changes.subscribe(() => this.focusFirstForm())
    }

    reset() {
        this.newPage = {
            name: '',
            code: '',
            type: <any>ApplicationType.Standard,
            accessAll: true,
            columnsCount: 1,
            url: '',
            openInNewTab: true,
            icon: '',
            changeCode: false,
            changeElementName: false,
            menuType: <any>NavTypes.NAV_TYPE_NAMESPACE,
            showName: true,
            moveUp: true,
            widget: <any>MenuWidgetType.Namespace,
            layout: <any>Layout.Tiles,
        };
    }

    showCreateModal() {
        if (this.showNamespace) {
            this.tab = this.Tabs.Namespace;
        } else if (this.showApplication) {
            this.tab = this.Tabs.Application;
        } else {
            this.tab = this.Tabs.Page
        }

        this.reset();
        this.showModal();
    }

    showEditModal(page: PageModel) {
        switch (page.type) {
            case PageType.Link:
                this.tab = Tabs.Link;
                break;
            case PageType.Namespace:
                this.tab = Tabs.Namespace;
                break;
            case PageType.Page:
                this.tab = Tabs.Page;
                break;
        }

        this.newPage = <INewPage>{
            __id: page.id,
            name: page.name,
            code: page.code,
            accessAll: true,
            columnsCount: page.data.columns,
            url: '',
            openInNewTab: page.data.openInNewTab,
            icon: page.icon,
            changeCode: false,
            menuType: page.data.menu!.type,
            showName: page.data.menu!.showName,
            moveUp: page.data.menu!.type === NavTypes.NAV_TYPE_NAMESPACE,
        };

        this.showModal();
    }

    setCreatedItemList(): void {

        this.createdItemList = [Tabs.Page, Tabs.Link];

        if (this.showApplication) {
            this.createdItemList.unshift(Tabs.Application);
            this.createdItemList.push(Tabs.Separator);
        }

        if (this.showNamespace) {
            this.createdItemList.unshift(Tabs.Namespace);
        }
    }

    private showModal() {
        this.modal.openPopup();
        this.focusFirstForm();
    }

    private focusFirstForm() {
        if (!this.forms.first || !this.forms.first.setFocus) {
            return;
        }

        // Задержка в 350 мс связана с тем, что каой-то контрол (предположительно emla-switcher) перехватывает фокус на себя.
        // Стоит разобраться если будет время.
        setTimeout(() => this.forms.first.setFocus(), 350);
    }
}
