import { MenuWidgetType, NavTypes } from '../../../app.constants';
export interface INewPage {
    __id?: string;
    name: string;
    code: string;
    type?: string;
    accessAll: boolean;
    columnsCount: number;
    url: string;
    openInNewTab: boolean;
    icon: string;
    changeCode: boolean;
    changeElementName: boolean;
    layout?: string;
    elementName?: string;
    menuType?: NavTypes;
    showName?: boolean;
    moveUp?: boolean;
    widget?: MenuWidgetType;
}
