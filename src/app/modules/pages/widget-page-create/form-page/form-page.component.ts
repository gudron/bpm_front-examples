import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as getSlug from 'speakingurl';
import { setTimeout } from 'timers';
import { ElmaPopupComponent } from '../../../../../shared/common/elma-popup';

import { Namespaces, NavTypes, PageType } from '../../../../app.constants';
import { IconpickerComponent } from '../../../../common/iconpicker/iconpicker.component';
import { Focuser } from '../focuser.interface';
import { INewPage } from '../widget-page-create.interface';
import { PageDataJson, PageJson, PageMenuJson } from './../../../../models/page.json';
import { PageStore } from './../../../../stores/page.store';

@Component({
    selector: 'app-form-page',
    templateUrl: './form-page.component.html',
    styleUrls: ['../widget-page-create.component.scss'],
})
export class FormPageComponent implements OnInit, Focuser {

    @Input() page: INewPage;
    @Input() modal: ElmaPopupComponent;
    @Input() baseUrl: string;
    @Input() namespace: string;
    @ViewChild('iconPicker') iconPicker: IconpickerComponent;
    @ViewChild('name') name: ElementRef;

    constructor (
        public pageStore: PageStore,
        private router: Router,
    ) {
    }

    ngOnInit () {
    }

    setFocus () {
        setTimeout(() => {
            this.name.nativeElement.focus();
        }, 0);
    }

    async create () {
        try {
            const json = <PageJson>{
                namespace: this.namespace,
                type: PageType.Page,
                name: this.page.name,
                code: this.page.code,
                icon: this.page.icon || this.iconPicker.defaultIcon,
                data: <PageDataJson>{
                    columns: this.page.columnsCount,
                    widgets: [],
                    menu: <PageMenuJson>{
                        type: NavTypes.NAV_TYPE_MAIN,
                    },
                },
            };

            if (this.page.__id) {
                json.__id = this.page.__id;
                await this.pageStore.update(json);
            } else {
                await this.pageStore.create(json);
                if (json.namespace === Namespaces.Global) {
                    this.router.navigate(['/', json.code]);
                } else {
                    this.router.navigate(['/', json.namespace, json.code]);
                }

            }

            this.modal.closePopup();
        } catch (err) {
            console.error('Error creating page', err);
        }
    }

    updateCode (prefix = '') {

        if (this.page.__id) {
            return;
        }

        if (this.page.name.length < 1) {
            this.page.changeCode = false;
        }

        if (!this.page.changeCode) {
            this.page.code = prefix + getSlug(this.page.name, { separator: '_' });
        }
    }

    typeCode () {
        this.page.changeCode = true;
    }
}
