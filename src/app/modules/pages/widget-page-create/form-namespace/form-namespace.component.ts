import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as getSlug from 'speakingurl';
import { setTimeout } from 'timers';
import { ElmaPopupComponent } from '../../../../../shared/common/elma-popup';

import { NavTypes, PageType, MenuWidgetType } from '../../../../app.constants';
import { Focuser } from '../focuser.interface';
import { PageJson, PageDataJson, PageMenuJson } from './../../../../models/page.json';
import { PageStore } from './../../../../stores/page.store';
import { INewPage } from '../widget-page-create.interface';
import { IconpickerComponent } from '../../../../common/iconpicker/iconpicker.component';

@Component({
    selector: 'app-form-namespace',
    templateUrl: './form-namespace.component.html',
    styleUrls: ['../widget-page-create.component.scss'],
})
export class FormNamespaceComponent implements OnInit, Focuser {

    @Input() page: INewPage;
    @Input() modal: ElmaPopupComponent;
    @Input() baseUrl: string;
    @Input() namespace: string;

    NavTypes = NavTypes;

    @ViewChild('iconPicker') iconPicker: IconpickerComponent;
    @ViewChild('name') name: ElementRef;

    constructor (
        public pageStore: PageStore,
        private router: Router,
    ) {
    }

    setFocus () {
        setTimeout(() => {
            this.name.nativeElement.focus();
        }, 0);
    }

    ngOnInit () {
        this.setDefaults();
    }

    async create () {
        try {

            // TODO: надо убрать эту штуку в настройку виджета меню
            let menuType = this.page.menuType;
            switch (this.page.widget) {
            case MenuWidgetType.Namespace:
                menuType = NavTypes.NAV_TYPE_NAMESPACE;
                break;
            case MenuWidgetType.Feed:
                menuType = NavTypes.NAV_TYPE_FEED;
                break;
            case MenuWidgetType.Disk:
                menuType = NavTypes.NAV_TYPE_DISK;
                break;
            case MenuWidgetType.Projects:
                menuType = NavTypes.NAV_TYPE_PROJECTS;
                break;
            case MenuWidgetType.Html:
                menuType = NavTypes.NAV_TYPE_HTML;
                break;
            }

            if (!this.page.moveUp && menuType === NavTypes.NAV_TYPE_NAMESPACE) {
                menuType = NavTypes.NAV_TYPE_MAIN_SUBMENU;
            }

            const json = <PageJson>{
                namespace: this.namespace,
                type: PageType.Namespace,
                name: this.page.name,
                code: this.page.code,
                icon: this.page.icon || this.iconPicker.defaultIcon,
                data: <PageDataJson>{
                    columns: 1, // Пока так решили
                    widgets: [],
                    menu: <PageMenuJson>{
                        type: menuType,
                        showName: this.page.showName,
                    },
                },
            };
            if (this.page.__id) {
                json.__id = this.page.__id;
                await this.pageStore.update(json);
            } else {
                await this.pageStore.create(json);
                this.router.navigate(['/', json.code]);
            }

            this.modal.closePopup();
        } catch (err) {
            console.error('Error creating namespace', err);
        }
    }

    updateCode (prefix = '') {
        if (this.page.__id) {
            return;
        }
        if (this.page.name.length < 1) {
            this.page.changeCode = false;
        }

        if (!this.page.changeCode) {
            this.page.code = prefix + getSlug(this.page.name, { separator: '_' });
        }
    }

    typeCode () {
        this.page.changeCode = true;
    }

    setDefaults () {
        this.page.menuType = NavTypes.NAV_TYPE_NAMESPACE;
        this.page.widget = MenuWidgetType.Namespace;
        this.page.moveUp = true;
        this.page.showName = true;
    }
}
