import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { setTimeout } from 'timers';
import { ElmaPopupComponent } from '../../../../../shared/common/elma-popup';
import { uuid4 } from '../../../../../shared/utils/uuid';
import { PageType } from '../../../../app.constants';
import { Focuser } from '../focuser.interface';
import { INewPage } from '../widget-page-create.interface';
import { PageJson } from './../../../../models/page.json';
import { PageStore } from './../../../../stores/page.store';

@Component({
    selector: 'app-form-separator',
    templateUrl: './form-separator.component.html',
    styleUrls: ['../widget-page-create.component.scss'],
})
export class FormSeparatorComponent implements Focuser {

    @Input() page: INewPage;
    @Input() modal: ElmaPopupComponent;
    @Input() namespace: string;

    @ViewChild('name') name: ElementRef;

    constructor (
        public pageStore: PageStore,
    ) {
    }

    setFocus () {
        setTimeout(() => {
            this.name.nativeElement.focus();
        }, 0);
    }

    async create () {
        const json = <PageJson>{
            namespace: this.namespace,
            code: uuid4(),
            name: this.page.name,
            type: PageType.Separator,
            icon: 'minus_horizontal',
            data: {},
        };

        if (this.page.__id) {
            json.__id = this.page.__id;
            await this.pageStore.update(json);
        } else {
            await this.pageStore.create(json);
        }

        this.modal.closePopup();
    }

}
