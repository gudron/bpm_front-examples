import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as getSlug from 'speakingurl';
import { setTimeout } from 'timers';
import { ElmaPopupComponent } from '../../../../../shared/common/elma-popup';
import { ApplicationType, NavTypes, WidgetType } from '../../../../app.constants';
import { Layout } from '../../../../application/modules/appview/list/list.enums';
import { Focuser } from '../focuser.interface';
import { PageJson, PageDataJson, PageMenuJson, PageWidgetJson } from './../../../../models/page.json';
import { ApplicationStore } from '../../../../application/services/application.store';
import { INewPage } from '../widget-page-create.interface';
import { IconpickerComponent } from '../../../../common/iconpicker/iconpicker.component';
import { Option } from '../../../../../shared/common';
import { ApplicationBuilder } from '../../../../application/models/application.builder';

@Component({
    selector: 'app-form-application',
    templateUrl: './form-application.component.html',
    styleUrls: ['../widget-page-create.component.scss'],
})
export class FormApplicationComponent implements Focuser {

    ApplicationType = ApplicationType;
    Layout = Layout;

    @Input() page: INewPage;
    @Input() modal: ElmaPopupComponent;
    @Input() baseUrl: string;
    @Input() namespace: string;

    @ViewChild('iconPicker') iconPicker: IconpickerComponent;
    @ViewChild('name') name: ElementRef;

    public readonly layoutVariant: Option[] = [
        {
            value: Layout.Tiles,
            icon: 'view_module',
        },
        {
            value: Layout.Table,
            icon: 'view_stream',
        },
    ];

    constructor (
        private router: Router,
        private applicationBuilder: ApplicationBuilder,
        public application: ApplicationStore,
    ) {
    }

    setFocus () {
        setTimeout(() => {
            this.name.nativeElement.focus();
        }, 0);
    }

    async create () {
        try {
            const json = <PageJson>{
                namespace: this.namespace,
                type: this.page.type,
                name: this.page.name,
                code: this.page.code,
                icon: this.page.icon || this.iconPicker.defaultIcon,
                data: <PageDataJson>{
                    layout: this.page.layout,
                    menu: <PageMenuJson>{
                        type: NavTypes.NAV_TYPE_NAMESPACE,
                    },
                    widgets: [<PageWidgetJson>{
                        type: WidgetType.Appview,
                        data: {
                            namespaceCode: this.namespace,
                            pageCode: this.page.code,
                        },
                    }],
                },
                system: false,
                hidden: false,
            };

            await this.applicationBuilder.create(json, this.page.elementName!);
            this.router.navigate(['/', json.namespace, json.code]);
            this.modal.closePopup();
        } catch (err) {
            console.error('Error creating application', err);
        }
    }

    updateCode () {
        if (this.page.name.length < 1) {
            this.page.changeCode = false;
        }

        if (!this.page.changeCode) {
            this.page.code = getSlug(this.page.name, { separator: '_' });
        }
    }

    updateElementName () {
        if (this.page.name.length < 1) {
            this.page.changeElementName = false;
        }

        if (!this.page.changeElementName) {
            this.page.elementName = this.page.name;
        }
    }

    typeCode () {
        this.page.changeCode = true;
    }

    typeElementName () {
        this.page.changeElementName = true;
    }
}

export interface ILayout {
    code: string;
    icon: string;
    title: string;
}
