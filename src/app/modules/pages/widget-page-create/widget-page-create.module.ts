import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ApplicationConfigModule } from '../../../application/modules/config/config.module';
import { FormSeparatorComponent } from './form-separator/form-separator.component';
import { WidgetPageCreateComponent } from './widget-page-create.component';
import { FormNamespaceComponent } from './form-namespace/form-namespace.component';
import { FormApplicationComponent } from './form-application/form-application.component';
import { FormPageComponent } from './form-page/form-page.component';
import { FormLinkComponent } from './form-link/form-link.component';
import { HiderComponent } from '../../../components/hider/hider.component';
import { AppCommonModule } from '../../../common/common.module';
import { ElmaCommonModule } from '../../../../shared/common/common.module';

@NgModule({
    imports: [
        CommonModule,
        ElmaCommonModule,
        AppCommonModule,
        RouterModule,
        FormsModule,
        ApplicationConfigModule,
    ],
    declarations: [
        HiderComponent,
        WidgetPageCreateComponent,

        FormNamespaceComponent,
        FormApplicationComponent,
        FormPageComponent,
        FormLinkComponent,
        FormSeparatorComponent,
    ],
    exports: [WidgetPageCreateComponent],
})
export class WidgetPageCreateModule { }
