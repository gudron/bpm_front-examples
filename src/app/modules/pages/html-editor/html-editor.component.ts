import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ElmaActiveModal } from '../../../../shared/common';

declare var SystemJS: any;

@Component({
    selector: 'app-pages-html-editor',
    template: `
        <div class="modal-header">
            <h4 class="modal-title">
                <span>{{'app.pages.html-editor@html-editor-header'|translate}}</span>
            </h4>
            <button type="button" class="close" aria-label="Close" (click)="tinymce.destroy(); activeModal.close()">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <elma-spinner *ngIf="loading"></elma-spinner>
            <textarea [style.opacity]="loading ? 0 : 1" id="tinymce" [innerHTML]="html"></textarea>
        </div>
        <div class="modal-footer">
            <button
                type="button"
                class="btn btn-admin"
                (click)="save();"
            >
                {{'app.pages.html-editor@save'|translate}}
            </button>
            <button
                type="button"
                class="btn"
                (click)="tinymce.destroy(); activeModal.close()"
            >
                {{'app.pages.html-editor@close'|translate}}
            </button>
        </div>
    `,
})
export class HtmlEditorComponent implements OnInit {

    @Input() html: string;
    @Output() onChange = new EventEmitter<string>();
    loading = true;
    tinymce: any;

    constructor (
        public activeModal: ElmaActiveModal,
    ) {
    }

    async ngOnInit () {
        this.initialize();
    }

    async initialize () {
        this.loading = true;
        if (this.tinymce) {
            this.tinymce.destroy();
        }
        const src = `${window.location.origin}/assets/tinymce/tinymce.min.js`;
        await SystemJS.import(src);
        const script: any = SystemJS.registry.get(src);
        const tinymce: any = script.default;
        tinymce.init({
            selector: '#tinymce',
            branding: false,
            theme_url: '/assets/tinymce/themes/modern/theme.min.js',
            skin_url: '/assets/tinymce/skins/lightgray',
            init_instance_callback: (editor: any) => {
                this.tinymce = editor;
                this.loading = false;
            }
        });
    }

    save () {
        this.onChange.emit(this.tinymce.getContent());
        this.tinymce.destroy();
    }



}
