import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HtmlEditorComponent } from './html-editor.component';
import { FormsModule } from '@angular/forms';
import { ElmaCommonModule } from '../../../../shared/common/common.module';

@NgModule({
    imports: [
        CommonModule,
        ElmaCommonModule,
        FormsModule,
    ],
    declarations: [
        HtmlEditorComponent,
    ],
    exports: [
        HtmlEditorComponent,
    ]
})
export class HtmlEditorModule {
}
