import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageSettingsComponent } from './page-settings.component';
import { RouterModule } from '@angular/router';
import { MobxAngularModule } from 'mobx-angular';
import { FormsModule } from '@angular/forms';
import { HtmlEditorModule } from '../html-editor/html-editor.module';
import { HtmlEditorComponent } from '../html-editor/html-editor.component';
import { ElmaCommonModule } from '../../../../shared/common/common.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MobxAngularModule,
        FormsModule,
        ElmaCommonModule,
        HtmlEditorModule,
    ],
    declarations: [PageSettingsComponent],
    exports: [PageSettingsComponent],
    entryComponents: [
        HtmlEditorComponent,
    ]
})
export class PageSettingsModule {
}
