import { Component, EventEmitter, Input, Output, TemplateRef, ViewChild, } from '@angular/core';
import { Router } from '@angular/router';
import { action } from 'mobx-angular';
import { WidgetType } from '../../../app.constants';
import { PageJson, PageWidgetJson } from '../../../models/page.json';
import { PageModel } from '../../../models/page.model';
import { WidgetJson, WidgetPlaceType } from '../../../models/widget.json';
import { WidgetModel } from '../../../models/widget.model';
import { PageStore, PageStoreError } from '../../../stores/page.store';
import { WidgetStore } from '../../../stores/widget.store';
import { HtmlEditorComponent } from '../html-editor/html-editor.component';
import { ElmaModalService, ElmaTranslateService, Option } from '../../../../shared/common';
import { ElmaPopoverComponent } from '../../../../shared/common';

export interface ISystemWidget {
    name: string;
    description: string;
    icon: string;
    type: WidgetType;
}

@Component({
    selector: 'app-page-settings',
    templateUrl: './page-settings.component.html',
    styleUrls: ['./page-settings.component.scss'],
})
export class PageSettingsComponent {

    @Input() creator = false;
    @Input() invisible = false;
    @Input() page: PageModel;
    @Input() widget: PageWidgetJson;
    @Input() widgetNum: number;
    @Output() onWidgetChange = new EventEmitter<PageWidgetJson>();
    @Output() onWidgetCreate = new EventEmitter<PageWidgetJson>();
    @ViewChild('pageMenu') pageMenu: ElmaPopoverComponent;
    @ViewChild('pageContextMenu') pageContextMenu: ElmaPopoverComponent;
    @ViewChild('creatingHtmlWidgetLoading') creatingHtmlWidgetLoading: TemplateRef<any>;
    @ViewChild('chooseWidget') chooseWidget: TemplateRef<any>;

    public customWidgetId: string;

    WidgetType = WidgetType;
    customWidgets: WidgetModel[] = [];

    private _selectedWidgetId = '';
    public get selectedWidgetId(): string {
        return this._selectedWidgetId;
    }
    public set selectedWidgetId(widgetId: string) {
        this._selectedWidgetId = widgetId;
    }
    public get customWidgetsOptionsSelect(): Option[] {

        const selectOptionList: Option[] = [
            {
                label: '',
                value: '',
            }
        ];

        this.customWidgets.forEach(widget => {
            const option = {
                label: widget.name,
                value: widget.id,
            }
            selectOptionList.push(option);
        });

        return selectOptionList;
    }
    systemWidgets: ISystemWidget[] = [];
    public checkedSystemWidget: WidgetType | undefined;

    constructor (
        private ts: ElmaTranslateService,
        public modal: ElmaModalService,
        private widgetStore: WidgetStore,
        public router: Router,
        private pageStore: PageStore,
    ) {
        (async () => {
            this.systemWidgets = [
                {type: WidgetType.Application, icon: 'system_cloud'},
                {type: WidgetType.Feed, icon: 'system_chat_fill'},
                {type: WidgetType.Tasks, icon: 'system_task'},
            ].map((type: {type: WidgetType, icon: string}) => {
                return <ISystemWidget>{
                    name: this.ts.get('app.pages.page-settings@system-widget-name-' + type.type),
                    icon: type.icon,
                    type: type.type,
                    description: this.ts.get('system-widget-description-' + type.type)
                };
            });
        })();
    }

    async loadWidgets () {
        try {
            this.customWidgets = await this.widgetStore.getWidgetsForPage();
        } catch (e) {
            this.customWidgets = [];
            throw e;
        }

    }

    setPage (page: PageModel) {
        this.page = page;
        this.widget = this.page.data.widgets[0];
    }

    @action
    async createWidgetForPageAndEdit() {

        const widget = await this.createEmptyWidget();

        const pageWidgetJson = <PageWidgetJson>{
            type: WidgetType.Custom,
            customWidgetId: widget.id,
            data: {},
        };

        await this.updateWidgetData(pageWidgetJson, this.widgetNum);

        this.router.navigate([
            'admin', 'widgets', 'edit', pageWidgetJson.customWidgetId
        ], {queryParams: {returnUrl: this.router.routerState.snapshot.url}});

        this.onWidgetCreate.emit(pageWidgetJson);
    }

    async createEmptyWidget (): Promise<WidgetModel> {
        const pageParent = await this.page.parent();
        let name: string;
        if (pageParent === this.page) {
            name = this.page.name;
        } else {
            name = `${pageParent.name} / ${this.page.name}`;
        }

        const widgetJson = <WidgetJson>{
            name: name,
            type: WidgetPlaceType.Page,
            description: '',

            html: '',
            styles: '',
            typescript: '',
            javascript: '',

            isCatalog: false,
            isHtml: false,
            published: false,
        };

        return await this.widgetStore.create(widgetJson);
    }

    @action
    async setCustomWidget (widgetNum = this.widgetNum) {
        await this.updateWidgetData(<PageWidgetJson>{
            type: WidgetType.Custom,
            customWidgetId: this.customWidgetId,
            data: {},
        }, widgetNum);
    }

    @action
    async setSystemWidget (widgetNum = this.widgetNum) {
        await this.updateWidgetData(<PageWidgetJson>{
            type: this.checkedSystemWidget,
            customWidgetId: '',
            data: {},
        }, widgetNum);
    }

    async updateWidgetData (pageWidgetJson: PageWidgetJson, widgetNum = this.widgetNum) {
        const pageJSON: PageJson = this.page.toJSON();
        pageJSON.data.widgets[widgetNum] = pageWidgetJson;
        try {
            this.page = await this.pageStore.update(pageJSON);
        } catch (e) {
            if (e === PageStoreError.PageNotFound && this.page.system) {
                this.page = await this.pageStore.create(pageJSON);
            }
        }

        this.onWidgetChange.emit(pageWidgetJson);
    }

    async removeWidget (widgetNum = this.widgetNum) {
        if (!this.widget) {
            console.error('No widget for remove');
            return;
        }
        if (this.widget.type === WidgetType.Custom && this.widget.customWidgetId) {
            const widget = await this.widgetStore.getById(this.widget.customWidgetId);
            if (!widget.isCatalog) {
                try {
                    await this.widgetStore.remove(widget.id);
                } catch (e) {
                    console.error('Deleting widget error', e);
                }
            }
        }

        const pageWidgetJson = <PageWidgetJson>{
            type: WidgetType.Empty,
            customWidgetId: '',
            data: {},
        };
        const pageJSON: PageJson = this.page.toJSON();
        pageJSON.data.widgets[widgetNum] = pageWidgetJson;
        this.page = await this.pageStore.update(pageJSON);
        this.onWidgetChange.emit(pageWidgetJson);
    }

    checkSystemWidget (type: WidgetType) {
        if (this.checkedSystemWidget === type) {
            this.checkedSystemWidget = undefined;
        } else {
            this.checkedSystemWidget = type;
        }
    }

    async openHtmlEditor (_widget?: WidgetModel, hardHtml?: string) {
        const widget = _widget || await this.createEmptyWidget();
        const modalRef = this.modal.open(HtmlEditorComponent, {size: 'lg'});
        const htmlEditor = <HtmlEditorComponent>modalRef.componentInstance;
        htmlEditor.html = hardHtml || widget.html;
        htmlEditor.initialize();
        const saveSubscription = htmlEditor.onChange.subscribe(async (html: string) => {
            modalRef.close();
            try {
                await this.saveHtmlToWidget(widget!.toJSON(), html);
                saveSubscription.unsubscribe();
            } catch (e) {
                // TODO: обработка ошибок компиляции https://git.elewise.com/elma365-frontend/main-app/issues/180
                this.openHtmlEditor(widget, html);
                throw e;
            }
        });
    }

    async saveHtmlToWidget (widgetJson: WidgetJson, html: string) {
        const loadingModalRef = this.modal.open(this.creatingHtmlWidgetLoading);

        try {
            widgetJson.isHtml = true;
            widgetJson.typescript = WidgetModel.getEmptyTypescript(widgetJson.__id!);
            widgetJson.html = html;
            widgetJson.published = false;
            await this.widgetStore.update(widgetJson.__id!, widgetJson);
            widgetJson = await this.widgetStore.compile(widgetJson);
            await this.updateWidgetData({
                type: WidgetType.Custom,
                customWidgetId: widgetJson.__id!,
                data: {}
            });
        } catch (e) {
            throw e;
        } finally {
            loadingModalRef.close();
        }
    }

    async openWidgetEditor () {
        const widget = await this.widgetStore.getById(this.widget.customWidgetId!);
        if (widget.isHtml) {
            this.openHtmlEditor(widget);
        } else {
            this.router.navigate(
                ['/', 'admin', 'widgets', 'edit', this.widget.customWidgetId],
                {queryParams: {returnUrl: this.router.routerState.snapshot.url}}
            )
        }
    }

    async openChooseWidgetDialog () {
        this.customWidgetId = '';
        await this.loadWidgets();
        this.modal.open(this.chooseWidget);
    }
}
