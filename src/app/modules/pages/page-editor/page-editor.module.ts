import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ElmaCommonModule } from '../../../../shared/common/common.module';
import { PageEditorComponent } from './page-editor.component';
import { MobxAngularModule } from 'mobx-angular';
import { NgxDnDModule } from '@swimlane/ngx-dnd';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MobxAngularModule,
        NgxDnDModule,
        ElmaCommonModule,
    ],
    declarations: [PageEditorComponent],
    exports: [PageEditorComponent],
})
export class PageEditorModule {
}
