import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { autorun, IReactionDisposer } from 'mobx';
import { computed, observable } from 'mobx-angular';
import { isNullOrUndefined } from 'util';
import { ElmaActiveModal } from '../../../../shared/common';
import { ElmaPopoverComponent } from '../../../../shared/common/elma-popover';
import { Namespaces, PageType } from '../../../app.constants';
import { PageJson } from '../../../models/page.json';
import { PageModel } from '../../../models/page.model';
import { ProfileService } from '../../../services/profile.service';
import { IPageFilter, PageStore, PageStoreError } from '../../../stores/page.store';
import { UserStore } from '../../../stores/user.store';

@Component({
    selector: 'app-page-editor',
    templateUrl: './page-editor.component.html',
    styleUrls: ['./page-editor.component.scss'],
})
export class PageEditorComponent implements OnInit, OnDestroy {

    PageType = PageType;

    @Input() pagesFilter: IPageFilter | IPageFilter = <IPageFilter>{ namespace: Namespaces.Global };
    @ViewChild('confirmDeletePopover') confirmDeletePopover: ElmaPopoverComponent;

    @observable
    pages = <PageModel[]>[];
    // Поле меняет ngx-dnd-container, поэтому нельзя его делать через observable
    userPages = <PageModel[]>[];

    changedDeletedAtPages: { [key: string]: boolean } = {};

    currentTabActiveCode = 'app.pages.page-editor@tab-active-namespaces';

    private pageIdForDelete: string;
    private autorunDisposer: IReactionDisposer;

    constructor (
        public activeModal: ElmaActiveModal,
        private profileService: ProfileService,
        private userStore: UserStore,
        private pageStore: PageStore,
    ) {
    }

    ngOnInit () {
        if (this.pagesFilter.namespace !== Namespaces.Global) {
            this.currentTabActiveCode = 'app.pages.page-editor@tab-active-applications';
        }

        // Делаем копии моделей страниц из стора - для того, что бы изменения применялись только после сохранения
        this.pages = Array.from(this.pageStore.filterList(this.pagesFilter))
            .map(p => {
                const json = Object.assign({}, p.toJSON());
                return new PageModel(this.pageStore, this.userStore, json);
            });

        this.autorunDisposer = autorun(
            () => {
                this.userPages = Array.from(this.pages)
                    .filter(p => !p.system && !p.deletedAt)
                    .sort(PageStore.orderBySort);
            },
            () => this.pages,
        );
    }

    ngOnDestroy (): void {
        this.autorunDisposer();
    }

    @computed
    get systemPages (): PageModel[] {
        return Array.from(this.pages)
            .filter(p => p.system)
            .sort(PageStore.orderBySort);
    }

    @computed
    get trashPages (): PageModel[] {
        return Array.from(this.pages)
            .filter(p => !p.system && !!p.deletedAt)
            .sort(PageStore.orderBySort);
    }

    private isPageRecovered (page: PageModel): boolean {
        const storePage = this.pageStore.list.find(p => p.id === page.id);
        if (!storePage) {
            throw new Error('Page not found');
        }

        return isNullOrUndefined(page.deletedAt) && !isNullOrUndefined(storePage.deletedAt);
    }

    private isPageDeleted (page: PageModel): boolean {
        const storePage = this.pageStore.list.find(p => p.id === page.id);
        if (!storePage) {
            throw new Error('Page not found');
        }

        return !isNullOrUndefined(page.deletedAt) && isNullOrUndefined(storePage.deletedAt);
    }

    private isPageChanged (page: PageModel): boolean {
        const storePage = this.pageStore.list.find(p => p.id === page.id);
        if (!storePage) {
            throw new Error('Page not found');
        }

        if (this.isPageRecovered(page) || this.isPageDeleted(page)) {
            // Удаленные и восстановленые страницы не считаем за изменившиеся. Таки страницы удаляются отдельным запросом.
            return false;
        }

        return page.sort !== storePage.sort || page.hidden !== storePage.hidden;
    }

    async save () {
        const systemPromises: Promise<PageModel>[] = this.systemPages
            .filter(p => this.isPageChanged(p))
            .map((p: PageModel) => {
                const json: PageJson = p.toJSON();
                return this.pageStore.update(json).catch((e) => {
                    if (e === PageStoreError.PageNotFound) {
                        // По умолчанию, системные страницы не хранятся на сервере.
                        // Но, в случае изменения ее видимости, она целиком создается на сервере.
                        return this.pageStore.create(json);
                    } else {
                        throw e;
                    }
                });
            });

        // Установка нового порядка пользовательских страниц
        this.userPages.forEach((p, index) => p.setSort(index));
        // Страницы пользователей обновляем отдельно от системных
        const userPromises: Promise<any>[] = this.userPages
            .filter(p => this.isPageChanged(p))
            .map(p => this.pageStore.update(p.toJSON()));

        const deletePromises: Promise<any>[] = this.pages
            .filter(p => this.changedDeletedAtPages[p.id])
            .filter(p => this.isPageDeleted(p))
            .map(p => p.delete());

        const recoveryPromises: Promise<any>[] = this.pages
            .filter(p => this.changedDeletedAtPages[p.id])
            .filter(p => this.isPageRecovered(p))
            .map(p => p.recover());

        await Promise.all(userPromises.concat(systemPromises, deletePromises));
        this.activeModal.close();
    }

    getRouterLink (page: PageModel): string[] {
        if (page.namespace === Namespaces.Global) {
            return [page.code];
        }

        return [page.namespace, page.code];
    }

    confirmDelete (event: Event, pageId: string) {
        this.pageIdForDelete = pageId;
        const target = <HTMLElement>event.target;
        this.confirmDeletePopover.hide();
        // Может быть тут дело в позиционировании родителей. Но по другому никак не работает.
        this.confirmDeletePopover.relativeTo = this.findParent('modal-body', target);
        this.confirmDeletePopover.show(target);
    }

    delete () {
        const page = this.userPages.find(p => p.id === this.pageIdForDelete);
        if (!page) {
            throw new Error('Page not found');
        }
        page.setDeleted(this.profileService.userId!);
        this.confirmDeletePopover.hide();
        this.changedDeletedAtPages[page.id] = true;
    }

    recover (page: PageModel) {
        page.setRecovered(this.profileService.userId!);
        this.changedDeletedAtPages[page.id] = true;
    }

    private findParent (className: string, elem: HTMLElement): HTMLElement | null {
        if (!elem.parentElement) {
            return null;
        }

        if (elem.parentElement.classList.contains(className)) {
            return elem.parentElement;
        }

        return this.findParent(className, elem.parentElement);
    }
}
