import { ChangeDetectionStrategy, Component, HostBinding, Input, OnInit, ViewChild } from '@angular/core';
import { computed } from 'mobx-angular';
import { ElmaModalService, ElmaTranslateService } from '../../../../shared/common';
import { Namespaces } from '../../../app.constants';
import { ProfileService } from '../../../services/profile.service';
import { AdminSystemPageCode } from '../../admin/admin.page';
import { PageEditorComponent } from '../../pages/page-editor/page-editor.component';
import { WidgetPageCreateComponent } from '../../pages/widget-page-create/widget-page-create.component';
import { PageModel } from './../../../models/page.model';
import { PageStore } from './../../../stores/page.store';

@Component({
    selector: 'app-navigation-main',
    templateUrl: './navigation-main.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationMainComponent implements OnInit {

    BASE_URL = window.location.origin;
    Namespaces = Namespaces;
    AdminSystemPageCode = AdminSystemPageCode;

    namespace: PageModel | undefined;

    @ViewChild('createPageWidget') createPageWidget: WidgetPageCreateComponent;

    @Input('currentPage') currentPage: PageModel;
    @Input('activePage') activePage: PageModel;

    @HostBinding('class.side-nav') public navClassGlobal = true;

    constructor(
        public pageStore: PageStore,
        public modal: ElmaModalService,
        private ts: ElmaTranslateService,
        private profile: ProfileService,
    ) {
    }

    async ngOnInit() { }

    @computed
    get pages(): PageModel[] {
        const globals = this.pageStore.list
            .filter(p => p.namespace === Namespaces.Global);
        const systems = globals.filter(p => p.system && (p.code !== AdminSystemPageCode || this.profile.hasAdministrationPrivilege))
            .sort(PageStore.orderBySort);
        const namespaces = globals.filter(p => !p.system)
            .sort(PageStore.orderBySort);

        return systems.concat(namespaces).filter(p => !p.hidden && !p.deletedAt);
    }

    openPageEditor () {
        this.modal.open(PageEditorComponent, {title: this.ts.get('app.pages.page-editor@title-namespaces'), size: 'sm'});
    }
}
