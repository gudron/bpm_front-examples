import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ElmaPopupComponent } from '../../../../../shared/common/elma-popup';
import { PageModel } from '../../../../models/page.model';
import { WidgetPageCreateComponent } from '../../../pages/widget-page-create/widget-page-create.component';

@Component({
    selector: 'app-navigation-main-append-namespace',
    templateUrl: 'append-namespace.component.html',
})

export class AppendNamespaceComponent implements OnInit {
    @Input() namespace: string;
    @Input() baseUrl: string;

    @ViewChild('popup') popup: ElmaPopupComponent;
    @ViewChild('createPageWidget') createPageWidget: WidgetPageCreateComponent;

    constructor () {
    }

    ngOnInit () {
    }

    show () {
        this.popup.openPopup();
    }

    createNew () {
        this.createPageWidget.showCreateModal();
        this.close();
    }

    close () {
        this.popup.closePopup();
    }

}
