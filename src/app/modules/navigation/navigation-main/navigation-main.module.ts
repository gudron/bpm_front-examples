import { MobxAngularModule } from 'mobx-angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppendNamespaceComponent } from './append-namespace/append-namespace.component';
import { ItemComponent } from './item/item.component';
import { NavigationMainComponent } from './navigation-main.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { WidgetPageCreateModule } from '../../../modules/pages/widget-page-create/widget-page-create.module';
import { PageEditorModule } from '../../pages/page-editor/page-editor.module';
import { PageEditorComponent } from '../../pages/page-editor/page-editor.component';
import { ElmaCommonModule } from '../../../../shared/common/common.module';

@NgModule({
    imports: [
        CommonModule,
        ElmaCommonModule,
        RouterModule,
        FormsModule,
        MobxAngularModule,
        WidgetPageCreateModule,
        PageEditorModule,
    ],
    declarations: [
        NavigationMainComponent,
        ItemComponent,
        AppendNamespaceComponent,
    ],
    exports: [NavigationMainComponent, ItemComponent],
    entryComponents: [PageEditorComponent],
})
export class NavigationMainModule {
}
