import { ChangeDetectionStrategy, Component, HostBinding, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { computed } from 'mobx-angular';
import { ElmaUIService } from '../../../../../shared/common';
import { Namespaces, NavTypes, PageType } from '../../../../app.constants';
import { ChannelsStore } from '../../../../messages/channel/services/channel.store';
import { ChatStore } from '../../../../messages/chat/services/chat.store';
import { PageModel } from '../../../../models/page.model';
import { ISidebarData, SidebarService } from '../../../../services/sidebar.service';
import { IPageFilter, PageStore } from '../../../../stores/page.store';
import { WidgetPageCreateComponent } from '../../../pages/widget-page-create/widget-page-create.component';

@Component({
    selector: 'app-navigation-main-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemComponent implements OnInit {

    PageType = PageType;

    @Input() page: PageModel;
    @Input() activePage: PageModel;
    @Input() isActive = false;
    @Input() isSub = false;

    @HostBinding('class.side-nav__item') public sideNavitemClass = true;

    @ViewChild('createPageWidget') createPageWidget: WidgetPageCreateComponent;
    @ViewChild('changeNamespaceWidget') changeNamespaceWidget: WidgetPageCreateComponent;

    @computed
    public get unreadCounter (): number {
        if (this.channelsStore.feedUnreadCounter > 0) {
            return this.channelsStore.feedUnreadCounter;
        } else {
            return this.chatStore.chatsWithMessagesCounter;
        }
    }

    BASE_URL = document.URL;

    constructor(
        protected router: Router,
        protected pageStore: PageStore,
        protected channelsStore: ChannelsStore,
        protected chatStore: ChatStore,
        protected sidebarService: SidebarService,
        protected ui: ElmaUIService,
    ) {
    }

    ngOnInit() { }

    get href(): string {
        if (this.page.type === PageType.Link) {
            return this.page.data.redirectTo || '';
        } else {
            return this.page.namespace === Namespaces.Global ? this.page.code : [this.page.namespace, this.page.code].join('/');
        }
    }

    @computed
    get subs(): PageModel[] {
        if (this.isActive && this.page.data.menu && this.page.data.menu.type === NavTypes.NAV_TYPE_MAIN_SUBMENU) {
            return this.pageStore.list.filter(p => p.namespace === this.page.code);
        } else {
            return [];
        }
    }

    activate(e: Event) {
        switch (this.page.type) {
            case PageType.Page:
                e.preventDefault();
                this.navigate();
                break;
            case PageType.Link:
                e.preventDefault();
                if (this.page.system) {
                    return this.navigate();
                }
                if (this.page.data.openInNewTab) {
                    (<any>window).open(this.page.data.redirectTo);
                } else {
                    (<any>window).location.href = this.page.data.redirectTo;
                }
                break;
            case PageType.Namespace:
                e.preventDefault();
                if (this.page.namespace === Namespaces.Global) {
                    if (this.page.data.redirectTo) {
                        this.router.navigate([this.page.code, this.page.data.redirectTo]);
                    } else {
                        if (this.ui.isDesktop) {
                            // Десктоп - берем первое приложение в сортированном списке приложений
                            const apps = this.pageStore
                                .filterList(<IPageFilter>{ namespace: this.page.code })
                                .filter(p => !p.hidden && !p.deletedAt)
                                .sort(PageStore.orderBySort);
                            if (apps.length) {
                                this.router.navigate([this.page.code, apps[0].code]);
                            } else {
                                this.router.navigate([this.page.code]);
                            }
                        } else {
                            // Мобилка - открываем раздел в меню
                            this.sidebarService.setMenu(<ISidebarData> {
                                type: NavTypes.NAV_TYPE_NAMESPACE,
                                page: this.page,
                            });
                        }
                    }
                } else {
                    this.router.navigate([this.page.namespace, this.page.code]);
                }
                break;
        }
    }

    navigate () {
        if (this.page.namespace === Namespaces.Global) {
            if (this.page.data.redirectTo) {
                this.router.navigate([this.page.code, this.page.data.redirectTo]);
            } else {
                this.router.navigate([this.page.code]);
            }
        } else {
            this.router.navigate([this.page.namespace, this.page.code]);
        }
    }
}
