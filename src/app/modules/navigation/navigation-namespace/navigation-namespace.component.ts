import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    HostBinding,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouteQueryParams } from 'angular-route-xxl';
import { autorun, IReactionDisposer } from 'mobx';
import { computed, observable } from 'mobx-angular';
import { Observable } from 'rxjs/Observable';
import { ElmaModalService, ElmaTranslateService } from '../../../../shared/common';
import { ElmaModalRef } from '../../../../shared/common/elma-popup/modal/modal-ref';
import { PageType } from '../../../app.constants';
import { ApplicationModel } from '../../../application/models/application.model';
import { ApplicationConfigFieldsComponent } from '../../../application/modules/config/fields/fields.component';
import { ApplicationStore } from '../../../application/services/application.store';
import { PageWidgetJson } from '../../../models/page.json';
import { PageEditComponent } from '../../../page/edit/edit.component';
import { ProfileService } from '../../../services/profile.service';
import { SidebarService } from '../../../services/sidebar.service';
import { PageEditorComponent } from '../../pages/page-editor/page-editor.component';
import { PageSettingsComponent } from '../../pages/page-settings/page-settings.component';
import { WidgetPageCreateComponent } from '../../pages/widget-page-create/widget-page-create.component';
import { PageModel } from './../../../models/page.model';
import { PageStore } from './../../../stores/page.store';
import { AppendApplicationComponent } from './append-application/append-application.component';
import { ApplicationOptionsComponent } from './application-options/application-options.component';
import { NamespaceOptionsComponent } from './namespace-options/namespace-options.component';

@Component({
    selector: 'app-navigation-namespace',
    templateUrl: './navigation-namespace.component.html',
    styleUrls: ['./navigation-namespace.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationNamespaceComponent implements OnInit, OnChanges, OnDestroy {
    PageType = PageType;

    @Input() namespace: PageModel;
    @Input() activePage: PageModel;

    @ViewChild('appendApplication') appendApplication: AppendApplicationComponent;
    @ViewChild('namespaceOptions') namespaceOptions: NamespaceOptionsComponent;
    @ViewChild('applicationOptions') applicationOptions: ApplicationOptionsComponent;
    @ViewChild('fieldsModal') fieldsModal: ApplicationConfigFieldsComponent;
    @ViewChild('pageSettings') pageSettings: PageSettingsComponent;

    @HostBinding('class.side-nav') public navClassGlobal = true;

    @RouteQueryParams('filterId') filterId$: Observable<string>;
    @observable filterId: string;

    @observable
    public currentApplication: ApplicationModel | undefined;

    private dispose: IReactionDisposer;

    BASE_URL = `${window.location.origin}/${window.location.pathname.split('/')[1]}`;

    constructor (
        private route: ActivatedRoute,
        private cdr: ChangeDetectorRef,
        private pageStore: PageStore,
        private applicationStore: ApplicationStore,
        private modal: ElmaModalService,
        private sidebarService: SidebarService,
        public translateService: ElmaTranslateService,
        public profileService: ProfileService,
    ) {
    }

    async ngOnInit () {
        this.dispose = autorun(
            () => {
                // Для только что созданного приложения отобразим форму редактирования полей
                if (this.justCreatedApp) {
                    this.showFields(this.justCreatedApp, true);
                    this.justCreatedApp.dropJustCreated();
                }
            },
            () => this.justCreatedApp,
        );

        this.filterId$.subscribe(value => (this.filterId = value));
    }

    async ngOnDestroy () {
        this.dispose();
    }

    async ngOnChanges () {
        if (this.activePage.type !== PageType.Application) {
            return;
        }

        this.currentApplication = undefined;

        // Namespace и code берем не из страницы, а из виджета, т.к. они могут отличаться (например, у системных аппов)
        const activeWidget = this.activePage.data.widgets.length ? this.activePage.data.widgets[0] : null;
        if (!activeWidget) {
            throw new Error(`No widgets on page ${this.activePage.namespace }:${ this.activePage.code }`);
        }
        const application =
            await this.applicationStore.getModelByNamespaceAndCode(activeWidget.data.namespaceCode, activeWidget.data.pageCode);
        if (!application) {
            throw new Error(`Application ${activeWidget.data.namespaceCode }:${ activeWidget.data.pageCode } not found`);
        }

        this.currentApplication = application;
        this.cdr.markForCheck();
    }

    @computed
    get applications (): PageModel[] {
        let list = this.pageStore.list
            .filter(p => p.namespace === this.namespace.code && !p.hidden);

        if (!this.namespace.deletedAt) {
            // Покажем удаленные страницы если раздел удален (для админа по сути только)
            list = list.filter(p => !p.deletedAt);
        }

        return list.sort((p1, p2) => p1.sort - p2.sort);
    }

    @computed
    get justCreatedApp (): PageModel | undefined {
        return this.pageStore.list.find(p => p.justCreated);
    }

    showFields (page: PageModel, justCreated?: boolean) {
        this.fieldsModal.show(page, justCreated);
    }

    openPageEditor () {
        const modalRef: ElmaModalRef = this.modal.open(PageEditorComponent, {
            title: this.translateService.get('app.pages.page-editor@title-applications'), size: 'sm',
        });
        const pageEditorComponent: PageEditorComponent = modalRef.componentInstance;
        pageEditorComponent.pagesFilter = { namespace: this.namespace.code };
    }

    hideContextMenu () {
        if (this.pageSettings && this.pageSettings.pageContextMenu && this.pageSettings.pageContextMenu.hide) {
            this.pageSettings.pageContextMenu.hide();
        }

        if (this.applicationOptions && this.applicationOptions.hide) {
            this.applicationOptions.hide();
        }

        if (this.namespaceOptions && this.namespaceOptions.hide) {
            this.namespaceOptions.hide();
        }
    }

    showContextMenu ($event: any, page: PageModel) {
        $event.stopPropagation();
        $event.preventDefault();

        this.hideContextMenu();

        switch (page.type) {
        case PageType.Application:
            this.applicationOptions.show($event, page);
            break;
        case PageType.Page:
            this.pageSettings.setPage(page);
            setTimeout(() => {
                this.pageSettings.pageContextMenu.show($event.target);
            }, 0);
            break;
        default:
            console.error('UNKNOWN CONTEXT MENU', page.type);
        }
    }

    hasContextMenu (page: PageModel) {
        if (!this.profileService.hasAdministrationPrivilege) {
            return false;
        }
        const isActiveApplication = page.type === PageType.Application && page.code === this.activePage.code;
        return page.type === PageType.Page || isActiveApplication;
    }

    changeWidget (pageWidgetJson: PageWidgetJson) {
        if (PageStore.isSamePage(this.pageSettings.page.toJSON(), this.activePage.toJSON())) {
            this.sidebarService.contentUpdated.emit();
        }
    }

    onPageClick () {
        this.sidebarService.close();
    }

    editPage ($event: Event, page: PageModel) {
        $event.stopPropagation();
        const modalRef: ElmaModalRef = this.modal.open(
            PageEditComponent,
            { title: this.translateService.get('app.page.edit@edit-title-' + page.type.toLowerCase()), size: 'sm' },
        );
        const pageEdit: PageEditComponent = modalRef.componentInstance;
        pageEdit.page = page;
    }
}
