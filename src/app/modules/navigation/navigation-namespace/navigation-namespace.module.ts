import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MobxAngularModule } from 'mobx-angular';
import { ApplicationConfigModule } from '../../../application/modules/config/config.module';
import { AppendApplicationComponent } from './append-application/append-application.component';
import { NavigationNamespaceComponent } from './navigation-namespace.component';
import { WidgetPageCreateModule } from '../../../modules/pages/widget-page-create/widget-page-create.module';
import { NamespaceOptionsComponent } from './namespace-options/namespace-options.component';
import { ApplicationOptionsComponent } from './application-options/application-options.component';
import { PageEditorModule } from '../../pages/page-editor/page-editor.module';
import { PageSettingsModule } from '../../pages/page-settings/page-settings.module';
import { ElmaCommonModule } from '../../../../shared/common/common.module';
import { WorkareaComponent } from './workarea/workarea.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        MobxAngularModule,
        WidgetPageCreateModule,
        ApplicationConfigModule,
        PageEditorModule,
        PageSettingsModule,
        MobxAngularModule,
        ElmaCommonModule,
    ],
    declarations: [
        NavigationNamespaceComponent,
        NamespaceOptionsComponent,
        ApplicationOptionsComponent,
        WorkareaComponent,
        AppendApplicationComponent,
    ],
    exports: [
        NavigationNamespaceComponent,
        ApplicationOptionsComponent,
    ],
})
export class NavigationNamespaceModule {
}
