import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ElmaModalService, ElmaTranslateService } from '../../../../../shared/common';
import { ElmaModalRef } from '../../../../../shared/common/elma-popup/modal/modal-ref';
import { PageEditComponent } from '../../../../page/edit/edit.component';
import { PageStore } from '../../../../stores/page.store';
import { NamespaceSettingsComponent } from '../../../namespace/config/settings/settings.component';
import { PageModel } from './../../../../models/page.model';
import { ElmaPopoverComponent } from '../../../../../shared/common/elma-popover';

@Component({
    selector: 'app-namespace-options',
    templateUrl: './namespace-options.component.html',
    styles: [`
        :host {
            display: block;
            position: absolute;
        }
    `],
})
export class NamespaceOptionsComponent implements OnInit {

    constructor (
        private modal: ElmaModalService,
        private translateService: ElmaTranslateService,
        private pageStore: PageStore,
        private router: Router,
    ) {
    }

    @Input() namespace: PageModel;

    @ViewChild('popover') popover: ElmaPopoverComponent;
    @ViewChild('confirmDeletePopover') confirmDeletePopover: ElmaPopoverComponent;

    show (event: any) {
        event.preventDefault();
        this.popover.show(event.target);
    }

    hide () {
        this.popover.hide();
    }

    async ngOnInit () {
    }

    configureNamespace () {

        const modalRef: ElmaModalRef = this.modal.open(
            NamespaceSettingsComponent,
            { title: this.translateService.get('app.pages.widget-page-create.form-namespace@edit-title'), size: 'sm' },
        );
        const namespaceSettings: NamespaceSettingsComponent = modalRef.componentInstance;
        namespaceSettings.page = this.namespace;
        this.hide();
    }


    confirmDelete ($event: any) {
        this.confirmDeletePopover.show($event.target);
    }

    delete () {
        this.pageStore.delete(this.namespace);
        this.router.navigate(['/']);
    }

}
