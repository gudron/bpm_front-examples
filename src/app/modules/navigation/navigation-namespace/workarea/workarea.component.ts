import { ChangeDetectionStrategy, Component, Input, OnChanges, } from '@angular/core';
import { observable } from 'mobx-angular';
import { ApplicationModel } from '../../../../application/models/application.model';
import { FilterModel } from '../../../../application/models/filter.model';
import { SidebarService } from '../../../../services/sidebar.service';

@Component({
    selector: 'app-navigation-workarea',
    templateUrl: './workarea.component.html',
    styleUrls: ['./workarea.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WorkareaComponent implements OnChanges {

    @observable
    @Input() application: ApplicationModel | undefined;

    @observable
    @Input() filterId: string | undefined;

    public newFilterName: string;
    public isEditMode = false;

    constructor (
        private sidebarService: SidebarService,
    ) {
    }

    ngOnChanges (): void {
        this.clearFiltersEditStates();
    }

    async renameFilter(event: Event, filter: FilterModel) {
        event.preventDefault();

        if (this.newFilterName.trim().length === 0) {
            return;
        }

        await filter.rename(this.newFilterName);
        this.clearFiltersEditStates();
    }

    async deleteFilter (filter: FilterModel) {
        await filter.delete();
    }

    toggleEditMode () {
        this.isEditMode = !this.isEditMode;
    }

    startFilterRename(filter: FilterModel) {
        this.clearFiltersEditStates(filter.id);
        this.newFilterName = filter.name;
        filter.isEditMode = true;
    }

    cancelFilterRename() {
        this.clearFiltersEditStates();
    }

    onFilterClick () {
        this.sidebarService.close();
    }

    private clearFiltersEditStates(exceptId?: string) {
        if (!this.application || !this.application.filters) {
            return;
        }

        let filters = this.application!.filters;
        if (exceptId) {
            filters = filters.filter(f => f.id !== exceptId);
        }

        filters.forEach(f => f.isEditMode = false);
    }
}
