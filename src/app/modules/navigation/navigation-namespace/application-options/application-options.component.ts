import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { PageModel } from 'app/models/page.model';
import { ElmaPopoverComponent } from '../../../../../shared/common';
import { ApplicationType } from '../../../../app.constants';
import { ApplicationModel } from '../../../../application/models/application.model';
import { ApplicationConfigFieldsComponent } from '../../../../application/modules/config/fields/fields.component';
import { PageStore } from '../../../../stores/page.store';

@Component({
    selector: 'app-application-options',
    templateUrl: './application-options.component.html',
    styles: [`
        :host {
            display: block;
            position: absolute;
        }
    `],
})
export class ApplicationOptionsComponent {

    @Input() application: ApplicationModel;
    @Output() showFieldsEvent = new EventEmitter<PageModel>();

    @ViewChild('popover') popover: ElmaPopoverComponent;
    @ViewChild('fieldsModal') fieldsModal: ApplicationConfigFieldsComponent;
    @ViewChild('confirmDeletePopover') confirmDeletePopover: ElmaPopoverComponent;

    ApplicationType = ApplicationType;

    page = <PageModel>{};

    constructor (
        private pageStore: PageStore,
        private router: Router,
    ) {

    }

    async show (event: any, page: PageModel) {
        event.preventDefault();

        this.page = page;
        this.popover.hide();
        this.popover.show(event.target);
    }

    hide () {
        if (this.popover && this.popover.hide) {
            this.popover.hide();
        }
        if (this.confirmDeletePopover) {
            this.confirmDeletePopover.hide();
        }
    }

    showFields () {
        this.showFieldsEvent.next(this.page);
        this.hide();
    }

    confirmDelete ($event: any) {
        this.confirmDeletePopover.show($event.target);
    }

    async delete () {

        const page = await this.pageStore.getByNamespaceAndCode(this.application.namespace, this.application.code);
        this.pageStore.delete(page);
        this.router.navigate(['/', this.application.namespace]);
        this.hide();
    }
}
