import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ElmaPopupComponent } from '../../../../../shared/common/elma-popup';
import { PageModel } from '../../../../models/page.model';
import { WidgetPageCreateComponent } from '../../../pages/widget-page-create/widget-page-create.component';

@Component({
    selector: 'app-navigation-namespace-append-application',
    templateUrl: 'append-application.component.html',
})

export class AppendApplicationComponent implements OnInit {
    @Input() namespace: PageModel;
    @Input() baseUrl: string;

    @ViewChild('popup') popup: ElmaPopupComponent;
    @ViewChild('createPageWidget') createPageWidget: WidgetPageCreateComponent;

    constructor () {
    }

    ngOnInit () {
    }

    show () {
        this.popup.openPopup();
    }

    createNew () {
        this.createPageWidget.showCreateModal();
        this.close();
    }

    close () {
        this.popup.closePopup();
    }

}
