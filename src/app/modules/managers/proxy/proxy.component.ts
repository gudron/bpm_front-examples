import { Component, ComponentRef } from '@angular/core';

@Component({
    selector: 'app-proxy',
    template: '<router-outlet (activate)="onActivateProxyRoute($event)" (deactivate)="onDeactivateProxyRoute($event)"></router-outlet>',
})
export class ProxyComponent {

    private _activator: Function = () => {};
    private _deactivator: Function = () => {};

    onActivate(handler: Function) {
        this._activator = handler;
    }

    onActivateProxyRoute(component: ComponentRef<any>) {
        this._activator();
    }

    onDeactivate(handler: Function) {
        this._deactivator = handler;
    }

    onDeactivateProxyRoute(component: ComponentRef<any>) {
        this._deactivator();
    }
}
