import { NgModule } from '@angular/core';
import { ProxyComponent } from './proxy.component';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [ProxyComponent],
    imports: [RouterModule],
    exports: [ProxyComponent]
})
export class ProxyModule {
}
