import { Component, OnDestroy, OnInit, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Namespaces, NavTypes, PageType, WidgetType } from 'app/app.constants';
import { PageModel } from 'app/models/page.model';
import { SidebarService } from 'app/services/sidebar.service';
import { PageStore } from 'app/stores/page.store';
import { Subscription } from 'rxjs/Subscription';
import { SearchService } from '../../../services/search.service';
import { PageWrapperComponent } from '../../pages/page-wrapper/page-wrapper.component';

@Component({
    selector: 'app-page',
    templateUrl: 'page-manager.component.html',
    styleUrls: ['page-manager.component.scss']
})
export class PageManagerComponent implements OnInit, OnDestroy {

    @ViewChildren('pageWrapper') pageWrappers: PageWrapperComponent[];

    PageType = PageType;

    public page: PageModel;
    public customPageCode: string | null = null;

    readonly WidgetType = WidgetType;

    private subscriptions: Subscription[];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private sidebarService: SidebarService,
        private searchService: SearchService,
        public pageStore: PageStore,
    ) {
    }

    mapParams (params: any) {
        const res = {
            namespace: params['namespace'],
            page: params['page'],
            realCode: params['subpage'] || params['page']  || params['namespace'],
            customPage: ''
        };
        if (String(res.page).startsWith('_')) {
            res.page = '';
        }
        if (String(res.namespace).startsWith('_')) {
            res.namespace = '';
        }
        if (res.realCode && String(res.realCode).startsWith('_')) {
            res.customPage = res.realCode
        }
        return res;
    }

    async paramsHandler(params: any) {
        let navType: NavTypes;
        let namespace: PageModel | undefined;

        const p = this.mapParams(params);

        const namespaceCode = p.namespace;
        const pageCode = p.page;
        const realCode = p.realCode;
        this.customPageCode = p.customPage;

        if (namespaceCode) {
            navType = namespaceCode === NavTypes.NAV_TYPE_DISK
                ? NavTypes.NAV_TYPE_DISK : NavTypes.NAV_TYPE_NAMESPACE;

            if (pageCode) {
                namespace = await this.pageStore.getByNamespaceAndCode(Namespaces.Global, namespaceCode);
                this.page = await this.pageStore.getByNamespaceAndCode(namespaceCode, pageCode);
            } else {
                // По факту это может быть не только раздел, но и страница
                // Поэтому получаем страницу и смотрим кто она на самом деле
                this.page = await this.pageStore.getByNamespaceAndCode(Namespaces.Global, namespaceCode);
                if (this.page.data.redirectTo) {
                    return this.router.navigate([this.page.code, this.page.data.redirectTo]);
                }
                if (this.page.type === PageType.Namespace) {
                    namespace = this.page
                } else {
                    navType = NavTypes.NAV_TYPE_MAIN
                }
            }
        } else {
            navType = NavTypes.NAV_TYPE_MAIN;
            this.page = await this.pageStore.getByNamespaceAndCode(Namespaces.System, NavTypes.NAV_TYPE_MAIN);
        }

        this.searchService.componentChanged.emit({});
        this.sidebarService.setComponent({
            type: navType,
            data: { namespace },
            page: this.page,
        });
    }

    async updateContent() {
        this.page = await this.pageStore.getByNamespaceAndCode(this.page.namespace, this.page.code); // get
        this.pageWrappers.forEach((wrapper, index) => {
            wrapper.changeWidget(this.page.data.widgets[index]);
        });
    }

    ngOnInit() {
        this.subscriptions = [
            this.activatedRoute.params.subscribe(this.paramsHandler.bind(this)),
            this.sidebarService.contentUpdated.subscribe(this.updateContent.bind(this))
        ]
    }

    ngOnDestroy () {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    get columns(): number[] {
        if (!this.page) {
            return [];
        }

        if (this.page.data.columns === 1) {
            return [0];
        } else if (this.page.data.columns === 2) {
            return [0, 1];
        } else {
            return [];
        }
    }
}
