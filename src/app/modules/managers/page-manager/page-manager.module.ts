import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MobxAngularModule } from 'mobx-angular';
import { ApplicationConfigModule } from '../../../application/modules/config/config.module';
import { ExternalWidgetModule } from '../../../components/external-widget/external-widget.module';
import { AppviewModule } from '../../../application/modules/appview/appview.module';
import { ExchangeModule } from '../../../exchange/exchange.module';
import { NamespaceModule } from '../../namespace/namespace.module';
import { PageSettingsModule } from '../../pages/page-settings/page-settings.module';
import { PageWrapperComponent } from '../../pages/page-wrapper/page-wrapper.component';
import { TaskListModule } from '../../system-widgets/tasks/tasks.module';
import { WidgetHtmlContentModule } from '../../system-widgets/widget-html-content/widget-html-content.module';
import { PageManagerComponent } from './page-manager.component';
import { ElmaCommonModule } from '../../../../shared/common/common.module';

@NgModule({
    imports: [
        CommonModule,
        ElmaCommonModule,
        FormsModule,
        WidgetHtmlContentModule,
        RouterModule,
        MobxAngularModule,
        NamespaceModule,
        ExternalWidgetModule,
        AppviewModule,
        PageSettingsModule,
        TaskListModule,
        ApplicationConfigModule,
        ExchangeModule,
    ],
    declarations: [
        PageWrapperComponent,
        PageManagerComponent,
    ],
    entryComponents: [],
    exports: [
        PageManagerComponent,
    ],
})
export class PageManagerModule {
}
