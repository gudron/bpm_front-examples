import { Component, HostBinding, HostListener, OnInit } from '@angular/core';
import { Namespaces, NavTypes } from '../../../app.constants';
import { PageModel } from '../../../models/page.model';
import { ProfileService } from '../../../services/profile.service';
import { ISidebarData, SidebarService } from '../../../services/sidebar.service';
import { PageStore } from '../../../stores/page.store';

const LOCALSTORAGE_SIDE_WIDTH_KEY = 'SIDEBAR_WIDTH';
const LOCALSTORAGE_SIDE_SIZE = 'SIDEBAR_SIZE';

const centeringIndentationHolder = 10;
const maxWidthSidebar = window.innerWidth - 700;
const minWidthSidebar = 150;

export enum SidebarColor {
    White = '@white',
    Blue = '@blue',
}

export enum SidebarSize {
    Small = 'small',
    Full = 'full',
    Draggable = 'draggable',
}

@Component({
    selector: 'app-nav-manager',
    templateUrl: 'nav-manager.component.html',
    styleUrls: ['nav-manager.component.scss']
})
export class NavManagerComponent implements OnInit {

    public page: PageModel;
    public activePage: PageModel;

    NavTypes = NavTypes;

    public isDragging = false;

    @HostBinding('class') get classes(): string {
        return [
            this.sidebarColor,
            this.sidebarSize,
        ].join(' ');
    }
    private sidebarColor: SidebarColor = SidebarColor.White;
    private sidebarSize: SidebarSize = SidebarSize.Full;

    @HostBinding('style.width.px') draggableWidth: number | null;

    @HostListener('document: mousemove', ['$event'])
    public sidebarDrag (event: MouseEvent) {

        if (this.isDragging && maxWidthSidebar >= event.x && event.x >= minWidthSidebar) {
            this.draggableWidth = event.x - centeringIndentationHolder;
        }
    }

    @HostListener('document: mouseup', ['$event'])
    public sidebarDragEnd(event: MouseEvent) {

        if (this.isDragging && maxWidthSidebar >= event.x && event.x >= minWidthSidebar) {
            this.draggableWidth = event.x - centeringIndentationHolder;
            localStorage.setItem(LOCALSTORAGE_SIDE_WIDTH_KEY, event.x.toString());
        }

        this.isDragging = false;
    }

    constructor(
        private sidebarService: SidebarService,
        public profileService: ProfileService,
        private pageStore: PageStore,
    ) {
        this.page = this.pageStore.mainPage;

        // Инициализируем состояние сайдбара именно тут, т.к. суть сайдбара - отображение меню
        this.sidebarService.setComponent(<ISidebarData>{
           type: NavTypes.NAV_TYPE_MAIN,
           page: this.page!,
        });

        if (this.sidebarService.currentState) {
            this.componentChangedHandler.call(this, this.sidebarService.currentState);
        }
    }

    async componentChangedHandler(data: ISidebarData) {
        if (data.page == null) {
            return;
        }
        setTimeout(async () => {
            this.activePage = data.page;
            this.page = await data.page.parent();
            if ([NavTypes.NAV_TYPE_MAIN, NavTypes.NAV_TYPE_MAIN_SUBMENU].indexOf(this.page.data.menu!.type) > -1) {
                this.sidebarColor = SidebarColor.Blue;
            } else {
                this.sidebarColor = SidebarColor.White;
            }

            if (this.page.data.menu!.type === NavTypes.NAV_TYPE_DISK) {
                this.sidebarSize = SidebarSize.Draggable;
                if (localStorage.getItem(LOCALSTORAGE_SIDE_WIDTH_KEY)) {
                    this.draggableWidth = +localStorage.getItem(LOCALSTORAGE_SIDE_WIDTH_KEY)!;
                }
            } else {
                if (localStorage.getItem(LOCALSTORAGE_SIDE_SIZE) && localStorage.getItem(LOCALSTORAGE_SIDE_SIZE) === SidebarSize.Small) {
                    this.sidebarSize = SidebarSize.Small;
                } else {
                    this.sidebarSize = SidebarSize.Full;
                }
                this.draggableWidth = null;
            }
        }, 0);
    }

    ngOnInit() {
        this.sidebarService.menuChanged.subscribe(this.componentChangedHandler.bind(this));
    }

    homeClick() {
        const currentPage = this.sidebarService.currentState ? this.sidebarService.currentState.page : undefined;
        if (currentPage && this.sidebarService.menuState && currentPage !== this.sidebarService.menuState.page) {
            // Открываем главное меню принудительно, если сейчас уже URL главной страницы
            this.sidebarService.setMenu(this.sidebarService.currentState!);
        } else if (currentPage && currentPage.namespace === Namespaces.System && currentPage.code === 'main') {
            // Закрываем меню, если меню в данный момент на главной
            this.sidebarService.close();
        }
    }

    public resizeSidebar() {
        this.sidebarSize = this.sidebarSize === SidebarSize.Full ? SidebarSize.Small : SidebarSize.Full;
        localStorage.setItem(LOCALSTORAGE_SIDE_SIZE, <string>this.sidebarSize);
    }

    public sidebarDrugStart (event: MouseEvent): void {
        event.preventDefault();
        event.stopPropagation();
        this.isDragging = true;
    }

}
