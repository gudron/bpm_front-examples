import { Component } from '@angular/core';
import { ElmaModalService, ElmaTranslateService } from '../../../../../shared/common';
import { BPMPublicComponent } from '../../../../bpm/modules/public/public.component';

@Component({
    selector: 'app-gagarin',
    templateUrl: './gagarin.component.html',
    styleUrls: ['./gagarin.component.scss'],
})
export class GagarinComponent {

    constructor (
        private modalService: ElmaModalService,
        private ts: ElmaTranslateService
    ) {
    }

    openProcessRunModal() {
        this.modalService.open(BPMPublicComponent, {title: this.ts.get('bpm.public@title'), size: 'xs'});
    }
}
