import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MobxAngularModule } from 'mobx-angular';
import { ElmaCommonModule } from '../../../../../shared/common';

import { GagarinComponent } from './gagarin.component';
import { BPMPublicModule } from '../../../../bpm/modules/public/public.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        ElmaCommonModule,
        MobxAngularModule,

        BPMPublicModule,
    ],
    declarations: [
        GagarinComponent,
    ],
    providers: [],
    exports: [
        GagarinComponent,
    ],
})
export class GagarinModule {
}
