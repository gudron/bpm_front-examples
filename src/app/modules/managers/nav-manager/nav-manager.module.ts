import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MessagesModule } from '../../../messages/messages.module';
import { AppSharedModule } from '../../../shared/shared.module';
import { DiskSidebarModule } from '../../disk/disk-sidebar/disk-sidebar.module';
import { MonitorSidebarModule } from '../../monitor/monitor-sidebar/monitor-sidebar.module';
import { NavigationMainModule } from '../../navigation/navigation-main/navigation-main.module';
import { NavigationNamespaceModule } from '../../navigation/navigation-namespace/navigation-namespace.module';
import { GagarinModule } from './gagarin/gagarin.module';
import { NavManagerComponent } from './nav-manager.component';
import { ElmaCommonModule } from '../../../../shared/common/common.module';

@NgModule({
    imports: [
        CommonModule,
        NavigationMainModule,
        DiskSidebarModule,
        MonitorSidebarModule,
        NavigationNamespaceModule,
        RouterModule,
        GagarinModule,
        MessagesModule,
        ElmaCommonModule,
        AppSharedModule,
    ],
    declarations: [
        NavManagerComponent,
    ],
    exports: [NavManagerComponent],
})
export class NavManagerModule {
}
