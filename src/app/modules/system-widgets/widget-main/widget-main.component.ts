import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-widget-main',
  templateUrl: './widget-main.component.html',
  styleUrls: ['./widget-main.component.scss']
})
export class WidgetMainComponent implements OnInit {

  constructor(
      private router: Router,
      private activatedRoute: ActivatedRoute,
  ) {
      if (!this.activatedRoute.snapshot.url.length) {
          this.router.navigate(['/main']);
      }
  }

  ngOnInit() {
  }

}
