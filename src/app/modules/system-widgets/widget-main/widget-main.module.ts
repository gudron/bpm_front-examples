import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetMainComponent } from './widget-main.component';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
    ],
    declarations: [WidgetMainComponent],
    exports: [WidgetMainComponent],
})
export class WidgetMainModule {
}
