import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WidgetHtmlContentComponent } from './widget-html-content.component';
import { ElmaCommonModule } from '../../../../shared/common/common.module';

@NgModule({
    imports: [
        CommonModule,
        ElmaCommonModule,
        FormsModule,
    ],
    declarations: [WidgetHtmlContentComponent],
    exports: [WidgetHtmlContentComponent],
})
export class WidgetHtmlContentModule {
}
