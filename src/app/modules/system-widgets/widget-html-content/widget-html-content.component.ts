import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-widget-html-content',
    templateUrl: 'widget-html-content.component.html',
    styleUrls: ['widget-html-content.component.scss']
})
export class WidgetHtmlContentComponent implements OnInit {

    @Input() data: any;

    constructor(
    ) {
    }

    ngOnInit() {
    }
}
