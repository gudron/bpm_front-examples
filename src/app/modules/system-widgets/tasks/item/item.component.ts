import { Location } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ElmaTranslateService } from '../../../../../shared/common';
import { Button } from '../../../../../shared/common/buttons';
import { ElmaPopoverComponent } from '../../../../../shared/common/elma-popover';
import { WidgetInstance, WidgetTemplateJson } from '../../../../../widget';
import { WidgetTemplateBuilder } from '../../../../../widget/services/widget-template-builder';
import { NULL_UUID } from '../../../../app.constants';
import { TemplateExecModel } from '../../../../bpm/models';
import { Exit } from '../../../../bpm/models/template-exec.model';
import { UserTaskSettings } from '../../../../bpm/schema';
import { BPTemplateStore } from '../../../../bpm/services/bptemplate.store';
import { TaskModel, TaskState } from '../../../../bpm/models/task.model';
import { ApplicationStore } from '../../../../application/services/application.store';
import { TaskForm } from '../../../../bpm/widgets/templates/task-form';
import { ItemForm } from '../../../../widgets/descriptors/item-form';
import { ItemFormPopup } from '../../../../widgets/descriptors/item-form-popup';
import { DynamicFormComponent, Field } from '../../../dynamic-form/dynamic-form.component';

@Component({
    selector: 'app-widget-tasks-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskItemComponent implements OnInit {
    task: TaskModel;
    template: TemplateExecModel;
    exits: Exit[];
    namespace: string;
    fields: Field[];
    taskSettings: UserTaskSettings;

    // Шаблон формы
    formTemplate: WidgetTemplateJson | undefined;
    // Данные для виджета формы
    widgetData: TaskForm.Data;
    // Экземпляр виджета формы
    widgetInstance: WidgetInstance;

    // Выбранное действие (используется для подтверждения)
    exit: Exit;
    // Основной компонент формы
    form: DynamicFormComponent | undefined;

    @ViewChild('confirmForm') confirmForm: DynamicFormComponent;
    @ViewChild('confirmPopover') confirmPopover: ElmaPopoverComponent;

    constructor (
        private location: Location,
        private activatedRoute: ActivatedRoute,
        private templateStore: BPTemplateStore,
        private translationService: ElmaTranslateService,
        private cdr: ChangeDetectorRef,
        private appStore: ApplicationStore,
        private builder: WidgetTemplateBuilder,
    ) {
    }

    async ngOnInit () {
        this.activatedRoute.data.subscribe((data: { task: TaskModel }) => {
            this.task = data.task;
        });

        const instance = this.task.instance;
        const templateJson = await this.templateStore.getPublicTemplateByID(instance.__template!.id, instance.__template!.version);
        this.template = new TemplateExecModel(this.templateStore, this.appStore, this.translationService, templateJson);
        this.fields = await this.template.userFormFields(this.task.path);
        this.namespace = this.template.namespace;
        this.taskSettings = this.template.process.userTask(this.task.path);

        // Определяем шаблон формы
        if (this.taskSettings.formCode) {
            this.formTemplate = this.template.forms.filter(f => f.code === this.taskSettings.formCode)[0];
        }
        if (!this.formTemplate) {
            this.formTemplate = TaskForm.getTemplate(this.builder).toJson();
        }

        // Данные для шаблона формы
        this.widgetData = <TaskForm.Data>{
            context: instance,
            fields: this.fields,
            task: this.task,
        };

        this.cdr.markForCheck();

        // Форма задачи отрисовывается только после заполнения полей, но передаётся в кнопки,
        // так что приходится делать такой вот финт ушами.
        setTimeout(async () => {
            await this.updateExits();

            this.cdr.markForCheck();
        });
    }

    onWidgetInit (instance: WidgetInstance) {
        this.widgetInstance = instance;

        const controller = <ItemForm.Controller>instance.controller;
        if (!controller || !controller.open) {
            throw new Error('No controller for form template');
        }

        // Устанавливаем заголовок
        if (controller instanceof ItemFormPopup.Controller) {
            (<ItemFormPopup.Controller>controller).title = this.task.name;
        }

        // Подписываемся на события попапа, и открываем его
        controller.onClose.subscribe(() => {
            this.close();
        });
        controller.open();

        // Инициализируем кнопки
        this.updateWidgetButtons();

        // Ищем первую форму. В целом это нехорошо. Надо будет продумать, когда будет несколько виджетов, работающих с формой.
        this.form = TaskItemComponent.findForm(this.widgetInstance);
    }

    async done (exitId: string, data: { [key: string]: any }) {
        if (await this.task.submit(exitId, data)) {
            this.close();
        } else {
            await this.updateExits();
            this.cdr.markForCheck();
        }
    }

    close () {
        this.location.back();
    }

    private async updateExits () {
        if (this.task.state === TaskState.Closed) {
            this.exits = [];

            return;
        }
        this.exits = await this.template.exits(this.task.path);
        if (this.task.state === TaskState.Assignment) {
            this.exits.unshift({
                id: NULL_UUID,
                name: this.translationService.get('app.system-widgets.tasks.item@take-in-work'),
                settings: {
                    buttonClass: 'primary',
                    skipCheck: true,
                    confirm: 'none',
                    confirmText: '',
                    formFields: [],
                    popoverSize: 'default',
                    interrupt: false,
                },
            });
        }
        this.updateWidgetButtons();
    }

    updateWidgetButtons () {
        if (!this.widgetInstance || !this.widgetInstance.updateData || !this.exits) {
            return;
        }
        const buttons: Button[] = [];
        for (const exit of this.exits) {
            buttons.push(<Button>{
                label: exit.name,
                action: async ($event) => this.actionClick(exit, $event),
            });
        }
        setTimeout(() => {
            const updateData = <TaskForm.Data>{
                actions: buttons,
            };
            this.widgetInstance.updateData(updateData);
            this.cdr.markForCheck();
        });
    }

    async actionClick (exit: Exit, $event: any) {
        if (!exit.settings.skipCheck && this.form && !this.form.valid) {
            // По сути просто отметит поля с ошибками и установит курсор в первое невалидное
            this.form.submit();
            return;
        }

        if (exit.settings.confirm === 'none' || !exit.settings.confirm) {
            // Переход без подтверждения
            await this.done(exit.id, this.form ? this.form.value : {});
            return;
        }

        // Подтверждение
        this.exit = exit;
        if (this.confirmForm && this.form) {
            this.confirmForm.init(this.form.value);
        }
        this.cdr.markForCheck();
        setTimeout(() => this.confirmPopover.toggle($event.target));
    }

    static findForm(instance: WidgetInstance): DynamicFormComponent | undefined {
        const dynamicForms = instance.components ? instance.components.filter(c =>
            c.instance instanceof DynamicFormComponent) : undefined;
        if (dynamicForms && dynamicForms.length > 0) {
            return <DynamicFormComponent>dynamicForms[0].instance;
        }
        if (instance.childInstances) {
            for (const childInstance of instance.childInstances) {
                const form = TaskItemComponent.findForm(childInstance);
                if (form) {
                    return form;
                }
            }
        }
        return undefined;
    }

    async confirmAction () {
        const mergedFields = this.form ? Object.assign({}, this.form.value, this.confirmForm.value) : this.confirmForm.value;
        await this.done(this.exit.id, mergedFields);
    }
}
