import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetModule } from '../../../../widget';
import { AppSharedModule } from '../../../shared/shared.module';
import { AppviewListModule } from '../../../application/modules/appview/list/list.module';
import { UserSelectModule } from '../../../shared/user-select/user-select.module';
import { TaskListComponent } from './list.component';
import { RouterModule } from '@angular/router';
import { MobxAngularModule } from 'mobx-angular';
import { TaskStore } from '../../../bpm/services/task.store';
import { TaskItemComponent } from './item/item.component';
import { DynamicFormModule } from '../../dynamic-form/dynamic-form.module';
import { ReassignTaskButtonComponent } from './reassign-button/reassign-button.component';
import { SearchComponent } from './search/search.component';
import { SearchModule } from './search/search.module';
import { TaskResolver } from './task.resolver';
import { TaskIconComponent } from './templates/task-icon.component';
import { TaskTileComponent } from './tile/task.component';
import { FormsModule } from '@angular/forms';
import { ElmaCommonModule } from '../../../../shared/common';

@NgModule({
    imports: [
        CommonModule,
        ElmaCommonModule,
        RouterModule,
        MobxAngularModule,
        DynamicFormModule,
        FormsModule,
        AppviewListModule,
        SearchModule,
        AppSharedModule,
        WidgetModule,
        UserSelectModule,
        RouterModule.forChild([
            {
                path: 'task/:id',
                component: TaskItemComponent,
                outlet: 'p',
                resolve: {
                    task: TaskResolver,
                }
            },
        ]),

    ],
    entryComponents: [
        SearchComponent,
        TaskIconComponent,
        ReassignTaskButtonComponent,
    ],
    providers: [
        TaskStore,
        TaskResolver,
    ],
    declarations: [
        TaskListComponent,
        TaskTileComponent,
        TaskItemComponent,
        TaskIconComponent,
        ReassignTaskButtonComponent,
    ],
    exports: [
        TaskListComponent,
        TaskTileComponent,
    ],
})
export class TaskListModule {
}
