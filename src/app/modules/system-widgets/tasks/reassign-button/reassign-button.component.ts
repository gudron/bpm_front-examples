import { Component, Input, ViewChild } from '@angular/core';
import { ElmaNotificationService, ElmaTranslateService } from '../../../../../shared/common';
import { ElmaPopoverComponent } from '../../../../../shared/common/elma-popover';
import { ComponentWidget } from '../../../../../widget/metadata';
import { TaskModel } from '../../../../bpm/models/task.model';
import { TaskStore } from '../../../../bpm/services/task.store';
import { UserSelectComponent } from '../../../../shared/user-select/user-select.component';
import * as moment from 'moment';

@Component({
    selector: 'app-reassign-task-button',
    templateUrl: './reassign-button.component.html',
    styleUrls: ['./reassign-button.component.scss'],
})
@ComponentWidget({
    code: 'reassign-task-button',
    name: 'Reassign task button',
    fields: [
        { code: 'task', type: 'TASK' },
    ],
})
export class ReassignTaskButtonComponent {
    @Input() task: TaskModel;

    @ViewChild('userSelect') userSelect: UserSelectComponent;
    @ViewChild('reassignTask') reassignTask: ElmaPopoverComponent;

    reassignUserIds: string[];
    reassignDate = '';
    reassignComment = '';

    constructor (
        private taskStore: TaskStore,
        private notificationService: ElmaNotificationService,
        private translationService: ElmaTranslateService,
    ) {
    }

    openReassign ($event: any) {
        this.reassignTask.toggle($event.currentTarget);
        this.userSelect.setFocus();
    }

    async submitReassign () {
        const instanceId = this.task.instance.__id;

        await this.taskStore.reassignTask(instanceId,
            this.task.id,
            this.reassignUserIds[0],
            moment(this.reassignDate).unix(),
            this.reassignComment
        );

        const title = this.translationService.get('app.system-widgets.tasks.item@reassign-success-title');
        const message = this.translationService.get('app.system-widgets.tasks.item@reassign-success-message');

        this.reassignTask.hide(false);

        this.notificationService.success(message, title);
    }
}
