import { ChangeDetectorRef, Component, HostListener, Input, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { observable } from 'mobx-angular';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { ElmaTranslateService, ElmaUIService } from '../../../../../shared/common';
import { SearchItem } from '../../../../messages/header-search/header-search.component';
import { ElasticQueryStringService } from '../../../../services/elastic-query-string.service';
import { UserStore } from '../../../../stores/user.store';
import { ApplicationModel } from '../../../../application/models/application.model';
import { ApplicationStore } from '../../../../application/services/application.store';
import { FilterModel } from '../../../../application/models/filter.model';
import { FormFieldModel } from '../../../../application/models/form-field.model';
import { SearchAction, SearchEvent, SearchService } from '../../../../services/search.service';

@Component({
    selector: 'app-tasks-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit, OnDestroy {

    @Input() application: ApplicationModel;
    @Input() fields: FormFieldModel[] = [];
    @observable
    @Input() filter: FilterModel | undefined = undefined;
    @Input() searchSubject: Subject<SearchEvent>;

    @ViewChild('itemTemplate') itemTemplate: TemplateRef<any>;
    @ViewChild('textSearchTemplate') textSearchTemplate: TemplateRef<any>;

    public searchString: string;
    public filteredFieldsCount = 0;
    public advancedSearchValues: { [key: string]: any } = {};
    public placeHolder: string;

    public selectedUser: any;
    public items: SearchItem[] = [];
    public searchError: string;
    public showResults = false;
    public isMobile: boolean;

    private searchStringChanged: Subject<string> = new Subject<string>();
    private searchSubjectSubscription: Subscription;
    private searchLimit = 4;
    private isMobileSubscription: Subscription;

    constructor (
        private router: Router,
        private translateService: ElmaTranslateService,
        private applicationStore: ApplicationStore,
        private searchService: SearchService,
        private userStore: UserStore,
        private cdr: ChangeDetectorRef,
        private ui: ElmaUIService,
    ) {
        this.isMobileSubscription = ui.isMobileObserver.subscribe((value: boolean) => this.isMobile = value);
    }

    ngOnInit () {
        if (this.filter) {
            this.advancedSearchValues = this.filter.values;
        }
        this.placeHolder = this.translateService.get('bpm.tasks@search-placeholder');

        this.searchSubjectSubscription = this.searchSubject.subscribe((event: SearchEvent) => {
            if (event.action === SearchAction.stop) {
                this.clear();
            }
        });
    }

    ngOnDestroy (): void {
        if (this.isMobileSubscription) {
            this.isMobileSubscription.unsubscribe();
        }
    }

    async change (term: string) {
        if (this.selectedUser === undefined) {
            const users = await this.userStore.getByTerm(term, this.searchLimit);
            this.items = users.map((r: any) => {
                const user: SearchItem = {
                    template: this.itemTemplate,
                    data: r,
                    additional: false,
                };
                user.data.searchItemType = 'searchElement';
                return user;
            });

            this.items.push(<SearchItem>{
                template: this.textSearchTemplate,
                data: {
                    name: this.translateService.get('bpm.tasks@search-result-link'),
                },
                additional: true
            });
            this.showResults = true;
        }
    }

    public start (event: Event) {
        const user = this.items.find(item => { return item.selected === true && !item.additional });
        if (user) {
            this.selectUser(user.data);
        } else {
            this.showResults = false;
            this.searchSubject.next({
                term: ElasticQueryStringService.wildcardifyQuery(this.searchString),
                filter: (this.selectedUser === undefined ? undefined : this.selectedUser.id),
                action: SearchAction.start,
            });
        }
    }

    public clear () {
        this.selectedUser = undefined;
        this.searchString = '';
        this.searchStringChanged.next();
        this.items = [];
        this.showResults = false;
        this.cdr.markForCheck();
    }

    public selectUser (user: any) {
        this.selectedUser = user;
        this.searchString = '';
        this.items = [];
        this.showResults = false;
        this.cdr.markForCheck();
        this.next();
    }

    public closeFilter () {
        this.selectedUser = undefined;
        this.showResults = false;
        this.next();
    }

    next () {
        this.searchSubject.next({
            term: ElasticQueryStringService.wildcardifyQuery(this.searchString),
            filter: (this.selectedUser !== undefined ? this.selectedUser.id : undefined),
            action: SearchAction.next,
        });
    }
    stop () {
        this.searchSubject.next({
            term: undefined,
            filter: undefined,
            action: SearchAction.stop,
        });
    }
    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent (event: KeyboardEvent) {
        if (this.showResults) {
            switch (event.code) {
                case 'ArrowDown':
                    this.selectNextItem();
                    event.preventDefault();
                    break;
                case 'ArrowUp':
                    this.selectNextItem(true);
                    event.preventDefault();
                    break;
            }
        }
        if (event.keyCode === 8) {
            if (!this.searchString && this.selectedUser) {
                this.clear();
            }
        }
    }
    selectNextItem(reverse = false) {
        let itemIndex = undefined;
        for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].selected) {
                itemIndex = i;
                break;
            }
        }
        if (itemIndex === undefined) {
            this.items[0].selected = true;
        } else {
            this.items[itemIndex].selected = false;
            const nextItem = (reverse ? itemIndex - 1 : itemIndex + 1);
            if (nextItem < 0) {
                this.items[this.items.length - 1].selected = true;
            } else if (nextItem > this.items.length - 1) {
                this.items[0].selected = true;
            } else {
                this.items[nextItem].selected = true;
            }

        }
    }
}
