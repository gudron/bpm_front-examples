import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MobxAngularModule } from 'mobx-angular';
import { ElmaCommonModule } from '../../../../../shared/common';
import { AppCommonModule } from '../../../../common/common.module';
import { SearchComponent } from './search.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,

        MobxAngularModule,

        AppCommonModule,
        ElmaCommonModule,
    ],
    exports: [SearchComponent],
    declarations: [
        SearchComponent,
    ],
})

export class SearchModule {
}
