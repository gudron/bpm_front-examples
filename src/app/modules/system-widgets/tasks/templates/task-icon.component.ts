import { Component, Input } from '@angular/core';
import { FieldTemplate } from '../../../../application/schema/field-template';
import { FieldModel } from '../../../../application/models/field.model';
import { ItemModel } from '../../../../application/models/item.model';

@Component({
    selector: 'app-task-icon',
    templateUrl: './task-icon.component.html',
})
export class TaskIconComponent implements FieldTemplate {
    @Input() field: FieldModel;
    @Input() item: ItemModel;

    constructor (
    ) {
    }

}
