import { Component, Injectable, Input, OnInit } from '@angular/core';
import { TaskModel } from '../../../../bpm/models/task.model';
import { UserStore } from '../../../../stores/user.store';
import { UserModel } from '../../../../models/user.model';
import { computed, observable } from 'mobx-angular';

@Component({
    selector: 'app-widget-tasks-list-tile',
    templateUrl: './task.component.html',
    styleUrls: ['./task.component.scss']
})

export class TaskTileComponent implements OnInit {
    @Input() task: TaskModel;
    user: UserModel;

    constructor (
        private userStore: UserStore
    ) {}

    async ngOnInit () {
        this.user = await this.userStore.getByID(this.task.createdBy);
    }
}
