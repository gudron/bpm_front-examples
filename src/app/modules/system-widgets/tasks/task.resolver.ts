import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { TaskModel } from '../../../bpm/models/task.model';
import { TaskStore } from '../../../bpm/services/task.store';

@Injectable()
export class TaskResolver implements Resolve<TaskModel> {

    constructor (
        private location: Location,
        private taskStore: TaskStore,
    ) {
    }

    async resolve (
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Promise<TaskModel> {
        try {
            const id = route.paramMap.get('id');
            if (id == null) {
                throw new Error('id not found');
            }
            return await this.taskStore.getTaskByID(id);
        } catch (err) {
            this.location.back();

            throw err;
        }
    }
}
