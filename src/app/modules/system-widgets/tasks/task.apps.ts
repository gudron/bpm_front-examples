import { DatetimeTypes, FieldJson, FieldViewJson } from '../../../models/field.json';
import { ElmaTranslateService } from '../../../../shared/common';
import { uuid4 } from '../../../../shared/utils/uuid';
import { FieldType, Namespaces, SystemCollections } from '../../../app.constants';
import { ApplicationJson } from '../../../application/schema/application.json';

export function GetTasksSystemApplicationJson (translationService: ElmaTranslateService): ApplicationJson {
    return <ApplicationJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.applications.tasks@name'),
        elementName: translationService.get('system.applications.tasks@elementName'),
        namespace: Namespaces.System,
        code: SystemCollections.Tasks,
        isSystem: true,
        fields: [
            <FieldJson>{
                code: 'type',
                type: FieldType.Template,
                view: <FieldViewJson>{
                    name: '',
                },
            },
            <FieldJson>{
                code: 'name',
                type: FieldType.String,
                indexed: false,
                required: false,
                searchable: true,
                view: <FieldViewJson>{
                    name: translationService.get('system.tasks.fields@name'),
                    key: true,
                },
            },
            <FieldJson>{
                code: '__createdAt',
                type: FieldType.Datetime,
                indexed: false,
                required: false,
                view: <FieldViewJson>{
                    name: translationService.get('system.tasks.fields@created_at'),
                    key: false,
                    data: {additionalType: DatetimeTypes.Date}
                },
            },
            // Обработчик напишем позже
            /* <FieldJson>{
             code: '__createdBy',
             type: FieldType.SystemUser,
             indexed: false,
             required: false,
             view: <FieldViewJson>{
             name: translationService.get('system.tasks.fields@created_by'),
             key: false,
             },
             }, */
        ]
    };
}

export function GetTaskAppsJson (translationService: ElmaTranslateService): ApplicationJson[] {
    return [GetTasksSystemApplicationJson(translationService)];
}
