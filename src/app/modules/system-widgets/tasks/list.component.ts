import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { ElmaTranslateService } from '../../../../shared/common';
import { AppviewConfig } from '../../../application/modules/appview/appview.interface';
import { ComponentChangedEvent, SearchAction, SearchEvent, SearchService } from '../../../services/search.service';
import { ApplicationModel } from '../../../application/models/application.model';
import { ApplicationStore } from '../../../application/services/application.store';
import { ListComponent } from '../../../application/modules/appview/list/list.component';
import { Layout } from '../../../application/modules/appview/list/list.enums';
import { FieldModel } from '../../../application/models/field.model';
import { ItemModel } from '../../../application/models/item.model';
import { SearchComponent } from './search/search.component';
import { GetTasksSystemApplicationJson } from './task.apps';
import { TaskIconComponent } from './templates/task-icon.component';

@Component({
    selector: 'app-widget-tasks-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskListComponent implements OnInit {
    readonly Layout = Layout;
    readonly TaskIconComponent = TaskIconComponent;

    config: AppviewConfig;
    application: ApplicationModel;
    searchableFields: FieldModel[] = [];
    private searchSubject = new Subject<SearchEvent>();
    private searchSubscription: Subscription;
    closedTasks = false;
    authorId: string | undefined;
    searchQuery: string | undefined;

    @ViewChild('list') list: ListComponent;

    constructor (
        private searchService: SearchService,
        private applicationStore: ApplicationStore,
        private translationService: ElmaTranslateService,
        private router: Router,
    ) {
        this.config = {namespaceCode: 'system', pageCode: 'tasks'};

        this.searchSubscription = this.searchSubject.subscribe((event: SearchEvent) => {
            this.authorId = event.filter;
            this.searchQuery = event.term;
            this.load();
            if (event.action === SearchAction.stop) {
                this.searchService.toggle(false);
            }
        });
    }

    async ngOnInit () {
        if (!this.applicationStore.getFromCache(this.config.namespaceCode, this.config.pageCode)) {
            const json = GetTasksSystemApplicationJson(this.translationService);
            json.namespace = this.config.namespaceCode;
            json.code = this.config.pageCode;
            this.applicationStore.addToCache(json);
        }
        this.application = await this.applicationStore.getModelByNamespaceAndCode(this.config.namespaceCode, this.config.pageCode);
        this.searchableFields = this.application.toJson().fields
            .filter(field => field.searchable)
            .map(field => new FieldModel(field));

        this.initSearch();
    }

    initSearch () {
        this.searchService.componentChanged.emit(<ComponentChangedEvent>{
            component: SearchComponent,
            inputs: {
                application: this.application,
                fields: this.searchableFields,
                searchSubject: this.searchSubject,
            },
        });
    }

    stopSearch () {
        this.searchQuery = '';
        this.authorId = undefined;

        this.searchSubject.next({
            action: SearchAction.stop,
        });

        this.load();
    }

    load () {
        const additionalParams = {
            closed: this.closedTasks,
            author: this.authorId,
            query: this.searchQuery,
        };
        this.list.additionalParams = additionalParams;
        this.list.load();
    }

    changeClosed (event: any) {
        this.closedTasks = event.value;
        this.load();
    }

    onItemClick(item: ItemModel) {
        this.router.navigate(
            ['', { outlets: { p: ['task', item.id]} }]
        );
    }

    addTask () {
        // todo
    }

}
