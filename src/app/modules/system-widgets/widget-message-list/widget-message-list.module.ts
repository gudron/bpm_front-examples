import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetMessageListComponent } from './widget-message-list.component';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
    ],
    declarations: [WidgetMessageListComponent],
    exports: [WidgetMessageListComponent],
})
export class WidgetMessageListModule {
}
