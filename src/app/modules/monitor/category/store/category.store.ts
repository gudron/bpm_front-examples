import { Injectable } from '@angular/core';
import { Namespaces } from '../../../../app.constants';
import { Collection } from '../../../../application/schema/collection.interface';
import { DefaultCollectionStore } from '../../../../application/services/collection.store.default';
import { APIService, APIUrlBuilder } from '../../../../services/api.service';
import { ProcessMonitorCategoryCode } from '../category.application';

const apiCateogryStatUrlPrefix = '/api/bpm/instances/stat/bycategory';

@Injectable()
export class MonitorHistoryCategoryStatsStore extends DefaultCollectionStore {
    private url = new APIUrlBuilder(apiCateogryStatUrlPrefix);
    constructor (
        private api: APIService,
    ) {
        super(api,
            <Collection>{
                namespace: Namespaces.System,
                code: ProcessMonitorCategoryCode,
            },
            apiCateogryStatUrlPrefix,
        );
    }
}
