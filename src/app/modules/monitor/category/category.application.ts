import { ElmaTranslateService } from '../../../../shared/common';
import { uuid4 } from '../../../../shared/utils/uuid';
import { FieldType, Namespaces } from '../../../app.constants';
import { ApplicationJson } from '../../../application/schema/application.json';
import { ApplicationSettingsJson, TableSettingsJson } from '../../../application/schema/settings.json';
import { FieldJson, FieldViewJson } from '../../../models/field.json';

export const ProcessMonitorCategoryCode = 'monitor_history_category';

export function GetMonitorCategoryApplicationJson (translationService: ElmaTranslateService): ApplicationJson {
    return <ApplicationJson>{
        __id: uuid4(),
        __createdAt: '2018-03-02T00:00:01.140905Z',
        __updatedAt: '2018-03-02T00:00:01.140905Z',
        name: translationService.get('app.modules.monitor.category@title'),
        elementName: translationService.get('app.modules.monitor.category.fields@elementName'),
        namespace: Namespaces.System,
        code: ProcessMonitorCategoryCode,
        isSystem: true,
        settings: <ApplicationSettingsJson>{
            tableSettings: <TableSettingsJson>{
                displayFields: ['__template', '__templateName', '__total', '__ended', '__active']
            }
        },
        fields: [
            <FieldJson>{
                code: '__template',
                type: FieldType.Template,
                view: <FieldViewJson>{
                    name: translationService.get('app.modules.monitor.history.cateogry.table@name'),
                    key: false,
                },
            },
            <FieldJson>{
                code: '__templateCode',
                searchable: false,
                type: FieldType.String,
                view: <FieldViewJson>{
                    name: translationService.get('app.modules.monitor.history.cateogry.table@code'),
                    key: false,
                },
            },
            <FieldJson>{
                code: '__templateName',
                type: FieldType.String,
                view: <FieldViewJson>{
                    name: translationService.get('app.modules.monitor.history.cateogry.table@name'),
                    key: true,
                },
            },
            <FieldJson>{
                code: '__total',
                type: FieldType.String,
                view: <FieldViewJson>{
                    name: translationService.get('app.modules.monitor.history.cateogry.table@total'),
                    key: false,
                },
            },
            <FieldJson>{
                code: '__ended',
                type: FieldType.String,
                view: <FieldViewJson>{
                    name: translationService.get('app.modules.monitor.history.cateogry.table@completed'),
                    key: false,
                },
            },
            <FieldJson>{
                code: '__active',
                type: FieldType.String,
                view: <FieldViewJson>{
                    name: translationService.get('app.modules.monitor.history.cateogry.table@active'),
                    key: false,
                },
            },
        ],
    };
}
