import { TestBed, inject } from '@angular/core/testing';

import { MonitorCategoryNavigationService } from './category-navigation.service';

describe('MonitorCategoryNavigationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MonitorCategoryNavigationService]
    });
  });

  it('should be created', inject([MonitorCategoryNavigationService], (service: MonitorCategoryNavigationService) => {
    expect(service).toBeTruthy();
  }));
});
