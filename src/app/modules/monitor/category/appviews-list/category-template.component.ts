import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { FieldModel } from '../../../../application/models/field.model';
import { ItemModel } from '../../../../application/models/item.model';
import { FieldTemplate } from '../../../../application/schema/field-template';
import { Template } from '../../../../bpm/schema';

@Component({
    selector: 'app-monitor-category-template-cell',
    templateUrl: './category-template.component.html',
})
export class CategoryTemplateCellComponent implements FieldTemplate, OnInit, AfterViewInit {
    @Input() field: FieldModel;
    @Input() item: ItemModel;

    public template1: Template;

    constructor (
    ) {
        console.log(this.field);
        console.log(this.item);
    }

    ngOnInit () {
        this.template1 = this.item.getByKey('__template');
    }

    ngAfterViewInit () {
    }

}
