import { Template } from '../../../../bpm/schema';

export interface MonitorHistoryCategoryStat {
    TotalCount: number,
    ActiveCount: number,
    EndedCount:  number,
}

export interface MonitorHistoryCategoryStatByTemplate {
    template: Template,
    __total: number,
    __active: number,
    __ended:  number,
}
