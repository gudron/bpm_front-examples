import { Location } from '@angular/common';
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RouteParams } from 'angular-route-xxl';
import { Subscription } from 'rxjs/Subscription';
import { ElmaTranslateService } from '../../../../shared/common';
import { Namespaces } from '../../../app.constants';
import { ItemModel } from '../../../application/models/item.model';
import { AppviewConfig } from '../../../application/modules/appview/appview.interface';
import { ListComponent } from '../../../application/modules/appview/list/list.component';
import { Layout } from '../../../application/modules/appview/list/list.enums';
import { ApplicationStore } from '../../../application/services/application.store';
import { Template } from '../../../bpm/schema';
import { ElmaTreeNode } from '../../../shared/elma-tree/schema/elma-tree-node.interface';
import { Page } from '../../../shared/pages/page.component';
import { MonitorSystemPageCode } from '../monitor.page';
import { CategoryTemplateCellComponent } from './appviews-list/category-template.component';
import { MonitorCategoryNavigationService } from './category-navigation.service';
import { ProcessMonitorCategoryCode } from './category.application';

@Component({
    selector: 'app-monitor-category-history',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss'],
})
export class MonitorCategoryComponent implements OnInit, AfterViewInit, OnDestroy {
    constructor (
        private location: Location,
        private router: Router,
        private route: ActivatedRoute,
        private appStore: ApplicationStore,
        private translationService: ElmaTranslateService,
        private categoryNavigationService: MonitorCategoryNavigationService,
        private cdr: ChangeDetectorRef,
    ) {
    }

    Layout = Layout;

    readonly CategoryTemplateCellComponent = CategoryTemplateCellComponent;
    private subscribe: Subscription;

    @RouteParams('id', {observable: false}) categoryId: string;

    @Input() config = <AppviewConfig>{
        namespaceCode: Namespaces.System,
        pageCode: ProcessMonitorCategoryCode,
    };

    @ViewChild('list') list: ListComponent;

    async ngOnInit () {
        const application = await this.appStore.getModelByNamespaceAndCode(
            this.config.namespaceCode, this.config.pageCode);
    }

    ngAfterViewInit() {
        this.subscribe = this.route.params.subscribe(async params => {
            if (params['id'] != null) {
                this.list.additionalParams = {
                    id: this.categoryId,
                };

                if (this.list.itemsLoaded) {
                    this.list.load();
                }

                this.categoryNavigationService.setActiveCategoryId(this.categoryId);
            }
        });
    }

    ngOnDestroy() {
        this.subscribe.unsubscribe();
    }

    onTemplateClick ($event: ItemModel) {
        this.categoryNavigationService.navigateToTemplate($event.getByKey('__template'));
    }

}
