import { Injectable, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ElmaTranslateService } from '../../../../shared/common';
import { Crumb, CrumbsService } from '../../../../shared/common/crumbs';
import { Template } from '../../../bpm/schema';
import { ElmaTreeNode } from '../../../shared/elma-tree/schema/elma-tree-node.interface';


@Injectable()
export class MonitorCategoryNavigationService implements OnInit {
    public currentTemplate?: Template | null;
    public currentCategoryNode: ElmaTreeNode;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private crumbsService: CrumbsService,
        private translationService: ElmaTranslateService,
    ) { }

    public crumbList: Crumb[] = [];

    private activeCategorySettingSource = new BehaviorSubject<string>('');
    private activeTemplateSettingSource = new BehaviorSubject<Template>(<Template>{});

    activeCategorySetting$: Observable<string> = this.activeCategorySettingSource.asObservable();
    activeTemplateSetting$: Observable<Template> = this.activeTemplateSettingSource.asObservable();

    ngOnInit() {

    }

    public setActiveCategoryId(categoryID: string) {
        this.activeCategorySettingSource.next(categoryID);
    }

    public setActiveTemplate(template: Template) {
        this.currentTemplate = template;
        this.activeTemplateSettingSource.next(template);
    }

    public navigateToCategory(categoryNode: ElmaTreeNode) {
        this.currentCategoryNode = categoryNode;
        this.currentTemplate = null;

        return this.navigate(['monitor', 'history', 'bycategory', categoryNode.data.id]);
    }

    public navigateToTemplate(template: Template) {
        this.currentTemplate = template;

        return this.navigate(['monitor', 'history', 'bytemplate', template.__id, { outlets: { p: null }} ] );
    }

    public navigateToTemplateByUrl(template: Template, url: string) {
        this.currentTemplate = template;

        return this.navigateByUrl(url);
    }

    public async buildCrumbs (): Promise<void> {

        this.crumbList = [];

        const list: Crumb[] = this.buildCategoryTreeCrubms(this.currentCategoryNode, this.crumbList);

        const slicedList: Crumb[] = list.slice();

        slicedList.unshift({
            name: this.translationService.get('system.pages.monitor@name'),
            click: this.crumbsClicked(['monitor', 'history']),
        });

        if (this.currentTemplate) {
            slicedList.push({
                name: this.currentTemplate.name,
                click: '',
            });
        }

        this.crumbList = slicedList;

        this.crumbsService.reset();

        this.crumbsService.userCrumbs = this.crumbList;
        this.crumbsService.trigger();
    }

    private buildCategoryTreeCrubms(currentNode: ElmaTreeNode, crumbList: Crumb[]): Crumb[] {
        const list: Crumb[] = crumbList.slice();

        list.unshift({
            name: currentNode.label,
            click: this.crumbsClicked(['monitor', 'history', 'bycategory', currentNode.data.id]),
        });

        const parentNode = currentNode.parent;

        if ( parentNode != null || parentNode !== undefined ) {
            return this.buildCategoryTreeCrubms(parentNode, list);
        }

        return list;
    }

    private crumbsClicked(command: any[]): string | (() => void)  {
        return async () => {
            this.navigate(command)
        }
    }


    private navigate (command: any[])  {
        this.buildCrumbs();

        return this.router.navigate([{outlets: {p: null, primary: command}}]);
    }

    private navigateByUrl (url: string)  {
        this.buildCrumbs();

        return this.router.navigateByUrl(url);
    }

}
