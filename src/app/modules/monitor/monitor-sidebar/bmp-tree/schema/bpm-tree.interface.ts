import { ElmaTreeCategoryInterface } from '../../../../../shared/elma-tree/schema/elma-tree-category.interface';

export interface BpmTreeCategoryInterface extends ElmaTreeCategoryInterface {
    id: string;
    parent: string;
    name: string;
}
