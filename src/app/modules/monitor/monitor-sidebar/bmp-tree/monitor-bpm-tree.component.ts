import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ElmaTranslateService } from '../../../../../shared/common';
import { ElmaTreeComponent } from '../../../../shared/elma-tree/elma-tree.component';
import { ElmaTreeNodeRenderedEventInterface } from '../../../../shared/elma-tree/schema/elma-tree-node-rendered-event.interface';
import { ElmaTreeNode } from '../../../../shared/elma-tree/schema/elma-tree-node.interface';
import { MonitorBpmTreeStore } from './store/tree.store';

@Component({
    selector: 'app-monitor-bpm-tree',
    templateUrl: './monitor-bpm-tree.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})

export class MonitorBpmTreeComponent implements OnInit {
    @Output() onSelectCategory = new EventEmitter<ElmaTreeNode>();
    @Output() onTreeRendered = new EventEmitter<ElmaTreeNodeRenderedEventInterface>();

    @Input() activeNodeId: string;

    @ViewChild('tree') tree: ElmaTreeComponent;
    localStorageKey = 'elma365.bpm.monitor.tree.wdiget';

    constructor (
        private router: Router,
        private activatedRoute: ActivatedRoute,
        protected translate: ElmaTranslateService,
        public monitorBpmTreeStore: MonitorBpmTreeStore,
    ) {
    }

    ngOnInit () {
    }

    setActiveNodeById(id: string): ElmaTreeNode | null {
        return this.tree.setActiveNodeById(id);
    }

    selectCategory($event: ElmaTreeNode) {
        this.onSelectCategory.emit($event);
    }

    onTreeIsRendered($event: ElmaTreeNodeRenderedEventInterface) {
        this.onTreeRendered.emit($event);
    }
}
