import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { ElmaTranslateService } from '../../../../../../shared/common';
import { BPTemplateStore } from '../../../../../bpm/services/bptemplate.store';
import { ElmaTreeStoreInterface } from '../../../../../shared/elma-tree/schema/elma-tree-store.interface';

import { MonitorHistoryStore } from '../../../history/store/monitorHistory.store';
import { BpmTreeCategoryInterface } from '../schema/bpm-tree.interface';

@Injectable()
export class MonitorBpmTreeStore implements ElmaTreeStoreInterface {

    constructor (
        private ets: ElmaTranslateService,
        private monitorStore: MonitorHistoryStore,
        private bpTemplateStore: BPTemplateStore,
    ) {
    }

    getByParentIds (ids: string[]|string): Promise<Response|BpmTreeCategoryInterface[]> {
        return this.bpTemplateStore.getCategoriesByParentIds(ids);
    }
}
