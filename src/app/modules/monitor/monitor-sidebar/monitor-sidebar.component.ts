import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ElmaTranslateService } from '../../../../shared/common';
import { Crumb, CrumbsService } from '../../../../shared/common/crumbs';
import { NULL_UUID, ROOT_CATEGORY } from '../../../app.constants';
import { DirectoryModel } from '../../../models/disk.model';
import { PageModel } from '../../../models/page.model';
import { ElmaTreeComponent } from '../../../shared/elma-tree/elma-tree.component';
import { ElmaTreeNodeRenderedEventInterface } from '../../../shared/elma-tree/schema/elma-tree-node-rendered-event.interface';
import { ElmaTreeNode } from '../../../shared/elma-tree/schema/elma-tree-node.interface';
import { PageStore } from '../../../stores/page.store';
import { MonitorCategoryNavigationService } from '../category/category-navigation.service';
import { MonitorBpmTreeComponent } from './bmp-tree/monitor-bpm-tree.component';

@Component({
    selector: 'app-navigation-monitor',
    styleUrls: ['./monitor-sidebar.component.scss'],
    templateUrl: './monitor-sidebar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonitorSidebarComponent implements OnInit, OnDestroy, AfterViewInit {

    @Input('currentPage') currentPage: PageModel;
    @Input('activePage') activePage: PageModel;

    @ViewChild('tree') tree: MonitorBpmTreeComponent;

    isTreeRendered = false;

    private activeCategorySettingRef: Subscription;

    constructor (
        private router: Router,
        private crumbsService: CrumbsService,
        private activatedRoute: ActivatedRoute,
        private translationService: ElmaTranslateService,
        private pageStore: PageStore,
        private categoryNavigationService: MonitorCategoryNavigationService,
        private cdr: ChangeDetectorRef,
    ) {

    }

    ngOnInit() {
    }

    ngAfterViewInit () {

    }

    ngOnDestroy () {
        this.activeCategorySettingRef.unsubscribe();
    }

    onCatSelect($event: ElmaTreeNode) {
        this.categoryNavigationService.navigateToCategory($event);
    }

    onTreeIsRendered($event: ElmaTreeNodeRenderedEventInterface) {
        this.isTreeRendered = true;

        this.activeCategorySettingRef = this.categoryNavigationService.activeCategorySetting$.subscribe((event: string) => {

            const activeTree = this.tree.setActiveNodeById(event);

            if (activeTree != null) {
                this.categoryNavigationService.currentCategoryNode = activeTree;
                this.categoryNavigationService.buildCrumbs();
            }
        });

        this.cdr.markForCheck();
    }
}
