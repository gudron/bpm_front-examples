import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MobxAngularModule } from 'mobx-angular';
import { ElmaCommonModule } from '../../../../shared/common';
import { EditorHeaderModule } from '../../../../shared/editor-header';
import { AppviewModule } from '../../../application/modules/appview/appview.module';
import { AppviewListModule } from '../../../application/modules/appview/list/list.module';
import { BPMTreeModule } from '../../../bpm/modules/tree/tree.module';
import { AppCommonModule } from '../../../common/common.module';
import { SidebarService } from '../../../services/sidebar.service';
import { AppSharedModule } from '../../../shared/shared.module';
import { UserModule } from '../../admin/user/user.module';
import { PageManagerModule } from '../../managers/page-manager/page-manager.module';
import { NavigationMainModule } from '../../navigation/navigation-main/navigation-main.module';
import { NavigationNamespaceModule } from '../../navigation/navigation-namespace/navigation-namespace.module';
import { MonitorBpmTreeComponent } from './bmp-tree/monitor-bpm-tree.component';
import { MonitorSidebarComponent } from './monitor-sidebar.component';


@NgModule({
    imports: [
        EditorHeaderModule,
        CommonModule,
        FormsModule,
        MobxAngularModule,

        AppCommonModule,
        ElmaCommonModule,

        AppviewModule,
        PageManagerModule,

        AppviewListModule,
        UserModule,

        NavigationMainModule,
        NavigationNamespaceModule,

        BPMTreeModule,
        AppSharedModule,
    ],
    declarations: [
        MonitorSidebarComponent,
        MonitorBpmTreeComponent
    ],
    entryComponents: [
        MonitorSidebarComponent,
        MonitorBpmTreeComponent
    ],
    providers: [
        SidebarService
    ],
    exports: [
        MonitorSidebarComponent,
        MonitorBpmTreeComponent
    ],
})
export class MonitorSidebarModule {
}
