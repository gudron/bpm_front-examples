import { ElmaTranslateService } from '../../../shared/common';
import { uuid4 } from '../../../shared/utils/uuid';
import { Namespaces, NavTypes, PageType } from '../../app.constants';
import { PageDataJson, PageJson, PageMenuJson } from '../../models/page.json';
import { AdminSystemPageCode } from '../admin/admin.page';
import { GetProcessMonitioringHistoryPageJson } from './history/history.page'

export const MonitorSystemPageCode = 'monitor';

export function GetProcessMonitioringSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.monitor@name'),
        namespace: Namespaces.Global,
        code: MonitorSystemPageCode,
        type: PageType.Link,
        icon: 'system_monitor',
        sort: 1,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            columns: 1,
            menu: <PageMenuJson>{
                type: NavTypes.NAV_TYPE_MONITOR,
            },
            redirectTo: 'history',
        },
    };
}

export function GetProcessMonitioringSeparatorSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.monitor@bpm-separator-title'),
        namespace: MonitorSystemPageCode,
        code: MonitorSystemPageCode,
        type: PageType.Separator,
        icon: 'minus_horizontal',
        sort: 3,
        system: true,
        hidden: false,
    };
}


export function GetProcessMonitioringPageJson (translationService: ElmaTranslateService): PageJson[] {
    return [
        GetProcessMonitioringSystemPageJson(translationService),
        GetProcessMonitioringHistoryPageJson(translationService),
        GetProcessMonitioringSeparatorSystemPageJson(translationService),
    ];
}
