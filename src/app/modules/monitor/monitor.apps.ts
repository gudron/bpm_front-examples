import { ElmaTranslateService } from '../../../shared/common';
import { ApplicationJson } from '../../application/schema/application.json';
import { GetMonitorCategoryApplicationJson } from './category/category.application';
import { GetMonitorHistoryApplicationJson } from './history/history.application';

export function GetMonitorAppsJson (translationService: ElmaTranslateService): ApplicationJson[] {
    return [
        GetMonitorHistoryApplicationJson(translationService),
        GetMonitorCategoryApplicationJson(translationService),
    ];
}
