import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MobxAngularModule } from 'mobx-angular';
import { DiagramBpmnModule } from '../../../diagram/bpmn/bpmn.module';
import { ElmaCommonModule, ElmaTranslateResolver } from '../../../shared/common';
import { EditorHeaderModule } from '../../../shared/editor-header';
import { OrgstructModule } from '../../../shared/orgstruct';
import { AppviewModule } from '../../application/modules/appview/appview.module';
import { AppviewListModule } from '../../application/modules/appview/list/list.module';
import { SearchModule } from '../../application/modules/appview/search/search.module';
import { BPMEditorModule } from '../../bpm/modules/editor/editor.module';
import { BPMListModule } from '../../bpm/modules/list/list.module';
import { BPTemplateByIdResolver } from '../../bpm/resolvers/bptemplateById.resolver';
import { AppCommonModule } from '../../common/common.module';
import { ExternalWidgetModule } from '../../components/external-widget/external-widget.module';
import { HeaderSearchModule } from '../../messages/header-search/header-search.module';
import { SidebarService } from '../../services/sidebar.service';
import { IsAdminGuard } from '../../services/user-guards';
import { AppSharedModule } from '../../shared/shared.module';
import { GroupModule } from '../admin/group/group.module';
import { RegistryModule } from '../admin/registry/registry.module';
import { UserModule } from '../admin/user/user.module';
import { DynamicFormModule } from '../dynamic-form/dynamic-form.module';
import { PageManagerModule } from '../managers/page-manager/page-manager.module';
import { CategoryTemplateCellComponent } from './category/appviews-list/category-template.component';
import { MonitorCategoryNavigationService } from './category/category-navigation.service';
import { MonitorCategoryComponent } from './category/category.component';
import { MonitorHistoryCategoryStatsStore } from './category/store/category.store';
import { InititatorCellComponent } from './history/appview-list/initiator-cell.component';
import { TasksCellComponent } from './history/appview-list/tasks-cell.component';
import { MonitorBpmnDiagramComponent } from './history/diagramer/monitor-bpmn-diagram.component';
import { MonitorHistoryComponent } from './history/history.component';
import { MonitorInstanceHistoryItemComponent } from './history/item/item.component';
import { MonitorInstanceItemHistoryResolver } from './history/resolvers/historyInstance.resolver';
import { MonitorInstanceByTemplateResolver } from './history/resolvers/historyInstanceByTemplate.resolver';
import { MonitorBpmTreeStore } from './monitor-sidebar/bmp-tree/store/tree.store';
import { MonitorComponent } from './monitor.component';

@NgModule({
    imports: [
        EditorHeaderModule,
        CommonModule,
        FormsModule,
        MobxAngularModule,

        AppCommonModule,
        ElmaCommonModule,
        OrgstructModule,

        AppviewModule,
        SearchModule,
        HeaderSearchModule,
        PageManagerModule,

        DynamicFormModule,

        BPMListModule,
        BPMEditorModule,  // Self contain routing
        DiagramBpmnModule,
        AppviewListModule,
        UserModule,
        GroupModule,
        RegistryModule,
        AppSharedModule,

        ExternalWidgetModule,

        RouterModule.forChild([
            {
                path: 'monitor',
                component: MonitorComponent,
                canActivate: [IsAdminGuard],
                resolve: {
                    translate: ElmaTranslateResolver,
                },
                children: [
                    {
                        path: 'history',
                        component: MonitorHistoryComponent,
                    },
                    {
                        path: 'history/bytemplate/:id',
                        component: MonitorHistoryComponent,
                        resolve: {
                            template: BPTemplateByIdResolver,
                        }
                    },
                    {
                        path: 'history/bytemplate/:id/:instanceId',
                        component: MonitorInstanceHistoryItemComponent,
                        outlet: 'p',
                        resolve: {
                            resolved: MonitorInstanceItemHistoryResolver,
                        }
                    },
                    {
                        path: 'history/bycategory/:id',
                        component: MonitorCategoryComponent,
                    },
                ],
            },
            {
                path: 'history/:instanceId',
                component: MonitorInstanceHistoryItemComponent,
                outlet: 'p',
                resolve: {
                    resolved: MonitorInstanceItemHistoryResolver,
                }
            },
        ]),
    ],
    declarations: [
        MonitorInstanceHistoryItemComponent,
        TasksCellComponent,
        InititatorCellComponent,

        CategoryTemplateCellComponent,

        MonitorComponent,
        MonitorHistoryComponent,
        MonitorCategoryComponent,
        MonitorBpmnDiagramComponent,
    ],
    entryComponents: [
        TasksCellComponent,
        InititatorCellComponent,

        CategoryTemplateCellComponent,
    ],
    providers: [
        SidebarService,
        MonitorInstanceByTemplateResolver,
        MonitorInstanceItemHistoryResolver,
        BPTemplateByIdResolver,
        MonitorBpmTreeStore,

        MonitorCategoryNavigationService,
        MonitorHistoryCategoryStatsStore,
    ],
})
export class MonitorModule {
}
