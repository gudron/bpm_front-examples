import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ElmaTranslateService } from '../../../shared/common';
import { Namespaces, NavTypes } from '../../app.constants';
import { SidebarService } from '../../services/sidebar.service';
import { PageStore } from '../../stores/page.store';
import { MonitorSystemPageCode } from './monitor.page';

@Component({
    selector: 'app-monitor',
    styleUrls: ['./monitor.component.scss'],
    templateUrl: './monitor.component.html',
})
export class MonitorComponent implements OnInit {

    constructor (
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private translationService: ElmaTranslateService,
        private sidebarService: SidebarService,
        private pageStore: PageStore,
    ) {

    }

    async ngOnInit () {
        this.sidebarService.setComponent({
            type: NavTypes.NAV_TYPE_DISK,
            page: await this.pageStore.getByNamespaceAndCode(Namespaces.Global, MonitorSystemPageCode),
        });
    }
}
