import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FieldModel } from '../../../../application/models/field.model';
import { ItemModel } from '../../../../application/models/item.model';
import { FieldTemplate } from '../../../../application/schema/field-template';
import { UserModel } from '../../../../models/user.model';
import { UserStore } from '../../../../stores/user.store';

@Component({
    selector: 'app-monitor-history-initiator-cell',
    templateUrl: './initiator-cell.component.html',
})
export class InititatorCellComponent implements FieldTemplate, OnInit {
    @Input() field: FieldModel;
    @Input() item: ItemModel;

    public user: UserModel;

    constructor (
        private userStore: UserStore,
        private cdr: ChangeDetectorRef,
    ) {
    }


    async ngOnInit () {
        await this.initUser();
    }

    public async initUser (): Promise<void> {
        try {
            this.user = await this.userStore.getByID(this.item.getByKey('__createdBy'));
            this.cdr.markForCheck()
        } finally {

        }
    }
}
