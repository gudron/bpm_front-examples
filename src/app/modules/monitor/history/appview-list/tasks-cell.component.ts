import { Component, Input, OnInit } from '@angular/core';
import { FieldTemplate } from '../../../../application/schema/field-template';
import { FieldModel } from '../../../../application/models/field.model';
import { ItemModel } from '../../../../application/models/item.model';
import { MonitorHistoryTaskInterface } from '../schema/monitorHistoryTask.interface';

@Component({
    selector: 'app-monitor-history-task-cell',
    templateUrl: './tasks-cell.component.html',
})
export class TasksCellComponent implements FieldTemplate, OnInit {
    @Input() field: FieldModel;
    @Input() item: ItemModel;

    public tasks: MonitorHistoryTaskInterface[] = [];

    constructor (
    ) {
    }

    ngOnInit () {
        const tasks = this.item.getByKey('__tasks');
        Object.keys(tasks).map(function(objectKey, index) {
            const value = tasks[objectKey];
            this.tasks.push({
                name: value.name,
                createdBy: value['__createdBy']
            });
        }, this);
    }

}
