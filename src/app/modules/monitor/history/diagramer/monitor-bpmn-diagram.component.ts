import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Operation } from 'rfc6902/diff';
import { Item, Process, Transition } from '../../../../../diagram/bpmn/schema';
import { HighlightElement } from '../../../../../diagram/bpmn/schema/element.interface';
import { DiagramTouchEvent } from '../../../../../diagram/schema/event.interface';
import { ElmaTranslateService } from '../../../../../shared/common';
import { ExecProcess } from '../../../../bpm/models/template-exec.model';
import { MonitorHistoryModel } from '../models/monitorHistory.model';

@Component({
    selector: 'app-monitor-bpmn-diagram',
    templateUrl: './monitor-bpmn-diagram.component.html',
    styleUrls: ['./monitor-bpmn-diagram.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonitorBpmnDiagramComponent implements OnInit {

    @Input() process: Process;
    @Input() processInstance: MonitorHistoryModel;

    @Output() onChange = new EventEmitter<Operation[]>();
    @Output() onTouch = new EventEmitter<DiagramTouchEvent>();

    highlights: HighlightElement[] = [];
    constructor (
        private translationService: ElmaTranslateService,
    ) {

    }

    ngOnInit () {
        this.prepareValues()
    }


    prepareValues () {
        Object.keys(this.process.items).map(function(key, index) {
            if (this.processInstance.flowHistory.items.hasOwnProperty(key)) {
                const isDone = this.processInstance.flowHistory.items[key];
                const item = this.process.items[key];
                if ( isDone ) {
                    const hightlightItem: HighlightElement = {
                        color: '#14b500',
                        path: `/items/${ item.id }`,
                        strokeWidth: 2.5
                    };

                    this.highlights.push(hightlightItem)
                }
            }
        }, this);

        Object.keys(this.process.transitions).map(function(key, index) {
            if (this.processInstance.flowHistory.transitions.hasOwnProperty(key)) {
                const isDone = this.processInstance.flowHistory.transitions[key];
                const transition = this.process.transitions[key];
                if ( isDone ) {
                    const hightlightItem: HighlightElement = {
                        color: '#0003ff',
                        path: `/transitions/${ transition.id }`,
                        strokeWidth: 2.5
                    };

                    this.highlights.push(hightlightItem)
                }
            }
        }, this);
    }
}
