import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Template } from '../../../../bpm/schema/template.interface';
import { BPTemplateStore } from '../../../../bpm/services/bptemplate.store';
import { UserModel } from '../../../../models/user.model';
import { UserStore } from '../../../../stores/user.store';
import { MonitorHistoryModel } from '../models/monitorHistory.model';
import { MonitorHistoryStore } from '../store/monitorHistory.store';

export interface MonitorInstanceItemHistoryResolverInterface {
    historyModel: MonitorHistoryModel;
    template: Template;
    author: UserModel;
}

@Injectable()
export class MonitorInstanceItemHistoryResolver implements Resolve<MonitorInstanceItemHistoryResolverInterface> {

    constructor (
        private location: Location,
        private monitorStore: MonitorHistoryStore,
        private templateStore: BPTemplateStore,
        private userStore: UserStore,
    ) {
    }

    async resolve (
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Promise<MonitorInstanceItemHistoryResolverInterface> {
        try {
            const id = route.paramMap.get('instanceId');
            if (id == null) {
                throw new Error('id not found');
            }

            const instance: MonitorHistoryModel = await this.monitorStore.getInstanceByID(id);

            return {
                historyModel: instance,
                template: await this.resolveTemplateByInstance(instance),
                author: await this.resolveUserByInstance(instance),
            }

        } catch (err) {
            this.location.back();

            throw err;
        }
    }

    async resolveTemplateByInstance(instance: MonitorHistoryModel): Promise<Template> {
        try {
            const id = instance.template!.id;
            if (id == null) {
                throw new Error('id not found');
            }

            const version = instance.template!.version;
            if (version == null) {
                throw new Error('version not found');
            }

            return await this.templateStore.getPublicTemplateByID(instance.template!.id,
                instance.template!.version);

        } catch (err) {
            this.location.back();

            throw err;
        }
    }

    async resolveUserByInstance(instance: MonitorHistoryModel): Promise<UserModel> {

        try {
            const id = instance.createdBy;
            if (id == null) {
                throw new Error('user id not found');
            }

            return await this.userStore.getByID(id);

        } catch (err) {
            this.location.back();

            throw err;
        }
    }
}
