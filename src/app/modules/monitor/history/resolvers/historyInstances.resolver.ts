import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { MonitorHistoryModel } from '../models/monitorHistory.model';
import { MonitorHistoryStore } from '../store/monitorHistory.store';

@Injectable()
export class MonitorInstancesItemsHistoryResolver implements Resolve<MonitorHistoryModel[]> {

    constructor (
        private location: Location,
        private monitorStore: MonitorHistoryStore,
    ) {
    }

    async resolve (
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Promise<MonitorHistoryModel[]> {
        try {

            return await this.monitorStore.getAllInstance();

        } catch (err) {
            this.location.back();

            throw err;
        }
    }
}
