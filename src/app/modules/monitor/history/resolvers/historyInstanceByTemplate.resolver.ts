import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { MonitorHistoryModel } from '../models/monitorHistory.model';
import { MonitorHistoryStore } from '../store/monitorHistory.store';

@Injectable()
export class MonitorInstanceByTemplateResolver implements Resolve<MonitorHistoryModel[]> {

    constructor (
        private location: Location,
        private monitorStore: MonitorHistoryStore,
    ) {
    }

    async resolve (
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Promise<MonitorHistoryModel[]> {
        try {

            const templateID = route.paramMap.get('id');
            if (templateID == null) {
                throw new Error('id not found');
            }

            return await this.monitorStore.getInstanceByTemplateID(templateID);

        } catch (err) {
            this.location.back();

            throw err;
        }
    }
}
