import { Location, PlatformLocation } from '@angular/common';
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Process } from '../../../../../diagram/bpmn/schema/process.interface';
import { ElmaTranslateService } from '../../../../../shared/common';
import { ElmaAutocompleteComponent } from '../../../../../shared/common/elma-autocomplete/elma-autocomplete.component';
import { ElmaComplexPopupComponent } from '../../../../../shared/common/elma-complex-popup/elma-complex-popup.component';
import { ElmaPopoverComponent } from '../../../../../shared/common/elma-popover';
import { ApplicationStore } from '../../../../application/services/application.store';
import { TemplateExecModel } from '../../../../bpm/models';
import { Template } from '../../../../bpm/schema';
import { BPTemplateStore } from '../../../../bpm/services/bptemplate.store';
import { UserModel } from '../../../../models/user.model';
import { DynamicFormComponent, Field } from '../../../dynamic-form/dynamic-form.component';
import { MonitorCategoryNavigationService } from '../../category/category-navigation.service';
import { MonitorHistoryModel } from '../models/monitorHistory.model';

@Component({
    selector: 'app-monitor-instance-history-edit',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonitorInstanceHistoryItemComponent implements OnInit, AfterViewInit {
    template: TemplateExecModel;
    templateJson: any;
    namespace: string;
    fields: Field[];
    instance: MonitorHistoryModel;
    author: UserModel;
    processJson: Process;

    @ViewChild('popup') popupControl: ElmaComplexPopupComponent;

    @ViewChild('form') form: DynamicFormComponent;
    @ViewChild('reassignTask') reassignTask: ElmaPopoverComponent;
    @ViewChild('typeaheadInput') searchUser: ElmaAutocompleteComponent;

    constructor (
        private location: PlatformLocation,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private appStore: ApplicationStore,
        private translationService: ElmaTranslateService,
        private templateStore: BPTemplateStore,
        private categoryNavigationService: MonitorCategoryNavigationService,
        private cdr: ChangeDetectorRef,
    ) {
    }

    @HostListener('window:popstate', ['$event'])
    onPopState(event: any) {

    }

    async ngOnInit () {
        this.activatedRoute.data.subscribe(async (data) => {
            this.instance = data.resolved.historyModel;
            this.author = data.resolved.author;

            this.templateJson = data.resolved.template;
            this.template = new TemplateExecModel(this.templateStore, this.appStore, this.translationService, this.templateJson);
            if (this.templateJson.process) {
                this.processJson = this.templateJson.process;
            }
            this.namespace = this.template.namespace;

            this.fields = await this.template.allFormFieldsFromContext();

            this.cdr.markForCheck();

            this.categoryNavigationService.setActiveTemplate(this.templateJson);
            this.categoryNavigationService.setActiveCategoryId(this.templateJson.category);

            this.popupControl.open();
        });
    }

    async done ($event: [string, { [key: string]: any }]) {

    }

    ngAfterViewInit() {
        this.location.onPopState(() => {
            this.navigateToParentTemplate();
        });
    }


    close () {
        this.navigateToParentTemplate();
    }

    private navigateToParentTemplate() {
        return this.router.navigate([{ outlets: { p: null } }]).then(() => {
            this.categoryNavigationService.navigateToTemplate(this.templateJson);
        })
    }

}
