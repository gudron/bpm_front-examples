import { ElmaTranslateService } from '../../../../shared/common';
import { uuid4 } from '../../../../shared/utils/uuid';
import { NavTypes, PageType } from '../../../app.constants';
import { PageDataJson, PageJson, PageMenuJson } from '../../../models/page.json';
import { MonitorSystemPageCode } from '../monitor.page';

export const ProcessMonitorHistorySystemPageCode = 'history';

export function GetProcessMonitioringHistoryPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.monitor.history@name'),
        namespace: MonitorSystemPageCode,
        code: ProcessMonitorHistorySystemPageCode,
        type: PageType.Link,
        icon: 'system_journal',
        sort: 2,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            menu: <PageMenuJson>{
                type: NavTypes.NAV_TYPE_NAMESPACE,
            },
            routerLink: ['/monitor/history', { outlets: { f: null, p: null } }],
        },
    };
}
