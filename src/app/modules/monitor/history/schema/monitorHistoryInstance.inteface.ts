import { Task } from '../../../../bpm/schema';

export interface MonitorHistoryInstanceInteface {
    __id: string;
    __name: string;
    __path: string;
    __template?: {
        id: string;
        version: number;
    };
    __tasks?: { [key: string]: Task };
    __createdAt?: string;
    __createdBy?: string;
    __updatedAt?: string;
    __updatedBy?: string;
    __completed?: boolean;
    [key: string]: any;
}
