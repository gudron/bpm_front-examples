export interface HistoryInstanceTemplateInterface {
    id: string;
    version: number;
}
