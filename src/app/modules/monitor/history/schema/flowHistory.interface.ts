export interface MonitorFlowHistoryInterface {
    [key: string]: boolean
}
