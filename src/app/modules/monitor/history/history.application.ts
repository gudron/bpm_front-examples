import { ElmaTranslateService } from '../../../../shared/common';
import { uuid4 } from '../../../../shared/utils/uuid';
import { FieldType, Namespaces } from '../../../app.constants';
import { ApplicationJson } from '../../../application/schema/application.json';
import { ApplicationSettingsJson, TableSettingsJson } from '../../../application/schema/settings.json';
import { DatetimeTypes, FieldJson, FieldViewJson } from '../../../models/field.json';

export const ProcessMonitorCollectionCode = 'bp_instances';

export function GetMonitorHistoryApplicationJson (translationService: ElmaTranslateService): ApplicationJson {
    return <ApplicationJson>{
        __id: uuid4(),
        __createdAt: '2018-03-02T00:00:01.140905Z',
        __updatedAt: '2018-03-02T00:00:01.140905Z',
        name: translationService.get('app.modules.monitor.history@title'),
        elementName: translationService.get('app.modules.monitor.history.fields@elementName'),
        namespace: Namespaces.System,
        code: ProcessMonitorCollectionCode,
        isSystem: true,
        settings: <ApplicationSettingsJson>{
            tableSettings: <TableSettingsJson>{
                displayFields: ['__name', '__tasks', '__createdBy', '__createdAt']
            }
        },
        fields: [
            <FieldJson>{
                code: '__name',
                type: FieldType.String,
                view: <FieldViewJson>{
                    name: translationService.get('app.modules.monitor.history.fields@name'),
                    key: true,
                },
            },
            <FieldJson>{
                code: '__tasks',
                type: FieldType.Template,
                view: <FieldViewJson>{
                    name: translationService.get('app.modules.monitor.history.fields@tasks'),
                    key: true,
                },
            },
            <FieldJson>{
                code: '__createdBy',
                type: FieldType.Template,
                view: <FieldViewJson>{
                    name: translationService.get('app.modules.monitor.history.fields@initiator'),
                    key: true,
                },
            },
            <FieldJson>{
                code: '__createdAt',
                type: FieldType.Datetime,
                view: <FieldViewJson>{
                    name: translationService.get('app.modules.monitor.history.fields@createdAt'),
                    data: {
                        additionalType: DatetimeTypes.Both,
                    },
                },
            },
        ],
    };
}
