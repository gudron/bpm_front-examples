import { Task } from '../../../../bpm/schema';
import { MonitorFlowHistoryInterface } from '../schema/flowHistory.interface';
import { HistoryInstanceTemplateInterface } from '../schema/historyInstanceTemplate.interface';

export interface MonitorHistoryJson {
    __id?: string;
    __name: string;
    __createdAt?: string;
    __updatedAt?: string;
    __deletedAt?: string;

    __createdBy: string;

    __tasks: Task[];
    __template: HistoryInstanceTemplateInterface
    __flowHistory: MonitorFlowHistoryInterface
}
