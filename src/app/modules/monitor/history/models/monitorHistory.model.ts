import { computed, observable } from 'mobx-angular';
import * as moment from 'moment';
import { Task } from '../../../../bpm/schema';
import { MonitorFlowHistoryInterface } from '../schema/flowHistory.interface';
import { HistoryInstanceTemplateInterface } from '../schema/historyInstanceTemplate.interface';
import { MonitorHistoryStore } from '../store/monitorHistory.store';
import { MonitorHistoryJson } from './monitorHistory.json';

export class MonitorHistoryModel {

    readonly id: string;

    constructor (
        private store: MonitorHistoryStore,
        json: MonitorHistoryJson,
    ) {
        if (json.__id == null) {
            throw new Error('__id is required');
        }
        this.id = json.__id;
        this._json = json;
        this.fromJson(json);
    }

    private _json: MonitorHistoryJson;
    get json(): MonitorHistoryJson {
        return this._json;
    }

    @observable private _name: string;
    @computed get name(): string {
        return this._name;
    }

    @observable private _tasks: Task[];
    @computed get tasks(): Task[] {
        return this._tasks;
    }

    @observable private _template: HistoryInstanceTemplateInterface;
    @computed get template(): HistoryInstanceTemplateInterface {
        return this._template;
    }

    @observable private _closed: boolean;
    @computed get closed (): boolean {
        return this._closed;
    }

    @observable private _createdBy: string;
    @computed get createdBy (): string {
        return this._createdBy;
    }

    @observable private _createdAt: moment.Moment;
    @computed get createdAt (): moment.Moment {
        return this._createdAt;
    }

    @observable private _updatedAt: moment.Moment;
    @computed get updatedAt (): moment.Moment {
        return this._updatedAt;
    }

    @observable private _deletedAt: moment.Moment;
    @computed get deletedAt (): moment.Moment {
        return this._deletedAt;
    }

    @observable private _flowHistory: MonitorFlowHistoryInterface;
    @computed get flowHistory (): MonitorFlowHistoryInterface {
        return this._flowHistory;
    }

    fromJson (json: MonitorHistoryJson) {
        this._json = json;
        this._name = json.__name;
        this._tasks = json.__tasks;
        this._template = json.__template;
        this._createdBy = json.__createdBy;
        this._createdAt = moment(json.__createdAt);
        this._updatedAt = moment(json.__updatedAt);
        this._deletedAt = moment(json.__deletedAt);
        this._flowHistory = json.__flowHistory;
    }
}
