import { AfterViewChecked, AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RouteParams } from 'angular-route-xxl';
import { ElmaTranslateService } from '../../../../shared/common';
import { Crumb, CrumbsService } from '../../../../shared/common/crumbs';
import { CrumbsComponent } from '../../../../shared/common/crumbs/crumbs.component';
import { Namespaces, NavTypes } from '../../../app.constants';
import { ItemModel } from '../../../application/models/item.model';
import { AppviewConfig } from '../../../application/modules/appview/appview.interface';
import { ListComponent } from '../../../application/modules/appview/list/list.component';
import { Layout } from '../../../application/modules/appview/list/list.enums';
import { ApplicationStore } from '../../../application/services/application.store';
import { Template } from '../../../bpm/schema';
import { ISidebarData, SidebarService } from '../../../services/sidebar.service';
import { Page } from '../../../shared/pages/page.component';
import { IPageFilter, PageStore } from '../../../stores/page.store';
import { MonitorCategoryNavigationService } from '../category/category-navigation.service';
import { MonitorSystemPageCode } from '../monitor.page';
import { InititatorCellComponent } from './appview-list/initiator-cell.component';
import { TasksCellComponent } from './appview-list/tasks-cell.component';
import { ProcessMonitorCollectionCode } from './history.application';

@Component({
    selector: 'app-monitor-history',
    templateUrl: './history.component.html',
})
export class MonitorHistoryComponent implements OnInit, AfterViewInit, AfterViewChecked {

    Layout = Layout;

    readonly TasksCellComponent = TasksCellComponent;
    readonly InitiatorCellComponent = InititatorCellComponent;

    @Input() config = <AppviewConfig>{
        namespaceCode: Namespaces.System,
        pageCode: ProcessMonitorCollectionCode,
    };

    @Input() homePage = <Page>{
        namespace: Namespaces.Global,
        code: MonitorSystemPageCode,
        name: this.translationService.get('system.pages.monitor@name'),
    };

    @RouteParams('id', {observable: false}) templateId: string;
    private template: Template;

    @ViewChild('list') list: ListComponent;
    @ViewChild('elmaCrumbs') elmaCrumbs: CrumbsComponent;

    constructor (
        private router: Router,
        private route: ActivatedRoute,
        private translationService: ElmaTranslateService,
        private sidebarService: SidebarService,
        private categoryNavigationService: MonitorCategoryNavigationService,
        private crumbsService: CrumbsService,
        private pageStore: PageStore,
        private applicationStore: ApplicationStore,
    ) {
    }

    async ngOnInit () {
        const application = await this.applicationStore.getModelByNamespaceAndCode(
            this.config.namespaceCode, this.config.pageCode);

        this.sidebarService.setComponent(<ISidebarData>{
            type: NavTypes.NAV_TYPE_NAMESPACE,
            page: this.pageStore.filterList(<IPageFilter>{
                namespace: MonitorSystemPageCode, code: MonitorSystemPageCode,
            })[0],
        });
    }

    ngAfterViewInit() {
        this.route.data.first().subscribe((data) => {
            if (data.template != null ) {
                this.template = data.template;

                this.categoryNavigationService.setActiveTemplate(this.template);
                this.categoryNavigationService.setActiveCategoryId(this.template.category);
            } else {
                this.initHistoryCrumbs();
            }
        });

        if (this.template != null) {
            this.config.namespaceCode = Namespaces.Global;
            this.config.pageCode = '_process_' + this.template.code;

             this.list.additionalParams = {
                __templateId: this.templateId,
             }
         }
    }

    ngAfterViewChecked() {
    }

    onItemClick (item: ItemModel) {
        this.router.navigate(
            ['', { outlets: { p: ['history', item.id] } }],
            {
                queryParamsHandling: 'preserve', // Сохраняем query string параметры
                relativeTo: this.route,
            },
        );
    }

    private initHistoryCrumbs() {
        const crumbs: Crumb[] = [];

        crumbs.push({
            name: this.translationService.get('system.pages.monitor@name'),
            click: '/monitor/history'
        }, {
            name: this.translationService.get('app.modules.monitor.history@title'),
            click: '',
        });

        this.crumbsService.reset();

        this.crumbsService.userCrumbs = crumbs;
        this.crumbsService.trigger();
    }

}
