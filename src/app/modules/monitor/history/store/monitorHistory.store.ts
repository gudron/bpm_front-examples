import { Injectable } from '@angular/core';
import { autorun, runInAction } from 'mobx';
import { action, computed, observable } from 'mobx-angular';
import { MAX_SEARCH_SIZE, Namespaces } from '../../../../app.constants';
import { Collection } from '../../../../application/schema/collection.interface';
import { DefaultCollectionStore } from '../../../../application/services/collection.store.default';
import { APIService, APIUrlBuilder } from '../../../../services/api.service';

import { ProfileService } from '../../../../services/profile.service';
import { ProcessMonitorCollectionCode } from '../history.application';
import { MonitorHistoryJson } from '../models/monitorHistory.json';
import { MonitorHistoryModel } from '../models/monitorHistory.model';

const apiInstacesUrlPrefix = '/api/bpm/instances/history';

@Injectable()
export class MonitorHistoryStore extends DefaultCollectionStore {
    private url = new APIUrlBuilder(apiInstacesUrlPrefix);
    constructor (
        apiService: APIService,
        private profileService: ProfileService,
    ) {
        super(apiService,
            <Collection>{
                namespace: Namespaces.System,
                code: ProcessMonitorCollectionCode,
            },
            apiInstacesUrlPrefix,
        );
    }

    async getAllInstance (): Promise<MonitorHistoryModel[]> {
        const res = await this.apiService.get(this.url.build({}));

        if (res.status !== 200) {
            throw new Error(res.text());
        }

        const models: MonitorHistoryModel[] = [];

        res.json()['result'].forEach(function(element: MonitorHistoryJson) {
            models.push(new MonitorHistoryModel(this, element))
        }, this);

        return models;
    }

    async getInstanceByTemplateID (templateID: string): Promise<MonitorHistoryModel[]> {
        const res = await this.getAllInstance();

        const models: MonitorHistoryModel[] = res.filter(function(element: MonitorHistoryModel) {
            return element.template.id === templateID
        }, this);

        return models;
    }

    async getInstanceByID (id: string): Promise<MonitorHistoryModel> {
        const res = await this.apiService.get(this.url.build({
            id: id,
        }));

        if (res.status !== 200) {
            throw new Error(res.text());
        }

        return new MonitorHistoryModel(this, res.json());
    }

}
