import { Component, HostBinding } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router } from '@angular/router';
import { ElmaTranslateService } from '../../../shared/common';
import { AdminSystemPageCode, GetAdminMainSystemPageJson } from './admin.page';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss'],
})
export class AdminComponent {

    @HostBinding('class.full-screen') isFullScreen = false;

    constructor (
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private translationService: ElmaTranslateService,
    ) {
        this.router.events
            .filter(event => event instanceof NavigationEnd)
            .subscribe((event: NavigationEnd) => {
                let shot: ActivatedRouteSnapshot | null = this.activatedRoute.snapshot;
                while (shot) {
                    if (shot.data.hasOwnProperty('fullScreen') && shot.data.fullScreen === true) {
                        this.isFullScreen = true;
                        return;
                    }
                    shot = shot.firstChild;
                }
                this.isFullScreen = false;

                if (event.url === '/' + AdminSystemPageCode) {
                    const pageJson = GetAdminMainSystemPageJson(translationService);
                    this.router.navigate([pageJson.namespace, pageJson.code]);
                }
            });
    }
}
