import { ElmaTranslateService } from '../../../shared/common';
import { uuid4 } from '../../../shared/utils/uuid';
import { Namespaces, NavTypes, PageType } from '../../app.constants';
import { PageDataJson, PageJson, PageMenuJson } from '../../models/page.json';
import { GetAdminGroupsSystemPageJson } from './group/group.page';
import { RegistryCollectionCode } from './registry/registry.application';
import { GetAdminUsersSystemPageJson } from './user/user.page';

export const AdminSystemPageCode = 'admin';
export const AdminProcessesSystemPageCode = 'process';
export const AdminMainSystemPageCode = 'main';

export function GetAdminSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.admin@name'),
        namespace: Namespaces.Global,
        code: AdminSystemPageCode,
        type: PageType.Namespace,
        icon: 'tools',
        sort: 0,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            menu: <PageMenuJson>{
                type: NavTypes.NAV_TYPE_NAMESPACE,
            },
            redirectTo: 'main',
        },
    };
}

export function GetAdminMainSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.admin.main@name'),
        namespace: AdminSystemPageCode,
        code: AdminMainSystemPageCode,
        type: PageType.Link,
        icon: 'building_house',
        sort: 1,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            routerLink: ['/admin/main', { outlets: { f: null } }],
        },
    };
}

export function GetAdminCompanySystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.admin.company-settings@name'),
        namespace: AdminSystemPageCode,
        code: 'company',
        type: PageType.Link,
        icon: 'system_file_tile',
        sort: 2,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            routerLink: ['/admin/company', { outlets: { f: null } }],
        },
    };
}

export function GetAdminCompanySeparatorSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.admin.company@name'),
        namespace: AdminSystemPageCode,
        code: 'company',
        type: PageType.Separator,
        icon: 'minus_horizontal',
        sort: 3,
        system: true,
        hidden: false,
    };
}

export function GetAdminOrgstructSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.admin.orgstruct@name'),
        namespace: AdminSystemPageCode,
        code: 'orgstruct',
        type: PageType.Link,
        icon: 'business_structure',
        sort: 4,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            routerLink: ['/admin/orgstruct', { outlets: { f: null, p: null } }],
        },
    };
}

export function GetAdminRegistrySystemPageJson (translationService: ElmaTranslateService): PageJson {
    return {
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('app.modules.admin.registry@title'),
        namespace: AdminSystemPageCode,
        code: 'registry',
        type: PageType.Link,
        icon: 'system_file_tile',
        sort: 5,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            routerLink: [`/admin/${RegistryCollectionCode}`, { outlets: { f: null } }],
        },
    };
}


export function GetAdminWorktimerSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.admin.worktimer@name'),
        namespace: AdminSystemPageCode,
        code: 'worktimer',
        type: PageType.Link,
        icon: 'system_file_tile',
        sort: 6,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            routerLink: ['/admin/worktimer', { outlets: { f: null } }],
        },
    };
}

export function GetAdminBpmSeparatorSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.admin.bpm@name'),
        namespace: AdminSystemPageCode,
        code: 'bpm',
        type: PageType.Separator,
        icon: 'minus_horizontal',
        sort: 7,
        system: true,
        hidden: false,
    };
}

export function GetAdminProcessesSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.admin.bpm.list@name'),
        namespace: AdminSystemPageCode,
        code: AdminProcessesSystemPageCode,
        type: PageType.Link,
        icon: 'system_file_replace',
        sort: 8,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            routerLink: ['/admin/process', { outlets: { f: null } }],
        },
    };
}

export function GetAdminViewSeparatorSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.admin.system@settings'),
        namespace: AdminSystemPageCode,
        code: '',
        type: PageType.Separator,
        icon: 'minus_horizontal',
        sort: 9,
        system: true,
        hidden: false,
    };
}

export function GetAdminWidgetsSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.admin.widgets@name'),
        namespace: AdminSystemPageCode,
        code: 'widgets',
        type: PageType.Link,
        icon: 'system_file_tile',
        sort: 10,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            routerLink: ['/admin/widgets', { outlets: { f: null } }],
        },
    };
}

export function GetAdminNotificationsSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('app.modules.admin.notifications@name'),
        namespace: AdminSystemPageCode,
        code: 'notifications',
        type: PageType.Link,
        icon: 'system_file_tile',
        sort: 12,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            routerLink: ['/admin/notifications', { outlets: { f: null } }],
        },
    };
}


export function GetAdminPagesJson (translationService: ElmaTranslateService): PageJson[] {
    return [
        GetAdminSystemPageJson(translationService),
        GetAdminMainSystemPageJson(translationService),
        GetAdminCompanySeparatorSystemPageJson(translationService),
        GetAdminOrgstructSystemPageJson(translationService),
        GetAdminUsersSystemPageJson(translationService),
        GetAdminGroupsSystemPageJson(translationService),
        GetAdminWorktimerSystemPageJson(translationService),
        GetAdminCompanySystemPageJson(translationService),
        GetAdminBpmSeparatorSystemPageJson(translationService),
        GetAdminProcessesSystemPageJson(translationService),
        GetAdminViewSeparatorSystemPageJson(translationService),
        GetAdminWidgetsSystemPageJson(translationService),
        GetAdminNotificationsSystemPageJson(translationService),
        GetAdminRegistrySystemPageJson(translationService),
    ];
}
