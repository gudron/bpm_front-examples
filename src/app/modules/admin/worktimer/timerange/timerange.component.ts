import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, } from '@angular/core';
import * as moment from 'moment';
import { ElmaTranslateService } from '../../../../../shared/common';


@Component({
    selector: 'app-admin-workingdays-timerange',
    templateUrl: './timerange.component.html',
    styleUrls: ['./timerange.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminTimerangeComponent implements OnInit {
    @Input() public from: number;
    @Input() public to: number;

    public fromDate: Date;
    public toDate: Date;

    @Output() changeRange: EventEmitter<[Number, Number]> = new EventEmitter<[Number, Number]>();

    constructor (
        private translateService: ElmaTranslateService,
    ) {
    }

    fillRange() {
        this.fromDate = moment().startOf('day').hour(0)
            .add(this.from, 's')
            .toDate();

        this.toDate = moment().startOf('day').hour(0)
            .add(this.to, 's')
            .toDate();
    }

    onChanged() {
        const fromDateMoment = moment(this.fromDate);
        const toDateMoment = moment(this.toDate);

        this.from = moment.duration({hours: fromDateMoment.hours(), minutes: fromDateMoment.minutes() } )
            .asSeconds();
        this.to = moment.duration({hours: toDateMoment.hours(), minutes: toDateMoment.minutes() } )
            .asSeconds();

        this.changeRange.emit([this.from, this.to]);
    }

    ngOnInit () {
        this.fillRange()
    }
}
