import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MobxAngularModule } from 'mobx-angular';
import { ElmaCommonModule, ElmaTranslateResolver } from '../../../../shared/common';
import { EditorHeaderModule } from '../../../../shared/editor-header';
import { AppCommonModule } from '../../../common/common.module';
import { AppSharedModule } from '../../../shared/shared.module';
import { GeneralSettingsResolver } from './store/generalSettings.resolver';
import { SpecialDaysResolver } from './store/specialDays.resolver';

import { AdminWorktimerComponent } from './worktimer.component';
import { AdminWorkingDaysComponent } from './workingdays/workingdays.component';
import { AdminTimerangeComponent } from './timerange/timerange.component';
import { AdminSpecialDaysComponent } from './specialdays/specialdays.component';
import { AdminDayscheduleComponent } from './dayschedule/dayschedule.component';

import { MomentModule } from 'angular2-moment';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,

        AppSharedModule,

        MobxAngularModule,

        EditorHeaderModule,

        AppCommonModule,
        ElmaCommonModule,

        MomentModule,

        RouterModule.forChild([
            {
                path: 'worktimer',
                component: AdminWorktimerComponent,
                data: {
                    crumb: 'work-timer',
                },
                resolve: {
                    generalSettings: GeneralSettingsResolver,
                    specialDays: SpecialDaysResolver

                },
            },
        ]),

    ],
    providers: [
        GeneralSettingsResolver,
        SpecialDaysResolver
    ],
    declarations: [
        AdminWorktimerComponent,
        AdminWorkingDaysComponent,
        AdminTimerangeComponent,
        AdminSpecialDaysComponent,
        AdminDayscheduleComponent
    ],
    exports: [],
})
export class AdminWorktimerModule {
}
