import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit,  } from '@angular/core';
import * as moment from 'moment';
import { ElmaTranslateService } from '../../../../../shared/common';
import { TimeRange } from '../schema/timerange.interface';
import { DayScheduleRangeModel } from '../models/dayScheduleRangeModel';

@Component({
    selector: 'app-admin-worktimer-dayschedule',
    templateUrl: './dayschedule.component.html',
    styleUrls: ['./dayschedule.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminDayscheduleComponent implements OnInit {
    @Input() public scheduleRange: DayScheduleRangeModel;

    constructor (
        private translateService: ElmaTranslateService,
    ) {
    }

    ngOnInit () {
    }

    setWorkingTimeSchedule (EventData: [Number, Number]) {
        this.scheduleRange.setWorkingTimeSchedule(EventData);
    }

    setLunchTimeSchedule (EventData: [Number, Number]) {
        this.scheduleRange.setLunchTimeSchedule(EventData);
    }
}
