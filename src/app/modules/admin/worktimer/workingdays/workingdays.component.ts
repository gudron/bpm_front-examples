import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewEncapsulation, } from '@angular/core';
import { ElmaTranslateService } from '../../../../../shared/common';

import { WeekendsModel} from '../models/workingDays.model';
import { WeekDay } from '../schema/weekDay.interface';

@Component({
    selector: 'app-admin-worktimer-workingdays',
    templateUrl: './workingdays.component.html',
    styles: [`
        :host {
            position: relative;
            width: 100%;
        }
    `],
    styleUrls: ['./workingdays.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminWorkingDaysComponent implements OnInit {
    @Input() public workingDays: WeekendsModel;

    constructor (
        private translateService: ElmaTranslateService,
    ) {
    }

    getWorkingDays (): WeekDay[] {
        return this.workingDays.days;
    }

    setDayState(key: string, state: boolean) {
        this.workingDays.setDay(key, state);
    }

    ngOnInit () {
    }
}
