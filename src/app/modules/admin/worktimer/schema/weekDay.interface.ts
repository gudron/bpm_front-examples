export interface WeekDay {
    name: string;
    key: string;
    isWorking: boolean;
}
