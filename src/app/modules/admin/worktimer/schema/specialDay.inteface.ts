import {TimeRange} from './timerange.interface';

export interface SpecialDay extends TimeRange {
    date: Date;
    holiday: boolean;
}

export interface SpecialDayJson extends TimeRange {
    date: number;
    holiday: boolean;
}
