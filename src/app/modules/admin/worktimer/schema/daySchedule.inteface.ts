import { TimeRange } from './timerange.interface';

export interface DayScheduleInterface {
    workingTime: TimeRange;
    lunchTime: TimeRange;
}
