import { DayScheduleInterface } from './daySchedule.inteface';
import { Weekends } from './workingDays.interface';

export interface GeneralInterface {
    workinDays:    Weekends;
    daySchedule:   DayScheduleInterface;
}
