export interface TimeRange {
    from: Number;
    to: Number;
}
