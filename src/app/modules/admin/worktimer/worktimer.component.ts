import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ElmaTranslateService } from '../../../../shared/common';
import { NavTypes } from '../../../app.constants';
import { ISidebarData, SidebarService } from '../../../services/sidebar.service';
import { IPageFilter, PageStore } from '../../../stores/page.store';
import { DayScheduleRangeModel } from './models/dayScheduleRangeModel';
import { SpecialDaysModel } from './models/specialDays.model';
import { WeekendsModel } from './models/workingDays.model';

import { GetAdminWorktimerSystemPageJson } from '../admin.page';
import { WorktimerStore } from './store/worktimer.store';

import 'rxjs/add/operator/first';

@Component({
    selector: 'app-admin-worktimer',
    templateUrl: './worktimer.component.html',
    styleUrls: ['./worktimer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminWorktimerComponent implements OnInit {
    public weekends: WeekendsModel;
    public specialDays: SpecialDaysModel;
    public dayScheduleRanges: DayScheduleRangeModel;

    constructor (
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private sidebarService: SidebarService,
        private pageStore: PageStore,
        private translationService: ElmaTranslateService,
        private woktimerStore: WorktimerStore,
    ) {
    }

    ngOnInit () {
        const pageJson = GetAdminWorktimerSystemPageJson(this.translationService);
        let generalSettings: { [key: string]: any } = {
            workingDays: null,
            daySchedule: null,
        };

        this.activatedRoute.data.first().subscribe((data) => {
            generalSettings = {
                workingDays: data.generalSettings.weekends,
                daySchedule: data.generalSettings.daySchedule,
            };

            this.specialDays = data.specialDays;
        });

        this.sidebarService.setComponent(<ISidebarData>{
            type: NavTypes.NAV_TYPE_NAMESPACE,
            page: this.pageStore.filterList(<IPageFilter>{ namespace: pageJson.namespace, code: pageJson.code })[0],
        });


        this.weekends = new WeekendsModel(generalSettings.workingDays, this.translationService);

        this.dayScheduleRanges = new DayScheduleRangeModel(generalSettings.daySchedule);

    }

    save () {

        this.woktimerStore.setGeneral({
            daySchedule: this.dayScheduleRanges.toJson(),
            weekends: this.weekends.toJson(),
        });

        this.woktimerStore.setSpecialDays({ specialDays: this.specialDays.toJson() });

        this.router.navigate(['..'], { relativeTo: this.activatedRoute });
    }
}
