import * as moment from 'moment';

import { ElmaTranslateService } from '../../../../../shared/common/index';

import { TimeRange } from '../schema/timerange.interface';

export class DayScheduleRangeModel {
    constructor (
        readonly dayScheduleJson: { [key: string]: TimeRange},
    ) {
        this.fillRanges();
    }

    public workingTime: TimeRange;
    public lunchTime: TimeRange;

    private fillRanges() {
        let ranges = this.dayScheduleJson;

        if (ranges == null) {
            ranges = {};
            ranges.workingTime = {
                from: moment.duration(9, 'h').asSeconds(),
                to: moment.duration(18, 'h').asSeconds()
            };

            ranges.lunchTime = {
                from: moment.duration(12, 'h').asSeconds(),
                to: moment.duration(13, 'h').asSeconds()
            };
        } else {
            ranges.workingTime = {
                from: ranges.workingTime.from,
                to: ranges.workingTime.to
            };

            ranges.lunchTime = {
                from: ranges.lunchTime.from,
                to: ranges.lunchTime.to
            };
        }

        this.workingTime = ranges.workingTime;
        this.lunchTime = ranges.lunchTime;
    }

    public toJson () {
        return {
            workingTime: this.workingTime,
            lunchTime: this.lunchTime,
        }

    }
    public setWorkingTimeSchedule(EventData: [Number, Number]) {
        this.workingTime.from = EventData[0];
        this.workingTime.to = EventData[1];
    }

    public setLunchTimeSchedule(EventData: [Number, Number]) {
        this.lunchTime.from = EventData[0];
        this.lunchTime.to = EventData[1];
    }
}
