import { computed } from 'mobx-angular';
import { ElmaTranslateService } from '../../../../../shared/common';
import { WeekDay } from '../schema/weekDay.interface';
import { Weekends } from '../schema/workingDays.interface';

const weekdays: ReadonlyArray<string> = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

export class WeekendsModel {
    constructor (
        public weekends: Weekends,
        private translateService: ElmaTranslateService,
    ) {
        this.fillDays();
    }
    private fillDays() {
        if (this.weekends == null) {
            this.weekends = {
                monday: false,
                tuesday: false,
                wednesday: false,
                thursday: false,
                friday: false,
                saturday: true,
                sunday: true,
            };
        }
    }

    @computed get days(): WeekDay[] {

        return weekdays.map((key: any) => {
            let weekDay: WeekDay;
            const isWorking: boolean = !!(this.weekends as any)[key];

            weekDay = {
                name: this.translateService.get('app.modules.admin.worktimer.weekdays.' + key + '@name'),
                key: key,
                isWorking: isWorking
            };

            return weekDay;
        }, this);

    }

    public setDay(key: string, state: boolean) {
        (<any>this.weekends)[key] = state;
    }

    public toJson() {
        return this.weekends;
    }
}
