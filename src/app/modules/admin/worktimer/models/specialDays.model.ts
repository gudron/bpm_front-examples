import { computed } from 'mobx-angular';
import * as moment from 'moment';

import { ElmaTranslateService } from '../../../../../shared/common';
import { SpecialDay, SpecialDayJson } from '../schema/specialDay.inteface';
import { TimeRange } from '../schema/timerange.interface';
import { DayScheduleRangeModel } from './dayScheduleRangeModel';

export class SpecialDaysModel {
    constructor (
        readonly specialDaysJson: SpecialDayJson[],
    ) {
        this.fillDays();
    }


    public specialDays: SpecialDay[] = [];

    private fillDays() {

        let days: SpecialDay[] = [];

        if (this.specialDaysJson == null) {
            days = [];
        } else {
            this.specialDaysJson.forEach(function (currentValue: SpecialDayJson) {
                const specialDay: SpecialDay = {
                    date: moment.unix(currentValue.date).toDate(),
                    holiday: currentValue.holiday,
                    from: currentValue.from,
                    to: currentValue.to
                };
                days.push(specialDay)
            });
        }

        this.specialDays = days;
    }

    @computed get days(): SpecialDay[] {

        return this.specialDays;
    }

    public addSpecialDay(specialday: SpecialDay) {
        this.specialDays.push(specialday);
    }

    public addEmptySpecialDay(work: TimeRange) {
        const currentDate = moment();

        this.addSpecialDay({
            date: currentDate.toDate(),
            holiday: false,
            from: work.from,
            to: work.to,
        })
    }

    public removeDayByIndex(index: number) {
        this.specialDays.splice(index, 1);
    }
    public setSpecialDay(index: number, specialday: SpecialDay) {
        this.specialDays[index] = specialday;
    }

    public toJson() {
        const result: any[] = [];

        this.specialDays.forEach(function(currentValue: SpecialDay, index: number) {
            const sd = {
                date: moment(currentValue.date).unix(),
                holiday: currentValue.holiday,
                from: currentValue.from,
                to: currentValue.to
            };
            result.push(sd)
        });

        return result;
    }

    public setSpecialDaySchedule(index: number, EventData: [Number, Number]) {
        this.specialDays[index].from = EventData[0];
        this.specialDays[index].to = EventData[1];
    }
}
