import { Injectable } from '@angular/core';
import { ElmaTranslateService } from '../../../../../shared/common';

import { APIService, APIUrlBuilder } from '../../../../services/api.service';
import { DayScheduleRangeModel } from '../models/dayScheduleRangeModel';
import { SpecialDaysModel } from '../models/specialDays.model';
import { GeneralInterface } from '../schema/general.interface';
import { SpecialDay } from '../schema/specialDay.inteface';
import { Weekends } from '../schema/workingDays.interface';

const categoryUrl = '/api/scheduler/general';
const specialDaysUrl = '/api/scheduler/specialDays';


@Injectable()
export class WorktimerStore {
    private schedulerUrlBuilder = new APIUrlBuilder(categoryUrl);
    private specialDaysUrlBuilder = new APIUrlBuilder(specialDaysUrl);


    constructor (
        private api: APIService,
        private translateService: ElmaTranslateService,
    ) {
    }

    async getGeneral (): Promise<any> {
        const res = await this.api.get(this.schedulerUrlBuilder.build({}));
        switch (res.status) {
            case 200:

                return res.json();

            case 404:
                return {
                    workingDays: null,
                    daySchedule: null,
                };
            }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async setGeneral (GeneralData: any): Promise<GeneralInterface> {
        const res = await this.api.put(this.schedulerUrlBuilder.build({}), GeneralData);
        switch (res.status) {
        case 200:

            return res.json();
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async getSpecialDays (): Promise<SpecialDaysModel> {
        const res = await this.api.get(this.specialDaysUrlBuilder.build({}));
        switch (res.status) {
            case 200:
                return new SpecialDaysModel(res.json()['specialDays']);

            case 404:
                return new SpecialDaysModel([]);

        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

    async setSpecialDays (SpecialDayData: any): Promise<SpecialDay[]> {
        const res = await this.api.put(this.specialDaysUrlBuilder.build({}), SpecialDayData);
        switch (res.status) {
        case 200:

            return res.json().specialDays;
        }

        throw new Error(`unexpected response code ${ res.status }`);
    }

}
