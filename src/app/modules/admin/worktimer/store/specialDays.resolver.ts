
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { SpecialDaysModel } from '../models/specialDays.model';
import { WorktimerStore } from './worktimer.store';

@Injectable()
export class SpecialDaysResolver implements Resolve<SpecialDaysModel> {

    constructor (private worktimerStore: WorktimerStore) {}

    async resolve (): Promise<SpecialDaysModel> {
        try {
            return await this.worktimerStore.getSpecialDays();
        } catch (err) {
            throw err;
        }
    }
}
