
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { GeneralInterface } from '../schema/general.interface';
import { WorktimerStore } from './worktimer.store';

@Injectable()
export class GeneralSettingsResolver implements Resolve<GeneralInterface> {

    constructor (private worktimerStore: WorktimerStore) {}

    async resolve (): Promise<GeneralInterface> {
        try {
            return await this.worktimerStore.getGeneral();
        } catch (err) {
            throw err;
        }
    }
}
