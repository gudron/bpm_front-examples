import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ElmaTranslateService } from '../../../../../shared/common';
import { TimeRange } from '../schema/timerange.interface';
import { SpecialDaysModel } from '../models/specialDays.model';
import { SpecialDay } from '../schema/specialDay.inteface';

@Component({
    selector: 'app-admin-worktimer-specialdays',
    templateUrl: './specialdays.component.html',
    styleUrls: ['./specialdays.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminSpecialDaysComponent implements OnInit {
    @Input() private work: TimeRange;
    @Input() public specialDays: SpecialDaysModel;

    public daterange: { [key: string]: Date };

    constructor (
        private translateService: ElmaTranslateService,
    ) {

    }

    ngOnInit () {
        this.daterange = {
            min: moment().startOf('year').toDate(),
            max: moment().startOf('year').add(1, 'y')
                .endOf('year').toDate(),
        };
    }

    getSpecialDays (): SpecialDay[] {
        return this.specialDays.days;
    }

    addEmptySpecialDay () {
        this.specialDays.addEmptySpecialDay(this.work);
    }

    setSpecialDaySchedule (index: number, EventData: [Number, Number]) {
        this.specialDays.setSpecialDaySchedule(index, EventData);
    }

    removeDayByIndex (index: number) {
        this.specialDays.removeDayByIndex(index);
    }
}
