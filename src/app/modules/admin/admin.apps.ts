import { ElmaTranslateService } from '../../../shared/common';
import { ApplicationJson } from '../../application/schema/application.json';
import { GetOrgunitsSystemApplicationJson } from './group/group.application';
import { GetRegistrySystemApplicationJson } from './registry/registry.application';
import { GetUsersSystemApplicationJson } from './user/user.application';

export function GetAdminAppsJson (translationService: ElmaTranslateService): ApplicationJson[] {
    return [
        GetUsersSystemApplicationJson(translationService),
        GetOrgunitsSystemApplicationJson(translationService),
        GetRegistrySystemApplicationJson(translationService),
    ];
}
