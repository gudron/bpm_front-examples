import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MobxAngularModule } from 'mobx-angular';
import { ElmaCommonModule, ElmaTranslateResolver } from '../../../shared/common';
import { EditorHeaderModule } from '../../../shared/editor-header';
import { OrgstructModule } from '../../../shared/orgstruct';
import { AppviewModule } from '../../application/modules/appview/appview.module';
import { AppviewListModule } from '../../application/modules/appview/list/list.module';
import { SearchModule } from '../../application/modules/appview/search/search.module';
import { BPMEditorModule } from '../../bpm/modules/editor/editor.module';
import { BPMListComponent } from '../../bpm/modules/list/list.component';
import { BPMListModule } from '../../bpm/modules/list/list.module';
import { AppCommonModule } from '../../common/common.module';
import { ExternalWidgetModule } from '../../components/external-widget/external-widget.module';
import { HeaderSearchModule } from '../../messages/header-search/header-search.module';
import { IsAdminGuard } from '../../services/user-guards';
import { AppSharedModule } from '../../shared/shared.module';
import { PageManagerModule } from '../managers/page-manager/page-manager.module';
import { AdminComponent } from './admin.component';
import { AdminCompanyComponent } from './company/company.component';
import { AdminCompanyModule } from './company/company.module';
import { CompanySettingsResolver } from './company/store/companySettings.resolver';
import { CompanySettingsStore } from './company/store/companySettings.store';
import { GroupComponent } from './group/group.component';
import { GroupModule } from './group/group.module';
import { AdminMainComponent } from './main/main.component';
import { AdminNotificationComponent } from './notification/notification.component';
import { NotificationModule } from './notification/notification.module';
import { AdminOrgstructComponent } from './orgstruct/orgstruct.component';
import { AdminRegistryComponent } from './registry/registry.component';
import { RegistryModule } from './registry/registry.module';
import { AdminUserComponent } from './user/user.component';
import { UserModule } from './user/user.module';
import { AdminWidgetModule } from './widget/widget.module';
import { AdminWorktimerComponent } from './worktimer/worktimer.component';
import { AdminWorktimerModule } from './worktimer/worktimer.module';

@NgModule({
    imports: [
        EditorHeaderModule,
        CommonModule,
        FormsModule,
        MobxAngularModule,

        AppSharedModule,
        AppCommonModule,
        ElmaCommonModule,
        OrgstructModule,

        AppviewModule,
        SearchModule,
        HeaderSearchModule,
        PageManagerModule,

        BPMListModule,
        BPMEditorModule,  // Self contain routing
        AdminWidgetModule,  // Self contain routing
        AdminWorktimerModule,
        AdminCompanyModule,
        AppviewListModule,
        UserModule,
        GroupModule,
        RegistryModule,
        NotificationModule,

        ExternalWidgetModule,

        RouterModule.forChild([
            {
                path: '',
                component: AdminComponent,
                canActivate: [IsAdminGuard],
                data: {
                    crumb: 'admin-area',
                },
                resolve: {
                    translate: ElmaTranslateResolver,
                },
                children: [
                    {
                        path: 'main',
                        component: AdminMainComponent,
                    },
                    {
                        path: 'users',
                        component: AdminUserComponent,
                        data: {
                            crumb: 'admin-users',
                        },
                        resolve: {
                            companySettings: CompanySettingsResolver,
                        },
                    },
                    {
                        path: 'groups',
                        component: GroupComponent,
                        data: {
                            crumb: 'admin-groups',
                        },
                    },
                    {
                        path: 'process',
                        component: BPMListComponent,
                        data: {
                            crumb: 'admin-process',
                        },
                    },
                    {
                        path: 'orgstruct',
                        component: AdminOrgstructComponent,
                        data: {
                            fullScreen: true,
                        },
                    },
                    {
                        path: 'worktimer',
                        component: AdminWorktimerComponent,
                        data: {
                            crumb: 'work-timer',
                        },
                        resolve: {
                            translate: ElmaTranslateResolver,
                        },
                    },
                    {
                        path: 'company',
                        component: AdminCompanyComponent,
                        data: {
                            crumb: 'company',
                        },
                        resolve: {
                            translate: ElmaTranslateResolver,
                        },
                    },
                    {
                        path: 'registry',
                        component: AdminRegistryComponent,
                        data: {
                            crumb: 'admin-registry',
                        },
                    },
                    {
                        path: 'notifications',
                        component: AdminNotificationComponent,
                        data: {
                            crumb: 'admin-notifications',
                        },
                    },
                ],
            },
        ]),
    ],
    declarations: [
        AdminComponent,
        AdminMainComponent,
        AdminOrgstructComponent,
    ],
    providers: [
        CompanySettingsStore
    ],
})
export class AdminModule {
}
