import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ElmaCommonModule } from '../../../shared/common/common.module';
import { ElmaTranslateResolver } from '../../../shared/common/translate/translate.resolver.service';
import { PipesModule } from '../../pipes/pipes.module';
import { SidebarService } from '../../services/sidebar.service';
import { IsAuthUserGuard } from '../../services/user-guards';
import { MSOfficeComponent } from '../../shared/file/type-file/msoffice/msoffice.component';
import { DiskRootFolderGuard } from './disk-guard';
import { DiskListingComponent } from './disk-listing/disk-listing.component';
import { DiskListingModule } from './disk-listing/disk-listing.module';
import { DiskPreviewComponent } from './disk-preview/disk-preview.component';
import { DiskPreviewModule } from './disk-preview/disk-preview.module';
import { FileResolveService } from './disk-resolver.service';
import { DiskComponent } from './disk.component';

@NgModule({
    imports: [
        PipesModule,
        CommonModule,
        ElmaCommonModule,
        DiskListingModule,
        DiskPreviewModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: 'files',
                component: DiskComponent,
                canActivate: [IsAuthUserGuard],
                resolve: {
                    translate: ElmaTranslateResolver,
                },
                children: [
                    {
                        path: '',
                        canActivate: [DiskRootFolderGuard],
                        component: DiskListingComponent
                    },
                    {
                        path: 'favorite',
                        component: DiskListingComponent,
                        data: {favorite: true}
                    },
                    {
                        path: 'editoffice/:id',
                        component: MSOfficeComponent,
                        data: { action: 'edit', fullScreen: true },
                        resolve: {
                            file: FileResolveService,
                        },
                    },
                    {
                        path: 'newoffice/:format',
                        component: MSOfficeComponent,
                        data: { action: 'editnew', fullScreen: true },
                        resolve: {
                            file: FileResolveService,
                        },
                    },
                    {
                        path: ':id',
                        component: DiskListingComponent
                    },
                    {
                        path: ':id/:command',
                        component: DiskListingComponent
                    },
                ],
            },
            {
                path: 'preview/:id',
                component: DiskPreviewComponent,
                outlet: 'p',
                data: { action: 'preview' },
                resolve: {
                    file: FileResolveService,
                },
            },
            {
                path: 'preview/:id/:mode',
                component: DiskPreviewComponent,
                outlet: 'p'
            },
            {
                path: 'edit/:id',
                component: MSOfficeComponent,
                outlet: 'p',
                data: { action: 'edit', fullScreen: true },
            },

        ]),
    ],
    declarations: [DiskComponent],
    exports: [DiskComponent],
    providers: [SidebarService, FileResolveService, DiskRootFolderGuard],
})
export class DiskModule {
}
