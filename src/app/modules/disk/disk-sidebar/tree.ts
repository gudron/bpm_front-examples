import { TreeItem } from 'app/modules/disk/disk-sidebar/tree-item';
import { ROOT_CATEGORY } from '../../../app.constants';
import { DirectoryModel } from '../../../models/disk.model';

export class Tree {

    root: TreeItem;
    private all: { [id: string]: TreeItem } = {};


    constructor (rootDir: DirectoryModel) {
        this.root = new TreeItem(rootDir);
        this.root.setTree(this);
    }

    addToAll (item: TreeItem) {

        if (this.all[item.model.id] && this.all[item.model.id].model.parent === item.model.parent) {
            return false;
        }
        this.all[item.model.id] = item;
        return true;
    }

    deleteToAll (treeId: string): void {
        delete this.all[treeId];
    }

    getAll (): TreeItem[] {
        return Object.keys(this.all).map(id => this.all[id]);
    }

    getAllActive (): TreeItem[] {
        return this.getAll().filter(item => item.active);
    }

    public add (item: TreeItem): boolean {

        if (item.model.parent && item.model.parent !== ROOT_CATEGORY) {
            let parentItem = null;
            if (parentItem = this.getItemById(item.model.parent)) {
                parentItem.add(item);
                return true;
            } else {
                return false;
            }
        } else {
            this.root.add(item);
            return true;
        }
    }

    getList (parentId = ''): TreeItem[] {
        if (!parentId) {
            return this.root.childs;
        } else {
            let parentItem = null;
            if (parentItem = this.getItemById(parentId)) {
                return parentItem.childs;
            } else {
                return [];
            }
        }
    }

    getItemById (id: string): TreeItem | null {
        return this.all[id] || null;
    }

    getAllChildsOf (id: string): TreeItem[] {
        const result: { [id: string]: TreeItem } = {};
        const recursive = (childs: TreeItem[]) => {
            for (const item of childs) {
                result[item.model.id] = item;
                if (item.childs && item.childs.length) {
                    recursive(item.childs);
                }
            }
        };
        recursive(this.all[id].childs);
        return Object.keys(result).map(key => this.all[key]);
    }
}
