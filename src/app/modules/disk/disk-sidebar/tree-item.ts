import { DirectoryModel } from '../../../models/disk.model';
import { Tree } from './tree';

export enum Action {
    Open = 1,
    Activate,
    Add,
    Rename,
    Move,
}

export declare interface ItemOuterEvent {
    // item: TreeItem;
    itemid?: string;
    item?: TreeItem;
    name?: string;
    directoryId?: string;
    parentId?: string;
    // tree: Tree;
    action: Action;
}

export declare interface ItemEvent {
    item: TreeItem,
    action: Action
}


export class TreeItem {
    model: DirectoryModel; // моделька, которая на самом деле лежит внутри элемента дерева
    private openedFlag = false;
    private activeFlag = false;

    public isAdminOnly = false;

    public get active (): boolean {
        return this.activeFlag;
    }

    public get opened (): boolean {
        return this.openedFlag;
    }

    childs: TreeItem[] = [];

    private _parentItem: TreeItem;

    public tree: Tree;


    constructor (model: DirectoryModel) {
        this.model = model;
    }

    get parentItem (): TreeItem {
        return this._parentItem;
    }

    set parentItem (item) {
        if (this.model.parent === item.model.id) {
            this._parentItem = item;
        } else {
            console.error('parent', item, 'child', this);
            throw new Error(`Try set parent ${item.model.id} for ${this.model.id}`);
        }
    }


    public setTree (tree: Tree) {
        if (!this.tree) {
            this.tree = tree;
        } else {
            console.warn('Trying replace Tree');
        }
    }

    public toggleOpen () {
        this.openedFlag = !this.openedFlag;
        // if (this.openedFlag) {
        //     window.localStorage.setItem()
        // }
    }

    public open () {
        if (!this.opened) {
            this.openedFlag = true;
        }
    }

    public close () {
        if (this.opened) {
            this.openedFlag = false;
        }
    }

    public activate () {
        if (!this.active) {
            this.activeFlag = true;
        }
    }

    public deactivate () {
        this.activeFlag = false;
    }

    add (item: TreeItem) {
        item.setTree(this.tree);
        item.parentItem = this;
        if (this.tree.addToAll(item)) {
            this.childs.push(item);
        }
        // else {
        //     console.warn(`Duplicate child with id ${item.model.id}`);
        // }
    }
}
