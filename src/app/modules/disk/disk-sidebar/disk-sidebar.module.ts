import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ElmaCommonModule } from '../../../../shared/common/common.module';
import { PipesModule } from '../../../pipes/pipes.module';
import { DiskSidebarComponent } from './disk-sidebar.component';
import { NavItemComponent } from './nav-item/nav-item.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ElmaCommonModule,
        PipesModule,
    ],
    declarations: [DiskSidebarComponent, NavItemComponent],
    exports: [DiskSidebarComponent],
})
export class DiskSidebarModule {
}
