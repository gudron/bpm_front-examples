import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DirectoryModel, FileModel } from 'app/models/disk.model';
import { SidebarService } from '../../../../services/sidebar.service';
import { TreeType } from '../disk-sidebar.enum';
import { Action, ItemEvent, TreeItem } from '../tree-item';

@Component({
    selector: 'app-nav-item',
    templateUrl: './nav-item.component.html',
    styleUrls: ['./nav-item.component.scss'],
})
export class NavItemComponent implements OnInit {

    public isAdminOnly = false;

    @Input() movingItem: FileModel | DirectoryModel;

    public get movingDirId (): string {

        if (this.movingItem) {
            return this.movingItem.id;
        }
        return '';
    }

    public TreeType = TreeType;
    @Input() treeType = TreeType.sidebar;

    public get isSidebar (): boolean {
        return this.treeType === TreeType.sidebar ? true : false;
    }

    // id последней открытой директории
    @Output() directoryId = new EventEmitter<string>();

    @Input() item: TreeItem;
    @Output() itemUpdated = new EventEmitter<ItemEvent>();

    constructor (private sidebarService: SidebarService) {
    }

    ngOnInit () {

        if (this.item.isAdminOnly) {
            this.isAdminOnly = true;
        }
    }

    public toggleOpen (event: Event, item: TreeItem) {

        event.stopPropagation();
        event.preventDefault();

        this.itemUpdated.emit({
            item: item,
            action: Action.Open,
        });
    }

    public handleChildItemUpdate (event: ItemEvent) {
        this.itemUpdated.emit(event);
    }

    // получение id выбранной директории перемещения
    public emitDirectoryId (directoryId: string): void {
        this.directoryId.emit(directoryId);
    }

    public closeSidebar () {
        this.sidebarService.close();
    }
}
