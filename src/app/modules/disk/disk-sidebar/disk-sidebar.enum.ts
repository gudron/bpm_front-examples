export enum TreeType {
    sidebar = 'sidebar',
    directory = 'directory',
    file = 'file',
}
