import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output, } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DirectoryModel, FileModel } from 'app/models/disk.model';
import { Tree } from 'app/modules/disk/disk-sidebar/tree';
import { ProfileService } from 'app/services/profile.service';
import { ElmaTranslateService } from '../../../../shared/common';
import { ROOT_CATEGORY } from '../../../app.constants';
import { UserModel } from '../../../models/user.model';
import { SidebarService } from '../../../services/sidebar.service';
import { DiskStore } from '../../../stores/disk.store';
import { baseDirectoryId } from '../disk.enum';
import { UserStore } from './../../../stores/user.store';
import { TreeType } from './disk-sidebar.enum';
import { Action, ItemEvent, ItemOuterEvent, TreeItem } from './tree-item';

// пока сырой вариант дерева, надо будет переделать, чтобы избавиться от неё или сделать её размер динамическим
const MaxTreeItemsCount = 1000;

@Component({
    selector: 'app-navigation-disk',
    templateUrl: './disk-sidebar.component.html',
    styleUrls: ['./disk-sidebar.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiskSidebarComponent implements OnInit, OnDestroy {

    public treeRendered = false;
    public favoriteActiveFlag: boolean;
    // id выбранной директории при варианте, когда компонент используется не в сайдбаре
    public idDirSelect: string;

    private tree: Tree;
    private sidebarSubscription: any;
    private localStorageEnabled = true;
    private localStorageKey = 'elma365.dirs_opened';
    private savedOpenedDirs: string[];
    private userID: string;

    public favoriteRoute: boolean;

    @Input() movingItem: FileModel | DirectoryModel;
    @Input() treeType = TreeType.sidebar;

    public get isSidebar (): boolean {
        return this.treeType === TreeType.sidebar ? true : false;
    }

    // id последней открытой директории
    @Output() directoryId = new EventEmitter<string>();

    public get items (): TreeItem[] {
        return this.tree.getList();
    }

    constructor (
        private diskStore: DiskStore,
        private profileService: ProfileService,
        private userStore: UserStore,
        private cdr: ChangeDetectorRef,
        private router: Router,
        private ts: ElmaTranslateService,
        private activatedRoute: ActivatedRoute,
        private sidebarService: SidebarService,
    ) {
        this.tree = new Tree(this.diskStore.getRootDir()!);
    }

    async ngOnInit () {

        this.favoriteRoute = this.activatedRoute.snapshot.data['favorite'];
        this.localStorageKey = this.localStorageKey + '.' + this.profileService.userId;
        await this.loadDir();
        this.sidebarSubscription = this.diskStore.sidebarEmitter.subscribe((async (event: ItemOuterEvent) => {
            this.processEvent(event);
        }));

    }

    ngOnDestroy () {
        this.sidebarSubscription.unsubscribe();
        this.diskStore.cleanCache();
    }

    public async loadDir (): Promise<void> {

        // TODO: это стоит вытаскивать отудато из глобальных переменных. А то каждый ебашит как хочет
        const isMobile = window.outerWidth < 1024;
        if (isMobile) {
            this.localStorageEnabled = false;
        }

        if (this.profileService.userId == null) {
            throw new Error('Not authorized');
        }
        this.userID = this.profileService.userId;
        //// Начальное состояние sidebar /////
        let totalOpenDirs: string[] = [ROOT_CATEGORY];

        // Итак, чтоб построить дерево нам надо
        // 1) Получить все открытые папки и их путь наверх
        totalOpenDirs = totalOpenDirs.concat(this.getOpenDirectoryAndTrace());
        // 2) Получить текущую папку
        if (this.diskStore.currentDirID) {
            totalOpenDirs.push(this.diskStore.currentDirID);
        }
        totalOpenDirs = this.uniqArr(totalOpenDirs);

        // Суммарный список директорий, который нам надо получить
        // только уберем собственную директорию пользователя, её отдельно получить надо
        const totalDirs = totalOpenDirs;
        totalDirs.push(this.userID);
        // Получить директории, которые находятся в :
        // 1) Корневой директории
        // 2) Текущей директории
        // 3) Во всех открытых директориях

        const treeDirs = await this.diskStore.sidebarAllOpenedDirs(this.uniqArr(totalDirs));
        // нужно допихнуть еще личную директорию пользователя, она в корне, потому будет видна
        const userDir = treeDirs.find(elem => elem.id === this.userID)!;
        const virtualUserDir = new DirectoryModel(this.diskStore, {
            __id: this.userID,
            name: this.ts.get('app.disk.listing.directory@my-files'),
            parent: ROOT_CATEGORY,
            parentsList: [ROOT_CATEGORY],
            __createdAt: '1970-01-01T00:00:00.00000001Z',
        });

        if (userDir) {
            virtualUserDir.setChildren(userDir.children);
            treeDirs[treeDirs.indexOf(userDir)] = virtualUserDir;
        } else {
            treeDirs.push(virtualUserDir);
        }

        await this.renderDir(treeDirs, isMobile);

        if (this.isSidebar && this.favoriteRoute) {
            this.activateFavorite();
        }
        this.treeRendered = true;
        this.cdr.markForCheck();
    }

    public getOpenDirectoryAndTrace (): string[] {

        if (this.localStorageEnabled) {
            try {
                this.savedOpenedDirs = JSON.parse((<any>window).localStorage.getItem(this.localStorageKey));
                if (!this.savedOpenedDirs) {
                    this.savedOpenedDirs = [];
                    window.localStorage.setItem(this.localStorageKey, '[]');
                }
            } catch (e) {
                this.savedOpenedDirs = [];
                window.localStorage.setItem(this.localStorageKey, '[]');
            }
            return this.savedOpenedDirs;
        }

        return [];
    }

    public async renderDir (treeDirs: DirectoryModel[], isMobile: boolean): Promise<void> {

        const mapDirs = new Map<string, DirectoryModel>();
        treeDirs.forEach((item) => {

            mapDirs.set(item.id, item);
        });

        let numberTreeDirs = mapDirs.size;
        let breakPoint = 0;

        while (numberTreeDirs > 0) {

            breakPoint++;
            if (breakPoint > MaxTreeItemsCount) {
                break;
            }

            for (const dir of treeDirs) {

                if (dir.id === ROOT_CATEGORY) {
                    continue;
                }
                if (dir.id === <string>baseDirectoryId.home || dir.id === <string>baseDirectoryId.shared) {
                    dir.name = await this.diskStore.tcBaseDirectoryName(dir.id);
                }

                const item = new TreeItem(dir);

                // директория /home - только для админа видна
                if (dir.id === <string>baseDirectoryId.home) {
                    item.isAdminOnly = true;
                }

                // переименовать те что лежат в /home
                if (dir.parent === <string>baseDirectoryId.home) {
                    try {
                        const user = await this.userStore.getByID(dir.id);
                        dir.setName(user.name);
                    } catch (e) {
                        console.error(e);
                    }
                }

                if (!isMobile) {

                    if (item.model.parent === ROOT_CATEGORY && !item.model.parentOpen) {
                        item.model.parentOpen = true;
                        numberTreeDirs--;
                    }

                    const parentDir = mapDirs.get(item.model.parent);
                    if (!item.model.parentOpen && parentDir && parentDir.parentOpen) {

                        const parentItem = this.tree.getItemById(item.model.parent);
                        if (parentItem) {
                            parentItem.open();
                        }
                        item.model.parentOpen = true;
                        numberTreeDirs--;
                    }
                }

                // сайдбар, кораска папок
                if (this.isSidebar && this.diskStore.currentDirID === item.model.id) {
                    item.activate();
                }

                this.tree.add(item);

                if (item.parentItem && item.parentItem.isAdminOnly) {
                    item.isAdminOnly = true;
                }
            }
        }
    }

    public async handleItemUpdated (event: ItemEvent) {

        let resp: DirectoryModel[];
        switch (+event.action) {
        case Action.Open:

            const item = this.tree.getItemById(event.item.model.id);
            if (!item) {
                console.warn('Item not found');
                return;
            }

            if (this.profileService.userId == null) {
                throw new Error('Not authorized');
            }

            item.toggleOpen();
            this.cdr.markForCheck();

            if (item.opened) {

                const user = await this.userStore.getByID(this.profileService.userId);
                resp = await this.diskStore.loadDirectoriesInDir(event.item.model.id);

                for (const ritem of resp) {
                    if (ritem.id === user.id) {
                        continue; // выбросим директорию админа
                    }
                    const nitem = new TreeItem(ritem);
                    item.add(nitem);

                }

                for (const ritem of resp) {
                    // и чтобы посчитать потомков у этой штуки, надо сразу запросить её директории
                    const chdirs = await this.diskStore.loadDirectoriesInDir(ritem.id);
                    ritem.setChildren(chdirs);
                    if (ritem.id === user.id) {
                        continue; // выбросим директорию админа
                    }
                    const nitem = new TreeItem(ritem);
                    item.add(nitem);
                }

                this.addToOpenedDirs(item.model.id);

            } else {

                for (const childItem of item.childs) {
                    childItem.close();
                }

                this.removeFromOpenedDirs(item.model.id);
            }

            break;
        }
        this.cdr.markForCheck();
    }

    private async setItemTree (user: UserModel, parentItem: TreeItem): Promise<void> {

        const resp = await this.diskStore.loadDirectoriesInDir(parentItem.model.id);

        for (const ritem of resp) {
            if (ritem.id === user.id) {
                continue; // выбросим директорию админа
            }
            const nitem = new TreeItem(ritem);

            const chdirs = await this.diskStore.loadDirectoriesInDir(ritem.id);
            ritem.setChildren(chdirs);
            const newItem = new TreeItem(ritem);

            if (this.savedOpenedDirs.indexOf(ritem.id)) {

                newItem.open();

                if (chdirs.length) {
                    await this.setItemTree(user, newItem);
                }
            }

            parentItem.add(newItem);
        }

    }

    private async processEvent (event: ItemOuterEvent, skipCheck: boolean = false) {
        switch (+event.action) {
        case Action.Activate:
            this.tree.getAllActive().forEach(tritem => {
                tritem.deactivate();
            });

            if (!event.itemid) {
                break;
            }

            let item = this.tree.getItemById(event.itemid);
            if (!item) {
                // Если нет итема в дереве, значит надо добавить его, и всех его родителей
                // начальная директория не корневая, надо достать всех потомков
                const dirParents = await this.diskStore.getDirectoryParentsList(event.itemid);
                // Открываем все родительские директории в цепочке от текущей
                dirParents.forEach((dirp, dirI) => {
                    if (dirI === 0) {
                        // Это директория первого уровня, она у нас уже добавлена
                        const lvl1Dir = this.tree.getItemById(dirp.id)!;
                        lvl1Dir.open();
                    } else {
                        const tritem = new TreeItem(dirp);
                        tritem.open();

                        if (dirp.id === event.itemid) {
                            tritem.activate();
                        }
                        this.tree.add(tritem);

                        if (dirI === dirParents.length - 1) {
                            item = tritem;
                        }
                    }
                });
            }

            if (!item) {
                console.error('NO item');
                return;
            }

            const resp = await this.diskStore.loadDirectoriesInDir(item.model.id);

            for (const nitem of resp) {
                if (nitem.id === this.userID) {
                    continue; // выбросим директорию админа
                }
                item.add(new TreeItem(
                    nitem,
                ));
            }
            item.open();
            item.activate();
            this.tree.getAllActive().forEach(tritem => {
                if (tritem !== item) {
                    tritem.deactivate();
                }
            });

            this.addToOpenedDirs(item.model.id);
            break;

        case Action.Add:
            this.tree.add(event.item!);
            break;

        case Action.Rename:

            if (event.directoryId && event.name) {

                const renameItem = this.tree.getItemById(event.directoryId);
                if (renameItem) {
                    renameItem.model.name = event.name;
                }
            }
            break;

        case Action.Move:

            if (event.itemid && event.parentId) {

                const moveTreeItem = this.tree.getItemById(event.itemid);
                const toTreeItem = this.tree.getItemById(event.parentId);

                if (moveTreeItem && toTreeItem) {

                    const parentTreeItem = moveTreeItem.parentItem;
                    parentTreeItem.model.children = parentTreeItem.model.children.filter((child) => {
                        return child.id !== event.itemid;
                    });
                    parentTreeItem.childs = parentTreeItem.childs.filter((child) => {
                        return child.model.id !== event.itemid;
                    });

                    moveTreeItem.model.parent = event.parentId;
                    moveTreeItem.tree.deleteToAll(event.itemid);
                    toTreeItem.add(moveTreeItem);
                    this.cdr.markForCheck();
                }

            }
            break;
        }

        if (this.favoriteRoute) {
            this.deactivateFavorite();
        }

        if (!skipCheck) {
            this.cdr.markForCheck();
        }
    }

    private addToOpenedDirs (itemid: string) {
        if (!this.localStorageEnabled) {
            return;
        }
        if (this.savedOpenedDirs.indexOf(itemid) === -1) {
            this.savedOpenedDirs.push(itemid);
            window.localStorage.setItem(this.localStorageKey, JSON.stringify(this.savedOpenedDirs));
        }

    }

    private removeFromOpenedDirs (itemid: string) {
        if (!this.localStorageEnabled) {
            return;
        }
        const index = this.savedOpenedDirs.indexOf(itemid);
        if (index > -1) {
            this.savedOpenedDirs.splice(index, 1);
            window.localStorage.setItem(this.localStorageKey, JSON.stringify(this.savedOpenedDirs));
        }
    }


    private uniqArr (arr: string[]): string[] {
        const tmp = [];
        for (let i = 0; i < arr.length; i++) {
            if (tmp.indexOf(arr[i]) === -1) {
                tmp.push(arr[i]);
            }
        }
        return tmp;
    }


    public emitDirectoryId (directoryId: string): void {

        // запоминаем id директории для окраски
        this.idDirSelect = directoryId;

        // снимаем активность с других папок
        this.tree.getAllActive().forEach(treetem => {
            treetem.deactivate();
        });
        // активируем выбарнную
        const item = this.tree.getItemById(directoryId);
        if (item) {
            item.activate();
        }

        this.directoryId.emit(directoryId);
    }

    public activateFavorite (): void {

        this.tree.getAllActive().forEach(treetem => {
            treetem.deactivate();
        });
        this.favoriteActiveFlag = true;
    }

    public deactivateFavorite (): void {
        this.favoriteActiveFlag = false;
    }

    public sidebarClose() {
        this.sidebarService.close();
    }
}
