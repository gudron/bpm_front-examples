import { ElmaTranslateService } from '../../../shared/common';
import { uuid4 } from '../../../shared/utils/uuid';
import { Namespaces, NavTypes, PageType } from '../../app.constants';
import { PageDataJson, PageJson, PageMenuJson } from '../../models/page.json';

export function GetDiskSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-20T07:08:01.140905Z',
        __updatedAt: '2017-07-20T07:08:01.140905Z',

        name: translationService.get('system.pages.files@name'),
        namespace: Namespaces.Global,
        code: 'files',
        type: PageType.Link,
        icon: 'file_type_doc',
        sort: 2,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            columns: 1,
            menu: <PageMenuJson>{
                type: NavTypes.NAV_TYPE_DISK,
            },
            widgets: [],
        },
    };
}
