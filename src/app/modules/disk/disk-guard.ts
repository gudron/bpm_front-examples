import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { DiskStore } from '../../stores/disk.store';

// Перенаправляет корень Диска на последнюю открытую папку
@Injectable()
export class DiskRootFolderGuard implements CanActivate {
    constructor (
        private router: Router,
        private ds: DiskStore,
    ) {
    }

    async canActivate (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const lastFolder = localStorage.getItem('lastFolder');
        if ( lastFolder ) {
            // Проверка на существование папки
            try {
                await this.ds.loadDirectory( lastFolder );
            } catch (e) {
                localStorage.removeItem('lastFolder');
                return true;
            }
            // Перенаправление в последнюю открытую папку
            this.router.navigate(['/', 'files', lastFolder, { outlets: { p: null } }]);
            return false;
        } else {
            // Перенаправление в корневую папку
            return true;
        }
    }
}
