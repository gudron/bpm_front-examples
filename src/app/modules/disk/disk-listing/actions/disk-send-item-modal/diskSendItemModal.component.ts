import { Component, Input } from '@angular/core';
import { FileModel } from '../../../../../models/disk.model';

@Component({
    selector: 'app-disk-send-item-modal',
    templateUrl: './diskSendItemModal.component.html',
    styleUrls: ['./diskSendItemModal.component.scss'],
})

export class DiskSendItemModalComponent {
    @Input() file: FileModel;

    constructor (
    ) {
    }
}
