import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ElmaActiveModal, ElmaTranslateService } from '../../../../../../shared/common';
import { FileModel } from '../../../../../models/disk.model';
import { DiskStore } from '../../../../../stores/disk.store';

@Component({
    selector: 'app-disk-file-rename-modal',
    templateUrl: './diskRenameModal.component.html',
    styleUrls: ['./diskRenameModal.component.scss'],
})
export class DiskRenameModalComponent implements OnInit {

    constructor (
        private diskStore: DiskStore,
        private activeModal: ElmaActiveModal,
        private ts: ElmaTranslateService,
    ) {}

    public renameError = '';
    @Input() currentFile: FileModel;
    public newName: string;
    @Output() refreshFileName = new EventEmitter<string>();

    ngOnInit () {
        this.newName = this.cleanFileName();
    }

    public get currentFiles (): FileModel[] {
        return this.diskStore.currentFiles;
    }

    public cancelRename (): void {
        this.activeModal.dismiss();
    }

    private getExt (filename: string): string {
        const ext = filename.split('.').pop();
        if (ext && ext !== filename) {
            return ext;
        } else {
            return '';
        }
    }

    private cleanFileName() {
        if (this.getExt(this.currentFile.name)) {
            return this.currentFile.name.substr(0, this.currentFile.name.lastIndexOf('.'));
        } else {
            return this.currentFile.name;
        }
    }

    public async updateName (): Promise<void> {

        if (this.newName !== this.cleanFileName()) {

            if (!this.getExt(this.currentFile.name) && this.newName.indexOf('.') !== -1) {
                this.renameError = this.ts.get('app.disk.listing.directory@rename-dot-exist');
            } else {
                try {
                    const renamed = this.newName + (this.getExt(this.currentFile.name) ? '.' + this.getExt(this.currentFile.name) : '');
                    const response = await this.diskStore.renameFile(
                        this.currentFile.id,
                        renamed
                    );

                    if (response.status === 409) {
                        this.renameError = this.ts.get('app.disk.listing.directory@error-name-isset');
                    } else {
                        this.cancelRename();
                    }

                    this.currentFiles.forEach(item => {
                        if (item.id === this.currentFile.id) {
                            item.name = renamed;
                        }
                    });

                    this.refreshFileName.emit(renamed);

                } catch (err) {

                    console.error(err);
                    this.cancelRename();

                }
            }
        } else {
            this.cancelRename();
        }
    }
}
