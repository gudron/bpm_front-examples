import {
    ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, TemplateRef,
    ViewChild,
} from '@angular/core';
import { Response } from '@angular/http';
import { ElmaActiveModal, ElmaModalService, ElmaTranslateService } from '../../../../../../shared/common';
import { DirectoryModel, FileModel } from '../../../../../models/disk.model';
import { DiskStore } from '../../../../../stores/disk.store';
import { TreeType } from '../../../disk-sidebar/disk-sidebar.enum';
import { httpCode } from '../../../disk.enum';
import { MoveEvent, SelectedItem } from '../../disk-listing.interface';
import { IMoveItem } from './diskMoveItemModal.interface';

@Component({
    selector: 'app-disk-move-item-modal',
    templateUrl: './diskMoveItemModal.component.html',
    styleUrls: ['./diskMoveItemModal.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiskMoveItemModalComponent implements OnInit {

    private numberMovedItem: number;
    public targetDirectoryID: string;

    public newFileName = '';
    public fileExtension = '';

    public currentMovingItem: FileModel | DirectoryModel;
    private successfullyMovedItemIDs: SelectedItem[] = [];

    public TreeType = TreeType;
    @Input() movingItemList: (FileModel | DirectoryModel)[] = [];

    public get movingType (): string {
        for (const item of this.movingItemList) {
            if (item instanceof DirectoryModel) {
                return <string>TreeType.directory;
            }
        }

        return <string>TreeType.file;
    }

    public parentDirId = '';

    public setParentDirId (): void {

        if (this.movingItemList[0]) {
            const movingItem = this.movingItemList[0];
            if (movingItem instanceof DirectoryModel) {
                this.parentDirId = movingItem.parent;
            } else if (movingItem instanceof FileModel) {
                this.parentDirId = movingItem.directory;
            }
        }
    }

    public get movedItemForBuildingTree (): FileModel | DirectoryModel {

        for (const item of this.movingItemList) {
            if (item instanceof DirectoryModel) {
                return item;
            }
        }
        return this.movingItemList[0];
    }

    @Output() movedItemIDs = new EventEmitter<MoveEvent>();

    @ViewChild('moveItemOption') public moveItemOption: TemplateRef<ElmaActiveModal>;

    constructor (
        private cdr: ChangeDetectorRef,
        public activeModal: ElmaActiveModal,
        private diskStore: DiskStore,
        private modalService: ElmaModalService,
        private ts: ElmaTranslateService,
    ) {
    }

    ngOnInit () {
        this.setParentDirId();
    }

    public setTargetDirectory (dirID: string) {
        this.targetDirectoryID = dirID;
    }

    public moveItems () {
        this.numberMovedItem = 0;
        const bodyData: IMoveItem = {
            dirId: this.targetDirectoryID,
        };
        this.movingItem(this.movingItemList[0], bodyData);

        this.activeModal.close();
        this.cdr.markForCheck();
    }

    public async movingItem (item: FileModel | DirectoryModel, bodyData: any, numberMovedItem: number = 0): Promise<void> {

        this.numberMovedItem = numberMovedItem;

        if (this.numberMovedItem > this.movingItemList.length - 1) {

            // все  помували
            const movedItemIDs = {
                targetId: this.targetDirectoryID,
                selectedItem: this.successfullyMovedItemIDs,
            };
            this.movedItemIDs.emit(movedItemIDs);
            return;
        }
        this.currentMovingItem = item;

        let response: Response;
        if (item instanceof DirectoryModel) {
            response = await this.diskStore.moveDirectory(item.id, bodyData);
        } else {
            response = await this.diskStore.moveFile(item.id, bodyData);
        }

        switch (response.status) {

        case httpCode.conflict:
            try {
                const newNameJson = await this.diskStore
                    .getFreeName(bodyData.dirId, { name: item.name }, 'directory');
                const jsonResponse = newNameJson.json();

                if (item instanceof FileModel) {
                    this.sliceNewName(jsonResponse['name']);
                } else {
                    this.fileExtension = '';
                    this.newFileName = jsonResponse['name'];
                }

                this.modalService.open(this.moveItemOption, {
                    size: 'sm',
                    title: this.ts.get('app.disk.listing.directory@move'),
                });

            } catch (err) {
                throw new Error('Can`t get item name');
            }
            break;

        case httpCode.statusOK:
            numberMovedItem++;
            this.successfullyMovedItemIDs.push(item);
            this.movingItem(this.movingItemList[numberMovedItem], bodyData, numberMovedItem);
            break;

        default:
            throw new Error('Can`t move item');
        }
    }

    public skipMovingItem () {
        const bodyData: IMoveItem = {
            dirId: this.targetDirectoryID,
        };
        this.numberMovedItem++;

        this.movingItem(this.movingItemList[this.numberMovedItem], bodyData, this.numberMovedItem);
    }

    public moveItemWithRename (): void {
        const fullName = this.newFileName + this.fileExtension;
        const bodyData: IMoveItem = {
            dirId: this.targetDirectoryID,
            name: fullName,
        };

        this.movingItem(this.movingItemList[this.numberMovedItem], bodyData, this.numberMovedItem);
    }

    private sliceNewName (fileName: string): void {
        const resMatch = fileName.match(/\.\D+$/);
        this.fileExtension = resMatch && resMatch[0] ? resMatch[0] : '';
        this.newFileName = fileName.replace(/\.\D+$/, '');
    }
}
