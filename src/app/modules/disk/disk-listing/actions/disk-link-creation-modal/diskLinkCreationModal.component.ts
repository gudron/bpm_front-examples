import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, } from '@angular/core';
import { ElmaActiveModal } from '../../../../../../shared/common';
import { LinkType } from '../../../../../models/disk.json';
import { DirectoryModel, FileModel } from '../../../../../models/disk.model';
import { DiskStore } from '../../../../../stores/disk.store';
import { TreeType } from '../../../disk-sidebar/disk-sidebar.enum';

@Component({
    selector: 'app-disk-link-creation-modal',
    templateUrl: './diskLinkCreationModal.component.html',
    styleUrls: ['./diskLinkCreationModal.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiskLinkCreationModalComponent implements OnInit {

    public targetDirectoryID: string;

    public TreeType = TreeType;

    @Input() selectedItemList: (FileModel | DirectoryModel)[] = [];
    public parentDirId = '';

    public setParentDirId (): void {

        if (this.selectedItemList[0]) {
            const movingItem = this.selectedItemList[0];
            this.parentDirId = movingItem instanceof DirectoryModel ? movingItem.parent : movingItem.directory;
        }
    }

    constructor (
        private diskStore: DiskStore,
        public activeModal: ElmaActiveModal,
        private cdr: ChangeDetectorRef,
    ) {
    }

    ngOnInit () {
        this.setParentDirId();
    }


    public setTargetDirectory (dirID: string) {
        this.targetDirectoryID = dirID;
    }

    public async createLink () {

        this.selectedItemList.forEach(async item => {

            const typeLink = item instanceof FileModel ? LinkType.File : LinkType.Directory;
            const jsonLink = {
                name: item.name,
                directory: this.targetDirectoryID,
                target: item.id,
                type: typeLink,
                favorite: false,
            };

            await this.diskStore.createLink(jsonLink);
        });

        this.activeModal.close();
        this.cdr.markForCheck();
    }
}
