import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild, } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ElmaModalService, ElmaPopoverComponent, ElmaTranslateService } from '../../../../../../shared/common';
import { LinkType } from '../../../../../models/disk.json';
import { DirectoryModel } from '../../../../../models/disk.model';
import { ProfileService } from '../../../../../services/profile.service';
import { DiskStore } from '../../../../../stores/disk.store';
import { DiskLinkCreationModalComponent } from '../../actions/disk-link-creation-modal/diskLinkCreationModal.component';
import { DiskMoveItemModalComponent } from '../../actions/disk-move-item-modal/diskMoveItemModal.component';
import { MoveEvent } from '../../disk-listing.interface';
import { DirectoryPermissionsComponent } from '../directory-permissions/directory-permissions.component';


@Component({
    selector: 'app-directory-context-menu',
    templateUrl: './directory-context-menu.component.html',
    styleUrls: ['./directory-context-menu.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DirectoryContextMenuComponent implements OnInit {

    @Input() directory: DirectoryModel;
    @Input() stayActiveElement: HTMLElement | null;

    public get currentDirs (): DirectoryModel[] {
        return this.diskStore.currentDirs;
    }

    public set currentDirs (dirsAndFiles: DirectoryModel[]) {
        this.diskStore.currentDirs = dirsAndFiles;
    }

    @Output() changeName = new EventEmitter<boolean>();
    @Output() moveItem = new EventEmitter<MoveEvent>();

    @ViewChild('ctxMenu') public ctxMenu: ElmaPopoverComponent;

    private movedDirsSubscribtion: Subscription;

    public readonly translatePrefix = 'app.disk.listing.directory.context-menu@';

    constructor (
        private cdr: ChangeDetectorRef,
        private modalService: ElmaModalService,
        private profileService: ProfileService,
        private diskStore: DiskStore,
        private ts: ElmaTranslateService,
    ) {
    }

    ngOnInit () {
    }

    public show (event: MouseEvent) {
        this.ctxMenu.toggle(<HTMLElement>event.target);
        this.cdr.markForCheck();
    }

    public openDirPermissions () {
        const modalRef = this.modalService.open(DirectoryPermissionsComponent,
            {
                title: this.ts.get('app.disk.listing.directory@permissions'),
                size: 'md',
            });
        modalRef.componentInstance.directory = this.directory;
        this.ctxMenu.hide();
    }

    public openCreateLink () {
        const modalRef = this.modalService.open(DiskLinkCreationModalComponent);
        modalRef.componentInstance.selectedItemList = [this.directory];
        this.ctxMenu.hide();
    }

    public async addToFavorites (): Promise<void> {

        const directory = this.directory;

        try {
            const userId = this.profileService.userId ? this.profileService.userId : '';
            const jsonLink = {
                name: directory.name,
                directory: userId,
                target: directory.id,
                type: LinkType.Directory,
                favorite: true,
            };
            await this.diskStore.createLink(jsonLink);
        } catch (error) {
            console.warn(error);
        }
        this.ctxMenu.hide();
    }

    public rename (): void {
        this.changeName.emit(true);
        this.ctxMenu.hide();
    }

    public openMoveDir (): void {

        const modalRef = this.modalService.open(DiskMoveItemModalComponent);
        const moveDirComponent = <DiskMoveItemModalComponent>modalRef.componentInstance;
        moveDirComponent.movingItemList = [this.directory];
        this.movedDirsSubscribtion = modalRef.componentInstance.movedItemIDs
            .subscribe((itemIDList: MoveEvent) => {
                this.handleMovedDirs(itemIDList);
            })
        ;
        this.ctxMenu.hide();
    }

    public handleMovedDirs (moveEvent: MoveEvent): void {

        this.diskStore.handleMovedItems(moveEvent);
        this.diskStore.outputResultMoveDir(moveEvent);
        this.moveItem.emit(moveEvent);
        this.movedDirsSubscribtion.unsubscribe();
    }
}
