import {
    ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, HostBinding, Input, OnInit, Output,
    ViewChild,
} from '@angular/core';
import * as moment from 'moment';
import { ElmaModalService, ElmaTranslateService } from '../../../../../shared/common';
import { PermissionsComponent } from '../../../../common/permissions/permissions.component';
import { LinkType } from '../../../../models/disk.json';
import { DirectoryModel } from '../../../../models/disk.model';
import { ProfileService } from '../../../../services/profile.service';
import { DiskStore } from '../../../../stores/disk.store';
import { DiskLinkCreationModalComponent } from '../actions/disk-link-creation-modal/diskLinkCreationModal.component';
import { MoveEvent, SelectedItem } from '../disk-listing.interface';
import { DirectoryPermissionsComponent } from './directory-permissions/directory-permissions.component';

@Component({
    selector: 'app-disk-directory',
    templateUrl: './disk-directory.component.html',
    styleUrls: ['./disk-directory.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiskDirectoryComponent implements OnInit {

    @Input() root: boolean;
    @Input() directory: DirectoryModel;
    @Input() renderCheckbox = true;
    @Output() directoryChange = new EventEmitter<DirectoryModel>();
    @Output() open = new EventEmitter<string>();

    onNameChange (newName: string) {
        this.directory.name = newName;
        this.directoryChange.emit(this.directory);
    }

    @Output() moveDirectory = new EventEmitter<MoveEvent>();

    // выделение директории, checked - выделили или сняли выделение
    @Output() directorySelected: EventEmitter<{ checked: boolean, selectedItem: SelectedItem }> =
        new EventEmitter<{ checked: boolean, selectedItem: SelectedItem }>();

    @ViewChild('permComponent') private permComponent: PermissionsComponent;
    @ViewChild('nameDir') nameDir: ElementRef;
    @HostBinding('class.is-active') public selected = false;
    @HostBinding('class.list-item') public listItem = true;

    public showMenuRename: boolean;
    public oldName: string;
    public renameError: string;

    constructor (
        private cdr: ChangeDetectorRef,
        private modalService: ElmaModalService,
        private profileService: ProfileService,
        private diskStore: DiskStore,
        private ts: ElmaTranslateService,
    ) {
    }

    ngOnInit () {
    }

    public createdOrUpdated () {
        const justCreated = moment(this.directory.createdAt).isSame(this.directory.updatedAt);
        const momentLabel = ' ' + moment(this.directory.updatedAt).format('DD.MM.YYYY HH:mm');
        const text = (justCreated
                        ? this.ts.get('app.disk.listing.directory@created-label')
                        : this.ts.get('app.disk.listing.directory@updated-label')
                        ) + momentLabel;
        return text;
    }

    public selectRow (event: boolean) {

        this.selected = !this.selected;
        const checked = event;
        this.directorySelected.emit({ checked: checked, selectedItem: this.directory });
    }

    public openDirPermissions () {
        const modalRef = this.modalService.open(DirectoryPermissionsComponent);
        modalRef.componentInstance.directory = this.directory;
    }

    public openCreateLink () {
        const modalRef = this.modalService.open(DiskLinkCreationModalComponent);
        modalRef.componentInstance.selectedFiles = [this.directory];
    }

    public async addToFavorites (): Promise<void> {

        const directory = this.directory;

        try {
            const userId = this.profileService.userId ? this.profileService.userId : '';
            const jsonLink = {
                name: directory.name,
                directory: userId,
                target: directory.id,
                type: LinkType.Directory,
                favorite: true,
            };
            await this.diskStore.createLink(jsonLink);
        } catch (error) {
            console.warn(error);
        }
    }

    public openMenuRename (): void {

        this.showMenuRename = true;
        this.oldName = this.directory.name;
        this.renameError = '';

        setTimeout(() => {
            this.nameDir.nativeElement.focus();
        }, 0);
    }

    public cancelRename (): void {
        this.directory.name = this.oldName;
        this.showMenuRename = false;
    }

    public async updateName (): Promise<void> {

        if (this.oldName !== this.directory.name) {

            try {
                const response = await this.diskStore.renameDirectory(this.directory.id, this.directory.name);

                if (response.status === 409) {
                    this.renameError = 'Директория с таким именем уже существует';
                } else {
                    this.showMenuRename = false;
                    this.diskStore.renameDirectoryEventSidebar(this.directory.name, this.directory.id);
                }

            } catch (err) {

                console.error(err);
                this.showMenuRename = false;

            } finally {

                this.cdr.markForCheck();
            }
        } else {
            this.cancelRename();
        }
    }

    public movingDirectory (moveEvent: MoveEvent): void {
        this.moveDirectory.emit(moveEvent);
    }
}
