import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ElmaActiveModal } from '../../../../../../shared/common';
import { PermissionsComponent } from '../../../../../common/permissions/permissions.component';
import { DirectoryModel } from '../../../../../models/disk.model';
import { PermissionType } from '../../../../../models/permission.json';
import { PermissionsModel } from '../../../../../models/permission.model';
import { DiskStore } from '../../../../../stores/disk.store';

@Component({
    selector: 'app-directory-permissions',
    templateUrl: './directory-permissions.component.html',
    styleUrls: ['./directory-permissions.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DirectoryPermissionsComponent implements OnInit {
    public permissions: PermissionsModel;

    // для permissions
    public DirectoryTotalPermissions = [
        PermissionType.Read,
        PermissionType.Create,
        PermissionType.Update,
        PermissionType.Delete,
        PermissionType.Assign,
    ];

    @Input() directory: DirectoryModel;

    @ViewChild('permComponent') private permComponent: PermissionsComponent;

    constructor (
        private diskStore: DiskStore,
        private cdr: ChangeDetectorRef,
        public activeModal: ElmaActiveModal,
    ) {
    }

    async ngOnInit () {
        try {
            const dirPermissions = await this.diskStore.getDirPermissions(this.directory.id);
            if (!dirPermissions.inheritParent) {
                // подсыпем родительских прав
                const parentDirPermissions = await this.diskStore.getDirPermissions(this.directory.parent);
                for (const perm of parentDirPermissions.values) {
                    perm.inherited = true;
                }
                dirPermissions.values.push(...parentDirPermissions.values);
            }
            this.permissions = dirPermissions;
        } catch (e) {
            throw e;
        } finally {
            this.cdr.markForCheck();
        }
    }


    public async savePermissions () {
        try {
            await this.diskStore.saveDirPermissions(this.directory.id, this.permComponent.permissions!.toJson());
        } catch (e) {
            throw e;
        }
        this.activeModal.dismiss();
        this.cdr.markForCheck();
    }
}
