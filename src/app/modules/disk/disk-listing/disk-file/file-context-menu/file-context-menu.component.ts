import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ElmaModalService, ElmaPopoverComponent, ElmaTranslateService } from '../../../../../../shared/common';
import { LinkType } from '../../../../../models/disk.json';
import { FileModel } from '../../../../../models/disk.model';
import { PermissionType } from '../../../../../models/permission.json';
import { PermissionsModel } from '../../../../../models/permission.model';
import { ProfileService } from '../../../../../services/profile.service';
import { DiskStore } from '../../../../../stores/disk.store';
import { DiskLinkCreationModalComponent } from '../../actions/disk-link-creation-modal/diskLinkCreationModal.component';
import { DiskMoveItemModalComponent } from '../../actions/disk-move-item-modal/diskMoveItemModal.component';
import { DiskRenameModalComponent } from '../../actions/disk-rename-modal/diskRenameModal.component';
import { DiskPermissionsComponent } from '../../../disk-preview/disk-permissions/disk-permissions.component';
import { DiskSendItemModalComponent } from '../../actions/disk-send-item-modal/diskSendItemModal.component';
import { MoveEvent } from '../../disk-listing.interface';

@Component({
    selector: 'app-file-context-menu',
    templateUrl: './file-context-menu.component.html',
    styleUrls: ['./file-context-menu.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FileContextMenuComponent implements OnInit {

    private movedFilesSubscribtion: Subscription;

    @Input() menuElement: ElementRef;
    @Input() src: string;
    @Input() file: FileModel;
    @Input() renameError: string;
    @Output() changeName = new EventEmitter<boolean>();
    @Output() saveName = new EventEmitter<boolean>();
    @Output() cancelSaveName = new EventEmitter<boolean>();
    @Output() moveItem = new EventEmitter<MoveEvent>();
    @Output() refreshName = new EventEmitter<string>();

    @Input() stayActiveElement: HTMLElement | null;

    // открытие сайдбара инфо о файле
    @Output() onShowFileInfo = new EventEmitter<any>();

    @ViewChild('ctxMenu') public ctxMenu: ElmaPopoverComponent;
    @ViewChild('nameFile') nameFile: ElementRef;

    public get currentFiles (): FileModel[] {
        return this.diskStore.currentFiles;
    }

    public set currentFiles (dirsAndFiles: FileModel[]) {
        this.diskStore.currentFiles = dirsAndFiles;
    }

    public readonly translatePrefix = 'app.disk.listing.file.context-menu@';

    constructor (
        private cdr: ChangeDetectorRef,
        private modalService: ElmaModalService,
        private diskStore: DiskStore,
        private profileService: ProfileService,
        private router: Router,
        private translateService: ElmaTranslateService,
    ) {
    }

    ngOnInit () {
    }

    public openSendFileMenu(): void {
        const modalRef = this.modalService.open(DiskSendItemModalComponent,
            {
                title: this.translateService.get('app.disk.listing.directory@send'),
                size: 'sm',
            });
        const sendFileComponent = <DiskSendItemModalComponent>modalRef.componentInstance;
        sendFileComponent.file = this.file;
        this.ctxMenu.hide();
    }

    public show (event: MouseEvent): void {
        this.ctxMenu.toggle(<HTMLElement>event.target);
        this.cdr.markForCheck();
    }

    public async openPermissions() {
        this.filePermissions = await this.getFilePermissions(this.file);
        const modalRef = this.modalService.open(
            DiskPermissionsComponent,
            {
                title: this.translateService.get('app.common.permissions@permissions'),
                size: 'md',
            }
        );
        modalRef.componentInstance.filePermissions = this.filePermissions;
        modalRef.componentInstance.currentFile = this.file;
        modalRef.componentInstance.permissionTypeCreate = PermissionType.Create;
        this.ctxMenu.hide();
    }

    // разрешения для файла
    public filePermissions: PermissionsModel;

    private async getFilePermissions (file: FileModel): Promise<PermissionsModel> {
        const filePermissions = await this.diskStore.getFilePermissions(file.id);

        if (!filePermissions.inheritParent) {
            const parentPermissions = await this.diskStore.getDirPermissions(file.directory);
            for (const perm of parentPermissions.values) {
                perm.inherited = true;
            }
            filePermissions.values.push(...parentPermissions.values);
        }
        return filePermissions;
    }


    public openMoveFile (): void {

        const modalRef = this.modalService.open(DiskMoveItemModalComponent,
            {
                title: this.translateService.get('app.disk.listing.directory@title-move-modal'),
                size: 'sm',
            });
        const moveFileComponent = <DiskMoveItemModalComponent>modalRef.componentInstance;
        moveFileComponent.movingItemList = [this.file];
        this.movedFilesSubscribtion = modalRef.componentInstance
            .movedItemIDs.subscribe((moveEvent: MoveEvent) => {
                this.handleMovedFiles(moveEvent);
            })
        ;
        this.ctxMenu.hide();
    }

    public handleMovedFiles (moveEvent: MoveEvent): void {

        this.diskStore.handleMovedItems(moveEvent);
        this.diskStore.outputResultMoveDir(moveEvent);
        this.moveItem.emit(moveEvent);
        this.movedFilesSubscribtion.unsubscribe();
    }

    public openCreateLink () {
        const modalRef = this.modalService.open(DiskLinkCreationModalComponent,
            {
                title: this.translateService.get('app.disk.listing.directory@title-createlink-modal'),
                size: 'sm',
            });
        const linkCreationModalComponent = <DiskLinkCreationModalComponent>modalRef.componentInstance;
        linkCreationModalComponent.selectedItemList = [this.file];
        this.ctxMenu.hide();
    }

    public openRenamePopup () {
        const modalRef = this.modalService.open(DiskRenameModalComponent,
            {
                title: this.translateService.get('app.disk.disk-listing.disk-file.file-context-menu@rename-title'),
                size: 'sm',
            });
        const renameModalComponent = <DiskRenameModalComponent>modalRef.componentInstance;
        renameModalComponent.currentFile = this.file;
        renameModalComponent.refreshFileName.subscribe(($e: string) => {
            this.refreshName.emit($e);
        })
    }

    public async addToFavorites (): Promise<void> {

        const file = this.file;

        try {
            const userId = this.profileService.userId ? this.profileService.userId : '';
            const jsonLink = {
                name: file.name,
                directory: userId,
                target: file.id,
                type: LinkType.File,
                favorite: true,
            };
            await this.diskStore.createLink(jsonLink);
            this.ctxMenu.hide();
        } catch (error) {
            console.warn(error);
        }
    }

    public showPropsMenu (): void {
        this.onShowFileInfo.emit();
    }

}
