import {
    ChangeDetectionStrategy, Component, ElementRef, EventEmitter, HostBinding, Input, OnInit, Output,
    ViewChild,
} from '@angular/core';
import * as moment from 'moment';
import { ElmaTranslateService } from '../../../../../shared/common';
import { FileModel } from '../../../../models/disk.model';
import { DiskStore } from '../../../../stores/disk.store';
import { SelectedItem } from '../disk-listing.interface';

@Component({
    selector: 'app-disk-file',
    templateUrl: './disk-file.component.html',
    styleUrls: ['./disk-file.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiskFileComponent implements OnInit {

    public oldName: string;
    public renameError: string;

    @Input() file: FileModel;

    @Output() fileSelected: EventEmitter<{ checked: boolean, selectedItem: SelectedItem }>
        = new EventEmitter<{ checked: boolean, selectedItem: SelectedItem }>();
    @Output() open = new EventEmitter<string>();

    @ViewChild('nameFile') nameFile: ElementRef;
    @HostBinding('class.is-active') public selected = false;
    @HostBinding('class.list-item') public listItem = true;

    public showMenuRename: boolean;

    constructor (
        private diskStore: DiskStore,
        private ts: ElmaTranslateService,
    ) {
    }

    ngOnInit () {
    }

    public createdOrUpdated () {
        const justCreated = moment(this.file.createdAt).isSame(this.file.updatedAt);
        const momentLabel = ' ' + moment(this.file.updatedAt).format('DD.MM.YYYY HH:mm');
        const text = (justCreated
                ? this.ts.get('app.disk.listing.directory@created-label')
                : this.ts.get('app.disk.listing.directory@updated-label')
        ) + momentLabel;
        return text;
    }

    public selectRow (event: boolean) {

        this.selected = !this.selected;
        const checked = event;
        this.fileSelected.emit({ checked: checked, selectedItem: this.file });
    }

    public async downloadFile () {
        try {
            const url = await this.diskStore.fileGetlink(this.file);
            if (url) {
                window.open(url, '_self');
            }
        } catch (e) {
            console.error('Can`t download file: ', e);
        }
    }
}
