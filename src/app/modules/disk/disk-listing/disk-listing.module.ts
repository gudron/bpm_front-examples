import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MomentModule } from 'angular2-moment';
import { MobxAngularModule } from 'mobx-angular';
import { NgUploaderModule } from 'ngx-uploader';
import { ElmaCommonModule } from '../../../../shared/common';
import { AppCommonModule } from '../../../common/common.module';
import { PipesModule } from '../../../pipes/pipes.module';
import { AppSharedModule } from '../../../shared/shared.module';
import { DiskSidebarModule } from '../disk-sidebar/disk-sidebar.module';
import { DiskLinkCreationModalComponent } from './actions/disk-link-creation-modal/diskLinkCreationModal.component';
import { DiskMoveItemModalComponent } from './actions/disk-move-item-modal/diskMoveItemModal.component';
import { DiskRenameModalComponent } from './actions/disk-rename-modal/diskRenameModal.component';
import { DiskSendItemModalComponent } from './actions/disk-send-item-modal/diskSendItemModal.component';
import { DirectoryContextMenuComponent } from './disk-directory/directory-context-menu/directory-context-menu.component';
import { DirectoryPermissionsComponent } from './disk-directory/directory-permissions/directory-permissions.component';
import { DiskDirectoryComponent } from './disk-directory/disk-directory.component';
import { DiskFileComponent } from './disk-file/disk-file.component';
import { FileContextMenuComponent } from './disk-file/file-context-menu/file-context-menu.component';
import { DiskListingComponent } from './disk-listing.component';
import { DiskSymLinkComponent } from './disk-symlink/disk-symlink.component';
import { DiskUploadComponent } from './disk-upload/disk-upload.component';


@NgModule({
    imports: [
        CommonModule,
        AppCommonModule,
        PipesModule,
        FormsModule,
        MomentModule,
        NgUploaderModule,
        RouterModule,
        DiskSidebarModule,
        MobxAngularModule,
        ElmaCommonModule,
        AppSharedModule,
    ],
    declarations: [
        DiskListingComponent,
        DiskFileComponent,
        FileContextMenuComponent,
        DiskDirectoryComponent,
        DirectoryPermissionsComponent,
        DiskUploadComponent,
        DiskMoveItemModalComponent,
        DiskSendItemModalComponent,
        DiskLinkCreationModalComponent,
        DiskRenameModalComponent,
        DiskSymLinkComponent,
        DirectoryContextMenuComponent,
    ],
    exports: [DiskListingComponent, DiskLinkCreationModalComponent, FileContextMenuComponent],
    entryComponents: [
        DirectoryPermissionsComponent,
        DiskMoveItemModalComponent,
        DiskRenameModalComponent,
        DiskSendItemModalComponent,
        DiskLinkCreationModalComponent,
    ],
})
export class DiskListingModule {
}
