import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UploadFile } from 'ngx-uploader';

@Component({
    selector: 'app-disk-upload',
    templateUrl: './disk-upload.component.html',
    styleUrls: ['./disk-upload.component.scss'],
})
export class DiskUploadComponent implements OnInit {

    @Input() file: UploadFile | UploadFile;

    @Output() uploadCanceled: EventEmitter<string> = new EventEmitter<string>();

    constructor () {
    }

    ngOnInit () {
    }

    public cancelUpload (id: string) {
        this.uploadCanceled.emit(id);
    }

}
