import { DirectoryModel, FileModel, LinkModel } from '../../../models/disk.model';
import * as moment from 'moment';

export interface IMoveFile {
    id: string;
    name: string;
}

export type SelectedItem = DirectoryModel | FileModel | LinkModel;

export interface SortItem {
    [index: string]: moment.Moment | string | number;
}

export interface MoveEvent {
    targetId: string;
    selectedItem: SelectedItem[];
}

export interface SortType {
    name: string;
    size: string;
    updatedAt: string;
}

export interface SortDirection {
    sortType: string;
    increment: boolean;
}


