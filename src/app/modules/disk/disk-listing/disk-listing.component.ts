import {
    ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, TemplateRef,
    ViewChild,
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { computed, observable } from 'mobx-angular';
import * as moment from 'moment';
import { UploadFile, UploadInput, UploadOutput } from 'ngx-uploader'; // UploadStatus
import { Subscription } from 'rxjs/Subscription';
import { ElmaActiveModal, ElmaModalService, ElmaUIService } from '../../../../shared/common';
import { Button } from '../../../../shared/common/buttons';
import { Crumb, CrumbsService } from '../../../../shared/common/crumbs';
import { ElmaPopoverComponent } from '../../../../shared/common/elma-popover';
import { ElmaPopupComponent } from '../../../../shared/common/elma-popup/elma-popup.component';
import { ElmaTranslateService } from '../../../../shared/common/translate/translate.service';
import { PermissionError, ROOT_CATEGORY } from '../../../app.constants';
import { DirectoryJson, FileJson, LinkType } from '../../../models/disk.json';
import { DirectoryModel, FileModel, LinkModel, PostlinkModel } from '../../../models/disk.model';
import { ProfileService } from '../../../services/profile.service';
import { InputFileAccept } from '../../../shared/elma-file-uploader/elma-file-uploader.enum';
import { UploadedFileList } from '../../../shared/elma-file-uploader/elma-file-uploader.interface';
import { HeaderMobileButton } from '../../../shared/headers/buttons/mobile-buttons.component';
import { DiskStore } from '../../../stores/disk.store';
import { UserStore } from '../../../stores/user.store';
import { ExtPictureList } from '../disk-preview/disk-preview.const';
import { TreeType } from '../disk-sidebar/disk-sidebar.enum';
import { DiskLinkCreationModalComponent } from './actions/disk-link-creation-modal/diskLinkCreationModal.component';
import { DiskMoveItemModalComponent } from './actions/disk-move-item-modal/diskMoveItemModal.component';
import { MoveEvent, SelectedItem, SortDirection, SortItem, SortType } from './disk-listing.interface';

@Component({
    selector: 'app-disk-listing',
    templateUrl: './disk-listing.component.html',
    styleUrls: ['./disk-listing.component.scss'],
})
export class DiskListingComponent implements OnInit, OnDestroy {

    public TreeType = TreeType;
    public currentDirectoryID: string;
    public favoriteRoute: boolean;
    // Создание новой директории
    public creatingNewDirectory = false; // флаг создания новой директории
    public newDirectoryError: string;
    public newDirectory: DirectoryJson = {
        name: '',
        parent: '',
    };

    public buttonsList = [{}];

    @Input() isModalWindow = false;
    @Input() multipleSelect = false;
    @Input() fileTypeRender: InputFileAccept = InputFileAccept.All;
    @Output() downloadFileIdList = new EventEmitter<Array<UploadedFileList>>();
    @ViewChild('createLinkPopup') createLinkPopup: ElmaPopupComponent;
    @ViewChild('moveItemPopup') moveItemPopup: ElmaPopupComponent;
    @ViewChild('officeFileName') officeFileNamePopup: ElmaPopupComponent;
    @ViewChild('officeFileNameInput') officeFileNameInput: ElementRef;
    @ViewChild('deletePopover') deletePopover: ElmaPopoverComponent;
    @ViewChild('createFileMobileButton') createFileMobileButton: TemplateRef<Button>;
    @Input() movingItemList: (FileModel | DirectoryModel)[] = [];

    @computed
    public get currentDirectories (): DirectoryModel[] {

        if (this.favoriteRoute) {
            return [];
        }

        return this.diskStore.currentDirs;
    }

    public set currentDirectories (currentDirs: DirectoryModel[]) {
        this.diskStore.currentDirs = currentDirs;
    }

    @computed
    public get currentFiles (): FileModel[] {

        if (this.favoriteRoute) {
            return [];
        }

        return this.diskStore.currentFiles;
    }

    public set currentFiles (currentFiles: FileModel[]) {
        this.diskStore.currentFiles = <FileModel[]>this.fileLikeItemTypeFilter(currentFiles);
    }

    @computed
    public get currentLinks (): LinkModel[] {
        return this.diskStore.currentLinks;
    }

    public set currentLinks (currentLinks: LinkModel[]) {
        this.diskStore.currentLinks = <LinkModel[]>this.fileLikeItemTypeFilter(currentLinks);
    }

    public uploadingFiles: UploadFile[] = [];

    public uploadInput: EventEmitter<UploadInput> = new EventEmitter<UploadInput>();
    public dragOver: boolean;

    private subscribe: Subscription;
    private movedFilesSubscribtion: Subscription;

    public permissionDenied: boolean; // флажок, что доступ запрещен
    public usersHomeDirID = '4e4fbacd-5575-58f6-9ac2-af6ef856bc7b';

    public get selectedItemList (): SelectedItem[] {
        return this.diskStore.selectedItemList;
    }

    public set selectedItemList (selectedItemList: SelectedItem[]) {
        this.diskStore.selectedItemList = selectedItemList;
    }

    public get selectedLinkList (): SelectedItem[] {
        return this.selectedItemList.filter((item: SelectedItem) => item instanceof LinkModel);
    }

    public get selectedFileList (): SelectedItem[] {
        return this.selectedItemList.filter((item: SelectedItem) => item instanceof FileModel);
    }

    public get selectedDirList (): SelectedItem[] {
        return this.selectedItemList.filter((item: SelectedItem) => item instanceof DirectoryModel);
    }

    public get selectedDirAndFileList (): (DirectoryModel | FileModel)[] {
        const movingDirAndFile: (DirectoryModel | FileModel)[] = [];
        this.selectedItemList.forEach((item: SelectedItem) => {
            if (item instanceof DirectoryModel || item instanceof FileModel) {
                movingDirAndFile.push(item);
            }
        });
        return movingDirAndFile;
    }


    // для мобилки
    public mobilePlusExtended = false; // Плюсик для создания файлов и директорий на мобилке

    @observable
    private filesLoaded = false;

    @observable
    private dirsLoaded = false;

    @observable
    private linksLoaded = false;

    @observable
    private allItemLoaded = false;

    @computed
    public get contentLoaded (): boolean {
        return this.filesLoaded && this.dirsLoaded && this.linksLoaded
            || this.allItemLoaded;
    }

    public get folderIsEmpty (): boolean {
        return (
            this.directories.length === 0 &&
            this.files.length === 0 &&
            this.uploadingFiles.length === 0 &&
            this.currentLinks.length === 0
        );
    }

    public typeLink: { [index: string]: string } = {
        file: 'file',
        directory: 'directory',
    };

    @ViewChild('inputNewDirectory') private inputNewDirectoryRef: ElementRef;
    @ViewChild('fileInputForm') private fileInputForm: ElementRef; // форму надо сбрасывать, если отменили загрузку файла. потому сюда тащим
    @ViewChild('uploadFilesContainer') private uploadFilesContainer: ElementRef;
    @ViewChild('newFilePopover') private newFilePopover: ElmaPopoverComponent;

    public sortType: SortType = {
        name: 'name',
        size: 'size',
        updatedAt: 'updatedAt',
    };

    public sortDirection: SortDirection = {
        sortType: this.sortType.name,
        increment: true,
    };

    public crumbList: Crumb[] = [];

    public isMobile: boolean;
    private isMobileSubscription: Subscription;

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private crumbsService: CrumbsService,
        private ts: ElmaTranslateService,
        public  diskStore: DiskStore,
        private profileService: ProfileService,
        private userStore: UserStore,
        private modalService: ElmaModalService,
        private cdr: ChangeDetectorRef,
        private activeModal: ElmaActiveModal,
        private ui: ElmaUIService,
    ) {
        this.isMobileSubscription = ui.isMobileObserver.subscribe((value: boolean) => {
            this.isMobile = value;
            this.generateButtons();
        });
        this.uploadInput = new EventEmitter<UploadInput>();
    }

    ngOnInit () {

        this.router.events
            .filter(event => event instanceof NavigationEnd)
            .subscribe(async (event: NavigationEnd) => {
                this.crumbsService.userCrumbs = this.crumbList;
                this.crumbsService.trigger();
            });

        this.subscribe = this.activatedRoute.params.subscribe(async params => {
            if (params['id']) {
                localStorage.setItem('lastFolder', params['id']);
            }
            await this.loadPage(params['id']);
        });

        this.generateButtons();
    }

    ngOnDestroy () {
        this.subscribe.unsubscribe();

        if (this.isMobileSubscription) {
            this.isMobileSubscription.unsubscribe();
        }
    }

    public async loadPage (listingItemId: string): Promise<void> {

        this.selectedItemList = [];
        this.permissionDenied = false;
        this.dirsLoaded = false;
        this.filesLoaded = false;
        this.linksLoaded = false;
        this.allItemLoaded = false;

        this.currentDirectoryID = listingItemId;
        this.newDirectory.parent = this.currentDirectoryID;
        this.diskStore.currentDirID = this.currentDirectoryID;

        this.favoriteRoute = this.activatedRoute.snapshot.data['favorite'];

        if (this.favoriteRoute) {
            await this.loadFavoriteLink();
        } else {
            await this.loadDirectoryItems();
        }

        const noChangeSortDirection = true;
        this.sort(this.sortDirection.sortType, noChangeSortDirection);

        this.selectedItemList = [];
        this.generateButtons();
    }

    public async goToParentFolder () {
        const parents = await this.diskStore.getDirectoryParentsList(this.currentDirectoryID);
        if (parents.length > 1) {
            const parentId = parents[parents.length - 2].id;
            const user = this.profileService.userId ? await this.userStore.getByID(this.profileService.userId) : null;
            if (!user || this.currentDirectoryID !== user.id) {
                // Если это не личная директория пользователя - выходим на родительскую
                // Если личная - должны выйти в корень
                if (this.isModalWindow) {
                    this.loadPage(parentId);
                } else {
                    this.router.navigate(['files', parentId, { outlets: { p: null } }]);
                }
                return;
            }
        }
        if (this.isModalWindow) {
            this.loadPage(<any>undefined);
        } else {
            localStorage.removeItem('lastFolder');
            this.router.navigate(['files']);
        }
    }

    public async open (item: SelectedItem): Promise<void> {

        const isFileLike = item instanceof FileModel || (item instanceof LinkModel && item.type === LinkType.File);
        const isDirectoryLike = item instanceof DirectoryModel || (item instanceof LinkModel && item.type === LinkType.Directory);

        if (this.isModalWindow) {
            if (isDirectoryLike) {
                await this.loadPage(item.id);
            }
        } else {
            if (isDirectoryLike) {
                await this.router.navigate([
                        'files',
                        item instanceof LinkModel ? item.target : item.id,
                        { outlets: { p: null } }
                    ]);
            } else if (isFileLike) {
                await this.router.navigate(['', { outlets: { p: [
                            'preview',
                            item instanceof LinkModel ? item.target : item.id
                    ] } }]);
            }
        }
    }

    @computed
    public get directories () {
        return this.currentDirectories;
    }

    @computed
    public get files () {
        return this.currentFiles;
    }

    @computed
    public get links () {
        return this.currentLinks;
    }

    public async loadFavoriteLink (): Promise<void> {

        (async () => {
            try {
                const userId = this.profileService.userId ? this.profileService.userId : '';
                this.currentLinks = await this.diskStore.loadLinksInDir(userId, true);
            } catch (e) {
                if (e instanceof PermissionError) {
                    this.permissionDenied = true;
                    return;
                }
                throw e;
            } finally {
                this.allItemLoaded = true;
            }
        }).apply(this);

        (async () => {
            this.crumbsService.reset();
            this.crumbsService.add({
                name: this.ts.get('app.disk.listing.directory@root-directory'),
                click: '/files',
            });
            this.crumbsService.add({
                name: this.ts.get('app.disk.listing.directory@title'),
                click: '/files/favorite',
            });
            this.crumbsService.trigger();
        }).apply(this);
    }

    public async loadDirectoryItems (): Promise<void> {

        // Загружаем файлы
        (async () => {
            try {
                this.currentFiles = await this.diskStore.loadFilesInDir(this.currentDirectoryID);
            } catch (e) {
                if (e instanceof PermissionError) {
                    this.permissionDenied = true;
                    return;
                }
                throw e;
            } finally {
                this.filesLoaded = true;
            }
        }).apply(this);

        // Загружаем ссылки
        (async () => {
            try {
                this.currentLinks = await this.diskStore.loadLinksInDir(this.currentDirectoryID);
            } catch (e) {
                if (e instanceof PermissionError) {
                    this.permissionDenied = true;
                    return;
                }
                throw e;
            } finally {
                this.allItemLoaded = true;
            }
        }).apply(this);

        await this.buildCrumbs();

        // Директории нам всеравно нужны еще и в дереве, поэтому их загрузим синхронно
        // а файлы асинхронно
        if (this.currentDirectoryID) {

            try {
                this.currentDirectories = await this.diskStore.loadDirectoriesInDir(this.currentDirectoryID, true);

            } catch (e) {

                if (e instanceof PermissionError) {
                    this.permissionDenied = true;
                    return;
                }
                throw e;
            } finally {
                this.dirsLoaded = true;
            }
            for (const d of this.currentDirectories) {

                if (d.parent === this.usersHomeDirID) {
                    try {
                        const user = await this.userStore.getByID(d.id);
                        d.setName(user.name);
                    } catch (e) {
                        console.error(e);
                    }
                }
            }
        } else {
            // если нет куррент директори, значит мы в рутовой, там нужно махинацию провести
            const dirs = await this.diskStore.loadDirectoriesInDir(this.currentDirectoryID, true);
            this.dirsLoaded = true;
            for (let d of dirs) {
                if (d.name === 'shared') {
                    const index = dirs.indexOf(d);
                    d = new DirectoryModel(this.diskStore, {
                        __id: d.id,
                        name: this.ts.get('app.disk.listing.directory@shared'),
                        parent: d.parent,
                        parentsList: d.parentsList,
                        __createdAt: d.createdAt.format(),
                        __updatedAt: d.updatedAt.format(),
                    });
                    dirs[index] = d;
                }
                if (d.id === this.usersHomeDirID) {
                    d.setName(this.ts.get('app.disk.listing.directory@home'));
                }

            }
            this.currentDirectories = dirs;
        }

        // Если мы в корневой директории, то надо еще отрисовать папку "Мои файлы"
        if (!this.currentDirectoryID) { // если корневая директория
            let userFiles: DirectoryModel | null = null;
            if (this.profileService.userId == null) {
                throw new Error('Not authorized');
            }
            try {
                const user = await this.userStore.getByID(this.profileService.userId);
                userFiles = await this.diskStore.loadDirectory(user.id);
            } catch (e) {
                console.error('error loading user dir:', e);
            }

            if (userFiles) {
                // создадим новую модельку, чтобы не изменять старую, а то с деревом ерунда получается
                const myfiles = new DirectoryModel(this.diskStore, {
                    __id: userFiles.id,
                    name: this.ts.get('app.disk.listing.directory@my-files'),
                    parent: userFiles.parent,
                    parentsList: userFiles.parentsList,
                    __createdAt: userFiles.createdAt.format(),
                    __updatedAt: userFiles.updatedAt.format(),
                });
                this.currentDirectories.push(myfiles);
            }
        }

        this.diskStore.sidebarOpenDirectory(this.currentDirectoryID);

    }

    public async buildCrumbs (): Promise<void> {

        this.crumbList = [];
        this.crumbList.push({
            name: this.ts.get('app.disk.listing.directory@root-directory'),
            click: this.getClickFieldForCrumbs('', '/files'),
        });

        if (this.currentDirectoryID) {

            // достаем всех предков
            const crumbDirs = await this.diskStore.getDirectoryParentsList(this.currentDirectoryID);
            if (this.profileService.userId == null) {
                throw new Error('Not authorized');
            }

            const user = await this.userStore.getByID(this.profileService.userId);
            // Если это личная директория пользователя - заменим крошки

            for (let cd of crumbDirs) {

                if (cd.name === 'shared') {
                    cd.setName(this.ts.get('app.disk.listing.directory@shared'));
                }

                if (cd.name === 'home') {
                    continue;
                }

                const checkAnyUserFolder = cd.name.split(':');
                if (checkAnyUserFolder[1] && checkAnyUserFolder[1] !== user.id) {
                    const anyUserFolder = await this.userStore.getByID(checkAnyUserFolder[1]);

                    if (anyUserFolder) {
                        this.crumbList.push({
                            name: this.ts.get('app.disk.listing.directory@home'),
                            click: this.getClickFieldForCrumbs(cd.id, `/files/${this.usersHomeDirID}`),
                        });
                        this.crumbList.push({
                            name: anyUserFolder.name,
                            click: this.getClickFieldForCrumbs(cd.id, `/files/${cd.id}`),
                        });
                        continue;
                    }
                }

                this.crumbList.push({
                    name: cd.name,
                    click: this.getClickFieldForCrumbs(cd.id, `/files/${cd.id}`),
                });


                if (cd.id === user.id) {

                    const nameDir = this.ts.get('app.disk.listing.directory@my-files');
                    cd = new DirectoryModel(this.diskStore, {
                        __id: user.id,
                        name: nameDir,
                        parent: ROOT_CATEGORY,
                    });

                    this.crumbList = [];
                    this.crumbList.push({
                        name: this.ts.get('app.disk.listing.directory@root-directory'),
                        click: this.getClickFieldForCrumbs('', '/files'),
                    });
                    this.crumbList.push({
                        name: this.ts.get('app.disk.listing.directory@my-files'),
                        click: this.getClickFieldForCrumbs(cd.id, `/files/${cd.id}`),
                    });
                }
            }
        }

        this.crumbsService.userCrumbs = this.crumbList;
        this.crumbsService.trigger();
    }

    private getClickFieldForCrumbs (dirId: string, path: string): string | (() => void) {

        if (this.isModalWindow) {
            return async () => {
                await this.loadPage(dirId);
            };
        } else {
            return async () => {
                if ( !dirId && !this.isModalWindow ) {
                    localStorage.removeItem('lastFolder');
                }
                this.router.navigate(['files', dirId, { outlets: { p: null } }]);
            };
        }
    }

    public handleItemSelected (event: { checked: boolean, selectedItem: SelectedItem }) {

        if (event.checked) {
            this.selectedItemList.push(event.selectedItem);
        } else {
            const index = this.selectedItemList.indexOf(event.selectedItem);
            if (index > -1) {
                this.selectedItemList.splice(index, 1);
            }
        }

        if (this.isModalWindow && !this.multipleSelect) {
            this.emitFileLikeItemList();
        }

        this.generateButtons();
    }


    public async onUploadOutput (output: UploadOutput): Promise<void> {
        if (!this.favoriteRoute) {
            if (output.type === 'allAddedToQueue') {
                setTimeout(() => {
                    // подождать, пока в контейнер напихаются загружаемые файлы
                    this.uploadFilesContainer.nativeElement.scrollIntoView(true);
                }, 0);
                await this.saveFiles();
            } else if (output.type === 'addedToQueue') {
                if (!output.file) {
                    console.warn('No uploaded file');
                    return;
                }
                // Сразу прорверим дупликацию имени файла, а то уродско получается, когда оно меняется после загрузки
                output.file.name = this.deduplicateName(output.file.name, 'file');
                this.uploadingFiles.push(output.file);
                this.mobilePlusExtended = false;
            } else if (output.type === 'cancelled') {
                // cancel не удаляет файл, просто останавливает его загрузку
            } else if (output.type === 'dragOver') {
                this.dragOver = true;
            } else if (output.type === 'dragOut') {
                this.dragOver = false;
            } else if (output.type === 'drop') {
                this.dragOver = false;
            } else if (output.type === 'removed') {
                this.uploadingFiles = this.uploadingFiles.filter((file: UploadFile) => file !== output.file);
                this.fileInputForm.nativeElement.reset();
            } else if (output.type === 'done') {
                if (!output.file) {
                    console.warn('No uploaded file');
                    return;
                }
                // Закончилась загрузка файла, можно сохранить его в справочнике
                const filejson: FileJson = {
                    name: output.file.name,
                    originalName: output.file.name,
                    size: output.file.size,
                    hash: output.file.id,
                    directory: this.currentDirectoryID,
                };

                let file: FileModel;
                try {
                    file = await this.createFileMetadata(filejson);
                } catch (e) {
                    console.error('create file in collection error: ', e);
                    this.uploadingFiles = this.uploadingFiles.filter((f: UploadFile) => f !== output.file);
                    this.fileInputForm.nativeElement.reset();
                    return;
                }

                this.currentFiles.push(file);
                this.uploadingFiles = this.uploadingFiles.filter((f: UploadFile) => f !== output.file);
                if (this.uploadingFiles.length === 0) {
                    // Загрузили все, надо сбросить форму, чтоб повторно можно было эти же файлы выбрать
                    this.fileInputForm.nativeElement.reset();
                }

                const noChangeSortDirection = true;
                this.sort(this.sortDirection.sortType, noChangeSortDirection);
            }
        }
    }

    private async uploadFile (file: UploadFile, postlink: PostlinkModel) {
        const mergedFileObject = Object.assign({}, file, postlink.formdata);

        // лайфхак, нам дальше нужен хэш, а положить его некуда, потому сунем в id файла, там всеравно нужно уникальное значение
        file.id = postlink.hash;
        const uploadInput: UploadInput = {
            type: 'uploadFile',
            url: postlink.link,
            id: file.id,
            method: 'POST',
            fileIndex: file.fileIndex,
            file: file,
            data: mergedFileObject,
            concurrency: 0,
        };
        this.uploadInput.emit(uploadInput);
    }


    public handleUploadCancelled (fileID: string) {
        this.uploadInput.emit({ type: 'cancel', id: fileID });
        this.uploadInput.emit({ type: 'remove', id: fileID });
    }

    private async createFileMetadata (filejson: FileJson): Promise<FileModel> {
        return this.diskStore.createFile(filejson);
    }

    private async saveFiles () {
        for (const ufile of this.uploadingFiles) {
            try {
                const postlink: any = await this.diskStore.getPostlink(<number>ufile.size);
                this.uploadFile(ufile, postlink);
            } catch (e) {
                console.error('file save error :: ', e);
            }
        }

    }

    public async prepareCreateDirectory () {
        this.creatingNewDirectory = true;
        this.newDirectory.name = this.deduplicateName(this.ts.get('app.disk.listing.directory@new-directory'), 'directory');
        const elem = <HTMLInputElement>this.inputNewDirectoryRef.nativeElement;

        elem.focus();
        setTimeout(() => {
            elem.setSelectionRange(0, 500);
            elem.scrollIntoView(false);
        }, 0);
    }


    private deduplicateName (name: string, type: 'file' | 'directory'): string {
        let checkItems: { name: string }[];

        if (type === 'file') {
            checkItems = this.currentFiles;
        } else {
            checkItems = this.currentDirectories;
        }


        const isDuplicate = checkItems.find(item => item.name === name);

        if (!isDuplicate) {
            return name;
        }

        // если дупликация, то ищем, нет ли уже таких файлов с цифиркой (x)

        let maxinst = 1;
        let newName: string = name;


        while (checkItems.find(item => item.name === newName)) {
            if (type === 'file') {
                const [absname, ext] = this.splitFileNameAndExt(name);
                newName = absname + `(${maxinst})`; // имя файла + (цифирка)
                if (ext) {
                    newName = newName + `.${ext}`; // если есть расширение - добавляем его
                }
            } else {
                newName = name + `(${maxinst})`;
            }
            maxinst++;
        }

        return newName;
    }

    private splitFileNameAndExt (filename: string): [string, string] {
        const dotIndex = filename.lastIndexOf('.');
        if (dotIndex === -1) {
            return [filename, ''];
        }

        const absfilename = filename.substr(0, dotIndex);
        const ext = filename.split('.').pop() || '';

        return [absfilename, ext];
    }


    public async createDirectory (dir: DirectoryJson) {
        try {
            const newdir = await this.diskStore.createDirectory(dir);
            this.currentDirectories.push(newdir);
            this.diskStore.addDirectory(newdir);
        } catch (e) {
            if (e.message === 'duplicate') {
                this.newDirectoryError = this.ts.get('app.disk.listing.directory@folder-duplicate-name');
                return;
            }
            this.newDirectoryError = e;
            return;
        }
        this.creatingNewDirectory = false;
        this.mobilePlusExtended = false;
        const elem = <HTMLInputElement>this.inputNewDirectoryRef.nativeElement;
        setTimeout(function() {
            elem.blur();
        }, 0);
    }

    public cancelCreateDirectory () {
        this.creatingNewDirectory = false;
        this.newDirectoryError = '';
    }


    public togglePlus () {
        this.mobilePlusExtended = !this.mobilePlusExtended;
    }

    public openMoveItem () {
        const modalRef = this.modalService.open(DiskMoveItemModalComponent, {
            size: 'sm',
            title: this.ts.get('app.disk.listing.directory@title-move-modal'),
        });
        modalRef.result.then(res => {
            this.selectedItemList = [];
            this.generateButtons();
        }).catch(() => {});
        const moveItemComponent = <DiskMoveItemModalComponent>modalRef.componentInstance;
        moveItemComponent.movingItemList = this.selectedDirAndFileList;
        this.movedFilesSubscribtion = modalRef.componentInstance.movedItemIDs.subscribe((moveEvent: MoveEvent) => {
            this.diskStore.handleMovedItems(moveEvent);
            this.diskStore.outputResultMoveDir(moveEvent);
            this.movedFilesSubscribtion.unsubscribe();
        });
    }


    public openCreateLink () {
        const modalRef = this.modalService.open(DiskLinkCreationModalComponent, {
            size: 'md',
            title: this.ts.get('app.disk.listing.directory@create-link-title'),
        });
        const linkCreationModalComponent = <DiskLinkCreationModalComponent>modalRef.componentInstance;
        linkCreationModalComponent.selectedItemList = this.selectedDirAndFileList;
    }

    public async addToFavorites (): Promise<void> {

        try {
            await this.selectedItemList.forEach(item => {

                const userId = this.profileService.userId ? this.profileService.userId : '';
                const type = item instanceof FileModel ? LinkType.File : LinkType.Directory;
                const jsonLink = {
                    name: item.name,
                    directory: userId,
                    target: item.id,
                    type: type,
                    favorite: true,
                };
                this.diskStore.createLink(jsonLink);
            });
        } catch (error) {
            console.warn(error);
        }
    }

    public deleteLink (linkId: string): void {
        this.currentLinks = this.currentLinks.filter(item => item.id !== linkId);
    }

    public async deleteSelectedItemList (): Promise<void> {

        try {
            const deletedLinks: string[] = [];

            await this.selectedItemList.forEach(async (item, index) => {
                if (item instanceof LinkModel) {
                    await this.diskStore.deleteLink(item.id);
                    deletedLinks.push(item.id);
                    this.currentLinks = this.currentLinks.filter(itemCurrentLink => itemCurrentLink.id !== item.id);
                }
            });
            this.selectedItemList = this.selectedItemList.filter(itemLink => deletedLinks.indexOf(itemLink.id) !== -1)

        } catch (error) {
            console.warn(error);
        }

        this.generateButtons();
    }

    public sort (sortType: string, noChangeSortDirection = false): void {

        if (this.sortDirection.sortType === sortType) {
            if (!noChangeSortDirection) {
                this.sortDirection.increment = !this.sortDirection.increment;
            }
        } else {
            this.sortDirection.increment = true;
        }
        this.sortDirection.sortType = sortType;

        this.currentDirectories = this.currentDirectories.sort((firstItem: SelectedItem, secondItem: SelectedItem) =>
            this.compareSortName(firstItem, secondItem),
        );
        this.currentFiles = this.currentFiles.sort((firstItem: SelectedItem, secondItem: SelectedItem) =>
            this.compareSortName(firstItem, secondItem),
        );
        this.currentLinks = this.currentLinks.sort((firstItem: SelectedItem, secondItem: SelectedItem) =>
            this.compareSortName(firstItem, secondItem),
        );
    }

    public compareSortName (firstItemFull: SelectedItem, secondItemFull: SelectedItem): number {

        const firstItem: SortItem = {
            name: firstItemFull.name,
            updatedAt: firstItemFull.updatedAt,
        };
        const secondItem: SortItem = {
            name: secondItemFull.name,
            updatedAt: secondItemFull.updatedAt,
        };

        if (firstItemFull instanceof FileModel && secondItemFull instanceof FileModel) {
            firstItem['size'] = firstItemFull.size;
            secondItem['size'] = secondItemFull.size;
        }

        const step = this.sortDirection.increment ? 1 : -1;
        const nameProperty = this.sortDirection.sortType;

        if (this.sortDirection.sortType === this.sortType.updatedAt) {

            const firstMoment = <moment.Moment>firstItem.updatedAt;
            const secondMoment = <moment.Moment>secondItem.updatedAt;

            if (firstMoment.isBefore(secondMoment)) {
                return step;
            }
            if (firstMoment.isAfter(secondMoment)) {
                return -step;
            }

        } else {

            if (firstItem[nameProperty] > secondItem[nameProperty]) {
                return step;
            }
            if (firstItem[nameProperty] < secondItem[nameProperty]) {
                return -step;
            }
        }

        return 0;
    }

    public emitFileLikeItemList (): void {

        const fileLikeItemList: UploadedFileList[] = [];

        this.selectedFileList.forEach(item => {

            const uploadedFile: UploadedFileList = {
                id: item.id,
                name: item.name,
            };

            fileLikeItemList.push(uploadedFile);
        });

        this.selectedLinkList.forEach(item => {
            if (item instanceof LinkModel && item.type === LinkType.File) {

                const uploadedFile: UploadedFileList = {
                    id: item.target,
                    name: item.name,
                };

                fileLikeItemList.push(uploadedFile);
            }
        });

        this.downloadFileIdList.emit(fileLikeItemList);
        this.activeModal.close();
    }

    public closeThisModalWindow (): void {
        this.downloadFileIdList.emit([]);
        this.activeModal.close();
    }

    public fileLikeItemTypeFilter (items: SelectedItem[]): SelectedItem[] {

        if (this.fileTypeRender === 'image/*') {

            return items.filter(item => {
                let ext = item.name.split('.').pop();
                ext = ext ? ext : '';
                return ExtPictureList.indexOf(ext.toLowerCase()) >= 0;
            });
        }

        return items;
    }

    public newOfficeFileName: string;
    public newOfficeFileFormat: string;

    public openNewOfficeFileName (format: string) {
        this.newOfficeFileName = '';
        this.newOfficeFileFormat = format;
        this.newFilePopover.hide();
        this.officeFileNamePopup.openPopup();
        setTimeout(() => {
            this.officeFileNameInput.nativeElement.focus();
        }, 0);
    }

    public closeNewOfficeFilePopup () {
        this.officeFileNamePopup.closePopup();
        setTimeout(() => {
            this.loadPage(this.currentDirectoryID);
        }, 10000);
    }

    public generateButtons () {
        const list = this;
        this.buttonsList = [];
        if ( this.selectedItemList.length > 0 ) {
            if (this.selectedFileList.length > 0 || this.selectedDirList.length > 0) {

                this.buttonsList.push(<Button>{
                    label: list.ts.get('app.disk.listing.directory@move'),
                    action: () => list.openMoveItem(),
                    icon: 'folder_type_move',
                    type: 'default',
                    style: 'icon',
                    tooltip: list.ts.get('app.disk.listing.directory@move'),
                    tooltipPosition: 'bottom'
                });

                this.buttonsList.push(<Button>{
                    label: list.ts.get('app.disk.listing.directory@create-link'),
                    action: () => list.openCreateLink(),
                    icon: 'system_link_horisontal',
                    type: 'default',
                    style: 'icon',
                    tooltip: list.ts.get('app.disk.listing.directory@create-link'),
                    tooltipPosition: 'bottom'
                });

                this.buttonsList.push(<Button>{
                    label: list.ts.get('app.disk.listing.directory@add-to-favorite'),
                    action: () => list.addToFavorites(),
                    icon: 'category_favorite',
                    type: 'default',
                    style: 'icon',
                    tooltip: list.ts.get('app.disk.listing.directory@add-to-favorite'),
                    tooltipPosition: 'bottom'
                });
            }

            if ( !this.isMobile ) {
                this.buttonsList.push(<Button>{
                    label: list.ts.get('app.disk.listing.directory@delete'),
                    action: ($e: any) => this.deletePopover.toggle($e.target),
                    icon: 'app_trash',
                    type: 'default',
                    style: 'icon'
                });
            }

        }
        if ( this.currentDirectoryID && this.currentDirectoryID !== this.usersHomeDirID ) {
            this.buttonsList.push(<HeaderMobileButton>{
                label: list.ts.get('app.modules.disk.disk-listing@create-file'),
                icon: 'plus',
                type: 'primary',
                // Кнопка добавления файла для мобильной версии
                // делается через отдельный шаблон с "label for",
                // т.к. через click не работает в iOS
                template: this.isMobile ? list.createFileMobileButton : null,
                action: ($e: any) => {
                    if (!this.isMobile) {
                        this.newFilePopover.toggle($e.target);
                    }
                }
            });

            this.buttonsList.push(<Button>{
                label: list.ts.get('app.disk.listing.directory@folder'),
                action: () => list.prepareCreateDirectory(),
                icon: 'folder_type_opened',
                type: 'default'
            });
        }
    }
}
