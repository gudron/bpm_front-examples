import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, HostBinding, Input, OnInit, Output, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { ElmaTranslateService } from '../../../../../shared/common';
import { ElmaPopoverComponent } from '../../../../../shared/common/elma-popover';
import { MobileContextComponent } from '../../../../common/mobile-context/mobile-context.component';
import { LinkModel } from '../../../../models/disk.model';
import { DiskStore } from '../../../../stores/disk.store';
import { SelectedItem } from '../disk-listing.interface';

@Component({
    selector: 'app-disk-symlink',
    templateUrl: './disk-symlink.component.html',
    styleUrls: ['./disk-symlink.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiskSymLinkComponent implements OnInit {

    private targetDirectoryID: string;

    @Input() public link: LinkModel;
    @Input() renderCheckbox = true;
    @Output() public linkDeleted = new EventEmitter<string>();
    @Output() linkSelected: EventEmitter<{ checked: boolean, selectedItem: SelectedItem }>
        = new EventEmitter<{ checked: boolean, selectedItem: SelectedItem }>();
    @Output() open = new EventEmitter<string>();

    @HostBinding('class.is-active') public selected = false;
    @HostBinding('class.list-item') public listItem = true;

    constructor (
        private diskStore: DiskStore,
        private ts: ElmaTranslateService,
    ) {
    }

    ngOnInit () {
    }

    public createdOrUpdated () {
        const justCreated = moment(this.link.createdAt).isSame(this.link.updatedAt);
        const momentLabel = ' ' + moment(this.link.updatedAt).format('DD.MM.YYYY HH:mm');
        const text = (justCreated
                ? this.ts.get('app.disk.listing.directory@created-label')
                : this.ts.get('app.disk.listing.directory@updated-label')
        ) + momentLabel;
        return text;
    }

    public selectRow (checked: boolean) {
        this.selected = checked;
        this.linkSelected.emit({ checked: checked, selectedItem: this.link });
    }

    @ViewChild('fileCtx') fileCtx: ElmaPopoverComponent;
    @ViewChild('fileCtxOpener') fileCtxOpener: ElementRef;

    public openContextMenu () {
        this.fileCtx.show(this.fileCtxOpener.nativeElement);
    }

    public async downloadFile() {
        const file = await this.diskStore.getFile(this.link.target);
        try {
            const url = await this.diskStore.fileGetlink(file);
            if (url) {
                window.open(url, '_self');
            }
        } catch (e) {
            console.error('Can`t download file: ', e);
        }
    }

    public async deleteLink (): Promise<void> {

        try {
            await this.diskStore.deleteLink(this.link.id);
            this.linkDeleted.emit(this.link.id);
        } catch (err) {
            console.error(err);
        }
    }
}
