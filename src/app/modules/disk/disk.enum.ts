export enum httpCode {
    conflict = 409,
    statusOK = 200,
}

export enum baseDirectoryId {
    shared = '8c110182-b022-5f31-ac3d-290fdc1d4fe1',
    home = '4e4fbacd-5575-58f6-9ac2-af6ef856bc7b',
}
