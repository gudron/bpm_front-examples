import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { FileModel } from '../../models/disk.model';
import { DiskStore } from '../../stores/disk.store';

@Injectable()
export class FileResolveService implements Resolve<FileModel> {

    private availableFormats = ['docx', 'xlsx', 'pptx'];

    constructor (
        private diskStore: DiskStore,
        private router: Router,
    ) {
    }

    async resolve (route: ActivatedRouteSnapshot): Promise<FileModel> {
        const id = route.paramMap.get('id');
        if (id == null) {
            // Если это editnew - это создание нового файла. У него нет id
            if (route.data['action'] === 'editnew') {
                const format = route.paramMap.get('format');
                if (!format || !this.availableFormats.includes(format)) {
                    throw new Error('invalid file format');
                }
                const filename = route.queryParamMap.get('filename');
                const directoryID = route.queryParamMap.get('directory');
                if (!filename) {
                    throw new Error('file name required');
                }
                if (!directoryID) {
                    throw new Error('directory required');
                }
                return this.createNewOfficeFile(filename, directoryID, format);
            }
            this.router.navigate(['/files']);

            throw new Error('file id not found');
        }

        return this.diskStore.getFile(id);
    }

    private async createNewOfficeFile (filename: string, directoryID: string, format: string): Promise<FileModel> {
        if (!filename.endsWith(format)) {
            filename = filename + '.' + format;
        }
        const newEmptyFile = {
            name: filename,
            originalName: filename,
            directory: directoryID,
            size: 0,
        };
        let file: FileModel;
        try {
            file = await this.diskStore.createFile(newEmptyFile);
        } catch (e) {
            throw e;
        }
        this.diskStore.currentFiles.push(file);
        return file;
    }
}
