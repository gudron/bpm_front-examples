export interface FileAndLink {
    id: string;
}

export interface RoutingParams {
    id: string;
    mode: string;
}
