import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    HostListener,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router, UrlSegmentGroup } from '@angular/router';
import { DiskStore } from 'app/stores/disk.store';
import { autorun, IReactionDisposer } from 'mobx';
import { computed, observable } from 'mobx-angular';
import { Subscription } from 'rxjs/Subscription';
import { ElmaTranslateService, ElmaUIService } from '../../../../shared/common';
import { ElmaComplexPopupComponent } from '../../../../shared/common/elma-complex-popup/elma-complex-popup.component';
import { ElmaPopupComponent } from '../../../../shared/common/elma-popup';
import { ElmaMenuItem } from '../../../../shared/common/elma-split-button';
import { ElmaNotificationService } from '../../../../shared/common/toast/notification.service';
import { PermissionError } from '../../../app.constants';
import { LinkType } from '../../../models/disk.json';
import { DirectoryModel, FileModel, LinkModel } from '../../../models/disk.model';
import { FileComponent } from '../../../shared/file/file.component';
import { MSOfficeHelper } from '../../../shared/file/type-file/msoffice/msoffice.helper';
import { FileContextMenuComponent } from '../disk-listing/disk-file/file-context-menu/file-context-menu.component';
import { DiskPermissionsComponent } from './disk-permissions/disk-permissions.component';
import { ExtPictureList } from './disk-preview.const';
import { FileAndLink, RoutingParams } from './disk-preview.interface';
import { FileType } from '../../../shared/file/file.interface';

const favoriteRoute = 'favorite';

@Component({
    selector: 'app-disk-preview',
    templateUrl: './disk-preview.component.html',
    styleUrls: ['./disk-preview.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiskPreviewComponent implements OnInit, OnDestroy {

    @ViewChild('previewPopup') previewPopup: ElmaComplexPopupComponent;
    @ViewChild('filePreview') filePreview: FileComponent;
    @ViewChild('fileInfoElemThird') fileInfoElemThird: ElementRef;
    @ViewChild('fileCtx') fileCtx: FileContextMenuComponent;
    @ViewChild('diskPermissions') diskPermissions: DiskPermissionsComponent;

    public currentDirectoryId: string;
    public name: string;
    public src: string | null;
    public extension: string;
    public message: string;
    @observable
    public currentFile: FileModel;
    public currentDirectory: DirectoryModel;

    @computed
    public get curentFileId (): string {
        return this.currentFile.id;
    }

    public showProp = false;
    @observable
    public opened = false;
    public fileInfoActivity = false;
    private infoHolderActivity = false;

    // левая стрелка
    @computed
    public get notEmtpyPrev (): boolean {

        if (this.currentFilesAndLinks.length && this.currentFile != null) {
            const idx = this.currentFilesAndLinks.findIndex((f: FileAndLink) => f.id === this.currentFile.id);
            return idx > 0;
        }

        return false;
    }

    // правая стрелка
    @computed
    public get notEmtpyNext (): boolean {

        if (this.currentFilesAndLinks.length && this.currentFile != null) {
            const idx = this.currentFilesAndLinks.findIndex((f: FileAndLink) => f.id === this.currentFile.id);

            return this.currentFilesAndLinks.length - 1 > idx;
        }

        return false;
    }


    private subGetLink: Subscription;

    private extPictureList: string[] = ExtPictureList;

    public fileType: FileType = FileType.Unknown;
    public FileType = FileType;

    @observable
    private currentFilesAndLinks: FileAndLink[] = [];

    public showMenuRename = false;

    @HostListener('keydown.escape', ['$event'])
    handleKeyboardEvent (event: KeyboardEvent) {

        if (this.showMenuRename) {
            event.stopPropagation();
            this.cancelRename();
        }
    }

    @HostListener('document: keydown', ['$event'])
    handleKeyboardArrowRight (event: KeyboardEvent) {

        switch (event.code) {

        default:
            break;

        case 'ArrowRight':
            if (this.showMenuRename) {
                event.stopPropagation();
            } else {
                this.pagingFiles('next');
            }
            break;

        case 'ArrowLeft':
            if (this.showMenuRename) {
                event.stopPropagation();
            } else {
                this.pagingFiles('prev');
            }
            break;
        }
    }

    private dispose: IReactionDisposer;
    public isMobile: boolean;
    private isMobileSubscription: Subscription;

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private diskStore: DiskStore,
        private ts: ElmaTranslateService,
        private msHelper: MSOfficeHelper,
        private ui: ElmaUIService,
        private notificationService: ElmaNotificationService,
    ) {
        this.isMobileSubscription = ui.isMobileObserver.subscribe((value: boolean) => {
            this.isMobile = value;
        });
    }

    ngOnInit () {

        this.setCurrentDirectoryId();
        this.dispose = autorun(() => {
            this.loadFilesAndLinks();
        });
        this.subGetLink = this.activatedRoute.params.subscribe(this.handleParams.bind(this));
    }

    ngOnDestroy () {
        this.dispose();
        this.subGetLink.unsubscribe();
    }

    public get currentFiles (): FileModel[] {
        return this.diskStore.currentFiles;
    }

    public newName: string;
    public renameError: string;
    @ViewChild('nameFile') nameFile: ElementRef;

    @ViewChild('renamePopup') public renamePopup: ElmaPopupComponent;

    public refreshName (name: string) {
        this.currentFile.name = name;
    }

    private cleanFileName() {
        if (this.getExt(this.currentFile.name)) {
            return this.currentFile.name.substr(0, this.currentFile.name.lastIndexOf('.'));
        } else {
            return this.currentFile.name;
        }
    }

    public openMenuRename (): void {

        this.showMenuRename = true;
        this.newName = this.cleanFileName();
        this.renameError = '';

        setTimeout(() => {
            this.nameFile.nativeElement.focus();
        }, 0);
    }

    public cancelRename (): void {
        this.showMenuRename = false;
    }

    public async updateName (): Promise<void> {

        if (this.newName !== this.cleanFileName()) {

            if (!this.getExt(this.currentFile.name) && this.newName.indexOf('.') !== -1) {
                this.renameError = this.ts.get('app.disk.listing.directory@rename-dot-exist');
                this.cancelRename();
            } else {
                try {
                    const renamed = this.newName + (this.getExt(this.currentFile.name) ? '.' + this.getExt(this.currentFile.name) : '');
                    const response = await this.diskStore.renameFile(
                        this.currentFile.id,
                        renamed
                    );

                    if (response.status === 409) {
                        this.renameError = this.ts.get('app.disk.listing.directory@error-name-isset');
                    } else {
                        this.cancelRename();
                    }

                    this.currentFiles.forEach(item => {
                        if (item.id === this.currentFile.id) {
                            item.name = renamed;
                        }
                    });

                    this.refreshName(renamed);

                } catch (err) {

                    console.error(err);
                    this.cancelRename();

                }
            }
        } else {
            this.cancelRename();
        }
    }



    public setCurrentDirectoryId (): void {

        const tree = this.router.parseUrl(this.router.url);
        const nameOutlet = 'primary';
        const primarySegment: UrlSegmentGroup = tree.root.children[nameOutlet];
        this.currentDirectoryId = primarySegment.segments[1].toString();
    }

    public loadFilesAndLinks (): void {

        if (this.currentDirectoryId === favoriteRoute) {

            this.currentFilesAndLinks = this.diskStore.currentLinks.reduce(this.getFileIdListFromLinkDirsAndFiles, []);
        } else {

            const currentFilesAndLinks: FileAndLink[] = this.diskStore.currentFiles;
            const currentLinkFiles: FileAndLink[] = this.diskStore.currentLinks
                .reduce(this.getFileIdListFromLinkDirsAndFiles, []);
            this.currentFilesAndLinks = currentFilesAndLinks.concat(currentLinkFiles);
        }
    }

    private getFileIdListFromLinkDirsAndFiles (linkTargetList: FileAndLink[], item: LinkModel): FileAndLink[] {

        if (item.type !== LinkType.Directory) {

            const LinkTarget: FileAndLink = {
                id: item.target,
            };
            linkTargetList.push(LinkTarget);
        }

        return linkTargetList;
    }

    private async handleParams (result: RoutingParams) {
        if (result.id) {

            try {

                this.currentFile = await this.diskStore.getFile(result.id);
                this.extension = this.getExt(this.currentFile.originalName);
                const linkResult: any = await this.diskStore.fileGetlink(this.currentFile);
                this.openPopup(this.currentFile, linkResult);
                this.currentDirectory = await this.diskStore.loadDirectory(this.currentFile.directory);
            } catch (e) {

                if (e instanceof PermissionError) {
                    this.message = 'permission denied';
                    // TODO: обработка ошибки в preview: https://git.elewise.com/elma365-frontend/main-app/issues/137
                }
                throw e;
            }
        }
    }

    private iterifyArr (arr: any, current: any) {
        let cur = current >= 0 ? current : 0;
        arr.next = (function () {
            cur += 1;
            return (cur >= this.length) ? false : this[cur];
        });
        arr.prev = (function () {
            cur -= 1;
            return (cur < 0) ? false : this[cur];
        });
        return arr;
    }

    public optionList: ElmaMenuItem[] = [
        {
            label: this.ts.get('app.disk.listing.directory@send-document'),
            command: () => {
            },
        },
        {
            label: this.ts.get('app.disk.listing.directory@send-for-approval'),
            command: () => {
            },
        },
        {
            label: this.ts.get('app.disk.listing.directory@send-for-review'),
            command: () => {
            },
        },
    ];

    private pagingFiles (direction: string) {

        const result: any = this.iterifyArr(
            this.currentFilesAndLinks,
            this.currentFilesAndLinks.findIndex(f => f['id'] === this.currentFile['id']),
        );

        let data;
        switch (direction) {
        case 'next':
            data = result.next();
            break;
        case 'prev':
            data = result.prev();
            break;
        }

        if (data) {

            // ВАЖНО: обнуляем параметр src, хранящий в себе ссылку
            // он входной параметр для других компонент, на нулевое значение завязано условие в ngChange
            this.src = null;
            this.message = '';
            this.router.navigate(['', { outlets: { p: ['preview', data.id] } }]);
        }

        this.fileInfoActivity = false;
    }

    private closePreview () {
        this.hidePopup(false);
    }

    private async downloadFile () {
        // this.diskStore.downloadFile();
    }

    private openPopup (file: any, link: any) {

        this.src = link;
        this.name = file.name;
        this.extension = this.getExt(file.originalName);
        this.fileType = this.getFileType(this.extension);

        if (this.extension === null) {
            this.message = this.ts.get('app.disk.listing.directory@file-not-extension');
        } else if (!this.src || this.fileType === FileType.Unknown) {
            this.message = this.ts.get('app.disk.listing.directory@file-cannot-view');
        }

        this.opened = true;
    }

    private hidePopup (visible: boolean) {
        if (!visible) {
            this.router.navigate(['', { outlets: { p: null } }]);
        }
    }

    private getExt (filename: string): string {
        const ext = filename.split('.').pop();
        if (ext && ext !== filename) {
            return ext;
        } else {
            return '';
        }
    }

    private getFileType (ext: string): FileType {
        let ft: FileType = FileType.Unknown;

        switch (true) {
        case this.isPicture(ext):
            ft = FileType.Image;
            break;

        case this.isMSOfficeDocument(ext):
            ft = FileType.OfficeDocument;
            break;

        case this.isText(ext):
            ft = FileType.Text;
            break;

        case this.isPDF(ext):
            ft = FileType.Pdf;
            break;

        case this.isVideo(ext):
            ft = FileType.Video;
            break;
        }

        return ft;
    }

    private isVideo (ext: string): boolean {
        return (ext === 'mp4');
    }

    private isPDF (ext: string): boolean {
        return (ext === 'pdf');
    }

    private isText (ext: string): boolean {
        return (ext === 'txt');
    }

    private isPicture (ext: string): boolean {
        return this.extPictureList.indexOf(ext.toLowerCase()) >= 0;
    }

    private isMSOfficeDocument (ext: string): boolean {
        try {
            if (this.msHelper.getViewURLByExt(ext).length > 0) {
                return true;
            }
        } catch (e) {
            return false;
        }
        return false;
    }

    public showSidebarFileInfo (ext: string): boolean {
        return !this.isMSOfficeDocument(ext);
    }

    public onShowFileInfo () {
        this.fileInfoActivity = true;
        this.fileInfoElemThird.nativeElement.classList.toggle('showed');
    }

    private onCloseFileInfo () {
        this.fileInfoActivity = false;
    }

    public toggleFileInfo() {
        this.previewPopup.toggleSidebar();
        this.fileInfoActivity = !this.fileInfoActivity;
    }

    private firstPageRendered () {
        this.infoHolderActivity = true;
    }

    private onExpand (elem: any) {
        elem.classList.toggle('is-expanded');
    }

    private async printPdf (): Promise<void> {

        if (!this.src) {
            return;
        }

        const oReq = new XMLHttpRequest();
        oReq.open('GET', this.src, true);
        oReq.setRequestHeader('Cache-Control', 'no-cache');
        oReq.responseType = 'blob';

        oReq.onload = (e) => {

            const responseBlob = oReq.response;
            const urlBlob = URL.createObjectURL(responseBlob);
            const iframeId = 'print_iframe';

            const deleteNode = document.body.children.namedItem(iframeId);
            if (deleteNode) {
                deleteNode.remove();
            }

            const printIframe = window.document.createElement('iframe');
            printIframe.src = urlBlob;
            printIframe.id = iframeId;
            printIframe.style.width = '100%';
            printIframe.style.height = '100%';
            printIframe.style.border = 'none';
            printIframe.style.zIndex = '-1';

            window.document.body.appendChild(printIframe);

            setTimeout(function() {
                printIframe.contentWindow.print()
            }, 0);
        };

        oReq.send();
    }

    public isFileEditable (): boolean {
        return this.msHelper.isEditable(this.currentFile.extension);
    }
}
