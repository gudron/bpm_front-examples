import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MomentModule } from 'angular2-moment';
import { MobxAngularModule } from 'mobx-angular';
import { ElmaCommonModule } from '../../../../shared/common';
import { AppCommonModule } from '../../../common/common.module';
import { PipesModule } from '../../../pipes/pipes.module';
import { AppSharedModule } from '../../../shared/shared.module';
import { DiskListingModule } from '../disk-listing/disk-listing.module';
import { DiskPermissionsComponent } from './disk-permissions/disk-permissions.component';
import { DiskPermissionsModule } from './disk-permissions/disk-permissions.module';
import { DiskPreviewComponent } from './disk-preview.component';

@NgModule({
    imports: [
        ElmaCommonModule,
        CommonModule,
        AppCommonModule,
        PipesModule,
        MomentModule,
        RouterModule,
        MobxAngularModule,
        AppSharedModule,
        FormsModule,
        DiskPermissionsModule,
        DiskListingModule,
    ],
    entryComponents: [
        DiskPermissionsComponent,
    ],
    declarations: [DiskPreviewComponent],
    exports: [DiskPreviewComponent],
})
export class DiskPreviewModule {
}
