import { Component, Input, ViewChild } from '@angular/core';
import { ElmaActiveModal } from '../../../../../shared/common';
import { PermissionsComponent } from '../../../../common/permissions/permissions.component';
import { FileModel } from '../../../../models/disk.model';
import { PermissionsModel } from '../../../../models/permission.model';
import { DiskStore } from '../../../../stores/disk.store';

@Component({
    selector: 'app-disk-permissions',
    templateUrl: './disk-permissions.component.html',
    styleUrls: ['./disk-permissions.component.scss'],
})
export class DiskPermissionsComponent {
    @Input() filePermissions: PermissionsModel;
    @Input() permissionTypeCreate: string;
    @Input() currentFile: FileModel;

    @ViewChild('permComponent') public permComponent: PermissionsComponent;

    public async savePermissions () {
        await this.diskStore.saveFilePermissions(this.currentFile.id, this.permComponent.permissions!.toJson());
        this.activeModal.dismiss();
    }

    constructor (
        private diskStore: DiskStore,
        public activeModal: ElmaActiveModal,
    ) {
    }
}
