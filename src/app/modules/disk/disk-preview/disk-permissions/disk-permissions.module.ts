import { NgModule } from '@angular/core';
import { ElmaCommonModule } from '../../../../../shared/common';
import { PermissionsModule } from '../../../../common/permissions/permissions.module';
import { DiskPermissionsComponent } from './disk-permissions.component';

@NgModule({
    imports: [
        PermissionsModule,
        ElmaCommonModule,
    ],
    declarations: [DiskPermissionsComponent],
    exports: [DiskPermissionsComponent],
})
export class DiskPermissionsModule {
}
