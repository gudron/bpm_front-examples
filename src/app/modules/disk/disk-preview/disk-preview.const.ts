export const ExtPictureList: string[] = [
    'jpg',
    'tiff',
    'gif',
    'bmp',
    'png',
];
