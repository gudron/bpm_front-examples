import { Component, OnInit } from '@angular/core';
import { Namespaces, NavTypes } from '../../app.constants';
import { SidebarService } from '../../services/sidebar.service';
import { PageStore } from '../../stores/page.store';

@Component({
    selector: 'app-disk',
    templateUrl: './disk.component.html',
    styleUrls: ['./disk.component.scss'],
})
export class DiskComponent implements OnInit {

    constructor (
        private sidebarService: SidebarService,
        private pageStore: PageStore,
    ) {
    }

    async ngOnInit () {
        this.sidebarService.setComponent({
            type: NavTypes.NAV_TYPE_DISK,
            page: await this.pageStore.getByNamespaceAndCode(Namespaces.Global, 'files'),
        });
    }
}
