import { EventEmitter, Output } from '@angular/core';
import { CommonWidgetTypes } from '../../../shared/common/widgets/widget-types';
import { WidgetZone } from '../../../widget';
import { WidgetFieldTypes } from '../../../widget/models/field-types';
import { WidgetField } from '../../../widget/schema/widget-field';

/**
 * Общее для различных виджетов форм элемента
 */
export namespace ItemForm {

    // Контроллер формы
    export interface Controller {
        open (starter?: HTMLElement): void;
        close (): void;
        onClose: EventEmitter<void>;
    }

    // Зоны виджета "Форма элемента"
    export namespace Zones {
        export const content = <WidgetZone> {
            code: '[content]',
        };
        export const footer = <WidgetZone> {
            code: '[footer]',
            locked: true,
        };
        export const sidebar = <WidgetZone> {
            code: '[sidebar]',
            accept: [ CommonWidgetTypes.SidebarWidget.code ],
        };
        export const headerControls = <WidgetZone> {
            code: '[headerControls]',
        };
    }

    // Получить список зон формы элемента
    export function getZones (): WidgetZone[] {
        return [
            Zones.content,
            Zones.footer,
            Zones.sidebar,
            Zones.headerControls,
        ];
    }

    // Поля виджета "Форма элемента"
    export namespace Fields {
        export const formType = <WidgetField>{
            code: 'formType',
            type: WidgetFieldTypes.String,
        };
        export const allowCompactMode = <WidgetField>{
            code: 'allowCompactMode',
            type: WidgetFieldTypes.Boolean,
        };
        export const allowMobileMode = <WidgetField>{
            code: 'allowMobileMode',
            type: WidgetFieldTypes.Boolean,
        };
        export const mobileMode = <WidgetField>{
            code: 'mobileMode',
            type: WidgetFieldTypes.String,
        };
    }

    // Получить список полей формы элемента
    export function getFields (): WidgetField[] {
        return [
            Fields.formType,
            Fields.allowCompactMode,
            Fields.allowMobileMode,
            Fields.mobileMode,
        ];
    }

}
