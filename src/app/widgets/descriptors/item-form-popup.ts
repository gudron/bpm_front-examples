import { EventEmitter } from '@angular/core';
import { ElmaComplexPopupComponent } from '../../../shared/common/elma-complex-popup/elma-complex-popup.component';
import { PopoverSize } from '../../../shared/common/elma-popover/elma-popover.component';
import { ElmaModalBodyComponent } from '../../../shared/common/modal-body/modal-body';
import { CommonWidgetTypes } from '../../../shared/common/widgets/widget-types';
import { WidgetDescriptor, WidgetInstance, WidgetTemplateBuilder } from '../../../widget';
import { TemplateWidgetDescriptor } from '../../../widget/descriptors/template-widget-descriptor';
import { ItemForm } from './item-form';

/**
 * Дескриптор виджета "Форма элемента на основе комплекс-попапа"
 */
export namespace ItemFormPopup {
    export const code = 'item-form-complex-popup';

    export class Controller implements ItemForm.Controller {
        private complexPopup: ElmaComplexPopupComponent;

        constructor (
            private instance: WidgetInstance,
        ) {
            this.complexPopup = <ElmaComplexPopupComponent>this.instance.childInstances![0].controller;
            this.complexPopup.onClose.subscribe(() => this.onClose.emit());
        }

        set title (value: string) {
            this.complexPopup.title = value;
        }

        open () {
            this.complexPopup.open();
        }

        close () {
            this.complexPopup.close();
        }

        onClose = new EventEmitter<void>();
    }

    export function getDescriptor (builder: WidgetTemplateBuilder): WidgetDescriptor {
        const template = builder.create(ElmaComplexPopupComponent)
            .zone('[popup-content]', zb => zb
                .add(ElmaModalBodyComponent, mb => mb
                    .zone('', mbz => mbz.addZoneContent(ItemForm.Zones.content.code))
                )
            )
            .zone('[popup-footer]', zb => zb.addZoneContent(ItemForm.Zones.footer.code))
            .zone('[popup-sidebar]', zb => zb.addZoneContent(ItemForm.Zones.sidebar.code))
            .zone('[popup-controls]', zb => zb.addZoneContent(ItemForm.Zones.headerControls.code))
            .template;
        const descriptor = new TemplateWidgetDescriptor(template);
        descriptor.code = code;
        descriptor.zones = ItemForm.getZones();
        descriptor.types = [ CommonWidgetTypes.Popup.code ];
        descriptor.fields = ItemForm.getFields();
        descriptor.controllerFactory = (instance: WidgetInstance) => new Controller(instance);
        return descriptor;
    }
}
