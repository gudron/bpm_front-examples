import { EventEmitter } from '@angular/core';
import { ElmaPopoverBodyComponent } from '../../../shared/common/elma-popover/elma-popover-body';
import { ElmaPopoverFooterComponent } from '../../../shared/common/elma-popover/elma-popover-footer';
import { ElmaPopoverComponent, PopoverSize } from '../../../shared/common/elma-popover/elma-popover.component';
import { CommonWidgetTypes } from '../../../shared/common/widgets/widget-types';
import { WidgetDescriptor, WidgetInstance, WidgetTemplateBuilder } from '../../../widget';
import { TemplateWidgetDescriptor } from '../../../widget/descriptors/template-widget-descriptor';
import { WidgetFieldTypes } from '../../../widget/models/field-types';
import { WidgetField } from '../../../widget/schema/widget-field';
import { ItemForm } from './item-form';

/**
 * Дескриптор виджета "Форма элемента на основе поповера"
 */
export namespace ItemFormPopover {
    export const code = 'item-form-popover';

    export namespace Fields {
        export const popoverSize = <WidgetField> {
            code: 'popoverSize',
            type: WidgetFieldTypes.String,
        };
    }

    export class Controller implements ItemForm.Controller {
        private popover: ElmaPopoverComponent;

        constructor (
            private instance: WidgetInstance,
        ) {
            this.popover = <ElmaPopoverComponent>this.instance.childInstances![0].controller;
            this.popover.onSelfClose.subscribe(() => this.onClose.emit());
        }

        set size (value: PopoverSize) {
            this.popover.size = value;
        }

        open (starter: HTMLElement): void {
            this.popover.show(starter);
        }

        close (): void {
            this.popover.hide();
        }

        onClose = new EventEmitter<void>();
    }

    export function getDescriptor (builder: WidgetTemplateBuilder): WidgetDescriptor {
        const template = builder.create(ElmaPopoverComponent)
            .zone('', zb => zb
                .add(ElmaPopoverBodyComponent, mb => mb
                    .zone('', mbz => mbz
                        .addZoneContent(ItemForm.Zones.content.code)
                        .addZoneContent(ItemForm.Zones.sidebar.code)
                    )
                )
                .add(ElmaPopoverFooterComponent, mb => mb
                    .zone('', mbz => mbz.addZoneContent(ItemForm.Zones.footer.code))
                )
            )
            .template;
        const descriptor = new TemplateWidgetDescriptor(template);
        descriptor.code = code;
        descriptor.zones = ItemForm.getZones();
        descriptor.types = [ CommonWidgetTypes.Popup.code ];
        const fields = ItemForm.getFields();
        fields.push(Fields.popoverSize);
        descriptor.fields = fields;
        descriptor.controllerFactory = (instance: WidgetInstance) => new Controller(instance);
        return descriptor;
    }
}
