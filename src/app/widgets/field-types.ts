/**
 * Общие типы полей виджетов приложения.
 */
export enum AppWidgetFieldTypes {
    // Произвольный объект (ключ-значение)
    Object = 'OBJECT',

    // Поле объекта
    ObjectField = 'OBJECT_FIELD',
}
