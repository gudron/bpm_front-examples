import { Injectable } from '@angular/core';
import { WidgetRegistry, WidgetTemplateBuilder, } from '../../widget/index';
import { ItemFormPopover } from './descriptors/item-form-popover';
import { ItemFormPopup } from './descriptors/item-form-popup';

@Injectable()
export class WidgetRegistrarService {

    constructor (
        private registry: WidgetRegistry,
        private builder: WidgetTemplateBuilder,
    ) {
    }

    init () {
        this.registry.set(ItemFormPopup.getDescriptor(this.builder));
        this.registry.set(ItemFormPopover.getDescriptor(this.builder));
    }

}
