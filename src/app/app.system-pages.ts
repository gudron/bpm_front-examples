import { ElmaTranslateService } from '../shared/common';
import { GetMainSystemPageJson } from './main.page';
import { GetFeedSystemPageJson } from './messages/messages.page';
import { PageJson } from './models/page.json';
import { GetAdminPagesJson } from './modules/admin/admin.page';
import { GetDiskSystemPageJson } from './modules/disk/disk.page';
import { GetMonitorAppsJson } from './modules/monitor/monitor.apps';
import { GetTasksPagesJson } from './modules/tasks/tasks.page';
import { GetProcessMonitioringPageJson } from './modules/monitor/monitor.page';

export function GetSystemPageJsons (translationService: ElmaTranslateService): PageJson[] {
    return [].concat.apply([], [
        [GetMainSystemPageJson(translationService)],
        GetTasksPagesJson(translationService),
        [
            GetFeedSystemPageJson(translationService),
            GetDiskSystemPageJson(translationService),
        ],
        GetProcessMonitioringPageJson(translationService),
        GetAdminPagesJson(translationService),
    ]);
}


