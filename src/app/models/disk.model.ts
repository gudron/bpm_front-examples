import { action, computed, observable } from 'mobx';

import * as moment from 'moment';
import { DiskStore } from '../stores/disk.store';
import { DirectoryJson, FileJson, LinkJson, LinkType, PostlinkJson, Thumbnails } from './disk.json';

export interface Store {}

export class FileModel {
    readonly id: string;

    @observable private _name: string;
    @computed get name(): string { return this._name; }
    set name(newName: string) { this._name = newName; }

    readonly originalName: string;

    @observable private _directory: string;
    @computed get directory(): string { return this._directory; }

    readonly size: number;

    @observable private _hash: string;
    @computed get hash(): string { return this._hash; }

    readonly createdAt:  moment.Moment;

    @observable private _updatedAt: moment.Moment;
    @computed get updatedAt(): moment.Moment { return this._updatedAt; }

    @observable private _deletedAt: moment.Moment | null;
    @computed get deletedAt(): moment.Moment | null { return this._deletedAt; }

    get extension(): string {
        const ext = this.originalName.split('.').pop();
        return ext ? ext : '';
    }

    public thumbnails: Thumbnails;

    constructor(
        private store: DiskStore,
        json: FileJson,
    ) {
        if (json.__id == null) {
            throw new Error('__id is required');
        }
        this.id = json.__id;
        this.originalName = json.originalName;
        this.size = json.size;
        this.createdAt = moment(json.__createdAt);
        if (json.thumbnails) {
            this.thumbnails = json.thumbnails
        }
        this.fromJson(json);
    }

    private fromJson(json: FileJson) {
        this._name = json.name;
        this._directory = json.directory;
        if (json.hash) {
            this._hash = json.hash;
        }
        this._updatedAt = moment(json.__updatedAt);
        if (json.__deletedAt != null) {
            this._deletedAt = moment(json.__deletedAt);
        } else {
            this._deletedAt = null;
        }
    }

    async getSrc () {
        return await this.store.fileGetlink(this);
    }

    async getThumbnailSrc (): Promise<string> {

        if (this.thumbnails && this.thumbnails.md64) {

            const fileJson: FileJson = {
                __id: '',
                name: this.name,
                originalName: '',
                directory: '',
                size: 0,
                hash: this.thumbnails.md64,
            };
            const thumbnailFile = new FileModel(this.store, fileJson);

            return await this.store.fileGetlink(thumbnailFile);
        }

        return '';
    }
}

export class LinkModel {
    readonly id: string;

    @observable private _name: string;
    @computed get name(): string { return this._name; }

    @observable private _directory: string;
    @computed get directory(): string { return this._directory; }

    readonly target: string;
    readonly type: LinkType;
    readonly favorite: boolean;
    readonly createdAt:  moment.Moment;

    @observable private _updatedAt: moment.Moment;
    @computed get updatedAt(): moment.Moment { return this._updatedAt; }

    @observable private _deletedAt: moment.Moment | null;
    @computed get deletedAt(): moment.Moment | null { return this._deletedAt; }

    constructor(
        private store: Store,
        json: LinkJson,
    ) {
        if (json.__id == null) {
            throw new Error('__id is required');
        }
        this.id = json.__id;
        this._directory = json.directory;
        this.target = json.target;
        this.type = json.type;
        this.favorite = json.favorite;
        this.createdAt = moment(json.__createdAt);
        this.fromJson(json);
    }

    private fromJson(json: LinkJson) {
        this._name = json.name;
        this._updatedAt = moment(json.__updatedAt);
        if (json.__deletedAt != null) {
            this._deletedAt = moment(json.__deletedAt);
        } else {
            this._deletedAt = null;
        }
    }
}

export class PostlinkModel {
    public hash: string;
    public link: string;
    public formdata: { [key: string]: string };

    constructor(
        private store: Store,
        json: PostlinkJson,
    ) {
        this.fromJson(json);
    }

    private fromJson(json: PostlinkJson) {
        this.hash = json.hash;
        this.link = json.link;
        this.formdata = json.formdata;
    }
}

export class DirectoryModel {
    readonly id: string;

    @observable private _name: string;
    @computed get name(): string { return this._name; }
    set name(newName: string) { this._name = newName; }

    @observable public parent: string;

    readonly parentsList: string[];

    readonly createdAt: moment.Moment;

    @observable private _updatedAt: moment.Moment;
    @computed get updatedAt(): moment.Moment { return this._updatedAt; }

    @observable private _deletedAt: moment.Moment | null;
    @computed get deletedAt(): moment.Moment | null { return this._deletedAt; }

    @observable private _childrenSet = false;
    get childrenIsSet(): boolean {
        return this._childrenSet;
    }

    @observable private _children: Map<string, DirectoryModel>;
    @computed get children(): DirectoryModel[] {
        return Array.from(this._children.values());
    }
    set children(childs: DirectoryModel[]) {
        childs.forEach((item: DirectoryModel) => {
            this._children.set(item.id, item);
        });
    }

    public parentOpen = false;

    @action
    public markChildrenSet() {
        this._childrenSet = true;
    }

    @action
    public setName(n: string) { this._name = n; }  // иногда может понадобиться

    @action
    public addChild(dir: DirectoryModel) {
        this._children.set(dir.id, dir);
    }

    @action
    public removeChild(dir: DirectoryModel) {
        this._children.delete(dir.id);
    }

    @action
    public setChildren(children: DirectoryModel[]) {
        for (const ch of children) {
            this._children.set(ch.id, ch);
        }
    }

    constructor(
        private store: Store | null,
        json: DirectoryJson,
    ) {
        if (json.__id == null) {
            throw new Error('__id is required');
        }
        this.id = json.__id;
        if (json.parentsList == null) {
            this.parentsList = [];
        } else {
            this.parentsList = json.parentsList;
        }
        this.createdAt = moment(json.__createdAt);
        this._children = new Map<string, DirectoryModel>();
        this.fromJson(json);
    }

    private fromJson(json: DirectoryJson) {
        this._name = json.name;
        this.parent = json.parent;
        this._updatedAt = moment(json.__updatedAt);
        if (json.__deletedAt != null) {
            this._deletedAt = moment(json.__deletedAt);
        } else {
            this._deletedAt = null;
        }
    }
}
