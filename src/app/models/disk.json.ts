export interface FileJson {
    __id?: string;
    name: string;
    originalName: string;
    directory: string;
    size: number;
    hash?: string; // может отсутствовать, если мы создаем пустой файл
    thumbnails?: Thumbnails;
    __createdAt?: string;
    __updatedAt?: string;
    __deletedAt?: string;
}

export interface Thumbnails {
    md64: string
}

export interface DirectoryJson {
    __id?: string;
    name: string;
    parent: string;
    parentsList?: string[];
    __createdAt?: string;
    __updatedAt?: string;
    __deletedAt?: string;
}

export interface LinkJson {
    __id?: string;
    name: string;
    directory: string;
    target: string;
    type: LinkType;
    favorite: boolean;
    __createdAt?: string;
    __updatedAt?: string;
    __deletedAt?: string;
}

export interface PostlinkJson {
    hash: string;
    link: string;
    formdata: { [key: string]: string };
}

export enum LinkType {
    File = 'file',
    Directory = 'directory'
}
