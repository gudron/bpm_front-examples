import { ElmaTranslateService } from '../shared/common';
import { uuid4 } from '../shared/utils/uuid';
import { Namespaces, NavTypes, PageType } from './app.constants';
import { PageDataJson, PageJson, PageMenuJson } from './models/page.json';

export function GetMainSystemPageJson (translationService: ElmaTranslateService): PageJson {
    return <PageJson>{
        __id: uuid4(),
        __createdAt: '2017-07-21T12:14:51.347023Z',
        __updatedAt: '2017-07-21T12:14:51.347023Z',

        name: translationService.get('system.pages.main@name'),
        namespace: Namespaces.System,
        code: 'main',
        type: PageType.Page,
        icon: 'menu_burger',
        sort: 0,
        system: true,
        hidden: false,
        data: <PageDataJson>{
            columns: 1,
            menu: <PageMenuJson>{
                data: '',
                type: NavTypes.NAV_TYPE_MAIN,
            },
            redirectTo: null,
            widgets: [],
        },
    };
}
