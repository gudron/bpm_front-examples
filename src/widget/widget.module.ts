import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { WidgetRendererComponent } from './modules/renderer/widget-renderer.component';
import { WidgetTemplateRendererComponent } from './modules/renderer/widget-template-renderer.component';
import { WidgetRegistry } from './services/widget-registry';
import { WidgetTemplateRenderer } from './services/widget-template-renderer';
import { WidgetTemplateBuilder } from './services/widget-template-builder';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        WidgetRendererComponent,
        WidgetTemplateRendererComponent,
    ],
    exports: [
        WidgetRendererComponent,
        WidgetTemplateRendererComponent,
    ]
})
export class WidgetModule {
    public static forRoot (): ModuleWithProviders {
        return {
            ngModule: WidgetModule,
            providers: [
                WidgetRegistry,
                WidgetTemplateRenderer,
                WidgetTemplateBuilder,
            ]
        };
    }
}
