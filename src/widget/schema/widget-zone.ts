import { WidgetType } from './widget-type';

/**
 * Зона в виджете для вставки других виджетов (аналог ng-content-а с селектором).
 */
export interface WidgetZone {
    /**
     * Код зоны.
     */
    code: string;

    /**
     * Название зоны.
     */
    name?: string;

    /**
     * Коды типов виджетов, которые можно вставлять в данную зону.
     */
    accept?: string[];

    /**
     * Если true, то зона заблокирована для редактирования в режиме конструктора
     */
    locked?: boolean;
}
