import { WidgetInstance } from './widget-instance';

/**
 * Интерфейс ангуляровского компонента, являющегося виджетом.
 * Компонент не обязательно должен реализовывать данный интерфейс.
 * Реализация требуется, если в нём нужна информация из WidgetInstance.
 */
export interface Widget {
    /**
     * Инициализировать виджет.
     * @param {WidgetInstance} widgetInstance
     */
    initWidget(widgetInstance: WidgetInstance): void;
}
