import { Injectable, Type } from '@angular/core';
import { WidgetTemplateJson } from '../';
import { MappedValue } from '../schema/mapped-value';
import { WidgetTemplateModel } from '../models/widget-template.model';
import { WidgetRegistry } from './widget-registry';

/**
 * Построитель шаблонов на основе ангуляровских компонентов.
 * Применяется для описания системных виджетов/шаблонов.
 */
// tslint:disable:no-use-before-declare
@Injectable()
export class WidgetTemplateBuilder {
    constructor (
        private _registry: WidgetRegistry,
    ) {
    }

    create (componentTypeOrCode: Type<any> | string): WidgetTemplateInstanceBuilder {
        return new WidgetTemplateInstanceBuilder(this._registry, componentTypeOrCode);
    }
}

// tslint:disable:no-use-before-declare
export class WidgetTemplateInstanceBuilder {
    template: WidgetTemplateModel;

    constructor (
        private _registry: WidgetRegistry,
        componentTypeOrCode: Type<any> | string,
    ) {
        const descriptor = typeof componentTypeOrCode === 'string'
            ? this._registry.get(<string>componentTypeOrCode)
            : this._registry.getByComponentType(<Type<any>>componentTypeOrCode);
        this.template = new WidgetTemplateModel(_registry);
        this.template.descriptor = descriptor;
    }

    zone (code: string, zoneBuilder?: (builder: WidgetTemplateZoneBuilder) => void): WidgetTemplateInstanceBuilder {
        const zoneContent: WidgetTemplateModel[] = [];
        if (!this.template.content) {
            this.template.content = {};
        }
        this.template.content![code ? code : ''] = zoneContent;
        if (zoneBuilder) {
            zoneBuilder(new WidgetTemplateZoneBuilder(this._registry, zoneContent));
        }
        return this;
    }

    set<T> (valuesBuilder: (values: T) => void): WidgetTemplateInstanceBuilder {
        if (!this.template.values) {
            this.template.values = {};
        }
        const tmp: any = {}
        valuesBuilder(tmp);
        for (const key of Object.keys(tmp)) {
            this.template.values[key] = tmp[key];
        }
        return this;
    }

    setValue (key: string, value: any): WidgetTemplateInstanceBuilder {
        if (!this.template.values) {
            this.template.values = {};
        }
        this.template.values[key] = value;
        return this;
    }

    mapValue (key: string, path: string[]): WidgetTemplateInstanceBuilder {
        if (!this.template.values) {
            this.template.values = {};
        }
        this.template.values[key] = <MappedValue>{ path: path };
        return this;
    }
}

export class WidgetTemplateZoneBuilder {
    constructor (
        private _registry: WidgetRegistry,
        private _content: WidgetTemplateModel[],
    ) {
    }

    add (
        componentTypeOrCode: Type<any> | string,
        templateBuilder?: (builder: WidgetTemplateInstanceBuilder) => void
    ): WidgetTemplateZoneBuilder {
        const builder = new WidgetTemplateInstanceBuilder(this._registry, componentTypeOrCode);
        if (templateBuilder) {
            templateBuilder(builder);
        }
        this._content.push(builder.template);
        return this;
    }

    addZoneContent (zoneCode: string): WidgetTemplateZoneBuilder {
        const builder = new WidgetTemplateInstanceBuilder(this._registry, 'zone-content');
        builder.setValue('zoneCode', zoneCode);
        this._content.push(builder.template);
        return this;
    }
}
