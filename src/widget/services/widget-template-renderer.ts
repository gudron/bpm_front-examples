import { Injectable, Injector, ViewContainerRef } from '@angular/core';
import { MappedValue, Widget, WidgetInstance, WidgetTemplate } from '../';
import { TemplateWidgetDescriptor } from '../descriptors/template-widget-descriptor';
import { WidgetField } from '../schema/widget-field';

/**
 * Отображатель шаблонов виджетов
 */
@Injectable()
export class WidgetTemplateRenderer {
    constructor (
        private _injector: Injector,
    ) {
    }

    /**
     * Отобразить шаблон виджета
     * @param {WidgetTemplate} template Шаблон виджета
     * @param data Данные для виджета
     * @param {ViewContainerRef} viewContainer Контейнер, в который будет срендерен данный шаблон
     * @returns {WidgetInstance} Экземпляр виджета
     */
    render (template: WidgetTemplate, data: any, viewContainer: ViewContainerRef): WidgetInstance {
        // Создаем фиктивный дескриптор
        const descriptor = new TemplateWidgetDescriptor(template);
        // Маппим все поля
        descriptor.fields = [];
        if (template.descriptor.fields) {
            if (!template.values) {
                template.values = [];
            }
            for (const field of template.descriptor.fields) {
                descriptor.fields.push(<WidgetField>{ code: field.code, type: field.type });
                if (template.values[field.code] === undefined) {
                    template.values[field.code] = <MappedValue>{ path: [ field.code ]};
                }
            }
        }
        const instances = descriptor.renderer.render(this._injector, viewContainer, data, {});
        // Берем 1-й дочерний элемент из фиктивного экземпляра (чтобы получить реальный экземпляр)
        return (<WidgetInstance>instances[0]).childInstances![0];
    }
}
