import {} from 'reflect-metadata';
import { Injectable, Injector, Type } from '@angular/core';
import { WidgetDescriptor, WidgetType } from '../';
import { ComponentWidgetDescriptor } from '../descriptors/component-widget-descriptor';
import { ZoneContentWidgetRenderer } from '../descriptors/zone-content-widget-renderer';

/**
 * Глобальный (статический) реестр. Используется для регистрации системных ангуляровских виджетов.
 */
interface WidgetGlobalRegistry {
    widgetComponentTypes: Type<any>[];
}

/**
 * Реестр виджетов. Хранит данные о типах и дескрипторах виджетов
 */
@Injectable()
export class WidgetRegistry {
    constructor (
        private _injector: Injector,
    ) {
        const zoneContentDescriptor = <WidgetDescriptor> {
            code: 'zone-content',
            renderer: new ZoneContentWidgetRenderer(),
            hidden: true,
        };
        this.set(zoneContentDescriptor);
        const globalRegistry = <WidgetGlobalRegistry><any>window;
        if (globalRegistry.widgetComponentTypes) {
            for (const componentType of globalRegistry.widgetComponentTypes) {
                this.addByComponentType(componentType);
            }
        }
    }

    /**
     * Добавить компонент, который будет добавлен в реестр при его инициализации
     */
    public static addComponentForInit (componentType: Type<any>) {
        const globalRegistry = <WidgetGlobalRegistry><any>window;
        if (!globalRegistry.widgetComponentTypes) {
            globalRegistry.widgetComponentTypes = [];
        }
        globalRegistry.widgetComponentTypes.push(componentType);
    }

    /**
     * Добавить или изменить дескриптор виджета в реестре
     * @param {WidgetDescriptor} descriptor
     */
    public set (descriptor: WidgetDescriptor) {
        const oldDescriptor = this._descriptorsByCode.get(descriptor.code);
        if (oldDescriptor) {
            this._allDescriptors = this._allDescriptors.filter(d => d !== oldDescriptor);
        }
        this._descriptorsByCode.set(descriptor.code, descriptor);
        this._allDescriptors.push(descriptor);
    }

    /**
     * Добавить дескриптор виджета ангуляровского компонента
     * @param {Type<any>} componentType
     * @returns {WidgetDescriptor}
     */
    public addByComponentType (componentType: Type<any>): WidgetDescriptor {
        let descriptor = this._descriptorsByComponentType.get(componentType);
        if (!descriptor) {
            descriptor = new ComponentWidgetDescriptor(this._injector, componentType);
            this._descriptorsByComponentType.set(componentType, descriptor);
            this.set(descriptor);
        }
        return descriptor!;
    }

    /**
     * Получить дескриптор виджета по типу ангуляровского компонента
     * @param {Type<any>} componentType
     * @returns {WidgetDescriptor}
     */
    public getByComponentType (componentType: Type<any>): WidgetDescriptor {
        const descriptor = this._descriptorsByComponentType.get(componentType);
        if (!descriptor) {
            throw new Error('Widget not found: ' + componentType);
        }
        return descriptor!;
    }

    /**
     * Получить дескриптор виджета по коду
     * @param {string} code
     * @returns {WidgetDescriptor}
     */
    public get (code: string): WidgetDescriptor {
        const descriptor = this._descriptorsByCode.get(code);
        if (!descriptor) {
            throw new Error('Widget not found: ' + code);
        }
        return descriptor!;
    }

    /**
     * Получить список дескрипторов виджетов
     * @returns {WidgetDescriptor[]}
     */
    public list (): WidgetDescriptor[] {
        return this._allDescriptors;
    }

    /**
     * Добавить тип виджета в реестр
     * @param {WidgetType} type
     */
    public addType (type: WidgetType) {
        this._typesByCode.set(type.code, type);
        this._allTypes.push(type);
    }

    /**
     * Получить тип виджета по коду
     * @param {string} code
     * @returns {WidgetType}
     */
    public getType (code: string): WidgetType {
        const type = this._typesByCode.get(code);
        if (!type) {
            throw new Error('Widget type not found: ' + code);
        }
        return type!;
    }

    /**
     * Получить список типов виджетов
     * @returns {WidgetType[]}
     */
    public typesList (): WidgetType[] {
        return this._allTypes;
    }

    private _allDescriptors: WidgetDescriptor[] = [];
    private _descriptorsByCode = new Map<string, WidgetDescriptor>();
    private _descriptorsByComponentType = new Map<Type<any>, WidgetDescriptor>();
    private _allTypes: WidgetType[] = [];
    private _typesByCode = new Map<string, WidgetType>();
}
