export { WidgetModule } from './widget.module';

export { MappedValue } from './schema/mapped-value';
export { NamedWidgetTemplate } from './schema/named-widget-template';
export { NamedWidgetTemplateJson } from './schema/named-widget-template.json';
export { Widget } from './schema/widget';
export { WidgetBuilder } from './schema/widget-builder';
export { WidgetDescriptor } from './schema/widget-descriptor';
export { WidgetDescriptorJson } from './schema/widget-descriptor.json';
export { WidgetDescriptorRenderer } from './schema/widget-descriptor-renderer';
export { WidgetInstanceContent } from './schema/widget-instance';
export { WidgetInstance } from './schema/widget-instance';
export { WidgetTemplate } from './schema/widget-template';
export { WidgetTemplateJson } from './schema/widget-template.json';
export { WidgetType } from './schema/widget-type';
export { WidgetZone } from './schema/widget-zone';

export { WidgetRegistry } from './services/widget-registry';
export { WidgetTemplateBuilder } from './services/widget-template-builder';
export { WidgetTemplateRenderer } from './services/widget-template-renderer';
