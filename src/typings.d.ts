///<reference path='../node_modules/immutable/dist/immutable.d.ts'/>
/* SystemJS module definition */
declare var module: NodeModule;

declare module '*.json' {
    const value: any;
    export default value;
}

interface NodeModule {
    id: string;
    hot?: boolean;
}
